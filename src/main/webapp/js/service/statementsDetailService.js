//服务层
app.service('statementsDetailService', function ($http) {
    //获取结算单信息
    this.getSession = function () {
        console.log("跳转到controller");
        return $http.post('../StatementsDetail/getSession');
    }
    //查询结算明细
    this.search = function (page, rows, searchEntity) {
        console.log("请求controller");
        return $http.post('../StatementsDetail/search?page=' + page + "&rows=" + rows, searchEntity)
    }
    //批量删除
    this.deleteSome = function (ids) {
        return $http.get('../StatementsDetail/delete?ids=' + ids);
    }
    //保存结算单
    this.save = function () {
        return $http.post('../StatementsDetail/save');
    }
})