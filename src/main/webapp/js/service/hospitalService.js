//服务层
app.service('hospitalService', function ($http) {

    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../hospital/findAll');
    }


    //查询实体
    this.findOne = function (id) {
        return $http.get('../hospital/findOne?id=' + id);
    }

    //增加
    this.insert = function (entity) {
        return $http.post('../hospital/insert', entity);
    }

    //修改
    this.update = function (entity) {
        return $http.post('../hospital/update', entity);
    }

    //删除单个
    this.delete = function (id) {
        dele = confirm("确认删除吗?");
        if(dele==true) {
           return $http.get('../hospital/delete?id=' + id);
        }
        else {
            null
        }
    }


    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../hospital/search?page=' + page + "&rows=" + rows, searchEntity);
    }


    this.saveSearchEntity = function (searchEntity) {
        return $http.post('../hospital/saveSearchEntity', searchEntity);
    }


    //读取列表数据绑定到表单中
    this.findAll=function(){
        return $http.get('../hospital/findAll');
    }


});
