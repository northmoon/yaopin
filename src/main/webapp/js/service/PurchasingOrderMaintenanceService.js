//服务层
app.service('PurchasingOrderMaintenanceService', function ($http) {
    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../PurchaseNote/findAll');
    }
    //分页
    this.findPage = function (page, rows) {
        return $http.get('../PurchaseNote/search?page=' + page + '&rows=' + rows);
    }
    //查询实体
    this.findOne = function (id) {
        return $http.get('../PurchaseNote/findOne?id=' + id);
    }
    //增加
    this.add = function (entity) {
        return $http.post('../PurchaseNote/add', entity);
    }
    //修改
    this.update = function (entity) {
        return $http.post('../PurchaseNote/update', entity);
    }
    //添加时获取医院信息
    this.addId=function (entity) {
        return $http.post('../PurchaseNote/save', entity);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../PurchaseNote/deleteOne?id=' + id);
    }
    //删除
    this.dele = function (ids) {
        return $http.get('../PurchaseNote/delete?ids=' + ids);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../PurchaseNote/search?page=' + page + "&rows=" + rows, searchEntity);
    }
    //将采购单信息保存在session中
    this.preservation=function (id) {
        return $http.get('../PurchaseNote/cache?id='+id);
    }
});