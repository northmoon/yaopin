//服务层
app.service('HospitalPurchasingService', function ($http) {

    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../HospitalPurchasing/findAll');
    }
    //分页
    this.findPage = function (page, rows) {
        return $http.get('../HospitalPurchasing/search?page=' + page + '&rows=' + rows);
    }
    //查询实体
    this.findOne = function (id) {
        return $http.get('../HospitalPurchasing/findOne?id=' + id);
    }
    //增加
    this.add = function (entity) {
        return $http.post('../HospitalPurchasing/add', entity);
    }
    //修改
    this.update = function (entity) {
        return $http.post('../HospitalPurchasing/update', entity);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../HospitalPurchasing/deleteOne?id=' + id);
    }
    //删除
    this.dele = function (ids) {
        return $http.get('../HospitalPurchasing/delete?ids=' + ids);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../HospitalPurchasing/search?page=' + page + "&rows=" + rows, searchEntity);
    }
   //
    this.removeId = function (idList) {
        return $http.post('../HospitalPurchasing/remove?idList=' +idList);
    }
    //插入到药品明细表
    this.addPn = function (idList) {
        return $http.post('../HospitalPurchasing/addPn?idList='+idList);
    }



});