//服务层
app.service('supplierService',function($http){
	    	
	//读取列表数据绑定到表单中
	this.findAll=function(){
		return $http.get('../supplier/findAll');
	}

    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../adminsupplier/search?page=' + page + "&rows=" + rows, searchEntity);
    }


    //添加一个
    this.add = function (Entity) {
        return $http.post('../adminsupplier/add', Entity);
    }

    //添加一个
    this.update = function (Entity) {
        return $http.post('../adminsupplier/update', Entity);
    }


    //查询一个
    this.findOne = function (id) {
        return $http.post('../adminsupplier/findone?id=' + id);
    }

    //删除一个
    this.deleteOne = function (id) {
        return $http.post('../adminsupplier/deleteone?id=' + id);
    }


});
