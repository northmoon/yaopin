//服务层
app.service('roleService', function ($http) {

    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../role/findAll');
    }

    //查询实体
    this.findOne = function (id) {
        return $http.get('../role/findOne?id=' + id);
    }
    //增加
    this.add = function (entity, ids) {
        return $http.post('../role/add?ids=' + ids, entity);
    }
    //修改
    this.update = function (entity, ids) {
        return $http.post('../role/update?ids=' + ids, entity);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../role/deleteOne?id=' + id);
    }
    this.saveSearchEntity = function (searchEntity) {
        return $http.post('../role/saveSearchEntity', searchEntity);
    }

});
