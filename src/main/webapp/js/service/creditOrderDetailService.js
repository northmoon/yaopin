//服务层
app.service('creditOrderDetailService', function ($http) {
    //获取退货单信息
    this.getSession=function () {
        console.log(22);
        return $http.post('../CreditOrderDetail/getInfo');
    }
   //查询退货明细
    this.search=function (page,rows,searchEntity) {
        console.log("请求controller");
        return $http.post('../CreditOrderDetail/search?page=' + page + "&rows=" + rows, searchEntity)
    }
    //批量删除
    this.deleteSome=function (ids) {
        return $http.get('../CreditOrderDetail/delete?ids='+ids);
    }
    //保存退货量
    this.save=function (map) {
        console.log(map);
        return $http.post('../CreditOrderDetail/save',map);
    }
    //保存退货单
    this.Preservation = function () {
        return $http.post('../CreditOrderDetail/preservation');
    }


})