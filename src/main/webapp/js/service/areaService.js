//服务层
app.service('areaService', function ($http) {

    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../drugCategory/findAll');
    }
    //分页
    this.findPage = function (page, rows) {
        return $http.get('../drugCategory/findPage?page=' + page + '&rows=' + rows);
    }
    //查询实体
    this.findOne = function (id) {
        return $http.get('../areaInfo/findOne?id=' + id);
    }
    //增加
    this.add = function (entity) {
        return $http.post('../areaInfo/add', entity);
    }
    //修改
    this.update = function (entity) {
        return $http.post('../areaInfo/update', entity);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../areaInfo/deleteOne?id=' + id);
    }
    //删除
    this.dele = function (ids) {
        return $http.get('../drugCategory/delete?ids=' + ids);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../areaInfo/search?page=' + page + "&rows=" + rows, searchEntity);
    }
    this.saveSearchEntity = function (searchEntity) {
        return $http.post('../drugCategory/saveSearchEntity', searchEntity);
    }
    this.exportsearchsave = function (searchEntity) {
        return $http.post('../drugCategory/savesearchEntity', searchEntity);
    }
    //按查询条件导出
    this.exportsearch = function (searchEntity) {
        return $http.post('../drugCategory/exportsearch', searchEntity);
    }

});
