//服务层
app.service('cancellationSupplyService', function ($http) {

    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../choiceSupply/findAll');
    }
    //分页
    this.findPage = function (page, rows) {
        return $http.get('../choiceSupply/findPage?page=' + page + '&rows=' + rows);
    }
    //查询实体
    this.findOne = function (id) {
        return $http.get('../choiceSupply/findOne?id=' + id);
    }
    //增加
    this.add = function (entity) {
        return $http.post('../drugInformation/add', entity);
    }
    //修改
    this.update = function (entity) {
        return $http.post('../drugInformation/update', entity);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../drugInformation/deleteOne?id=' + id);
    }
    //删除
    this.dele = function (ids) {
        return $http.get('../drugInformation/delete?ids=' + ids);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../Supply/searchSupply?page=' + page + "&rows=" + rows, searchEntity);
    }

    //按查询条件导出
    this.exportsearchInformation = function (searchEntity) {
        return $http.post('../drugInformation/savesearchEntity', searchEntity);
    }

    //保存供货信息
    this.removeSupplier = function (selected) {
        return $http.post('../supplier/removesupplier', selected);
    }



});
