//服务层
app.service('statementsService', function ($http) {
    //添加时获取医院信息
    this.addId = function (entity) {
        console.log("传到退货controller");
        return $http.post('../Statements/getInfo', entity);
    }
   this.addStatements=function (entity) {
        return $http.post('../Statements/add', entity);
    }

    //搜索
    this.search = function (page, rows, searchEntity) {
        console.log("查询结算单controller");
        return $http.post('../Statements/findPage?page=' + page + "&rows=" + rows, searchEntity);
    }

    //将退货单信息保存在session中
    this.preservation=function (id) {
        return $http.get('../Statements/cache?id='+id);
    }


})