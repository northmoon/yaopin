//服务层
app.service('supplierListAuditService', function ($http) {

    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../supplierList/findAll');
    }
    //分页
    this.findPage = function (page, rows) {
        return $http.get('../supplierList/findPage?page=' + page + '&rows=' + rows);
    }
    //查询实体
    this.findOne = function (id) {
        return $http.get('../supplierList/findOne?id=' + id);
    }
    //增加
    this.add = function (entity) {
        return $http.post('../supplierList/add', entity);
    }
    //修改
    this.update = function (entitys) {
        return $http.post('../supplierList/update', entitys);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../supplierList/deleteOne?id=' + id);
    }
    //删除
    this.dele = function (ids) {
        return $http.get('../supplierList/delete?ids=' + ids);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../supplierList/search?page=' + page + "&rows=" + rows, searchEntity);
    }



});
