//服务层
app.service('organizationInfoService', function ($http) {

    //查询实体
    this.findOne = function (oid) {
        return $http.get('../organizationInfo/findOne?oid=' + oid);
    }

    //增加
    this.insert = function (entity) {
        return $http.post('../organizationInfo/insert', entity);
    }

    //修改
    this.update = function (entity) {
        return $http.post('../organizationInfo/update', entity);
    }

    //删除单个
    this.delete = function (oid) {
        dele = confirm("确认删除吗?");
        if(dele==true) {
            return $http.get('../organizationInfo/delete?oid=' + oid);
        }
        else {
            null
        }
    }

    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../organizationInfo/search?page=' + page + "&rows=" + rows, searchEntity);
    }


    this.saveSearchEntity = function (searchEntity) {
        return $http.post('../organizationInfo/saveSearchEntity', searchEntity);
    }



});
