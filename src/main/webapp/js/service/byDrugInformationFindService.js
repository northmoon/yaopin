//服务层
app.service('byDrugInformationFindService', function ($http) {

    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../hospitalTransactionSchedule/findAll');
    }
    //分页
    this.findPage = function (page, rows) {
        return $http.get('../hospitalTransactionSchedule/findPage?page=' + page + '&rows=' + rows);
    }
    //查询实体
    this.findOne = function (id) {
        return $http.get('../hospitalTransactionSchedule/findOne?id=' + id);
    }
    //增加
    this.add = function (entity) {
        return $http.post('../hospitalTransactionSchedule/add', entity);
    }
    //修改
    this.update = function (entity) {
        return $http.post('../hospitalTransactionSchedule/update', entity);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../hospitalTransactionSchedule/deleteOne?id=' + id);
    }
    //删除
    this.dele = function (ids) {
        return $http.get('../hospitalTransactionSchedule/delete?ids=' + ids);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../hospitalTransactionSchedule/search?page=' + page + "&rows=" + rows, searchEntity);
    }

    this.saveSearchEntity = function (searchEntity) {
        return $http.post('../hospitalTransactionSchedule/saveSearchEntity', searchEntity);
    }


});
