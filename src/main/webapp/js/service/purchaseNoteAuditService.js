//服务层
app.service('purchaseNoteAuditService', function ($http) {

    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../purchaseNote/findAll');
    }
    //分页
    this.findPage = function (page, rows) {
        return $http.get('../purchaseNote/findPage?page=' + page + '&rows=' + rows);
    }
    //查询实体
    this.findOne = function (id) {
        return $http.get('../purchaseNote/findOne?id=' + id);
    }
    //增加
    this.add = function (entity) {
        return $http.post('../purchaseNote/add', entity);
    }
    //修改
    this.update = function (entitys) {
        return $http.post('../purchaseNote/update', entitys);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../purchaseNote/deleteOne?id=' + id);
    }
    //删除
    this.dele = function (ids) {
        return $http.get('../purchaseNote/delete?ids=' + ids);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../purchaseNote/search?page=' + page + "&rows=" + rows, searchEntity);
    }



});
