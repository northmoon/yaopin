//服务层
app.service('supplierListService', function ($http) {

    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../supplierListService/findAll');
    }
    //分页
    this.findPage = function (page, rows) {
        return $http.get('../supplierListService/search?page=' + page + '&rows=' + rows);
    }
    //查询实体
    this.findOne = function (id) {
        return $http.get('../supplierListService/findOne?id=' + id);
    }
    //增加
    this.add = function (entity) {
        return $http.post('../supplierListService/add', entity);
    }
    //修改
    this.update = function (entity) {
        return $http.post('../supplierListService/update', entity);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../supplierListService/deleteOne?id=' + id);
    }
    //删除
    this.dele = function (ids) {
        return $http.get('../supplierListService/delete?ids=' + ids);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../SupplierListService/search?page=' + page + "&rows=" + rows, searchEntity);
    }
    //插入到采购表
    this.addPn = function (idList) {
        return $http.post('../SupplierListService/addPn?idList='+idList);
    }


});