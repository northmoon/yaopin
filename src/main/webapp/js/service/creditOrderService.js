//服务层
app.service('creditOrderService', function ($http) {
    //添加时获取医院信息
    this.addId = function (entity) {
        console.log("传到controller");
        return $http.post('../CreditOrder/save', entity);
    }
    this.addCreditOrder=function (entity) {
        return $http.post('../CreditOrder/addCreditOrder', entity);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        console.log("查询退货单controller");
        return $http.post('../CreditOrder/findPage?page=' + page + "&rows=" + rows, searchEntity);
    }
    //将退货单信息保存在session中
    this.preservation=function (id) {
        return $http.get('../CreditOrder/cache?id='+id);
    }


})