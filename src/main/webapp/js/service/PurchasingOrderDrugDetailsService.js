//服务层
app.service('PurchasingOrderDrugDetailsService', function ($http) {
    //读取列表数据绑定到表单中
    this.findAll = function () {
        return $http.get('../PurchaseOrderDrugDetails/findAll');
    }
    //分页
    this.findPage = function (page, rows) {
        return $http.get('../PurchaseOrderDrugDetails/search?page=' + page + '&rows=' + rows);
    }
    //查询实体
    this.findOne = function (id) {
        return $http.get('../PurchaseOrderDrugDetails/findOne?id=' + id);
    }
    //增加
    this.add = function (entity) {
        return $http.post('../PurchaseOrderDrugDetails/add', entity);
    }
    //修改
    this.update = function (entity) {
        return $http.post('../PurchaseOrderDrugDetails/update', entity);
    }
    //添加时获取医院信息
    this.addId=function (entity) {
        return $http.post('../PurchaseOrderDrugDetails/save', entity);
    }
    //删除单个
    this.deleteOne = function (id) {
        return $http.get('../PurchaseOrderDrugDetails/deleteOne?id=' + id);
    }
    //删除
    this.deleteList = function (ids) {
        console.log(999)
        return $http.get('../PurchaseOrderDrugDetails/delete?ids=' + ids);
    }
    //搜索
    this.search = function (page, rows, searchEntity) {
        return $http.post('../PurchaseOrderDrugDetails/search?page=' + page + "&rows=" + rows, searchEntity);
    }
    //获取采购单信息
    this.getSession=function () {
        console.log(22);
        return $http.post('../PurchaseOrderDrugDetails/getInfo');
    }
    //根据采购单id查询药品明细
   this.findList=function () {
        console.log("药品明细");
        return $http.post('../PurchaseOrderDrugDetails/findList');
    }
    //保存采购量
    this.purchaseVolume=function (map) {
        console.log("保存采购量");
        return $http.post('../PurchaseOrderDrugDetails/baoCun',map);
    }
});