//控制层
app.controller('supplierListAuditController', function ($scope, $controller, supplierListAuditService, qualityLevelService, drugClassificationService, supplierService, toaster) {

    $controller('baseController', {$scope: $scope});//继承

    //读取列表数据绑定到表单中  
    $scope.findAll = function () {
        supplierListAuditService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }


    //定义记录选中id的数组
    $scope.ids = [];

    $scope.states = [];

    $scope.sum = 0;

    $scope.change = function (id, a) {
        $scope.sum = 0;
        angular.forEach($scope.list, function (pojo) {
            if (pojo.state != 0) {
                $scope.sum = $scope.sum + 1;
            }
        })
        var index = $scope.ids.indexOf(id);
        if (index != -1) {
            $scope.ids.splice(index, 1);
            $scope.states.splice(index, 1);
            if (a != 0) {
                $scope.ids.push(id);
                $scope.states.push(a);
            }
        } else {
            angular.forEach($scope.list, function (pojo) {
                if (pojo.id == id) {
                    pojo.checked = false;
                }
            })
            $scope.select_all = false;
        }

    }

    $scope.select_all = false;

    $scope.selectAll = function () {
        if (!$scope.select_all) {
            $scope.ids = [];
            $scope.states = [];
            angular.forEach($scope.list, function (pojo) {
                pojo.checked = true;
                if (pojo.state != 0) {
                    $scope.ids.push(pojo.id);
                    $scope.states.push(pojo.state);
                }
            })

        } else {
            angular.forEach($scope.list, function (pojo) {
                pojo.checked = false;
                $scope.ids = [];
                $scope.states = [];
            })
        }
    }

    //更新复选框选中状态
    $scope.updateSelection1 = function ($event, id, state) {
        //判断选中状态
        if ($event.target.checked) {//选中状态
            $scope.ids.push(id);
            $scope.states.push(state);
        } else {
            //取消勾选，移除当前id值  //参数一：移除位置的元素的索引值  参数二：从该位置移除几个元素
            var index = $scope.ids.indexOf(id);
            $scope.ids.splice(index, 1);
            $scope.states.splice(index, 1);
        }
        console.log("sum=" + $scope.sum);
        console.log("ids.length=" + $scope.ids.length);
        if ($scope.sum == $scope.ids.length) {
            $scope.select_all = true;
        } else {
            $scope.select_all = false;
        }
    }

    //批量修改
    $scope.update = function () {
        if ($scope.ids.length != 0) {
            if (confirm("审核结果提交后将不允许更改，是否继续？")) {
                $scope.entitys = {
                    "ids": $scope.ids,
                    "states": $scope.states,
                };
                console.log("ids=" + $scope.ids);
                console.log("states=" + $scope.states);
                console.log("entitys=" + $scope.entitys);
                //获取选中的复选框
                supplierListAuditService.update($scope.entitys).success(
                    function (response) {
                        if (response.success) {
                            $scope.select_all = false;
                            toaster.pop('success', response.message);
                            $scope.reloadList();//刷新列表
                        }
                    }
                );
            }
        } else {
            alert("请至少选择一条数据！");
        }
    }

    //分页
    $scope.findPage = function (page, rows) {
        supplierListAuditService.findPage(page, rows).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    $scope.addClick = function () {
        $scope.entity = {};
        $("#sp1").text("");
    }

    //查询实体
    $scope.findOne = function (id) {
        supplierListAuditService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }


    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
    }


    $scope.searchEntity = {};//定义搜索对象

    //搜索
    $scope.search = function (page, rows) {
        supplierListAuditService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    //定义药品状态数组
    $scope.status = ['审核中', '审核通过', '审核不通过'];
    $scope.state = ['正常', '暂停交易'];

    $scope.supplierMap = {};
    $scope.qualityLevelMap = {};
    $scope.drugClassificationList = {};

    $scope.selectList = function () {
        $scope.selectSupplierList();
        $scope.selectDrugClassificationList();
        $scope.selectQualityLevelList();
    }

    //定义查询所有供货商的方法
    $scope.selectSupplierList = function () {
        supplierService.findAll().success(function (response) {
            $scope.supplierMap = response;
        })
    }

    //定义查询所有药品类别的方法
    $scope.selectDrugClassificationList = function () {
        drugClassificationService.findAll().success(function (response) {
            for (var i = 0; i < response.length; i++) {
                $scope.drugClassificationList = response;
            }
        })
    }

    //定义查询所有质量层次的方法
    $scope.selectQualityLevelList = function () {
        qualityLevelService.findAll().success(function (response) {
            $scope.qualityLevelMap = response;
        })
    }
});	
