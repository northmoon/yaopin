//控制层
app.controller('drugInformationController', function ($scope, $http, $controller, drugInformationService, drugCategoryService, drugClassificationService, qualityLevelService, toaster) {

    $controller('baseController', {$scope: $scope});//继承

    //读取列表数据绑定到表单中  
    $scope.findAll = function () {
        drugInformationService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }

    //分页
    $scope.findPage = function (page, rows) {
        drugInformationService.findPage(page, rows).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    $scope.addClick = function () {
        $scope.entity = {};
        $("#sp1").text("");
    }

    //查询实体
    $scope.findOne = function (id) {
        drugInformationService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }

    $scope.verify = function () {
        if ($scope.entity.drugCategory.genericName == null || $scope.entity.drugCategory.genericName == "") {
            $("#sp1").text("通用名不可为空");
            return;
        }
        if ($scope.entity.drugCategory.dosageForm == null || $scope.entity.drugCategory.dosageForm == "") {
            $("#sp1").text("剂型不可为空");
            return;
        }
        if ($scope.entity.drugCategory.specification == null || $scope.entity.drugCategory.specification == "") {
            $("#sp1").text("规格不可为空");
            return;
        }
        if ($scope.entity.drugCategory.unit == null || $scope.entity.drugCategory.unit == "") {
            $("#sp1").text("单位不可为空");
            return;
        }
        if ($scope.entity.drugCategory.coefficient == null || $scope.entity.drugCategory.coefficient == "") {
            $("#sp1").text("转换系数不可为空");
            return;
        }
        if ($scope.entity.drugInformation.enterpriseName == null || $scope.entity.drugInformation.enterpriseName == "") {
            $("#sp1").text("生产企业名称不可为空");
            return;
        }
        if ($scope.entity.drugInformation.drugName == null || $scope.entity.drugInformation.drugName == "") {
            $("#sp1").text("药品名不可为空");
            return;
        }
        $("#sp1").text("");
        $scope.save();
    }

    //保存
    $scope.save = function () {
        var serviceObject;//服务层对象
        if ($scope.entity.drugInformation.id != null) {//如果有ID
            serviceObject = drugInformationService.update($scope.entity); //修改
        } else {
            serviceObject = drugInformationService.add($scope.entity);//增加
        }
        serviceObject.success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $("#bt1").click();
                    $scope.reloadList();
                } else {
                    $("#sp1").html(response.message);
                }
            }
        );
    }
    //删除单个
    $scope.deleteOne = function (id) {
        drugInformationService.deleteOne(id).success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $scope.reloadList();//刷新列表
                }
            }
        );
    }

    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
    }


    //批量删除
    $scope.dele = function () {
        //获取选中的复选框
        drugInformationService.dele($scope.selectIds).success(
            function (response) {
                if (response.success) {
                    $scope.reloadList();//刷新列表
                }
            }
        );
    }

    $scope.searchEntity = {};//定义搜索对象

    //搜索
    $scope.search = function (page, rows) {
        drugInformationService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                console.log(response.toString());
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    //初始化entity对象
    // $scope.entity={};


    //定义药品状态数组
    $scope.status = ['正常', '取消交易'];

    //初始化药品类别列表
    $scope.drugClassificationList = [];

    $scope.drugCategoryMap = {};

    $scope.qualityLevelMap = {};

    $scope.selectList = function () {
        $scope.selectDrugCategoryList();
        $scope.selectDrugClassificationList();
        $scope.selectQualityLevelList();
    }
    //定义查询所有药品类别的方法
    $scope.selectDrugClassificationList = function () {
        drugClassificationService.findAll().success(function (response) {
            for (var i = 0; i < response.length; i++) {
                $scope.drugClassificationList = response;
            }
        })
    }
    //定义查询所有药品品目的方法
    $scope.selectDrugCategoryList = function () {
        drugCategoryService.findAll().success(function (response) {
            $scope.drugCategoryMap = response;
        })
    }
    //定义查询所有质量层次的方法
    $scope.selectQualityLevelList = function () {
        qualityLevelService.findAll().success(function (response) {
            $scope.qualityLevelMap = response;
        })
    }
    //导入
    $scope.upload = function () {
        var form = new FormData();
        var file = document.getElementById("file").files[0];
        form.append("file", file);
        console.log(form);
        console.log(file)
        $.ajax({
            url: "../drugInformation/upload",
            type: "post",
            data: form,
            datatype: "json",
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var file1 = $("#file");
                file1.after(file1.clone().val(""));
                file1.remove();
                $scope.messageText = response.message;
            }
        })
    }

$scope.saveSearchEntity = function () {
        drugInformationService.saveSearchEntity($scope.searchEntity).success(function (response) {
            window.location.href = 'http://localhost:8080/drugInformation/exportExcel';
        })
    }

    $scope.selected = [];//定义一个数组
    //全选方法，并将所有的id一并传入selected数组中
    $scope.all = function($event){
        debugger;
        var list=$scope.list;
        angular.forEach($scope.list, function (v, k) {
            //console.log(v.id);//读到所有的数组数据
            $scope.selected.push(v.id);
            //console.log(k);//读到所有的对象的下标
        });
        var checkbox = $event.target ;
        var checked = checkbox.checked ;
        if(checked){
            $scope.x=true;
        }
        else{
            $scope.x=false;
            $scope.selected=[];
        }
    };

    $scope.updateSelection = function($event,id){ //单选更新selected
        var checkbox = $event.target ;
        var checked = checkbox.checked ;
        if(checked){
            $scope.selected.push(id);
        }else{
            var idx = $scope.selected.indexOf(id) ;
            $scope.selected.splice(idx,1);
        }
    }
    //确认供货保存信息
    $scope.saveSupplier = function (page,rows) {
        drugInformationService.saveSupplier($scope.selected).success(function (response) {
            $scope.selected=[];
        })
    };

 
});	
