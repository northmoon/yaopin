//控制层
app.controller('hospitalController', function ($scope, $http, $controller, hospitalService,toaster) {

    $controller('baseController', {$scope: $scope});


    //搜索
    $scope.search = function (page, rows) {
        hospitalService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }


    //查询实体
    $scope.findOne = function (id) {
        hospitalService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }


    $scope.verify = function () {
        if ($scope.entity.hospitalName == null || $scope.entity.hospitalName == "") {
            $("#sp1").text("医院名称不可为空");
            return;
        }
        if ($scope.entity.mailingAddress == null || $scope.entity.mailingAddress == "") {
            $("#sp1").text("通讯地址不可为空");
            return;
        }
        if ($scope.entity.zipCode == null || $scope.entity.zipCode == "") {
            $("#sp1").text("邮政编码不可为空");
            return;
        }
        if ($scope.entity.areaId == null || $scope.entity.areaId == "") {
            $("#sp1").text("所属地区不可为空");
            return;
        }
        if ($scope.entity.hospitalLevel == null || $scope.entity.hospitalLevel == "") {
            $("#sp1").text("医院级别不可为空");
            return;
        }
        if ($scope.entity.bedNum == null || $scope.entity.bedNum == "") {
            $("#sp1").text("床位数不可为空");
            return;
        }
        if ($scope.entity.phone == null || $scope.entity.phone == "") {
            $("#sp1").text("院办电话不可为空");
            return;
        }
        if ($scope.entity.type == null || $scope.entity.type == "") {
            $("#sp1").text("医院类型不可为空");
            return;
        }
        $("#sp1").text("");
        $scope.save();
    }


    //保存
    $scope.save = function () {
        var serviceObject;//服务层对象
        if ($scope.entity.id != null) {//如果有ID
            serviceObject = hospitalService.update($scope.entity); //修改
        } else {
            serviceObject = hospitalService.insert($scope.entity);//增加
        }
        serviceObject.success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $("#bt1").click();
                    $scope.reloadList();
                } else {
                    $("#sp1").html(response.message);
                }
            }
        );
    }

    //删除单个
    $scope.delete = function (id) {
        hospitalService.delete(id).success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $scope.reloadList();//刷新列表
                }
            }
        );
    }

    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
        window.location.reload();
    }


    $scope.searchEntity = {};//定义搜索对象

    $scope.saveSearchEntity = function () {
        hospitalService.saveSearchEntity($scope.searchEntity).success(function (response) {
            window.location.href = 'http://localhost:8080/hospital/exportExcel';
        })
    }


    //初始化地理位置类别列表
    $scope.areaInfoList = [];

    //定义查询所有地区
    $scope.selectAreaInfoList = function () {
        hospitalService.findAll().success(function (response) {
            for (var i = 0; i < response.length; i++) {
                $scope.areaInfoList = response;
            }
        })
    }



})

