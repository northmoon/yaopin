//控制层
app.controller('organizationInfoController', function ($scope, $http, $controller, organizationInfoService,toaster) {

    $controller('baseController', {$scope: $scope});

    //搜索
    $scope.search = function (page, rows) {
        organizationInfoService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }


    //查询实体
    $scope.findOne = function (oid) {
        organizationInfoService.findOne(oid).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }


    $scope.verify = function () {
        if ($scope.entity.oname == null || $scope.entity.oname == "") {
            $("#sp1").text("监督机构名称");
            return;
        }
        if ($scope.entity.address == null || $scope.entity.address == "") {
            $("#sp1").text("联系地址");
            return;
        }
        if ($scope.entity.postcode == null || $scope.entity.postcode == "") {
            $("#sp1").text("邮政编码");
            return;
        }
        if ($scope.entity.contactname == null || $scope.entity.contactname == "") {
            $("#sp1").text("联系人");
            return;
        }
        if ($scope.entity.contactphone == null || $scope.entity.contactphone == "") {
            $("#sp1").text("联系电话");
            return;
        }
        if ($scope.entity.fax == null || $scope.entity.fax == "") {
            $("#sp1").text("传真");
            return;
        }
        if ($scope.entity.email == null || $scope.entity.email == "") {
            $("#sp1").text("邮箱email");
            return;
        }
        if ($scope.entity.website == null || $scope.entity.website == "") {
            $("#sp1").text("网址");
            return;
        }
        $("#sp1").text("");
        $scope.save();
    }


    //保存
    $scope.save = function () {
        var serviceObject;//服务层对象
        if ($scope.entity.oid != null) {//如果有ID
            serviceObject = organizationInfoService.update($scope.entity); //修改
        } else {
            serviceObject = organizationInfoService.insert($scope.entity);//增加
        }
        serviceObject.success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $("#bt1").click();
                    $scope.reloadList();
                } else {
                    $("#sp1").html(response.message);
                }
            }
        );
    }

    //删除单个
    $scope.delete = function (oid) {
        organizationInfoService.delete(oid).success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $scope.reloadList();//刷新列表
                }
            }
        );
    }

    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
        window.location.reload();
    }


    $scope.searchEntity = {};//定义搜索对象

    $scope.saveSearchEntity = function () {
        organizationInfoService.saveSearchEntity($scope.searchEntity).success(function (response) {
            window.location.href = 'http://localhost:8080/organizationInfo/exportExcel';
        })
    }






})

