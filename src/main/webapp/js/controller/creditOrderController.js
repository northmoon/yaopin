//药品明细
app.controller('creditOrderController', function ($scope, $http, $controller, creditOrderService, toaster) {
        $controller('baseController', {$scope: $scope});//继承
        //json初始化
        $scope.searchEntity = {};
        //添加时获取实体
        $scope.addHospitalInfo = function () {
            console.log("获取医院信息");
            creditOrderService.addId().success(
                function (response) {
                    $scope.entity = response;
                }
            );
        }
        $scope.pop = function () {
            toaster.pop('success', "操作成功");
        }
        //添加退货单
        $scope.addCreditOrder = function () {
            var serviceObject;//服务层对象
            creditOrderService.addCreditOrder($scope.entity).success(
                function (response) {
                    if (response.success) {
                        $scope.pop();
                        $("#bt").click();
                        $scope.reloadList();
                    } else {
                        $("#sp1").html(response.message);
                    }
                }
            )
        }
        //添加退货单之后的保存
        $scope.baoCun = function () {
            if ($scope.entity.creditOrderNum == null || $scope.entity.creditOrderNum == "") {
                $("#sp1").text("退货单编号不可为空");
                return;
            }
            if ($scope.entity.linkman == null || $scope.entity.linkman == "") {
                $("#sp1").text("联系人不可为空");
                return;
            }
            if ($scope.entity.phone == null || $scope.entity.phone == "") {
                $("#sp1").text("联系电话不可为空");
                return;
            }
            if ($scope.entity.remark == null || $scope.entity.remark == "") {
                $("#sp1").text("备注不可为空");
                return;
            }
            $("#sp1").text("");
            $scope.addCreditOrder();
        }
        //定义退货单状态数组
        $scope.status = ['未提交', '已提交至供货商'];
        $scope.search = function (page, rows) {
            console.log("查询退货单")
            creditOrderService.search(page, rows, $scope.searchEntity).success(
                function (response) {
                    $scope.list = response.rows;
                    $scope.paginationConf.totalItems = response.total;//更新总记录数
                }
            );
        }

    //将采购单信息保存在后台session中
    $scope.preservation = function (id) {
        creditOrderService.preservation(id).success(
            function (response) {
                window.location.href = "../admin/CreditOrderDetail.html";
            })
    }

    }
);
