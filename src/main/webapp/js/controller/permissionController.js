//控制层
app.controller('permissionController', function ($scope, $controller, permissionService, permissionService, toaster) {

    //读取列表数据绑定到表单中
    $scope.findAll = function () {
        permissionService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }

    $scope.addClick = function () {
        $scope.entity = {permissionName: "", url: ""};
    }

    //查询实体
    $scope.findOne = function (id) {
        permissionService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }

    //保存
    $scope.save = function () {
        if ($scope.entity.permissionName.replace(/\s*/g, "") == "") {
            toaster.pop('error', "权限名不可为空！");
            return;
        }
        if ($scope.entity.url.replace(/\s*/g, "") == "") {
            toaster.pop('error', "url不可为空！");
            return;
        }
        var serviceObject;//服务层对象
        if ($scope.entity.id != null) {//如果有ID
            serviceObject = permissionService.update($scope.entity); //修改
        } else {
            serviceObject = permissionService.add($scope.entity);//增加
        }
        serviceObject.success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $("#bt1").click();
                    $("#shua").click();
                } else {
                    toaster.pop('error', response.message);
                }
            }
        );
    }
    //删除单个
    $scope.deleteOne = function (id) {
        permissionService.deleteOne(id).success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $("#shua").click();
                }
            }
        );
    }

    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
    }



    //初始化entity对象
    // $scope.entity={};


});	
