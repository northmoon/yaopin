//控制层
app.controller('areaController', function ($scope, $http, $controller, areaService, drugClassificationService, toaster) {

    $controller('baseController', {$scope: $scope});//继承

    //读取列表数据绑定到表单中  
    $scope.findAll = function () {
        drugCategoryService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }

    //分页
    $scope.findPage = function (page, rows) {
        drugCategoryService.findPage(page, rows).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    $scope.addClick = function () {
        $scope.entity = {};
        $("#sp1").text("");
    }

    //查询实体
    $scope.findOne = function (id) {
        areaService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }

    $scope.verify = function () {
        if ($scope.entity.aname == null || $scope.entity.aname == "") {
            $("#sp1").text("名称不可为空");
            return;
        }
        if ($scope.entity.grade == null || $scope.entity.grade == "") {
            $("#sp1").text("等级不可为空");
            return;
        }
        if ($scope.entity.fid == null || $scope.entity.fid == "") {
            $("#sp1").text("等级不可为空");
            return;
        }
        $("#sp1").text("");
        $scope.save();
    }

    //保存
    $scope.save = function () {
        var serviceObject;//服务层对象
        if ($scope.entity.aid != null) {//如果有ID
            serviceObject = areaService.update($scope.entity); //修改
        } else {
            serviceObject = areaService.add($scope.entity);//增加
        }
        $scope.pop();
        $("#bt1").click();
        $scope.reloadList();
    }
    //删除单个
    $scope.deleteOne = function (id) {
        areaService.deleteOne(id).success(
            function (response) {
                    $scope.pop();
                    $scope.reloadList();//刷新列表
            }
        );
    }

    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
    }


    //批量删除
    $scope.dele = function () {
        //获取选中的复选框
        drugCategoryService.dele($scope.selectIds).success(
            function (response) {
                if (response.success) {
                    $scope.reloadList();//刷新列表
                }
            }
        );
    }

    $scope.searchEntity = {};//定义搜索对象

    //搜索
    $scope.search = function (page, rows) {
        areaService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    //初始化entity对象
    // $scope.entity={};


    $scope.saveSearchEntity = function () {
        drugCategoryService.saveSearchEntity($scope.searchEntity).success(function (response) {
            window.location.href = 'http://localhost:8080/drugCategory/exportExcel';
        })
    }


    //定义药品状态数组
    $scope.status = ['正常', '关闭'];

    //初始化药品类别列表
    $scope.drugClassificationList = [];

    //定义查询所有药品类别的方法
    $scope.selectDrugClassificationList = function () {
        drugClassificationService.findAll().success(function (response) {
            for (var i = 0; i < response.length; i++) {
                $scope.drugClassificationList = response;
            }
        })
    }
    //导入
    $scope.upload = function () {
        var form = new FormData();
        var file = document.getElementById("file").files[0];
        form.append("file", file);
        console.log(form);
        console.log(file);
        $.ajax({
            url: "../drugCategory/upload",
            type: "post",
            data: form,
            datatype: "json",
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var file1 = $("#file");
                file1.after(file1.clone().val(""))
                file1.remove();
                $scope.messageText = response.message;
            }

        })
        // $http({
        //     method: "post",
        //     url: "../drugCategory/upload",
        //     date: form,
        //     headers: {'Content-Type':undefined},
        //     transformRequest: angular.identity
        // }).success(function (response) {
        //     console.log(response.message);
        // })
    }
});	
