//药品明细
app.controller('purchaseAndWarehousingController', function ($scope, $http, $controller, purchaseAndWarehousingService, toaster) {
    $controller('baseController', {$scope: $scope});//继承
    //json初始化
    $scope.searchEntity = {};
    //定义采购状态
    $scope.status = ['未确认送货', '已发货', '已入库', '无法供货', '到期未送货'];
    //读取列表数据绑定到表单中
    $scope.findAll = function () {
        console.log("查询");
        console.log($scope.searchEntity);
        purchaseAndWarehousingService.findAll($scope.searchEntity).success(
            function (response) {
                $scope.list = response;
                console.log($scope.list);
            }
        );
    }

    /***
     * 把要退货的药品信息添加到退货明细中
     */
    var serviceObject;
    $scope.add=function () {
        console.log();
        var r = confirm("你确定要添加到该退货单嘛");
        if (r == true) {
            purchaseAndWarehousingService.add($scope.selected).success(
                function (response) {
                    if (response.success) {
                        window.location.href="../admin/CreditOrderDetail.html";
                    }
                }
            );
        }
    }
    //将入库信息添加到结算单
    $scope.addS = function () {
        console.log('结算保存');
        var r = confirm("确认结算后不允许更改，是否继续？");
        if (r == true) {
            purchaseAndWarehousingService.add1($scope.selected).success(
                function (response) {
                    if (response.success) {
                        window.location.href = "../admin/StatementsDetail.html";
                    }
                }
            )
        }
    }

    //全选方法，并将所有的id一并传入selected数组中
    $scope.selected = [];//定义一个数组
    $scope.all = function ($event) {
        var list = $scope.list;
        angular.forEach($scope.list, function (v, k) {
            //console.log(v.id);//读到所有的数组数据
            $scope.selected.push(v.purchaseOrderWarehousing.id);
            //console.log(k);//读到所有的对象的下标
        });
        var checkbox = $event.target;
        var checked = checkbox.checked;
        if (checked) {
            $scope.x = true;
        }
        else {
            $scope.x = false;
            $scope.selected = [];
        }
    };
    $scope.updateSelection = function ($event, id) { //单选更新selected
        var checkbox = $event.target;
        var checked = checkbox.checked;
        if (checked) {
            $scope.selected.push(id);
           ;
            console.log($scope.selected);
        } else {
            var idx = $scope.selected.indexOf(id);
            $scope.selected.splice(idx, 1);
        }
    }


})