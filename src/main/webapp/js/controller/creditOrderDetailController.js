//药品明细
app.controller('creditOrderDetailController', function ($scope, $http, $controller, creditOrderDetailService, toaster) {
        $controller('baseController', {$scope: $scope});//继承
        //json初始化
        $scope.searchEntity = {};
        $scope.data = {};
        $scope.status3 = ['未提交', '已提交至供货商'];
        $scope.stat = {};
        $scope.getInfo = function () {
            console.log("获取退货单信息");
            console.log($scope.data);
            creditOrderDetailService.getSession().success(
                function (response) {
                    $scope.data = response;
                    var stat = $scope.data.state-1;
                    $scope.stat = stat;
                    alert($scope.stat);

                }
            )
        }
        $scope.save = function () {
            console.log("保存推货量");
            console.log($scope.map);
            $scope.map = {
                "ids": $scope.selected,
                "returnNum": $scope.tuihuoliang,
                "returnSum": $scope.tuihuojine,
                "returnCause": $scope.reason
            };
            var r = confirm("确认退货后不允许更改，是否继续？");
            if (r == true) {
                creditOrderDetailService.save($scope.map).success(
                    function (response) {
                        if (response.success) {
                            $scope.reloadList();//刷新列表
                        }
                    }
                )
            }
        }
        $scope.search = function (page, rows) {
            console.log("查询退货药品");
            console.log($scope.searchEntity);
            creditOrderDetailService.search(page, rows, $scope.searchEntity).success(
                function (response) {
                    $scope.list = response.rows;
                    $scope.paginationConf.totalItems = response.total;//更新总记录数
                }
            )
        }
        //批量删除
        $scope.deleteSome = function () {
            var r = confirm("您确定要删除吗？");
            if (r == true) {
                creditOrderDetailService.deleteSome($scope.selected).success(
                    function (response) {
                        if (response.success) {
                            $scope.reloadList();//刷新列表
                        }
                    }
                )
            }
        }

        //保存之后的跳转
        $scope.tiaoZhuan = function () {
            var r = confirm("提交退货单后将不允许添加修改退货药品，是否继续？");
            if (r == true) {
                creditOrderDetailService.Preservation().success(
                    function (response) {
                        if (response.success) {
                            window.location.href = "../admin/ReturnOrder.html";
                        }
                    }
                )
            }
        }
        $scope.selected = [];//定义一个数组
        $scope.tuihuoliang = [];//定义一个数组
        $scope.tuihuojine = [];//定义一个数组
        $scope.reason = [];//定义一个数组
        //全选方法，并将所有的id一并传入selected数组中
        $scope.all = function ($event) {
            var list = $scope.list;
            angular.forEach($scope.list, function (v, k) {
                //console.log(v.id);//读到所有的数组数据
                $scope.selected.push(v.creditOrderDetail.id);
                $scope.tuihuoliang.push(v.creditOrderDetail.returnNum);
                $scope.tuihuojine.push(v.creditOrderDetail.returnSum);
                $scope.reason.push(v.creditOrderDetail.returnCause);
                //console.log(k);//读到所有的对象的下标
            });
            var checkbox = $event.target;
            var checked = checkbox.checked;
            if (checked) {
                $scope.x = true;
            }
            else {
                $scope.x = false;
                $scope.selected = [];
                $scope.tuihuoliang = [];
                $scope.tuihuojine = [];
                $scope.reason = [];
            }
        };
        $scope.updateSelection = function ($event, id, returnNum, returnSum, returnCause) { //单选更新selected
            var checkbox = $event.target;
            var checked = checkbox.checked;
            if (checked) {
                $scope.selected.push(id);
                $scope.tuihuoliang.push(returnNum);
                $scope.tuihuojine.push(returnSum);
                $scope.reason.push(returnCause);
                console.log($scope.selected);
            } else {
                var idx = $scope.selected.indexOf(id);
                var idx1 = $scope.tuihuoliang.indexOf(id);
                var idx2 = $scope.tuihuojine.indexOf(id);
                var idx3 = $scope.reason.indexOf(id);
                $scope.selected.splice(idx, 1);
                $scope.tuihuoliang.splice(idx1, 1);
                $scope.tuihuojine.splice(idx2, 1);
                $scope.reason.splice(idx3, 1);
            }
        }
    }
);
