//控制层
app.controller('HospitalPurchasingController', function ($scope, $http, $controller, HospitalPurchasingService, drugClassificationService, toaster) {

    $controller('baseController', {$scope: $scope});//继承
    //读取列表数据绑定到表单中  
    $scope.findAll = function () {
        HospitalPurchasingService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }
    //定义药品状态数组
    $scope.status = ['正常', '取消交易'];
    $scope.statu = ['正常', '关闭'];
    //初始化药品类别列表
    $scope.drugClassificationList = [];
    //json初始化
    $scope.searchEntity = {};
    //初始化药品类别列表
    $scope.drugClassificationList = [];

    //定义查询所有药品类别的方法
    $scope.selectDrugClassificationList = function () {
        drugClassificationService.findAll().success(function (response) {
            for (var i = 0; i < response.length; i++) {
                $scope.drugClassificationList = response;
            }
        })
    }
    //搜索
    $scope.search = function (page, rows) {
        HospitalPurchasingService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                //  alert(12);
                console.log(response);
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }
    $scope.selected = [];//定义一个数组
    //全选方法，并将所有的id一并传入selected数组中
    $scope.all = function ($event) {
        //debugger;
        var checkbox = $event.target;
        var checked = checkbox.checked;
        if (checked) {
            var list = $scope.list;
            angular.forEach($scope.list, function (v, k) {
                //console.log(v.id);//读到所有的数组数据
                $scope.selected.push(v.id);
                //console.log(k);//读到所有的对象的下标
            });
            console.log($scope.selected);
            $scope.x = true;
        }
        else {
            $scope.x = false;
            $scope.selected = [];
            console.log($scope.selected);
        }
    };
    $scope.updateSelection = function ($event, id) { //单选更新selected
        var checkbox = $event.target;
        var checked = checkbox.checked;
        if (checked) {
            $scope.selected.push(id);
            console.log($scope.selected);
        } else {
            var idx = $scope.selected.indexOf(id);
            $scope.selected.splice(idx, 1);

        }
    }
        //批量删除
        $scope.removeIdList = function () {
            var r = confirm("您确定要删除所勾选的采购药品吗？");
            if (r == true) {
                //获取选中的复选框
                HospitalPurchasingService.removeId($scope.selected).success(
                    function (response) {
                        if (response.success) {
                            $scope.reloadList();//刷新列表
                        }
                    }
                );
            }
        }
        //插入到采购表
        $scope.addPn = function () {
            HospitalPurchasingService.addPn($scope.selected).success(
                function (response) {
                    if (response.success) {
                        toaster.pop('success', "添加成功");
                        $scope.reloadList();
                        window.location.href = "../admin/DrugDetails.html"
                    }
                }
            )
        }
    });


