//药品明细
app.controller('statementsDetailController', function ($scope, $http, $controller, statementsDetailService, toaster) {
        $controller('baseController', {$scope: $scope});//继承
        //json初始化
        $scope.searchEntity = {};
        $scope.status1 = ['未提交', '已提交至供货商'];
        $scope.status2 = ['未确认结算', '已确认结算'];
        $scope.getSession = function () {
            console.log("获取结算单信息");
            statementsDetailService.getSession().success(
                function (response) {
                    $scope.entity = response;
                }
            )
        }
        $scope.search = function (page, rows) {
            console.log("查询结算药品");
            statementsDetailService.search(page, rows, $scope.searchEntity).success(
                function (response) {
                    console.log($scope.searchEntity);
                    $scope.list = response.rows;
                    $scope.paginationConf.totalItems = response.total;//更新总记录数
                }
            )
        }
        //批量删除
        $scope.deleteSome = function () {
            var r = confirm("您确定要删除吗？");
            if (r == true) {
                statementsDetailService.deleteSome($scope.selected).success(
                    function (response) {
                        if (response.success) {
                            $scope.reloadList();//刷新列表
                        }
                    }
                )
            }
        }
        //保存之后的跳转
        $scope.tiaoZhuan = function () {
            var r = confirm("提交结算单后将不允许添加修改结算药品，是否继续？");
            if (r == true) {
                statementsDetailService.save().success(
                    function (response) {
                        if (response.success) {
                            window.location.href = "../admin/Statements.html";
                        }
                    }
                )
            }
        }
        $scope.selected = [];//定义一个数组
        $scope.tuihuoliang = [];//定义一个数组
        $scope.tuihuojine = [];//定义一个数组
        $scope.reason = [];//定义一个数组
        //全选方法，并将所有的id一并传入selected数组中
        $scope.all = function ($event) {
            var list = $scope.list;
            angular.forEach($scope.list, function (v) {
                //console.log(v.id);//读到所有的数组数据
                $scope.selected.push(v.statementsDetail.id);
                //console.log(k);//读到所有的对象的下标
            });
            var checkbox = $event.target;
            var checked = checkbox.checked;
            if (checked) {
                $scope.x = true;
            }
            else {
                $scope.x = false;
            }
        };
        $scope.updateSelection = function ($event, id) { //单选更新selected
            var checkbox = $event.target;
            var checked = checkbox.checked;
            if (checked) {
                $scope.selected.push(id);
                console.log($scope.selected);
            } else {
                var idx = $scope.selected.indexOf(id);
                $scope.selected.splice(idx, 1);
            }
        }
    }
);
