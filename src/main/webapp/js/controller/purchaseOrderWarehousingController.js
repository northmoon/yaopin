//药品明细
app.controller('purchaseOrderWarehousingController', function ($scope, $http, $controller, purchaseOrderWarehousingService, toaster) {
        $controller('baseController', {$scope: $scope});//继承
        //读取列表数据绑定到表单中
        $scope.findAll = function () {
            console.log("采购入库controller");
            purchaseOrderWarehousingService.findAll($scope.searchEntity).success(
                function (response) {
                    $scope.list = response;
                    console.log($scope.list);
                }
            );
        }
    //保存入库
    $scope.purchaseVolume = function () {
        console.log('保存入库');
        $scope.map = {"ids": $scope.selected, "receipt":  $scope.rukuliang,"invoiceNum":$scope.fapiaohao,"batchNum":$scope.yaopinpihao,"drug_validity": $scope.years};
        console.log($scope.map);
        console.log($scope.selected);
        var r = confirm("确认入库后不允许更改，是否继续？");
        if (r == true) {
            purchaseOrderWarehousingService.purchaseVolume($scope.map).success(
                function (response) {
                    if (response.success) {
                        window.location.href = "../admin/purchaseOrderWarehousing.html";
                    }
                }
            )
        }
    }

        //json初始化
        $scope.searchEntity = {};
        $scope.selected = [];//定义id数组
        //定义入库量数组
        $scope.rukuliang = [];
        //定义发票号数组
        $scope.fapiaohao = [];
        //定义药品批号数组
        $scope.yaopinpihao = [];
        //定义药品有效期数组
        $scope.years=[];
        //全选方法，并将所有的id一并传入selected数组中
        $scope.all = function ($event) {
            var list = $scope.list;
            angular.forEach($scope.list, function (v) {
                //console.log(v.id);//读到所有的数组数据
                $scope.selected.push(v.purchaseOrderDrugDetails.id);
                $scope.rukuliang.push(v.purchaseOrderWarehousing.receipt);
                $scope.fapiaohao.push(v.purchaseOrderWarehousing.invoiceNum);
                $scope.yaopinpihao.push(v.purchaseOrderWarehousing.batchNum);
                $scope.years.push(v.purchaseOrderWarehousing.drugValidity);
                //console.log(k);//读到所有的对象的下标
            });
            var checkbox = $event.target;
            var checked = checkbox.checked;
            if (checked) {
                $scope.x = true;
            }
            else {
                $scope.x = false;
                $scope.selected = [];
                $scope.rukuliang = [];
                $scope.fapiaohao = [];
                $scope.yaopinpihao = [];
                $scope.years=[];
            }
        };
        $scope.updateSelection = function ($event, id,receipt,invoiceNum,batchNum,drugValidity ) { //单选更新selected
            var checkbox = $event.target;
            var checked = checkbox.checked;
            if (checked) {
                $scope.selected.push(id);
                $scope.rukuliang.push(receipt);
                $scope.fapiaohao.push(invoiceNum);
                $scope.yaopinpihao.push(batchNum);
                $scope.years.push(drugValidity);
                console.log($scope.selected);
            } else {
                var idx = $scope.selected.indexOf(id);
                var idx1 = $scope.selected.indexOf(receipt);
                var idx2 = $scope.selected.indexOf(invoiceNum);
                var idx3 = $scope.selected.indexOf(batchNum);
                var idx4 = $scope.selected.indexOf(drugValidity);
                $scope.selected.splice(idx, 1);
                $scope.rukuliang.splice(idx1, 1);
                $scope.fapiaohao.splice(idx2, 1);
                $scope.yaopinpihao.splice(idx3, 1);
                $scope.years.splice(idx4, 1);
            }
        }
    }
);
