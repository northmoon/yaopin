//控制层
app.controller('supplierListController', function ($scope, $http, $controller, supplierListService,drugClassificationService,  toaster) {

    $controller('baseController', {$scope: $scope});//继承
    //读取列表数据绑定到表单中  
    $scope.findAll = function () {
        supplierListService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }
    //定义药品状态数组
    $scope.status = ['正常', '取消交易'];
    $scope.statu = ['正常', '关闭'];
    //初始化药品类别列表
    $scope.drugClassificationList = [];
    //json初始化
    $scope.searchEntity = {};

    //初始化药品类别列表
    $scope.drugClassificationList = [];

    //定义查询所有药品类别的方法
    $scope.selectDrugClassificationList = function () {
        drugClassificationService.findAll().success(function (response) {
            for (var i = 0; i < response.length; i++) {
                $scope.drugClassificationList = response;
            }
        })
    }

    //搜索
    $scope.search = function (page, rows) {
        supplierListService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }


    $scope.selected = [];//定义一个数组
    //全选方法，并将所有的id一并传入selected数组中
    $scope.all = function($event){
        //debugger;
        var list=$scope.list;
        angular.forEach($scope.list, function (v, k) {
            //console.log(v.id);//读到所有的数组数据
            $scope.selected.push(v.id);
            //console.log(k);//读到所有的对象的下标
        });
        var checkbox = $event.target ;
        var checked = checkbox.checked ;
        if(checked){
            $scope.x=true;
        }
        else{
            $scope.x=false;
            $scope.selected=[];
        }
    };
    $scope.updateSelection = function($event,id){ //单选更新selected
        var checkbox = $event.target ;
        var checked = checkbox.checked ;
        if(checked){
            $scope.selected.push(id);
        }else{
            var idx = $scope.selected.indexOf(id) ;
            $scope.selected.splice(idx,1);
        }
    }
  //插入到采购表
    $scope.addPn=function () {
        var r = confirm("您确定要添加到采购单吗？");
        if (r == true) {
            supplierListService.addPn($scope.selected).success(
                function (response) {
                    if (response.success) {
                        toaster.pop('success', "修改成功");
                        $scope.reloadList();
                    }
                }
            )
        }
    }


});	
