//控制层
app.controller('purchaseNoteAuditController', function ($scope, $http, $controller, purchaseNoteAuditService, toaster) {

    $controller('baseController', {$scope: $scope});//继承

    //读取列表数据绑定到表单中  
    $scope.findAll = function () {
        purchaseNoteAuditService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }


    //定义记录选中id的数组
    $scope.ids = [];

    $scope.purchaseOrderStatu = [];

    $scope.auditOpinions = [];

    $scope.sum = 0;

    $scope.change = function (id, a, auditOpinion) {
        $scope.sum = 0;
        angular.forEach($scope.list, function (pojo) {
            if (pojo.purchaseOrderStatus != 2) {
                $scope.sum = $scope.sum + 1;
            }
        })
        var index = $scope.ids.indexOf(id);
        if (index != -1) {
            $scope.ids.splice(index, 1);
            $scope.purchaseOrderStatu.splice(index, 1);
            $scope.auditOpinions.splice(index, 1);
            if (a != 2) {
                $scope.ids.push(id);
                $scope.purchaseOrderStatu.push(a);
                $scope.auditOpinions.push(auditOpinion);
            }
        } else {
            angular.forEach($scope.list, function (pojo) {
                if (pojo.id == id) {
                    pojo.checked = false;
                }
            })
            $scope.select_all = false;
        }

    }

    $scope.select_all = false;

    $scope.selectAll = function () {
        if (!$scope.select_all) {
            $scope.ids = [];
            $scope.purchaseOrderStatu = [];
            $scope.auditOpinions = [];
            angular.forEach($scope.list, function (pojo) {
                pojo.checked = true;
                if (pojo.purchaseOrderStatus != 2) {
                    $scope.ids.push(pojo.id);
                    $scope.purchaseOrderStatu.push(pojo.purchaseOrderStatus);
                    $scope.auditOpinions.push(pojo.auditOpinion);
                }
            })
        } else {
            angular.forEach($scope.list, function (pojo) {
                pojo.checked = false;
                $scope.ids = [];
                $scope.purchaseOrderStatu = [];
                $scope.auditOpinions = [];
            })
        }
    }

    //更新复选框选中状态
    $scope.updateSelection1 = function ($event, id, purchaseOrderStatus, auditOpinion) {
        //判断选中状态
        if ($event.target.checked) {//选中状态
            $scope.ids.push(id);
            $scope.purchaseOrderStatu.push(purchaseOrderStatus);
            $scope.auditOpinions.push(auditOpinion);
        } else {
            //取消勾选，移除当前id值  //参数一：移除位置的元素的索引值  参数二：从该位置移除几个元素
            var index = $scope.ids.indexOf(id);
            $scope.ids.splice(index, 1);
            $scope.purchaseOrderStatu.splice(index, 1);
            $scope.auditOpinions.splice(index, 1);
        }
        if ($scope.sum == $scope.ids.length) {
            $scope.select_all = true;
        } else {
            $scope.select_all = false;
        }
    }

    //批量修改
    $scope.update = function () {
        if ($scope.ids.length != 0) {
            if (confirm("审核结果提交后将不允许更改，是否继续？")) {

                $scope.entitys = {
                    "ids": $scope.ids,
                    "purchaseOrderStatu": $scope.purchaseOrderStatu,
                    "auditOpinions": $scope.auditOpinions
                };
                console.log("ids=" + $scope.ids);
                console.log("purchaseOrderStatu=" + $scope.purchaseOrderStatu);
                console.log("auditOpinions=" + $scope.auditOpinions);
                console.log("entitys=" + $scope.entitys);
                //获取选中的复选框
                purchaseNoteAuditService.update($scope.entitys).success(
                    function (response) {
                        if (response.success) {
                            $scope.select_all = false;
                            toaster.pop('success', response.message);
                            $scope.reloadList();//刷新列表
                        }
                    }
                );
            }
        } else {
            alert("请至少选择一条数据！");
        }
    }

    //分页
    $scope.findPage = function (page, rows) {
        purchaseNoteAuditService.findPage(page, rows).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    $scope.addClick = function () {
        $scope.entity = {};
        $("#sp1").text("");
    }

    //查询实体
    $scope.findOne = function (id) {
        purchaseNoteAuditService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }


    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
    }


    $scope.searchEntity = {};//定义搜索对象

    //搜索
    $scope.search = function (page, rows) {
        purchaseNoteAuditService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    //定义药品状态数组
    $scope.status = ['审核中', '审核通过', '审核不通过'];


});	
