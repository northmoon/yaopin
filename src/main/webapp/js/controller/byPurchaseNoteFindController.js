//控制层
app.controller('byPurchaseNoteFindController', function ($scope, $http, $controller, byPurchaseNoteFindService,supplierService, drugClassificationService, qualityLevelService, toaster) {

    $controller('baseController', {$scope: $scope});//继承

    //读取列表数据绑定到表单中  
    $scope.findAll = function () {
        byPurchaseNoteFindService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }

    //查询实体
    $scope.findOne = function (id) {
        byPurchaseNoteFindService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }

    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
    }


    $scope.searchEntity = {};//定义搜索对象

    //搜索
    $scope.search = function (page, rows) {
        byPurchaseNoteFindService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    //初始化entity对象
    // $scope.entity={};

    //定义采购状态状态数组
    $scope.purStatu = ['','未确认送货', '已发货', '已入库', '无法供货', '到期未供货'];

    //定义药品状态数组
    $scope.status = ['正常', '取消交易'];

    //初始化药品类别列表
    $scope.drugClassificationList = [];
    $scope.supplierMap = {};
    $scope.qualityLevelMap = {};

    $scope.selectList = function () {
        $scope.selectDrugClassificationList();
        $scope.selectQualityLevelList();
        $scope.selectSupplierList();
    }

    //定义查询所有供货商的方法
    $scope.selectSupplierList = function () {
        supplierService.findAll().success(function (response) {
            $scope.supplierMap = response;
        })
    }

    //定义查询所有药品类别的方法
    $scope.selectDrugClassificationList = function () {
        drugClassificationService.findAll().success(function (response) {
            for (var i = 0; i < response.length; i++) {
                $scope.drugClassificationList = response;
            }
        })
    }
    //定义查询所有质量层次的方法
    $scope.selectQualityLevelList = function () {
        qualityLevelService.findAll().success(function (response) {
            $scope.qualityLevelMap = response;
        })
    }

    $scope.saveSearchEntity = function () {
        byPurchaseNoteFindService.saveSearchEntity($scope.searchEntity).success(function (response) {
            window.location.href = 'http://localhost:8080/hospitalTransactionSchedule/exportExcel';
        })
    }
});	
