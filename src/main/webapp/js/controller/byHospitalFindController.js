//控制层
app.controller('byHospitalFindController', function ($scope, $controller, byHospitalFindService, toaster) {

    $controller('baseController', {$scope: $scope});//继承


    //查询实体
    $scope.findOne = function (id) {
        byHospitalFindService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }

    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
    }


    $scope.searchEntity = {};//定义搜索对象

    //搜索
    $scope.search = function (page, rows) {
        byHospitalFindService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    //初始化entity对象
    // $scope.entity={};

    //定义采购状态状态数组
    $scope.purStatu = ['', '未确认送货', '已发货', '已入库', '无法供货', '到期未供货'];

    $scope.saveSearchEntity = function () {
        byHospitalFindService.saveSearchEntity($scope.searchEntity).success(function (response) {
            window.location.href = 'http://localhost:8080/hospitalTransactionSchedule/exportExcel';
        })
    }

});	
