//控制层
app.controller('PurchasingOrderMaintenanceController', function ($scope, $http, $controller,PurchasingOrderMaintenanceService, toaster) {
    $controller('baseController', {$scope: $scope});//继承
    //读取列表数据绑定到表单中  
    $scope.findAll = function () {
        PurchasingOrderMaintenanceService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }
    //定义采购单状态数组
    $scope.status = ['未提交', '审核中', '审核通过', '审核不通过'];
    //json初始化
    $scope.searchEntity = {};
    //搜索
    $scope.search = function (page, rows) {
        PurchasingOrderMaintenanceService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }
    //删除单个
    $scope.deleteOne = function (id, status) {
        console.log(status)
            if (status != "1") {
                toaster.pop('error', "只能对采购单状态为未提交的才可以删除");
                return;
            }
        var r = confirm("您确定要删除该采购单吗？");
        if (r == true) {
            PurchasingOrderMaintenanceService.deleteOne(id).success(
                function (response) {
                    if (response.success) {
                        toaster.pop('success', response.message);
                        $scope.reloadList();//刷新列表
                    }
                }
            );
        }
    }
    //保存
    $scope.save = function () {
        var serviceObject;//服务层对象
        if ($scope.entity.id != null) {//如果有ID
            serviceObject = PurchasingOrderMaintenanceService.update($scope.entity); //修改
        }
        serviceObject.success(
            function (response) {
                if (response.success) {
                    toaster.pop('success', "修改成功");
                    $("#bt1").click();
                    $scope.reloadList();
                } else {
                    $("#sp1").html(response.message);
                }
            }
        );
    }
    $scope.verify = function () {
        if ($scope.entity.hospital.hospitalName == null || $scope.entity.hospital.hospitalName == "") {
            $("#sp1").text("医院名称不可为空");
            return;
        }
        if ($scope.entity.purchaseNumber == null || $scope.entity.purchaseNumber == "") {
            $("#sp1").text("采购单编号不可为空");
            return;
        }
        if ($scope.entity.purchaseOrderName == null || $scope.entity.purchaseOrderName == "") {
            $("#sp1").text("采购单名称不可为空");
            return;
        }
        if ($scope.entity.buildSingle == null || $scope.entity.buildSingle == "") {
            $("#sp1").text("建单时间不可为空");
            return;
        }
        if ($scope.entity.submit == null || $scope.entity.submit == "") {
            $("#sp1").text("提交时间不可为空");
            return;
        }
        if ($scope.entity.auditDate == null || $scope.entity.auditDate == "") {
            $("#sp1").text("审核时间不可为空");
            return;
        }
        if ($scope.entity.purchaseOrderStatus == null || $scope.entity.purchaseOrderStatus == "") {
            $("#sp1").text("采购单状态不可为空");
            return;
        }
        $("#sp1").text("");
        $scope.save();
    }
    //添加时获取实体
    $scope.addId = function () {
        PurchasingOrderMaintenanceService.addId().success(
            function (response) {
                $scope.entity = response;
            }
        );
    }
    //添加采购单
    $scope.baocun = function () {
        var serviceObject;//服务层对象
        serviceObject = PurchasingOrderMaintenanceService.add($scope.entity);//增加
        serviceObject.success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $("#bt").click();
                    $scope.reloadList();
                } else {
                    $("#sp1").html(response.message);
                }
            }
        );
    }
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
    }
    //添加保存
    $scope.verify1 = function () {
        if ($scope.entity.purchaseNumber == null || $scope.entity.purchaseNumber == "") {
            $("#sp1").text("采购单编号不可为空");
            return;
        }
        if ($scope.entity.buildSingle == null || $scope.entity.buildSingle == "") {
            $("#sp1").text("建单时间不可为空");
            return;
        }
        if ($scope.entity.submit == null || $scope.entity.submit == "") {
            $("#sp1").text("提交时间不可为空");
            return;
        }
        if ($scope.entity.auditDate == null || $scope.entity.auditDate == "") {
            $("#sp1").text("审核时间不可为空");
            return;
        }
        if ($scope.entity.purchaseOrderStatus == null || $scope.entity.purchaseOrderStatus == "") {
            $("#sp1").text("采购单状态不可为空");
            return;
        }
        $("#sp1").text("");
        $scope.baocun();
    }

    //将采购单信息保存在后台session中
    $scope.preservation = function (id) {
        PurchasingOrderMaintenanceService.preservation(id).success(
            function (response) {
                window.location.href = "/admin/DrugDetails.html";
            })
    }

    //查询实体
    $scope.findOne = function (id) {
        PurchasingOrderMaintenanceService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }
    $scope.selected = [];//定义一个数组
    //全选方法，并将所有的id一并传入selected数组中
    $scope.all = function ($event) {
        var list = $scope.list;
        angular.forEach($scope.list, function (v, k) {
            //console.log(v.id);//读到所有的数组数据
            $scope.selected.push(v.id);
            //console.log(k);//读到所有的对象的下标
        });
        var checkbox = $event.target;
        var checked = checkbox.checked;
        if (checked) {
            $scope.x = true;
        }
        else {
            $scope.x = false;
            $scope.selected = [];
        }
    };
});
