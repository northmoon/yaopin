//控制层
app.controller('userController', function ($scope, $controller, userService, roleService, toaster) {

    $controller('baseController', {$scope: $scope});//继承

    //读取列表数据绑定到表单中
    $scope.findAll = function () {
        userService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }

    //分页
    $scope.findPage = function (page, rows) {
        userService.findPage(page, rows).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    $scope.addClick = function () {
        $scope.entity = {uid: "", password: "", ucategory: "", uname: "", unitid: ""};
        $scope.unitMap = {};
        $scope.roleids = [];
    }

    //查询实体
    $scope.findOne = function (id) {
        userService.findOne(id).success(
            function (response) {
                $scope.selectUnit(response.ucategory);
                $scope.entity = response;
                $scope.a = response.unitid;
                $scope.roles = $scope.entity.roleList;
                $scope.roleids = [];
                angular.forEach($scope.roles, function (role) {
                    $scope.roleids.push(role.id);
                })
            }
        );
    }
    //更新复选框选中状态
    $scope.checkOne = function ($event, id) {
        //判断选中状态
        if ($event.target.checked) {//选中状态
            $scope.roleids.push(id);
        } else {
            //取消勾选，移除当前id值  //参数一：移除位置的元素的索引值  参数二：从该位置移除几个元素
            var index = $scope.roleids.indexOf(id);
            $scope.roleids.splice(index, 1);
        }
    }
    //保存
    $scope.save = function () {
        if ($scope.entity.uid.replace(/\s*/g, "") == "") {
            toaster.pop('error', "账号不可为空！");
            return;
        }
        if ($scope.entity.password.replace(/\s*/g, "") == "") {
            toaster.pop('error', "密码不可为空！");
            return;
        }
        console.log($scope.entity.ucategory);
        if ($scope.entity.ucategory.length == 0) {
            toaster.pop('error', "用户类别不可为空！");
            return;
        }
        if ($scope.entity.uname.replace(/\s*/g, "") == "") {
            toaster.pop('error', "用户名称不可为空！");
            return;
        }
        if ($("#so1").val().replace(/\s*/g, "") == "") {
            toaster.pop('error', "用户单位不可为空！");
            return;
        }
        if ($scope.roleids.length == 0) {
            toaster.pop('error', "用户角色不可为空！");
            return;
        }


        var serviceObject;//服务层对象
        if ($scope.entity.id != null) {//如果有ID
            serviceObject = userService.update($scope.entity, $scope.roleids); //修改
        } else {
            serviceObject = userService.add($scope.entity, $scope.roleids);//增加
        }
        serviceObject.success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $("#bt1").click();
                    $scope.reloadList();
                } else {
                    toaster.pop('error', response.message);
                }
            }
        );
    }
    //删除单个
    $scope.deleteOne = function (id) {
        userService.deleteOne(id).success(
            function (response) {
                if (response.success) {
                    $scope.pop();
                    $scope.reloadList();//刷新列表
                }
            }
        );
    }

    //成功提示框
    $scope.pop = function () {
        toaster.pop('success', "操作成功");
    }


    //批量删除
    $scope.dele = function () {
        //获取选中的复选框
        userService.dele($scope.selectIds).success(
            function (response) {
                if (response.success) {
                    $scope.reloadList();//刷新列表
                }
            }
        );
    }

    $scope.searchEntity = {};//定义搜索对象
    $scope.roleids = [];

    //搜索
    $scope.search = function (page, rows) {
        userService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    //初始化entity对象
    // $scope.entity={};


    //定义药品状态数组
    $scope.state = ['关闭', '正常'];
    $scope.categoryList = ['系统管理员', '卫生局', '卫生院', '卫生所', '供货商'];

    //查询所有角色
    $scope.selectAllRole = function () {
        roleService.findAll().success(function (response) {
            $scope.allRole = response;
        })
    }

    //决定状态
    $scope.expression = function (id) {
        return $scope.roleids.indexOf(id) != -1;
    }

    $scope.unitMap = {};

    $scope.changUcategory = function (ucategory) {
        if (ucategory == "") {
            $scope.unitMap = {};
            return;
        }
        $scope.searchEntity.unitid = "";
        $scope.selectUnit(ucategory);
    }
    $scope.changUcategory1 = function (ucategory) {
        if (ucategory == "") {
            $scope.unitMap = {};
            return;
        }
        $scope.selectUnit(ucategory);
    }
    $scope.selectUnit = function (ucategory) {
        userService.selectUnit(ucategory).success(function (response) {
            $scope.unitMap = response;
            $scope.entity.unitid = $scope.a;
        })
    }
});	
