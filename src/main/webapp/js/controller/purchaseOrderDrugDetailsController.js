//药品明细
app.controller('purchaseOrderDrugDetailsController', function ($scope, $http, $controller, PurchasingOrderDrugDetailsService, toaster) {
        $controller('baseController', {$scope: $scope});//继承
        //读取列表数据绑定到表单中
        $scope.findAll = function () {
            PurchasingOrderMaintenanceService.findAll().success(
                function (response) {
                    $scope.list = response;
                }
            );
        }
        //根据id查询药品明细
        $scope.findList = function () {
            console.log(11111);
            PurchasingOrderDrugDetailsService.findList().success(
                function (response) {
                    $scope.list = response;
                }
            );
        }
        //批量删除
        $scope.deleteList = function () {
            var r = confirm("您确定要删除吗？");
            if (r == true) {
                console.log(999)
                //获取选中的复选框
                PurchasingOrderDrugDetailsService.deleteList($scope.selected).success(
                    function (response) {
                        if (response.success) {
                            // $scope.reloadList();//刷新列表
                            window.location.href = "../admin/DrugDetails.html";
                        }
                    }
                );
            }

        }
        //保存采购量
        $scope.purchaseVolume = function () {
            console.log('保存采购')
            $scope.map = {"ids": $scope.selected, "caigouliang": $scope.caigouliang};
            console.log($scope.map);
            console.log($scope.selected);
            console.log($scope.caigouliang);
            var r = confirm("您确定要保存采购量吗？");
            if (r == true) {
                PurchasingOrderDrugDetailsService.purchaseVolume($scope.map).success(
                    function (response) {
                        if (response.success) {
                            window.location.href = "../admin/DrugDetails.html";
                        }
                    }
                )
            }
        }
        //跳转到采购单维护
        $scope.tiaoZhuan=function(){
            var r = confirm("提交采购单后将不允许添加修改采购药品，是否继续？");
            if (r == true) {
                window.location.href = "../admin/PurchasingOrderMaintenance.html";
            }
            }
        //json初始化
        $scope.searchEntity = {};
        $scope.selectDrugDetailList = function () {
            console.log(22);
            PurchasingOrderDrugDetailsService.getSession().success(
                function (response) {
                    $scope.entity = response;
                }
            )
        }
        //定义采购状态
        $scope.status = ['未确认送货', '已发货', '已入库', '无法供货', '到期未送货'];
        $scope.selected = [];//定义一个数组
        //定义采购量数组
        $scope.caigouliang = [];
        //全选方法，并将所有的id一并传入selected数组中
        $scope.all = function ($event) {
            debugger;
            var list = $scope.list;
            angular.forEach($scope.list, function (v, k) {
                //console.log(v.id);//读到所有的数组数据
                $scope.selected.push(v.id);
                $scope.caigouliang.push(v.amountPurchased);

                //console.log(k);//读到所有的对象的下标
            });
            var checkbox = $event.target;
            var checked = checkbox.checked;
            if (checked) {
                $scope.x = true;
            }
            else {
                $scope.x = false;
                $scope.selected = [];
                $scope.caigouliang = [];
            }
        };
        $scope.updateSelection = function ($event, id, amountPurchased) { //单选更新selected
            var checkbox = $event.target;
            var checked = checkbox.checked;
            if (checked) {
                $scope.selected.push(id);
                $scope.caigouliang.push(amountPurchased);
                console.log($scope.selected);
            } else {
                var idx = $scope.selected.indexOf(id);
                $scope.selected.splice(idx, 1);
                $scope.caigouliang.splice(idx, 1);
            }
        }
    }
);
