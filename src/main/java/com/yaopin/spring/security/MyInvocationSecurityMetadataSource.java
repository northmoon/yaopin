package com.yaopin.spring.security;

import java.util.*;

import com.yaopin.entity.Permission;
import com.yaopin.entity.Role;
import com.yaopin.mapper.PermissionMapper;
import com.yaopin.mapper.RoleMapper;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

import com.yaopin.spring.security.tool.AntUrlPathMatcher;
import com.yaopin.spring.security.tool.UrlMatcher;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;

public class MyInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {
    private UrlMatcher urlMatcher = new AntUrlPathMatcher();
    private static Map<String, Collection<ConfigAttribute>> resourceMap = null;


    private RoleMapper roleMapper;
    private PermissionMapper permissionMapper;

    //tomcat启动时实例化一次
    public MyInvocationSecurityMetadataSource(RoleMapper roleMapper,PermissionMapper permissionMapper) {
        this.roleMapper = roleMapper;
        this.permissionMapper = permissionMapper;
        loadResourceDefine();
    }

    //tomcat开启时加载一次，加载所有url和权限（或角色）的对应关系
    private void loadResourceDefine() {
        resourceMap = new HashMap<String, Collection<ConfigAttribute>>();
        List<Permission> permissions = permissionMapper.selectByExample(null);
        for (Permission permission : permissions) {
            String url = permission.getUrl();
            List<Role> roles = roleMapper.selectByPermissionId(permission.getId());
            Collection<ConfigAttribute> atts = new ArrayList<ConfigAttribute>();
            for (Role role : roles) {
                String roleName = role.getRoleName();
                ConfigAttribute ca = new SecurityConfig("ROLE_" + roleName);
                atts.add(ca);
            }
            resourceMap.put(permission.getUrl(), atts);
        }
    }

    //参数是要访问的url，返回这个url对于的所有权限（或角色）
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        // 将参数转为url
        String url = ((FilterInvocation) object).getRequestUrl();
        Iterator<String> ite = resourceMap.keySet().iterator();
        while (ite.hasNext()) {
            String resURL = ite.next();
            if (urlMatcher.pathMatchesUrl(resURL, url)) {
                return resourceMap.get(resURL);
            }
        }
        return null;
    }

    public boolean supports(Class<?> clazz) {
        return true;
    }

    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }
}