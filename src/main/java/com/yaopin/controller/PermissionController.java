package com.yaopin.controller;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.entity.Permission;
import com.yaopin.service.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/permission")
@Secured({"ROLE_ADMIN"})
public class PermissionController {
    @Autowired
    private IPermissionService permissionService;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public List<Permission> findAll() {
        return permissionService.findAll();
    }

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {
        return permissionService.findPage(page, rows);
    }

    /**
     * 增加
     *
     * @param permission
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Permission permission) {
        try {
            permissionService.add(permission);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        }
    }

    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public Permission findOne(Integer id) {
        return permissionService.findOne(id);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping("/deleteOne")
    public Result delete(Integer id) {
        try {
            permissionService.deleteOne(id);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }


}
