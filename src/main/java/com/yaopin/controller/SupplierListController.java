package com.yaopin.controller;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.entity.SupplierList;
import com.yaopin.service.SupplierListService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;
/**
 * @ClassName SupplierListController
 * @Author donghongyu
 * @Date 2019/9/6 14:43
 **/
@RestController
@RequestMapping("/SupplierListService")
public class SupplierListController {
/**
** @Description  分页条件查询
 * @Param
 */
    @Resource
    private SupplierListService supplierListService;
    @RequestMapping("/search")
    public PageResult search(@RequestBody SupplierList supplierList, int page, int rows) {
        return supplierListService.findPage( supplierList,page, rows);
    }
    /**
    ** @Description  批量添加
     * @Param
     */
    @RequestMapping("/addPn")
    public Result add(@RequestParam List<Integer> idList){
        try {
            supplierListService.addPn(idList);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }
    }




