package com.yaopin.controller;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.entity.SupplierList;
import com.yaopin.service.ISupplierListAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
@RestController
@RequestMapping("/supplierList")
@PreAuthorize("hasAnyRole('ORGANIZATION','WEISHENGYUAN')")
public class SupplierListAuditController {
    @Autowired
    private ISupplierListAuditService supplierListAuditService;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public Map<String, SupplierList> findAll() {
        return supplierListAuditService.findAll();
    }

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {
        return supplierListAuditService.findPage(page, rows);
    }


    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public SupplierList findOne(Integer id) {
        return supplierListAuditService.findOne(id);
    }


    /**
     * 批量修改
     *
     * @param map
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody Map<String, List<String>> map) {
        try {
            String message = supplierListAuditService.update(map);
            return new Result(true, message);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }

    /**
     * 查询+分页
     *
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody SupplierList supplierList, int page, int rows) {
        return supplierListAuditService.findPage(supplierList, page, rows);
    }

}
