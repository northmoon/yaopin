package com.yaopin.controller;

import com.yaopin.entity.AreaInfo;
import com.yaopin.entity.Hospital;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.service.HospitalService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;


@RestController
@RequestMapping("/hospital")
public class HospitalController {

    @Resource
    private HospitalService hospitalService;


    /**
     * 查询+分页
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody Hospital hospital, int page, int rows) {
        return hospitalService. findPage(hospital, page, rows);
    }


    @RequestMapping("/insert")
    public Result insert(@RequestBody Hospital hospital) throws Exception {
        try {
        hospitalService.addhospital(hospital);
            return new Result(true, "成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
//        return null;
    }

    /**
     * 按照医院表id删除单个信息
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try {
            hospitalService.deletehospital(id);
            return new Result(true, "");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "");
        }
        //        return "hospital";
    }


    /**
     * 根据id查询修改数据
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public Hospital finOne(Integer id) {
        return hospitalService.findOne(id);
    }

    /**
     * 修改
     * @param hospital
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody Hospital hospital){
        try{
            hospitalService.update(hospital);
            return new Result(true,"成功");
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,"失败");
        }
    }

    /**
     * 获得地区表信息
     * @return
     */
    @RequestMapping("/findAll")
    public List<AreaInfo> findAll() {
        List<AreaInfo> list = hospitalService.findAll();
        return list;
    }


    @RequestMapping("/saveSearchEntity")
    public void saveSearchEntity(@RequestBody Hospital hospital , HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("hospital", hospital);
    }

}
