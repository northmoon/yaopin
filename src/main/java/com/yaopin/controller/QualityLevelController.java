package com.yaopin.controller;

import com.yaopin.entity.DrugClassification;
import com.yaopin.entity.QualityLevel;
import com.yaopin.service.IDrugClassificationService;
import com.yaopin.service.IQualityLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/qualityLevel")
@PreAuthorize("hasAnyRole('ORGANIZATION','HOSPITAL','WEISHENGYUAN','SUPPLIER')")
public class QualityLevelController {

    @Autowired
    private IQualityLevelService qualityLevelService;

    @RequestMapping("/findAll")
    public Map<String, QualityLevel> findAll() {
        Map<String, QualityLevel> map = qualityLevelService.findAll();
        return map;
    }
}
