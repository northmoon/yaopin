package com.yaopin.controller;

import com.yaopin.entity.*;
import com.yaopin.entity.DrugInformation;
import com.yaopin.service.IDrugInformationService;
import com.yaopin.service.IDrugInformationService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@PreAuthorize("hasAnyRole('ORGANIZATION','HOSPITAL','WEISHENGYUAN','SUPPLIER')")
@RestController
@RequestMapping("/drugInformation")
public class DrugInformationController {

    @Autowired
    private IDrugInformationService drugInformationService;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public List<DrugInfo> findAll() {
        return drugInformationService.findAll();
    }

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {
        return drugInformationService.findPage(page, rows);
    }

    /**
     * 增加
     *
     * @param drugInfo
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody DrugInfo drugInfo) {
        try {
            drugInformationService.add(drugInfo);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }

    /**
     * 导入
     *
     * @param file
     * @return
     */
    @RequestMapping("/upload")
    public Result add(@RequestParam("file") MultipartFile file) {
        try {
            Map<String, String> errorMessage = drugInformationService.saveMore(file);
            return new Result(true, "共有" + errorMessage.get("successSum") + "条导入成功," + "导入失败的行数如下，请检查：" + errorMessage.get("errorList"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "导入失败");
        }
    }

    /**
     * 修改
     *
     * @param drugInfo
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody DrugInfo drugInfo) {
        try {
            drugInformationService.update(drugInfo);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }

    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public DrugInfo findOne(Integer id) {
        return drugInformationService.findOne(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(Integer[] ids) {
        try {
            drugInformationService.delete(ids);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }

    /**
     * 删除单个
     *
     * @param id
     * @return
     */
    @RequestMapping("/deleteOne")
    public Result delete(Integer id) {
        try {
            drugInformationService.deleteOne(id);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }

    /**
     * 查询+分页
     *
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody DrugInfo drugInfo, int page, int rows) {
        return drugInformationService.findPage(drugInfo, page, rows);
    }

    /*
     * 下载模板
     * */
    @RequestMapping("/download")
    public ResponseEntity<byte[]> download(String fileName, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String path = request.getSession().getServletContext().getRealPath("template") + "\\" + fileName;
        HttpHeaders headers = new HttpHeaders();
        File file = new File(path);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", fileName);
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
    }

    /*
     * 导出
     * */
    @RequestMapping("/exportExcel")
    public String exportExcel(HttpServletResponse response) {
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            String fileName = new String(("药品信息导出").getBytes(), "ISO8859_1");
            Date date = new Date();
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            String newTime = format.format(date);
            response.setHeader("Content-disposition", "attachment; filename=" + fileName + "_" + newTime + ".xlsx");// 组装附件名称和格式
            drugInformationService.exportExcel(outputStream);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    //保存查询条件
    @RequestMapping("/saveSearchEntity")
    public PageResult savesearchEntity(@RequestBody DrugInfo drugInfo,HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("key",drugInfo);
        return null;
    }


    /***
     * 按查询条件导出
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/exportExcelWithSearch")
    public String exportExcelWithSearch(HttpServletRequest request,HttpServletResponse response) {
        HttpSession session = request.getSession();
        DrugInfo drugInfo=(DrugInfo)session.getAttribute("key");
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            String fileName = new String(("药品品目导出").getBytes(), "ISO8859_1");
            Date date = new Date();
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            String newTime = format.format(date);
            response.setHeader("Content-disposition", "attachment; filename=" + fileName + "_" + newTime + ".xlsx");// 组装附件名称和格式
            drugInformationService.exportExcelWithSearch(outputStream,drugInfo);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }
}
