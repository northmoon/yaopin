package com.yaopin.controller;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.SearchReturnorder;
import com.yaopin.entity.SearchSettlement;
import com.yaopin.service.IReturnorderService;
import com.yaopin.service.ISettlementService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/Settlement")
public class SettlementController {
    @Autowired
    private ISettlementService iSettlementService;


    @RequestMapping("/search")
    public PageResult search(@RequestBody SearchSettlement searchSettlement, int page, int rows) {
        PageResult list = iSettlementService.findReturn(searchSettlement,page,rows);
        return list;
    }


//    @RequestMapping("/confirmSupplier")
//    public Integer confirmSupplier(@RequestBody List<Integer> list) {
//        Integer i=iPurchasingorderService.confirmSupplier(list);
//        return i;
//    }
//
//    @RequestMapping("/unableSupplier")
//    public Integer unableSupplier(@RequestBody List<Integer> list) {
//        Integer i=iPurchasingorderService.unableSupplier(list);
//        return i;
//    }

    @RequestMapping("/savesupplier")
    public void  savesupplier(@RequestBody List<Integer> list) {
        System.out.println(list);
        iSettlementService.saveWithList(list);
    }


    //保存查询条件
    @RequestMapping("/savesearchEntity")
    public PageResult savesearchEntity(@RequestBody SearchSettlement searchSettlement,HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("key3",searchSettlement);
        return null;
    }


    /*
     * 按查询条件导出
     * */
    @RequestMapping("/exportsearch")
    public String exportsearch(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        SearchSettlement searchSettlement=(SearchSettlement)session.getAttribute("key3");
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            String fileName = new String(("采购单处理导出").getBytes(), "ISO8859_1");
            Date date = new Date();
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            String newTime = format.format(date);
            response.setHeader("Content-disposition", "attachment; filename=" + fileName + "_" + newTime + ".xlsx");// 组装附件名称和格式
            iSettlementService.exportsearch(outputStream,searchSettlement);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    /*
     * 下载模板
     * */
    @RequestMapping("/download")
    public ResponseEntity<byte[]> download(String fileName, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String path = request.getSession().getServletContext().getRealPath("template") + "\\" + fileName;
        HttpHeaders headers = new HttpHeaders();
        File file = new File(path);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", fileName);
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
    }





}
