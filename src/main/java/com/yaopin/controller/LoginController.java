package com.yaopin.controller;

import com.yaopin.entity.RandomValidateCode;
import com.yaopin.entity.Result;
import com.yaopin.entity.Role;
import com.yaopin.entity.UserInfo;
import com.yaopin.mapper.RoleMapper;
import com.yaopin.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private ILoginService loginService;

    @Autowired
    private RoleMapper roleMapper;

    //获取验证码
    @RequestMapping("/checkCode")
    public void getCheckCode(HttpServletRequest request, HttpServletResponse response, String a) throws ServletException, IOException {
        //设置响应类型,告诉浏览器输出的内容为图片
        response.setContentType("image/jpeg");

        //设置响应头信息，告诉浏览器不要缓存此内容
        response.setHeader("pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expire", 0);

        RandomValidateCode randomValidateCode = new RandomValidateCode();
        try {
            randomValidateCode.getRandcode(request, response);//输出图片方法
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //检查是否存在该账号
    @RequestMapping("/findUid")
    public Result findUid(String uid) {
        UserInfo userInfo = loginService.findUid(uid);
        if (userInfo != null) {
            return new Result(true, "√");
        } else {
            return new Result(false, "未发现该账号，请检查");
        }
    }

    //校验验证码
    @RequestMapping("/check")
    public Result checkCode(String checkCode, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String randomcode_key = (String) session.getAttribute("randomcode_key");
        if (randomcode_key.equalsIgnoreCase(checkCode)) {
            return new Result(true, "验证通过！");
        } else {
            return new Result(false, "请输入正确的验证码！");
        }
    }

    //获取用户名称
    @RequestMapping("/getUserName")
    public Map<String, String> getUserName() {
        String uid = SecurityContextHolder.getContext().getAuthentication().getName();
        UserInfo user = loginService.findUid(uid);
        Map<String, String> map = new HashMap<>();
        map.put("loginName", user.getUname());
        return map;
    }

    //获取用户角色
    @RequestMapping("/getRoleName")
    public List<String> getRoleNaem() {
        List<String> roleNames = new ArrayList<String>();
        String uid = SecurityContextHolder.getContext().getAuthentication().getName();
        UserInfo user = loginService.findUid(uid);
        List<Role> roles = roleMapper.selectByuserid(user.getId());
        for (Role role : roles) {
            roleNames.add(role.getRoleName());
        }
        return roleNames;
    }
}
