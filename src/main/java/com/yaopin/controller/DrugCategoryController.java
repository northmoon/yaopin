package com.yaopin.controller;

import com.sun.deploy.net.HttpResponse;
import com.yaopin.entity.DrugCategory;
import com.yaopin.entity.DrugInformation;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.service.IDrugCategoryService;
import org.apache.commons.io.FileUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/drugCategory")
@Secured({"ROLE_HOSPITAL","ROLE_ORGANIZATION","ROLE_WEISHENGYUAN","ROLE_SUPPLIER"})
public class DrugCategoryController {
    @Autowired
    private IDrugCategoryService drugCategoryService;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public Map<String, DrugCategory> findAll() {
        return drugCategoryService.findAll();
    }

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {
        return drugCategoryService.findPage(page, rows);
    }

    /**
     * 增加
     *
     * @param drugCategory
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody DrugCategory drugCategory) {
        try {
            drugCategoryService.add(drugCategory);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }

    /**
     * 导入
     *
     * @param file
     * @return
     */
    @RequestMapping("/upload")
    public Result add(@RequestParam("file") MultipartFile file) {
        try {
            Map<String, String> errorMessage = drugCategoryService.saveMore(file);
            return new Result(true, "共有" + errorMessage.get("successSum") + "条导入成功," + "导入失败的行数如下，请检查：" + errorMessage.get("errorList"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "导入失败");
        }
    }

    /**
     * 修改
     *
     * @param drugCategory
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody DrugCategory drugCategory) {
        try {
            drugCategoryService.update(drugCategory);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }

    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public DrugCategory findOne(Integer id) {
        return drugCategoryService.findOne(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(Integer[] ids) {
        try {
            drugCategoryService.delete(ids);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }

    /**
     * 批量删除
     *
     * @param id
     * @return
     */
    @RequestMapping("/deleteOne")
    public Result delete(Integer id) {
        try {
            drugCategoryService.deleteOne(id);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }

    /**
     * 查询+分页
     *
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody DrugCategory drugCategory, int page, int rows) {
        return drugCategoryService.findPage(drugCategory, page, rows);
    }

    /*
     * 下载模板
     * */
    @RequestMapping("/download")
    public ResponseEntity<byte[]> download(String fileName, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String path = request.getSession().getServletContext().getRealPath("template") + "\\" + fileName;
        HttpHeaders headers = new HttpHeaders();
        File file = new File(path);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", fileName);
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
    }

    /*
     * 导出
     * */
    @RequestMapping("/exportExcel")
    public String exportExcel(HttpServletRequest request,HttpServletResponse response) {
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            String fileName = new String(("药品品目导出").getBytes(), "ISO8859_1");
            Date date = new Date();
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            String newTime = format.format(date);
            response.setHeader("Content-disposition", "attachment; filename=" + fileName + "_" + newTime + ".xlsx");// 组装附件名称和格式
            HttpSession session = request.getSession();
            DrugCategory drugCategory=(DrugCategory)session.getAttribute("key");
            drugCategoryService.exportExcel(outputStream,drugCategory);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }


    @RequestMapping("/saveSearchEntity")
    public PageResult savesearchEntity(@RequestBody DrugCategory drugCategory,HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("key",drugCategory);
        return null;
    }
    /*
     * 按查询条件导出
     * */
    @RequestMapping("/exportsearch")
    public String exportsearch(HttpServletRequest request,HttpServletResponse response) {
        HttpSession session = request.getSession();
        DrugCategory drugCategory=(DrugCategory)session.getAttribute("key");
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            String fileName = new String(("药品品目导出").getBytes(), "ISO8859_1");
            Date date = new Date();
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            String newTime = format.format(date);
            response.setHeader("Content-disposition", "attachment; filename=" + fileName + "_" + newTime + ".xlsx");// 组装附件名称和格式
            drugCategoryService.exportsearch(outputStream,drugCategory);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }
}
