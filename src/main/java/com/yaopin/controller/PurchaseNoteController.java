package com.yaopin.controller;

import com.yaopin.entity.*;
import com.yaopin.mapper.HospitalMapper;
import com.yaopin.mapper.HospitalPurchasingMapper;
import com.yaopin.mapper.PurchaseNoteMapper;
import com.yaopin.service.PurchaseNoteService;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName PurchaseNoteController
 * @Author donghongyu
 * @Date 2019/9/9 14:33
 **/
@RestController
@RequestMapping("/PurchaseNote")
public class PurchaseNoteController {
    @Resource
    private PurchaseNoteService purchaseNoteService;
    @Resource
    private HospitalMapper hospitalMapper;
    @Resource
    private PurchaseNoteMapper purchaseNoteMapper;
    /**
     * 采购单查询加分页
     *
     * @return
     */
    @RequestMapping("/search")
    public PageResult findPage(@RequestBody PurchaseNote purchaseNote, int page, int rows, HttpSession session) {
        return purchaseNoteService.findPage(purchaseNote, page, rows);
    }
    /**
     * 批量删除
     *
     * @param id
     * @return
     */
    @RequestMapping("/deleteOne")
    public Result delete(Integer id) {
        try {
            purchaseNoteService.deleteOne(id);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "只有采购单状态为未提交才可以进行删除");
        }
    }
    /**
     * 修改
     *
     * @param purchaseNote
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody PurchaseNote purchaseNote) {
        try {
            purchaseNoteService.update(purchaseNote);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }
    /**
     * * @Description 查询实体
     *
     * @Param id
     */
    @RequestMapping("/findOne")
    public PurchaseNote findOne(Integer id) {
        return purchaseNoteService.findOne(id);
    }
    /**
     * * @Description 添加采购单
     *
     * @Param
     */
    @RequestMapping("/add")
    public Result add(@RequestBody PurchaseNote purchaseNote, HttpServletRequest request) {
        try {
            request.setCharacterEncoding("UTF-8");
            purchaseNoteService.add(purchaseNote);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, "请重新增加");
        }
    }
    /**
     * * @Description  添加时获取医院编号和医院名称
     *
     * @Param
     */
    @RequestMapping("/save")
    public PurchaseNote save() {
        Integer hospitalId = 1;
        Hospital hospital = hospitalMapper.selectByPrimaryKey(hospitalId);
        PurchaseNote purchaseNote = new PurchaseNote();
        purchaseNote.setHospitalId(hospital.getId());
        purchaseNote.setHospital(hospital);
        purchaseNote.setPurchaseOrderName(hospital.getHospitalName() + new Date() + "采购单");
        purchaseNote.setBuildSingle(new Date());
        return purchaseNote;
    }
    /**
    ** @Description  将采购单信息保存在session中
     * @Param
     */
    @RequestMapping("/cache")
    public void returnSession(int id, HttpServletRequest request){
       HttpSession session= request.getSession();
       session.setAttribute("hospitalId",id);
        System.out.println( session.getAttribute("hospitalId"));
    }
}
