package com.yaopin.controller;

import com.yaopin.entity.DrugClassification;
import com.yaopin.service.IDrugClassificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/drugClassification")
@PreAuthorize("hasAnyRole('ORGANIZATION','HOSPITAL','WEISHENGYUAN','SUPPLIER')")
public class DrugClassificationController {
    @Autowired
    private IDrugClassificationService drugClassificationService;

    @RequestMapping("/findAll")
    public List<DrugClassification> findAll() {
        List<DrugClassification> list = drugClassificationService.findAll();
        return list;
    }

}
