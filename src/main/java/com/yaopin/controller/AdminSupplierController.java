package com.yaopin.controller;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.Supplier;
import com.yaopin.service.ILoginService;
import com.yaopin.service.ISupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/adminsupplier")
@PreAuthorize("hasAnyRole('ADMIN')")
public class AdminSupplierController {
    @Autowired
    private ISupplierService supplierService;

    @Autowired
    private ILoginService loginService;


    @RequestMapping("/deleteone")
    public void deleteone(int id) {
        supplierService.deleteOne(id);
    }

    @RequestMapping("/findone")
    public Supplier findOne(int id) {
        return supplierService.findone(id);
    }

    @RequestMapping("/search")
    public PageResult search(@RequestBody Supplier supplier, int page, int rows) {
        return supplierService.findPage(supplier, page, rows);
    }

    @RequestMapping("/add")
    public void add(@RequestBody Supplier supplier) {
        supplierService.add(supplier);
    }

    @RequestMapping("/findAll")
    public Map<String, Supplier> findAll() {
        Map<String, Supplier> map = supplierService.findAll();
        return map;
    }

    @RequestMapping("/update")
    public void update(@RequestBody Supplier supplier) {
        supplierService.updete(supplier);
    }

}
