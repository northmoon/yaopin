package com.yaopin.controller;

import com.yaopin.entity.*;
import com.yaopin.service.StatementsDetailService;
import com.yaopin.service.StatementsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
/**
 * @ClassName StatementsDetailController
 * @Author donghongyu
 * @Date 2019/9/23 9:16
 **/
@RestController
@RequestMapping("/StatementsDetail")
public class StatementsDetailController {
    @Resource
    private StatementsDetailService statementsDetailService;
    @Resource
    private StatementsService statementsService;
    /**
    ** @Description  获取session中的值
     * @Param
     */
    @RequestMapping("/getSession")
    public Statements getSession(HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer statementsId =(Integer) session.getAttribute("statementsId");
        Statements session1 = statementsDetailService.getSession(statementsId);
        return session1;
    }
    @RequestMapping("/search")
    public PageResult findPage(@RequestBody StatementsDetail statementsDetail,int page,int rows,HttpServletRequest request){
     return statementsDetailService.findPage(statementsDetail,page,rows,request);
    }
    @RequestMapping("/delete")
    public Result delete(@RequestParam List<Integer> ids){
        try {
            statementsDetailService.delete(ids);
            return new Result(true,"删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,e.getMessage());
        }
    }
    @RequestMapping("/save")
    public Result save(HttpServletRequest request){
        try {
            statementsService.save(request);
            return new Result(true,"修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,e.getMessage());
        }
    }
}
