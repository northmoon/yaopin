package com.yaopin.controller;

import com.yaopin.entity.*;
import com.yaopin.mapper.HospitalMapper;
import com.yaopin.mapper.StatementsMapper;
import com.yaopin.service.StatementsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
/**
 * @ClassName StatementsController
 * @Author donghongyu
 * @Date 2019/9/22 19:55
 **/
@RestController
@RequestMapping("/Statements")
public class StatementsController {
    @Resource
    private HospitalMapper hospitalMapper;
    @Resource
    private StatementsService statementsService;

    /**
     ** @Description  初始化结算单信息
     * @Param
     */
    @RequestMapping("/getInfo")
    public Statements save(){
        Integer hospitalId = 1;
        Hospital hospital = hospitalMapper.selectByPrimaryKey(hospitalId);
        Statements statements = new Statements();
        statements.setHospitalId(hospitalId);
        statements.setHospital(hospital);
        statements.setStatementsName(hospital.getHospitalName()+new Date()+"结算单");
        statements.setCreateTime(new Date());
        statements.setSubmitTime(new Date());
        statements.setCreatePeople("李四");
        statements.setState(1);
        return statements;
    }
    /**
    ** @Description  添加结算单信息
     * @Param
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Statements statements){
        try {
            statementsService.add(statements);
            return  new Result(true,"添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false,message);
        }
    }
    /**
    ** @Description  查询所有结算单
     * @Param
     */
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody Statements statements,int page,int rows){
        return statementsService.findPage(statements,page,rows);
    }



    @RequestMapping("/cache")
    public void setSession(Integer id,HttpServletRequest request){
        HttpSession session = request.getSession();
        session.setAttribute("statementsId",id);
        System.out.println(id);
    }
}
