package com.yaopin.controller;

import com.yaopin.entity.UserInfo;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.service.IUserService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Secured({"ROLE_ADMIN"})
public class UserController {
    @Autowired
    private IUserService userService;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public Map<String, UserInfo> findAll() {
        return userService.findAll();
    }

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {
        return userService.findPage(page, rows);
    }

    /**
     * 增加
     *
     * @param userInfo
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody UserInfo userInfo, int[] ids) {
        try {
            userService.add(userInfo,ids);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        }
    }


    /**
     * 修改
     *
     * @param userInfo
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody UserInfo userInfo, int[] ids) {
        try {
            userService.update(userInfo,ids);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        }
    }

    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public UserInfo findOne(Integer id) {
        return userService.findOne(id);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping("/deleteOne")
    public Result delete(Integer id) {
        try {
            userService.deleteOne(id);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }

    /**
     * 查询+分页
     *
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody UserInfo userInfo, int page, int rows) {
        return userService.findPage(userInfo, page, rows);
    }


    /*
     * 查询单位信息
     * */
    @RequestMapping("/selectUnit")
    public Map<String, String> selectUnit(Integer ucategory) {
        return userService.selectUnit(ucategory);
    }
}
