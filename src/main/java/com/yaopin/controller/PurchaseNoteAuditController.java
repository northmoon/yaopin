package com.yaopin.controller;

import com.yaopin.entity.PurchaseNote;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.service.IPurchaseNoteAuditService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/purchaseNote")
@PreAuthorize("hasAnyRole('ORGANIZATION','WEISHENGYUAN')")
public class PurchaseNoteAuditController {
    @Autowired
    private IPurchaseNoteAuditService purchaseNoteAuditService;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public Map<String, PurchaseNote> findAll() {
        return purchaseNoteAuditService.findAll();
    }

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {
        return purchaseNoteAuditService.findPage(page, rows);
    }


    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public PurchaseNote findOne(Integer id) {
        return purchaseNoteAuditService.findOne(id);
    }


    /**
     * 批量修改
     *
     * @param map
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody Map<String, List<String>> map) {
        try {
            String message = purchaseNoteAuditService.update(map);
            return new Result(true, message);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }

    /**
     * 查询+分页
     *
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody PurchaseNote purchaseNote, int page, int rows) {
        return purchaseNoteAuditService.findPage(purchaseNote, page, rows);
    }


}
