package com.yaopin.controller;

import com.yaopin.entity.*;
import com.yaopin.service.IAreaInfoService;
import com.yaopin.service.ILoginService;
import com.yaopin.service.ISupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/supplier")
@PreAuthorize("hasAnyRole('ORGANIZATION','HOSPITAL','WEISHENGYUAN','SUPPLIER')")
public class SupplierController {

    @Autowired
    private ISupplierService supplierService;


    @RequestMapping("/search")
    public PageResult search(@RequestBody Supplier supplier, int page, int rows) {
        return supplierService.findPage(supplier, page, rows);
    }

    @RequestMapping("/savesupplier")
    public void  savesupplier(@RequestBody List<Integer> list) {
        supplierService.saveWithList(list);
        return;
    }


    @RequestMapping("/removesupplier")
    public void  removesupplier(@RequestBody List<Integer> list) {
        supplierService.removeWithList(list);
        return;
    }

    @RequestMapping("/findAll")
    public Map<String, Supplier> findAll() {
        Map<String, Supplier> map = supplierService.findAll();
        return map;
    }

}
