package com.yaopin.controller;
import com.yaopin.entity.DetailedWarehousing;
import com.yaopin.entity.PurchaseOrderDrugDetails;
import com.yaopin.entity.Result;
import com.yaopin.service.PurchaseOrderWarehousingService;
import com.yaopin.service.StatementsDetailService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @ClassName PurchaseOrderWarehousingController
 * @Author donghongyu
 * @Date 2019/9/18 16:56
 **/
@RestController
@RequestMapping("/PurchaseOrderWarehousing")
public class PurchaseOrderWarehousingController {
    @Resource
    private PurchaseOrderWarehousingService purchaseOrderWarehousingService;
    @Resource
    private StatementsDetailService statementsDetailService;
    @RequestMapping("/findAll")
    public List<DetailedWarehousing> findAll(@RequestBody PurchaseOrderDrugDetails purchaseOrderDrugDetails) {
        return purchaseOrderWarehousingService.findAll(purchaseOrderDrugDetails);

    }
    @RequestMapping("/add")
    public Result addDw(@RequestBody Map<String,List<String>>map){
        try {
            List<String> ids = map.get("ids");
            List<String> receipt = map.get("receipt");
            List<String> invoiceNum = map.get("invoiceNum");
            List<String>batchNum = map.get("batchNum");
            List<String> drugValidity = map.get("drug_validity");
            purchaseOrderWarehousingService.add(ids,receipt,invoiceNum,batchNum,drugValidity);
           return new Result(true, "以成功入库");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }
    /**
    ** @Description  将入库信息添加到结算单
     * @Param
     */
    @RequestMapping("/Settlement")
    public Result addSt(@RequestParam List<Integer> ids, HttpServletRequest request){
        try {
            statementsDetailService.add(ids,request);
            return new Result(true,"添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,e.getMessage());
        }
    }
}
