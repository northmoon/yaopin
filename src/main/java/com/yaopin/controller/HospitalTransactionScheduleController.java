package com.yaopin.controller;

import com.yaopin.entity.DrugInfo;
import com.yaopin.entity.HospitalTransactionSchedule;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.service.IHospitalTransactionScheduleService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/hospitalTransactionSchedule")
@PreAuthorize("hasAnyRole('ORGANIZATION','HOSPITAL','WEISHENGYUAN','SUPPLIER')")
public class HospitalTransactionScheduleController {

    @Autowired
    private IHospitalTransactionScheduleService hospitalTransactionScheduleService;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public List<HospitalTransactionSchedule> findAll() {
        return hospitalTransactionScheduleService.findAll();
    }


    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public HospitalTransactionSchedule findOne(Integer id) {
        return hospitalTransactionScheduleService.findOne(id);
    }

    /**
     * 查询+分页
     *
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody HospitalTransactionSchedule hospitalTransactionSchedule, int page, int rows) {
        PageResult result = hospitalTransactionScheduleService.findPage(hospitalTransactionSchedule, page, rows);
        return result;
    }

    @RequestMapping("/saveSearchEntity")
    public void saveSearchEntity(@RequestBody HospitalTransactionSchedule hospitalTransactionSchedule, HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("hospitalTransactionSchedule", hospitalTransactionSchedule);
    }

    /*
     * 导出
     * */
    @RequestMapping("/exportExcel")
    public String exportExcel(HttpServletResponse response, HttpServletRequest request) {
        HttpSession session = request.getSession();
        HospitalTransactionSchedule hospitalTransactionSchedule = (HospitalTransactionSchedule) session.getAttribute("hospitalTransactionSchedule");
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            String fileName = new String(("药品交易信息导出").getBytes(), "ISO8859_1");
            Date date = new Date();
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            String newTime = format.format(date);
            response.setHeader("Content-disposition", "attachment; filename=" + fileName + "_" + newTime + ".xlsx");// 组装附件名称和格式
            hospitalTransactionScheduleService.exportExcel(outputStream,hospitalTransactionSchedule);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }
}
