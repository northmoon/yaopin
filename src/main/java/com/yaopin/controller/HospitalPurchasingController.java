package com.yaopin.controller;

import com.yaopin.entity.HospitalPurchasing;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.service.HospitalPurchasingService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @ClassName HospitalPurchasingController
 * @Author donghongyu
 * @Date 2019/9/9 9:19
 **/
@RestController
@RequestMapping("/HospitalPurchasing")
public class HospitalPurchasingController {
    @Resource
    private HospitalPurchasingService hospitalPurchasingService;
    /**
     * * @Description  条件查询供货表
     *
     * @Param
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody HospitalPurchasing hospitalPurchasing, int page, int rows) {
        PageResult pageResult = hospitalPurchasingService.findPage(hospitalPurchasing, page, rows);
        return pageResult;
    }
    /**
     * * @Description  删除采购单中的供货单
     *
     * @Param idList
     */
    @RequestMapping("/remove")
    public Result removeIdList(int[] idList) {
        try {
            hospitalPurchasingService.removeIdList(idList);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }
    @RequestMapping("/exportExcel")
    public String exportExcel(HttpServletResponse response) {
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            String fileName = new String(("药品信息导出").getBytes(), "ISO8859_1");
            Date date = new Date();
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            String newTime = format.format(date);
            response.setHeader("Content-disposition", "attachment; filename=" + fileName + "_" + newTime + ".xlsx");// 组装附件名称和格式
            hospitalPurchasingService.exportExcel(outputStream);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }
    /**
     ** @Description  批量添加
     * @Param
     */
    @RequestMapping("/addPn")
    public Result add(@RequestParam List<Integer> idList,HttpServletRequest request){
        try {
            hospitalPurchasingService.addPn(idList,request);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }
}

