package com.yaopin.controller;

import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.HospitalMapper;
import com.yaopin.service.CreditOrderService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;


/**
 * @ClassName CreditOrderController
 * @Author donghongyu
 * @Date 2019/9/19 21:13
 **/
@RestController
@RequestMapping("/CreditOrder")
public class CreditOrderController {
   // creditOrderController
    @Resource
    private HospitalMapper hospitalMapper;
    @Resource
    private CreditOrderService creditOrderService;
    /**
    ** @Description  添加页面初始化
     * @Param
     */
    @RequestMapping("/save")
    public CreditOrder save() {
        Integer hospitalId = 1;
        Hospital hospital = hospitalMapper.selectByPrimaryKey(hospitalId);
        CreditOrder creditOrder = new CreditOrder();
        creditOrder.setHospitalId(hospital.getId());
        creditOrder.setHospital(hospital);
        creditOrder.setCreditOrderName(hospital.getHospitalName() + new Date() + "退货单");
        creditOrder.setSubmissionTime(new Date());
        creditOrder.setCreatePeople("张三");
        creditOrder.setCreateTime(new Date());
        creditOrder.setState(1);
        System.out.println(creditOrder.getHospital().getId());
        return creditOrder;
    }
    /**
    ** @Description  添加退货单
     * @Param
     */
    @RequestMapping("/addCreditOrder")
    public Result addCreditOrder(@RequestBody CreditOrder creditOrder){
        try {
            creditOrderService.add(creditOrder);
            return new Result(true, "添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }
    /**
    ** @Description  查询退货单
     * @Param
     */
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody CreditOrder creditOrder,int page,int rows){
        return creditOrderService.findPage(creditOrder, page, rows);
    }
    /**
    ** @Description  将退货单信息保存在session中
     * @Param
     */
    @RequestMapping("/cache")
    public void returnSession(int id, HttpServletRequest request){
        HttpSession session= request.getSession();
        session.setAttribute("creditOrderId",id);
    }
}
