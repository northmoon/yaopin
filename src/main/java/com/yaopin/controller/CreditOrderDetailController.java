package com.yaopin.controller;

import com.yaopin.entity.*;
import com.yaopin.service.CreditOrderDetailService;
import com.yaopin.service.CreditOrderService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


/**
 * @ClassName CreditOrderDetailController
 * @Author donghongyu
 * @Date 2019/9/20 14:15
 **/
@RestController
@RequestMapping("/CreditOrderDetail")
public class CreditOrderDetailController {
    @Resource
    private CreditOrderDetailService creditOrderDetailService;
    @Resource
    private CreditOrderService creditOrderService;

    @RequestMapping("/getInfo")
    public CreditOrder getSession(HttpServletRequest request) {
        HttpSession creditOrderId = request.getSession();
        Integer creditOrderId1 = (Integer) creditOrderId.getAttribute("creditOrderId");
        CreditOrder session = creditOrderDetailService.findSession(creditOrderId1);
        return session;
    }
    @RequestMapping("/add")
    public Result add(Integer[] ids, HttpServletRequest request) {
        try {
            creditOrderDetailService.add(ids, request);
            return new Result(true, "添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }

    @RequestMapping("/search")
    public PageResult findPage(@RequestBody CreditOrderDetail creditOrderDetail, int page, int rows,HttpServletRequest request) {
        return creditOrderDetailService.findPage(creditOrderDetail, page, rows,request);
    }

    /**
     * * @Description  批量删除
     *
     * @Param
     */
    @RequestMapping("/delete")
    public Result deleteSome(Integer[] ids) {
        try {
            creditOrderDetailService.deleteSome(ids);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }
    /**
    ** @Description  保存退货量
     * @Param  
     */
    @RequestMapping("/save")
    public Result save(@RequestBody  Map<String,List<String>> map){
        try {
            List<String> ids = map.get("ids");
            List<String> returnNum = map.get("returnNum");
            List<String> returnSum = map.get("returnSum");
            List<String> returnCause = map.get("returnCause");
            creditOrderDetailService.update(ids,returnNum,returnSum);
            return new Result(true,"以成功保存退货量");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false,message);
        }
    }
    @RequestMapping("/preservation")
    public Result Preservation(HttpServletRequest request){
        try {
             creditOrderService.preservation(request);
            return new Result(true,"修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,e.getMessage());
        }
    }
}
