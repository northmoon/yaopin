package com.yaopin.controller;

import com.yaopin.entity.*;
import com.yaopin.service.IPurchasingorderService;
import com.yaopin.service.ISupplierService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/Purchasingorder")
public class PurchasingorderController {
    @Autowired
    private IPurchasingorderService iPurchasingorderService;


    @RequestMapping("/search")
    public PageResult search(@RequestBody SearchPurchasing searchPurchasing, int page, int rows) {
        PageResult list = iPurchasingorderService.findPurchasing(searchPurchasing,page,rows);
        return list;
    }


    @RequestMapping("/confirmSupplier")
    public Integer confirmSupplier(@RequestBody List<Integer> list) {
        Integer i=iPurchasingorderService.confirmSupplier(list);
        return i;
    }

    @RequestMapping("/unableSupplier")
    public Integer unableSupplier(@RequestBody List<Integer> list) {
        Integer i=iPurchasingorderService.unableSupplier(list);
        return i;
    }


    //保存查询条件
    @RequestMapping("/savesearchEntity")
    public PageResult savesearchEntity(@RequestBody SearchPurchasing searchPurchasing,HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("key2",searchPurchasing);
        return null;
    }


    /*
     * 按查询条件导出
     * */
    @RequestMapping("/exportsearch")
    public String exportsearch(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        SearchPurchasing searchPurchasing=(SearchPurchasing)session.getAttribute("key2");
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            String fileName = new String(("采购单处理导出").getBytes(), "ISO8859_1");
            Date date = new Date();
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            String newTime = format.format(date);
            response.setHeader("Content-disposition", "attachment; filename=" + fileName + "_" + newTime + ".xlsx");// 组装附件名称和格式
            iPurchasingorderService.exportsearch(outputStream,searchPurchasing);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    /*
     * 下载模板
     * */
    @RequestMapping("/download")
    public ResponseEntity<byte[]> download(String fileName, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String path = request.getSession().getServletContext().getRealPath("template") + "\\" + fileName;
        HttpHeaders headers = new HttpHeaders();
        File file = new File(path);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", fileName);
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
    }


    /**
     * 导入
     *
     * @param file
     * @return
     */
    @RequestMapping("/upload")
    public Result add(@RequestParam("file") MultipartFile file) {
        try {
            Map<String, String> errorMessage = iPurchasingorderService.saveMore(file);
            return new Result(true, "共有" + errorMessage.get("successSum") + "条导入成功," + "导入失败的行数如下，请检查：" + errorMessage.get("errorList"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "导入失败");
        }
    }


}
