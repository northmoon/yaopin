package com.yaopin.controller;
import com.yaopin.entity.PurchaseNote;
import com.yaopin.entity.PurchaseOrderDrugDetails;
import com.yaopin.entity.Result;
import com.yaopin.service.PurchaseNoteService;
import com.yaopin.service.PurchaseOrderDrugDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
/**
 * @ClassName 药品明细
 * @Author donghongyu
 * @Date 2019/9/14 17:20
 **/
@RestController
@RequestMapping("/PurchaseOrderDrugDetails")
public class PurchaseOrderDrugDetailsController {
    @Resource
    private PurchaseOrderDrugDetailsService purchaseOrderDrugDetailsService;
    @Resource
    private PurchaseNoteService purchaseNoteService;
     /**
     ** @Description  初始化采购单信息
      * @Param
      */
     @RequestMapping("/getInfo")
    public PurchaseNote getSession(HttpServletRequest request){
         HttpSession session=request.getSession();
         Integer hospitalId = (Integer)session.getAttribute("hospitalId");
         return   purchaseNoteService.findSession(hospitalId);
     }
     /**
     ** @Description  根据采购单id查询药品明细
      * @Param
      */
     @RequestMapping("/findList")
     public List<PurchaseOrderDrugDetails> findList(HttpServletRequest request){
         HttpSession session=request.getSession();
         Integer hospitalId = (Integer)session.getAttribute("hospitalId");
         return  purchaseOrderDrugDetailsService.findList(hospitalId);
     }
     /**
     ** @Description  根据明细id删除
      * @Param
      */
     @RequestMapping("/delete")
     public Result delete(Integer[] ids) {
         try {
             purchaseOrderDrugDetailsService.delete(ids);
             return new Result(true, "删除成功");
         } catch (Exception e) {
             e.printStackTrace();
             return new Result(false, "删除失败");
         }
     }
     @RequestMapping("/baoCun")
     public Result baoCun(@RequestBody Map<String,List<Integer>> map){
         try {
             List<Integer> ids = map.get("ids");
             List<Integer> caigouliang=map.get("caigouliang");
             purchaseOrderDrugDetailsService.baoCun(ids,caigouliang);
             return new Result(true, "保存成功");
         } catch (Exception e) {
             e.printStackTrace();
             return new Result(false, "请重新保存");
         }
     }
}
