package com.yaopin.controller;

import com.yaopin.entity.AreaInfo;
import com.yaopin.entity.PageResult;
import com.yaopin.service.IAreaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/areaInfo")
public class AreaInfoController {
    @Autowired
    private IAreaInfoService areaInfoService;

    @RequestMapping("/findAll")
    public List<AreaInfo> findAll() {
        return areaInfoService.findAll();
    }


    @RequestMapping("/search")
    public PageResult search(@RequestBody AreaInfo areaInfo, int page, int rows) {
        return areaInfoService.findPage(areaInfo,page,rows);
    }

    @RequestMapping("/add")
    public void add(@RequestBody AreaInfo areaInfo) {
        areaInfoService.add(areaInfo);
    }

    @RequestMapping("/findOne")
    public AreaInfo findOne(int id) {
        return areaInfoService.findone(id);
    }

    @RequestMapping("/update")
    public void update(@RequestBody AreaInfo areaInfo) {
        areaInfoService.update(areaInfo);
    }

    @RequestMapping("/deleteOne")
    public void deleteOne(int id) {
        areaInfoService.deleteOne(id);
    }

}
