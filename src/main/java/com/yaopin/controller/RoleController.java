package com.yaopin.controller;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.entity.Role;
import com.yaopin.service.IRoleService;
import com.yaopin.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/role")
@Secured({"ROLE_ADMIN"})
public class RoleController {
    @Autowired
    private IRoleService roleService;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public List<Role> findAll() {
        return roleService.findAll();
    }


    /**
     * 增加
     *
     * @param role
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Role role,int[] ids) {
        try {
            roleService.add(role,ids);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
    }

    /**
     * 修改
     *
     * @param role
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody Role role,int[] ids) {
        try {
            roleService.update(role,ids);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }

    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public Role findOne(Integer id) {
        return roleService.findOne(id);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping("/deleteOne")
    public Result delete(Integer id) {
        try {
            roleService.deleteOne(id);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }
}
