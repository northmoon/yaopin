package com.yaopin.controller;


import com.yaopin.entity.OrganizationInfo;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.Result;
import com.yaopin.service.OrganizationInfoService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/organizationInfo")
public class OrganizationController {

    @Resource
    private OrganizationInfoService organizationInfoService;

    /**
     * 查询+分页
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody OrganizationInfo organizationInfo, int page, int rows) {
        return organizationInfoService.findPage(organizationInfo, page, rows);
    }


    /**
     * 新增
     * @param organizationInfo
     * @return
     * @throws Exception
     */
    @RequestMapping("/insert")
    public Result insert(@RequestBody OrganizationInfo organizationInfo) throws Exception {
        try {
            organizationInfoService.addhospital(organizationInfo);
            return new Result(true, "成功");
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new Result(false, message);
        }
//        return null;
    }

    /**
     * 删除单个
     * @param oid
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(Integer oid){
        try {
            organizationInfoService.deleteOrInfo(oid);
            return new Result(true, "");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "");
        }
    }



    /**
     * 根据id查询修改数据
     * @param oid
     * @return
     */
    @RequestMapping("/findOne")
    public OrganizationInfo finOne(Integer oid) {
        return organizationInfoService.findOne(oid);
    }

    /**
     * 修改
     * @param organizationInfo
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody OrganizationInfo organizationInfo){
        try{
            organizationInfoService.update(organizationInfo);
            return new Result(true,"成功");
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,"失败");
        }
    }



}
