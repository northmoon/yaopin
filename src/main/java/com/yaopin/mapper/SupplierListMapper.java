package com.yaopin.mapper;

import com.yaopin.entity.SupplierList;
import com.yaopin.entity.SupplierListExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierListMapper {
    int countByExample(SupplierListExample example);

    int deleteByExample(SupplierListExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SupplierList record);

    int insertSelective(SupplierList record);

    List<SupplierList> selectByExample(SupplierListExample example);

    SupplierList selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SupplierList record, @Param("example") SupplierListExample example);

    int updateByExample(@Param("record") SupplierList record, @Param("example") SupplierListExample example);

    int updateByPrimaryKeySelective(SupplierList record);

    int updateByPrimaryKey(SupplierList record);
}