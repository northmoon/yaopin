package com.yaopin.mapper;

import com.yaopin.entity.Role;
import com.yaopin.entity.RoleExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RoleMapper {
    int countByExample(RoleExample example);

    int deleteByExample(RoleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    List<Role> selectByExample(RoleExample example);

    Role selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Role record, @Param("example") RoleExample example);

    int updateByExample(@Param("record") Role record, @Param("example") RoleExample example);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    @Select("select r.id id,r.role_name roleName,role_desc roleDesc from role r,user_role ur where r.id=ur.role_id and ur.user_id = #{userid}")
    List<Role> selectByuserid(Integer userid);

    @Select("select r.id id,r.role_name roleName,role_desc roleDesc from role r,role_permission rp where r.id=rp.role_id and rp.permission_id = #{id}")
    List<Role> selectByPermissionId(Integer id);

    @Select("select id id,role_name roleName, role_desc roleDesc from role where role_name = #{roleName}")
    Role selectByRoleName(String roleName);
}













