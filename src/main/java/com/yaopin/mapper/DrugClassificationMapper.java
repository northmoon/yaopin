package com.yaopin.mapper;

import com.yaopin.entity.DrugClassification;
import com.yaopin.entity.DrugClassificationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DrugClassificationMapper {
    int countByExample(DrugClassificationExample example);

    int deleteByExample(DrugClassificationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DrugClassification record);

    int insertSelective(DrugClassification record);

    List<DrugClassification> selectByExample(DrugClassificationExample example);

    DrugClassification selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DrugClassification record, @Param("example") DrugClassificationExample example);

    int updateByExample(@Param("record") DrugClassification record, @Param("example") DrugClassificationExample example);

    int updateByPrimaryKeySelective(DrugClassification record);

    int updateByPrimaryKey(DrugClassification record);
}