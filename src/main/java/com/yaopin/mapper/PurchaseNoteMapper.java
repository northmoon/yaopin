package com.yaopin.mapper;

import com.yaopin.entity.PurchaseNote;
import com.yaopin.entity.PurchaseNoteExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PurchaseNoteMapper {
    int countByExample(PurchaseNoteExample example);

    int deleteByExample(PurchaseNoteExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PurchaseNote record);

    int insertSelective(PurchaseNote record);

    List<PurchaseNote> selectByExample(PurchaseNoteExample example);

    PurchaseNote selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PurchaseNote record, @Param("example") PurchaseNoteExample example);

    int updateByExample(@Param("record") PurchaseNote record, @Param("example") PurchaseNoteExample example);

    int updateByPrimaryKeySelective(PurchaseNote record);

    int updateByPrimaryKey(PurchaseNote record);
}