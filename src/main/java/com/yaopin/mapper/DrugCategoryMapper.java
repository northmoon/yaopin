package com.yaopin.mapper;

import com.yaopin.entity.DrugCategory;
import com.yaopin.entity.DrugCategoryExample;
import com.yaopin.entity.DrugClassification;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface DrugCategoryMapper {

    @Select("select MAX(category_num) from drug_category")
    String slectMaxNum();

    @Select("SELECT * from drug_category WHERE generic_name =#{genericName} AND dosage_form=#{dosageForm} AND specification = #{specification} AND unit = #{unit} AND coefficient =#{coefficient}")
    @Results({
            @Result(column = "generic_name", property = "genericName"),
            @Result(column = "dosage_form", property = "dosageForm")
    })
    DrugCategory selectByDC(DrugCategory drugCategory);

    int countByExample(DrugCategoryExample example);

    int deleteByExample(DrugCategoryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DrugCategory record);

    int insertSelective(DrugCategory record);

    List<DrugCategory> selectByExample(DrugCategoryExample example);

    DrugCategory selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DrugCategory record, @Param("example") DrugCategoryExample example);

    int updateByExample(@Param("record") DrugCategory record, @Param("example") DrugCategoryExample example);

    int updateByPrimaryKeySelective(DrugCategory record);

    int updateByPrimaryKey(DrugCategory record);
}