package com.yaopin.mapper;

import com.yaopin.entity.HospitalTransactionSchedule;
import com.yaopin.entity.HospitalTransactionScheduleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HospitalTransactionScheduleMapper {
    int countByExample(HospitalTransactionScheduleExample example);

    int deleteByExample(HospitalTransactionScheduleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(HospitalTransactionSchedule record);

    int insertSelective(HospitalTransactionSchedule record);

    List<HospitalTransactionSchedule> selectByExample(HospitalTransactionScheduleExample example);

    HospitalTransactionSchedule selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") HospitalTransactionSchedule record, @Param("example") HospitalTransactionScheduleExample example);

    int updateByExample(@Param("record") HospitalTransactionSchedule record, @Param("example") HospitalTransactionScheduleExample example);

    int updateByPrimaryKeySelective(HospitalTransactionSchedule record);

    int updateByPrimaryKey(HospitalTransactionSchedule record);
}