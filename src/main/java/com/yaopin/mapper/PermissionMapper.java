package com.yaopin.mapper;

import com.yaopin.entity.Permission;
import com.yaopin.entity.PermissionExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface PermissionMapper {
    int countByExample(PermissionExample example);

    int deleteByExample(PermissionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Permission record);

    int insertSelective(Permission record);

    List<Permission> selectByExample(PermissionExample example);

    Permission selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Permission record, @Param("example") PermissionExample example);

    int updateByExample(@Param("record") Permission record, @Param("example") PermissionExample example);

    int updateByPrimaryKeySelective(Permission record);

    int updateByPrimaryKey(Permission record);

    @Select("select p.id id,p.permission_name permissionName,p.url url from permission p,role_permission rp where p.id=rp.permission_id and rp.role_id=#{roleid}")
    List<Permission> selectByRoleid(Integer roleid);


    @Select("select * from permission where url=#{url}")
    Permission selectByUrl(String url);
}