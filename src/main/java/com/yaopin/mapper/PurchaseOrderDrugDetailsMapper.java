package com.yaopin.mapper;

import com.yaopin.entity.PurchaseOrderDrugDetails;
import com.yaopin.entity.PurchaseOrderDrugDetailsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PurchaseOrderDrugDetailsMapper {
    int countByExample(PurchaseOrderDrugDetailsExample example);

    int deleteByExample(PurchaseOrderDrugDetailsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PurchaseOrderDrugDetails record);

    int insertSelective(PurchaseOrderDrugDetails record);

    List<PurchaseOrderDrugDetails> selectByExample(PurchaseOrderDrugDetailsExample example);

    PurchaseOrderDrugDetails selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PurchaseOrderDrugDetails record, @Param("example") PurchaseOrderDrugDetailsExample example);

    int updateByExample(@Param("record") PurchaseOrderDrugDetails record, @Param("example") PurchaseOrderDrugDetailsExample example);

    int updateByPrimaryKeySelective(PurchaseOrderDrugDetails record);

    int updateByPrimaryKey(PurchaseOrderDrugDetails record);

    int updateStateById(PurchaseOrderDrugDetails record);
}