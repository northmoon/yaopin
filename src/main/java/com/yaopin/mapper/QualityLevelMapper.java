package com.yaopin.mapper;

import com.yaopin.entity.QualityLevel;
import com.yaopin.entity.QualityLevelExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface QualityLevelMapper {
    int countByExample(QualityLevelExample example);

    int deleteByExample(QualityLevelExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(QualityLevel record);

    int insertSelective(QualityLevel record);

    List<QualityLevel> selectByExample(QualityLevelExample example);

    QualityLevel selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") QualityLevel record, @Param("example") QualityLevelExample example);

    int updateByExample(@Param("record") QualityLevel record, @Param("example") QualityLevelExample example);

    int updateByPrimaryKeySelective(QualityLevel record);

    int updateByPrimaryKey(QualityLevel record);

    @Select("select * from quality_level where quality_level = #{qualityLevel}")
    QualityLevel selectByQuality(String qualityLevel);
}