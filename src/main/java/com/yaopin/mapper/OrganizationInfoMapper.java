package com.yaopin.mapper;

import com.yaopin.entity.OrganizationInfo;
import com.yaopin.entity.OrganizationInfoExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrganizationInfoMapper {
    int countByExample(OrganizationInfoExample example);

    int deleteByExample(OrganizationInfoExample example);

    int deleteByPrimaryKey(Integer oid);

    int insert(OrganizationInfo record);

    int insertSelective(OrganizationInfo record);

    List<OrganizationInfo> selectByExample(OrganizationInfoExample example);

    OrganizationInfo selectByPrimaryKey(Integer oid);

    int updateByExampleSelective(@Param("record") OrganizationInfo record, @Param("example") OrganizationInfoExample example);

    int updateByExample(@Param("record") OrganizationInfo record, @Param("example") OrganizationInfoExample example);

    int updateByPrimaryKeySelective(OrganizationInfo record);

    int updateByPrimaryKey(OrganizationInfo record);

//  @Select("select * from organization_info where oname=#{oname} and address =#{address}  and postcode=#{postcode}  and contactname=#{contactname}  and contactphone=#{contactphone} and fax=#{fax}  and email=#{email} and website=#{website}")
    @Select("SELECT * from organization_info WHERE oname =#{oname} AND address=#{address}")
    OrganizationInfo selectOrganizationInfo(OrganizationInfo organizationInfo);



}