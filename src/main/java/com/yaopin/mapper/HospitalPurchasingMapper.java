package com.yaopin.mapper;

import com.yaopin.entity.HospitalPurchasing;
import com.yaopin.entity.HospitalPurchasingExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HospitalPurchasingMapper {
    int countByExample(HospitalPurchasingExample example);

    int deleteByExample(HospitalPurchasingExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(HospitalPurchasing record);

    int insertSelective(HospitalPurchasing record);

    List<HospitalPurchasing> selectByExample(HospitalPurchasingExample example);

    HospitalPurchasing selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") HospitalPurchasing record, @Param("example") HospitalPurchasingExample example);

    int updateByExample(@Param("record") HospitalPurchasing record, @Param("example") HospitalPurchasingExample example);

    int updateByPrimaryKeySelective(HospitalPurchasing record);

    int updateByPrimaryKey(HospitalPurchasing record);
}