package com.yaopin.mapper;

import com.yaopin.entity.DrugDirectory;
import com.yaopin.entity.DrugDirectoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DrugDirectoryMapper {
    int countByExample(DrugDirectoryExample example);

    int deleteByExample(DrugDirectoryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DrugDirectory record);

    int insertSelective(DrugDirectory record);

    List<DrugDirectory> selectByExample(DrugDirectoryExample example);

    DrugDirectory selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DrugDirectory record, @Param("example") DrugDirectoryExample example);

    int updateByExample(@Param("record") DrugDirectory record, @Param("example") DrugDirectoryExample example);

    int updateByPrimaryKeySelective(DrugDirectory record);

    int updateByPrimaryKey(DrugDirectory record);
}