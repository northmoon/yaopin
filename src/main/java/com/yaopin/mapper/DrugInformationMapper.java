package com.yaopin.mapper;

import com.yaopin.entity.DrugInformation;
import com.yaopin.entity.DrugInformationExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DrugInformationMapper {
    int countByExample(DrugInformationExample example);

    int deleteByExample(DrugInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DrugInformation record);

    int insertSelective(DrugInformation record);

    List<DrugInformation> selectByExample(DrugInformationExample example);

    DrugInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DrugInformation record, @Param("example") DrugInformationExample example);

    int updateByExample(@Param("record") DrugInformation record, @Param("example") DrugInformationExample example);

    int updateByPrimaryKeySelective(DrugInformation record);

    int updateByPrimaryKey(DrugInformation record);

    @Select("SELECT * from drug_information WHERE drug_category_id =#{drugCategoryId} AND enterprise_name=#{enterpriseName} AND drug_name = #{drugName}")
    @Results({
            @Result(column = "drug_category_id", property = "drugCategoryId"),
            @Result(column = "enterprise_name", property = "enterpriseName"),
            @Result(column = "drug_name", property = "drugName")
    })
    DrugInformation selectByDI(DrugInformation drugInformation);

    @Select("select MAX(serial_num) from drug_information")
    String slectMaxNum();

    @Select("select * from drug_information where serial_num= #{serialNum}")
    DrugInformation selectBySerialNum(String serialNum);
}