package com.yaopin.mapper;

import com.yaopin.entity.StatementsDetail;
import com.yaopin.entity.StatementsDetailExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StatementsDetailMapper {
    int countByExample(StatementsDetailExample example);

    int deleteByExample(StatementsDetailExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(StatementsDetail record);

    int insertSelective(StatementsDetail record);

    List<StatementsDetail> selectByExample(StatementsDetailExample example);

    StatementsDetail selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") StatementsDetail record, @Param("example") StatementsDetailExample example);

    int updateByExample(@Param("record") StatementsDetail record, @Param("example") StatementsDetailExample example);

    int updateByPrimaryKeySelective(StatementsDetail record);

    int updateByPrimaryKey(StatementsDetail record);

    int updateByid(StatementsDetail record);
}