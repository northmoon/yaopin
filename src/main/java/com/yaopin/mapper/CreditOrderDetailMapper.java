package com.yaopin.mapper;

import com.yaopin.entity.CreditOrderDetail;
import com.yaopin.entity.CreditOrderDetailExample;
import org.apache.ibatis.annotations.Param;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.List;

public interface CreditOrderDetailMapper {
    int countByExample(CreditOrderDetailExample example);

    int deleteByExample(CreditOrderDetailExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CreditOrderDetail record);
    int insert1(CreditOrderDetail record);

    int insertSelective(CreditOrderDetail record);

    List<CreditOrderDetail> selectByExample(CreditOrderDetailExample example);

    CreditOrderDetail selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CreditOrderDetail record, @Param("example") CreditOrderDetailExample example);

    int updateByExample(@Param("record") CreditOrderDetail record, @Param("example") CreditOrderDetailExample example);

    int updateByPrimaryKeySelective(CreditOrderDetail record);

    int updateByPrimaryKey(CreditOrderDetail record);

    int updateByPrimaryKey1(CreditOrderDetail record);
	
	
	int updateByid(CreditOrderDetail record);
}