package com.yaopin.mapper;

import com.yaopin.entity.UserRoleExample;
import com.yaopin.entity.UserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserRoleMapper {
    int countByExample(UserRoleExample example);

    int deleteByExample(UserRoleExample example);

    int deleteByPrimaryKey(UserRole key);

    int insert(UserRole record);

    int insertSelective(UserRole record);

    List<UserRole> selectByExample(UserRoleExample example);

    int updateByExampleSelective(@Param("record") UserRole record, @Param("example") UserRoleExample example);

    int updateByExample(@Param("record") UserRole record, @Param("example") UserRoleExample example);

    @Delete("delete from user_role where user_id=#{id}")
    void deleteByUserId(Integer id);

    @Delete("delete from user_role where role_id=#{id}")
    void deleteByRoleId(Integer id);
}