package com.yaopin.mapper;

import com.yaopin.entity.PurchaseOrderWarehousing;
import com.yaopin.entity.PurchaseOrderWarehousingExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PurchaseOrderWarehousingMapper {
    int countByExample(PurchaseOrderWarehousingExample example);

    int deleteByExample(PurchaseOrderWarehousingExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PurchaseOrderWarehousing record);

    int insertSelective(PurchaseOrderWarehousing record);

    List<PurchaseOrderWarehousing> selectByExample(PurchaseOrderWarehousingExample example);

    PurchaseOrderWarehousing selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PurchaseOrderWarehousing record, @Param("example") PurchaseOrderWarehousingExample example);

    int updateByExample(@Param("record") PurchaseOrderWarehousing record, @Param("example") PurchaseOrderWarehousingExample example);

    int updateByPrimaryKeySelective(PurchaseOrderWarehousing record);

    int updateByPrimaryKey(PurchaseOrderWarehousing record);
}