package com.yaopin.mapper;

import com.yaopin.entity.Hospital;
import com.yaopin.entity.HospitalExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface HospitalMapper {
    int countByExample(HospitalExample example);

    int deleteByExample(HospitalExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Hospital record);

    int insertSelective(Hospital record);

    List<Hospital> selectByExample(HospitalExample example);

    Hospital selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Hospital record, @Param("example") HospitalExample example);

    int updateByExample(@Param("record") Hospital record, @Param("example") HospitalExample example);

    int updateByPrimaryKeySelective(Hospital record);

    int updateByPrimaryKey(Hospital record);

    @Select("SELECT * from hospital WHERE hospital_name =#{hospitalName} AND mailing_address=#{mailingAddress} AND zip_code = #{zipCode} AND hospital_level = #{hospitalLevel} AND bed_num =#{bedNum}")
    Hospital selectHospital(Hospital hospital);
   
   
   
   
   
    @Select("select id from hospital where hospital_name like #{hospitalName}")
    List<Integer> selectLikeName(String hospitalName);
}