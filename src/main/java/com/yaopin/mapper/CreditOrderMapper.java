package com.yaopin.mapper;

import com.yaopin.entity.CreditOrder;
import com.yaopin.entity.CreditOrderExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CreditOrderMapper {
    int countByExample(CreditOrderExample example);

    int deleteByExample(CreditOrderExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CreditOrder record);

    int insertSelective(CreditOrder record);

    List<CreditOrder> selectByExample(CreditOrderExample example);

    CreditOrder selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CreditOrder record, @Param("example") CreditOrderExample example);

    int updateByExample(@Param("record") CreditOrder record, @Param("example") CreditOrderExample example);

    int updateByPrimaryKeySelective(CreditOrder record);

    int updateByPrimaryKey(CreditOrder record);
}