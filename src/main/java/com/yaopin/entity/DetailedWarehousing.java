package com.yaopin.entity;

/**
 * @ClassName DetailedWarehousing  药品明细和入库
 * @Author donghongyu
 * @Date 2019/9/18 16:50
 **/
public class DetailedWarehousing implements Cloneable {
    private PurchaseOrderDrugDetails purchaseOrderDrugDetails;
    private PurchaseOrderWarehousing purchaseOrderWarehousing;
    public PurchaseOrderDrugDetails getPurchaseOrderDrugDetails() {
        return purchaseOrderDrugDetails;
    }

    public void setPurchaseOrderDrugDetails(PurchaseOrderDrugDetails purchaseOrderDrugDetails) {
        this.purchaseOrderDrugDetails = purchaseOrderDrugDetails;
    }

    public PurchaseOrderWarehousing getPurchaseOrderWarehousing() {
        return purchaseOrderWarehousing;
    }
    public void setPurchaseOrderWarehousing(PurchaseOrderWarehousing purchaseOrderWarehousing) {
        this.purchaseOrderWarehousing = purchaseOrderWarehousing;
    }
    @Override
    public Object clone(){
        DetailedWarehousing detailedWarehousing = null;
        try {
            detailedWarehousing=(DetailedWarehousing)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return detailedWarehousing;
    }
}
