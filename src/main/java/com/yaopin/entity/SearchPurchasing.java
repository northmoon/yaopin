package com.yaopin.entity;

import java.util.Date;

public class SearchPurchasing {
    private PurchaseNote purchaseNote;

    private PurchaseOrderDrugDetails purchaseOrderDrugDetails;

    private DrugCategory drugCategory;

    private DrugInformation drugInformation;

    private Date startTime;

    private Date endTime;

    private String hospitalname;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname;
    }

    public PurchaseNote getPurchaseNote() {
        return purchaseNote;
    }

    public void setPurchaseNote(PurchaseNote purchaseNote) {
        this.purchaseNote = purchaseNote;
    }

    public PurchaseOrderDrugDetails getPurchaseOrderDrugDetails() {
        return purchaseOrderDrugDetails;
    }

    public void setPurchaseOrderDrugDetails(PurchaseOrderDrugDetails purchaseOrderDrugDetails) {
        this.purchaseOrderDrugDetails = purchaseOrderDrugDetails;
    }

    public DrugCategory getDrugCategory() {
        return drugCategory;
    }

    public void setDrugCategory(DrugCategory drugCategory) {
        this.drugCategory = drugCategory;
    }

    public DrugInformation getDrugInformation() {
        return drugInformation;
    }

    public void setDrugInformation(DrugInformation drugInformation) {
        this.drugInformation = drugInformation;
    }

}
