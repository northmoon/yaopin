package com.yaopin.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Auther: wyan
 * @Date: 2018/11/23 09:56
 * @Description:
 */
public class DateUtil {
    /**
     * 功能描述:接受日期参数转换字符串返回
     * @param: [date]
     * @return: java.lang.String
     * @auther: wyan
     * @date: 2018/11/23 9:57
     */
    public static  String formatDateToStr(Date date){

        //创建日期格式化类
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    /**
     * 功能描述:接受字符串返回日期
     * @param: [dateStr]
     * @return: java.util.Date
     * @auther: wyan
     * @date: 2018/11/23 10:54
     */
    public static  Date parseStrToDate(String dateStr){

        //创建日期格式化类
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
