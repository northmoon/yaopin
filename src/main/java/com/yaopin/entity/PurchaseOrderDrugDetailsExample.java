package com.yaopin.entity;

import java.util.ArrayList;
import java.util.List;

public class PurchaseOrderDrugDetailsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PurchaseOrderDrugDetailsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdIsNull() {
            addCriterion("purchase_note_id is null");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdIsNotNull() {
            addCriterion("purchase_note_id is not null");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdEqualTo(Integer value) {
            addCriterion("purchase_note_id =", value, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdNotEqualTo(Integer value) {
            addCriterion("purchase_note_id <>", value, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdGreaterThan(Integer value) {
            addCriterion("purchase_note_id >", value, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("purchase_note_id >=", value, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdLessThan(Integer value) {
            addCriterion("purchase_note_id <", value, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdLessThanOrEqualTo(Integer value) {
            addCriterion("purchase_note_id <=", value, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdIn(List<Integer> values) {
            addCriterion("purchase_note_id in", values, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdNotIn(List<Integer> values) {
            addCriterion("purchase_note_id not in", values, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdBetween(Integer value1, Integer value2) {
            addCriterion("purchase_note_id between", value1, value2, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andPurchaseNoteIdNotBetween(Integer value1, Integer value2) {
            addCriterion("purchase_note_id not between", value1, value2, "purchaseNoteId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdIsNull() {
            addCriterion("drug_information_id is null");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdIsNotNull() {
            addCriterion("drug_information_id is not null");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdEqualTo(Integer value) {
            addCriterion("drug_information_id =", value, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdNotEqualTo(Integer value) {
            addCriterion("drug_information_id <>", value, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdGreaterThan(Integer value) {
            addCriterion("drug_information_id >", value, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("drug_information_id >=", value, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdLessThan(Integer value) {
            addCriterion("drug_information_id <", value, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdLessThanOrEqualTo(Integer value) {
            addCriterion("drug_information_id <=", value, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdIn(List<Integer> values) {
            addCriterion("drug_information_id in", values, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdNotIn(List<Integer> values) {
            addCriterion("drug_information_id not in", values, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdBetween(Integer value1, Integer value2) {
            addCriterion("drug_information_id between", value1, value2, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andDrugInformationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("drug_information_id not between", value1, value2, "drugInformationId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdIsNull() {
            addCriterion("suppliers_id is null");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdIsNotNull() {
            addCriterion("suppliers_id is not null");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdEqualTo(Integer value) {
            addCriterion("suppliers_id =", value, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdNotEqualTo(Integer value) {
            addCriterion("suppliers_id <>", value, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdGreaterThan(Integer value) {
            addCriterion("suppliers_id >", value, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("suppliers_id >=", value, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdLessThan(Integer value) {
            addCriterion("suppliers_id <", value, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdLessThanOrEqualTo(Integer value) {
            addCriterion("suppliers_id <=", value, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdIn(List<Integer> values) {
            addCriterion("suppliers_id in", values, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdNotIn(List<Integer> values) {
            addCriterion("suppliers_id not in", values, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdBetween(Integer value1, Integer value2) {
            addCriterion("suppliers_id between", value1, value2, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andSuppliersIdNotBetween(Integer value1, Integer value2) {
            addCriterion("suppliers_id not between", value1, value2, "suppliersId");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceIsNull() {
            addCriterion("bidding_price is null");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceIsNotNull() {
            addCriterion("bidding_price is not null");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceEqualTo(Integer value) {
            addCriterion("bidding_price =", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceNotEqualTo(Integer value) {
            addCriterion("bidding_price <>", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceGreaterThan(Integer value) {
            addCriterion("bidding_price >", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("bidding_price >=", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceLessThan(Integer value) {
            addCriterion("bidding_price <", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceLessThanOrEqualTo(Integer value) {
            addCriterion("bidding_price <=", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceIn(List<Integer> values) {
            addCriterion("bidding_price in", values, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceNotIn(List<Integer> values) {
            addCriterion("bidding_price not in", values, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceBetween(Integer value1, Integer value2) {
            addCriterion("bidding_price between", value1, value2, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceNotBetween(Integer value1, Integer value2) {
            addCriterion("bidding_price not between", value1, value2, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andTradingIsNull() {
            addCriterion("trading is null");
            return (Criteria) this;
        }

        public Criteria andTradingIsNotNull() {
            addCriterion("trading is not null");
            return (Criteria) this;
        }

        public Criteria andTradingEqualTo(Integer value) {
            addCriterion("trading =", value, "trading");
            return (Criteria) this;
        }

        public Criteria andTradingNotEqualTo(Integer value) {
            addCriterion("trading <>", value, "trading");
            return (Criteria) this;
        }

        public Criteria andTradingGreaterThan(Integer value) {
            addCriterion("trading >", value, "trading");
            return (Criteria) this;
        }

        public Criteria andTradingGreaterThanOrEqualTo(Integer value) {
            addCriterion("trading >=", value, "trading");
            return (Criteria) this;
        }

        public Criteria andTradingLessThan(Integer value) {
            addCriterion("trading <", value, "trading");
            return (Criteria) this;
        }

        public Criteria andTradingLessThanOrEqualTo(Integer value) {
            addCriterion("trading <=", value, "trading");
            return (Criteria) this;
        }

        public Criteria andTradingIn(List<Integer> values) {
            addCriterion("trading in", values, "trading");
            return (Criteria) this;
        }

        public Criteria andTradingNotIn(List<Integer> values) {
            addCriterion("trading not in", values, "trading");
            return (Criteria) this;
        }

        public Criteria andTradingBetween(Integer value1, Integer value2) {
            addCriterion("trading between", value1, value2, "trading");
            return (Criteria) this;
        }

        public Criteria andTradingNotBetween(Integer value1, Integer value2) {
            addCriterion("trading not between", value1, value2, "trading");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedIsNull() {
            addCriterion("amount_purchased is null");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedIsNotNull() {
            addCriterion("amount_purchased is not null");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedEqualTo(Integer value) {
            addCriterion("amount_purchased =", value, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedNotEqualTo(Integer value) {
            addCriterion("amount_purchased <>", value, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedGreaterThan(Integer value) {
            addCriterion("amount_purchased >", value, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedGreaterThanOrEqualTo(Integer value) {
            addCriterion("amount_purchased >=", value, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedLessThan(Integer value) {
            addCriterion("amount_purchased <", value, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedLessThanOrEqualTo(Integer value) {
            addCriterion("amount_purchased <=", value, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedIn(List<Integer> values) {
            addCriterion("amount_purchased in", values, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedNotIn(List<Integer> values) {
            addCriterion("amount_purchased not in", values, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedBetween(Integer value1, Integer value2) {
            addCriterion("amount_purchased between", value1, value2, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andAmountPurchasedNotBetween(Integer value1, Integer value2) {
            addCriterion("amount_purchased not between", value1, value2, "amountPurchased");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountIsNull() {
            addCriterion("purchase_amount is null");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountIsNotNull() {
            addCriterion("purchase_amount is not null");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountEqualTo(Float value) {
            addCriterion("purchase_amount =", value, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountNotEqualTo(Float value) {
            addCriterion("purchase_amount <>", value, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountGreaterThan(Float value) {
            addCriterion("purchase_amount >", value, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountGreaterThanOrEqualTo(Float value) {
            addCriterion("purchase_amount >=", value, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountLessThan(Float value) {
            addCriterion("purchase_amount <", value, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountLessThanOrEqualTo(Float value) {
            addCriterion("purchase_amount <=", value, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountIn(List<Float> values) {
            addCriterion("purchase_amount in", values, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountNotIn(List<Float> values) {
            addCriterion("purchase_amount not in", values, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountBetween(Float value1, Float value2) {
            addCriterion("purchase_amount between", value1, value2, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchaseAmountNotBetween(Float value1, Float value2) {
            addCriterion("purchase_amount not between", value1, value2, "purchaseAmount");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusIsNull() {
            addCriterion("purchasing_status is null");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusIsNotNull() {
            addCriterion("purchasing_status is not null");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusEqualTo(String value) {
            addCriterion("purchasing_status =", value, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusNotEqualTo(String value) {
            addCriterion("purchasing_status <>", value, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusGreaterThan(String value) {
            addCriterion("purchasing_status >", value, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusGreaterThanOrEqualTo(String value) {
            addCriterion("purchasing_status >=", value, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusLessThan(String value) {
            addCriterion("purchasing_status <", value, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusLessThanOrEqualTo(String value) {
            addCriterion("purchasing_status <=", value, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusLike(String value) {
            addCriterion("purchasing_status like", value, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusNotLike(String value) {
            addCriterion("purchasing_status not like", value, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusIn(List<String> values) {
            addCriterion("purchasing_status in", values, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusNotIn(List<String> values) {
            addCriterion("purchasing_status not in", values, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusBetween(String value1, String value2) {
            addCriterion("purchasing_status between", value1, value2, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andPurchasingStatusNotBetween(String value1, String value2) {
            addCriterion("purchasing_status not between", value1, value2, "purchasingStatus");
            return (Criteria) this;
        }

        public Criteria andChangeNumIsNull() {
            addCriterion("change_num is null");
            return (Criteria) this;
        }

        public Criteria andChangeNumIsNotNull() {
            addCriterion("change_num is not null");
            return (Criteria) this;
        }

        public Criteria andChangeNumEqualTo(Integer value) {
            addCriterion("change_num =", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumNotEqualTo(Integer value) {
            addCriterion("change_num <>", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumGreaterThan(Integer value) {
            addCriterion("change_num >", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("change_num >=", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumLessThan(Integer value) {
            addCriterion("change_num <", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumLessThanOrEqualTo(Integer value) {
            addCriterion("change_num <=", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumIn(List<Integer> values) {
            addCriterion("change_num in", values, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumNotIn(List<Integer> values) {
            addCriterion("change_num not in", values, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumBetween(Integer value1, Integer value2) {
            addCriterion("change_num between", value1, value2, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumNotBetween(Integer value1, Integer value2) {
            addCriterion("change_num not between", value1, value2, "changeNum");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}