package com.yaopin.entity;

import java.util.Date;

public class SearchReturnorder implements  Cloneable{
    private PurchaseNote purchaseNote;

    private DrugCategory drugCategory;

    private DrugInformation drugInformation;

    private CreditOrder creditOrder;

    private CreditOrderDetail creditOrderDetail;

    private String hospitalname;

    private Date purchasestartTime;

    private Date purchaseendTime;

    private Date returnstartTime;

    private Date returnendTime;

    public Date getPurchasestartTime() {
        return purchasestartTime;
    }

    public void setPurchasestartTime(Date purchasestartTime) {
        this.purchasestartTime = purchasestartTime;
    }

    public Date getPurchaseendTime() {
        return purchaseendTime;
    }

    public void setPurchaseendTime(Date purchaseendTime) {
        this.purchaseendTime = purchaseendTime;
    }

    public Date getReturnstartTime() {
        return returnstartTime;
    }

    public void setReturnstartTime(Date returnstartTime) {
        this.returnstartTime = returnstartTime;
    }

    public Date getReturnendTime() {
        return returnendTime;
    }

    public void setReturnendTime(Date returnendTime) {
        this.returnendTime = returnendTime;
    }

    public PurchaseNote getPurchaseNote() {
        return purchaseNote;
    }

    public void setPurchaseNote(PurchaseNote purchaseNote) {
        this.purchaseNote = purchaseNote;
    }

    public DrugCategory getDrugCategory() {
        return drugCategory;
    }

    public void setDrugCategory(DrugCategory drugCategory) {
        this.drugCategory = drugCategory;
    }

    public DrugInformation getDrugInformation() {
        return drugInformation;
    }

    public void setDrugInformation(DrugInformation drugInformation) {
        this.drugInformation = drugInformation;
    }

    public CreditOrder getCreditOrder() {
        return creditOrder;
    }

    public void setCreditOrder(CreditOrder creditOrder) {
        this.creditOrder = creditOrder;
    }

    public CreditOrderDetail getCreditOrderDetail() {
        return creditOrderDetail;
    }

    public void setCreditOrderDetail(CreditOrderDetail creditOrderDetail) {
        this.creditOrderDetail = creditOrderDetail;
    }

    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname;
    }

    @Override
    public Object clone()  {
        SearchReturnorder searchReturnorder=null;
        try {
            searchReturnorder=(SearchReturnorder)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        if (drugCategory != null){
            searchReturnorder.drugCategory=((DrugCategory)drugCategory.clone());
        }
        if (drugInformation != null){
            searchReturnorder.drugInformation=((DrugInformation)drugInformation.clone());
        }
        if (purchaseNote != null){
            searchReturnorder.purchaseNote=((PurchaseNote)purchaseNote.clone());
        }
        if (creditOrder != null){
            searchReturnorder.creditOrder=((CreditOrder)creditOrder.clone());
        }
        return searchReturnorder;
    }
}
