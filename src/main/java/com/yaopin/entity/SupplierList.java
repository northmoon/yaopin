package com.yaopin.entity;

public class SupplierList implements Cloneable {
    private Integer id;

    private Integer enterpriseId;



    private Integer drugInformationId;

    private Integer supplyState;

    private Supplier supplier;

    private DrugInformation drugInformation;

    private Integer state;

    private Integer changenum;

    public Integer getChangenum() {
        return changenum;
    }

    public void setChangenum(Integer changenum) {
        this.changenum = changenum;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public DrugInformation getDrugInformation() {
        return drugInformation;
    }

    public void setDrugInformation(DrugInformation drugInformation) {
        this.drugInformation = drugInformation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(Integer enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public Integer getDrugInformationId() {
        return drugInformationId;
    }

    public void setDrugInformationId(Integer drugInformationId) {
        this.drugInformationId = drugInformationId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getSupplyState() {
        return supplyState;
    }

    public void setSupplyState(Integer supplyState) {
        this.supplyState = supplyState;
    }

    public Object clone() {
        SupplierList supplierList = null;
        try {
            supplierList = (SupplierList) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        supplierList.drugInformation = (DrugInformation) drugInformation.clone();
        supplierList.supplier = (Supplier) supplier.clone();
        return supplierList;
    }
}