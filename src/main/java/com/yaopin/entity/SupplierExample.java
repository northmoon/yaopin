package com.yaopin.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class SupplierExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SupplierExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andFirmNameIsNull() {
            addCriterion("firm_name is null");
            return (Criteria) this;
        }

        public Criteria andFirmNameIsNotNull() {
            addCriterion("firm_name is not null");
            return (Criteria) this;
        }

        public Criteria andFirmNameEqualTo(String value) {
            addCriterion("firm_name =", value, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameNotEqualTo(String value) {
            addCriterion("firm_name <>", value, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameGreaterThan(String value) {
            addCriterion("firm_name >", value, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameGreaterThanOrEqualTo(String value) {
            addCriterion("firm_name >=", value, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameLessThan(String value) {
            addCriterion("firm_name <", value, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameLessThanOrEqualTo(String value) {
            addCriterion("firm_name <=", value, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameLike(String value) {
            addCriterion("firm_name like", value, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameNotLike(String value) {
            addCriterion("firm_name not like", value, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameIn(List<String> values) {
            addCriterion("firm_name in", values, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameNotIn(List<String> values) {
            addCriterion("firm_name not in", values, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameBetween(String value1, String value2) {
            addCriterion("firm_name between", value1, value2, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmNameNotBetween(String value1, String value2) {
            addCriterion("firm_name not between", value1, value2, "firmName");
            return (Criteria) this;
        }

        public Criteria andFirmTypeIsNull() {
            addCriterion("firm_type is null");
            return (Criteria) this;
        }

        public Criteria andFirmTypeIsNotNull() {
            addCriterion("firm_type is not null");
            return (Criteria) this;
        }

        public Criteria andFirmTypeEqualTo(Integer value) {
            addCriterion("firm_type =", value, "firmType");
            return (Criteria) this;
        }

        public Criteria andFirmTypeNotEqualTo(Integer value) {
            addCriterion("firm_type <>", value, "firmType");
            return (Criteria) this;
        }

        public Criteria andFirmTypeGreaterThan(Integer value) {
            addCriterion("firm_type >", value, "firmType");
            return (Criteria) this;
        }

        public Criteria andFirmTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("firm_type >=", value, "firmType");
            return (Criteria) this;
        }

        public Criteria andFirmTypeLessThan(Integer value) {
            addCriterion("firm_type <", value, "firmType");
            return (Criteria) this;
        }

        public Criteria andFirmTypeLessThanOrEqualTo(Integer value) {
            addCriterion("firm_type <=", value, "firmType");
            return (Criteria) this;
        }

        public Criteria andFirmTypeIn(List<Integer> values) {
            addCriterion("firm_type in", values, "firmType");
            return (Criteria) this;
        }

        public Criteria andFirmTypeNotIn(List<Integer> values) {
            addCriterion("firm_type not in", values, "firmType");
            return (Criteria) this;
        }

        public Criteria andFirmTypeBetween(Integer value1, Integer value2) {
            addCriterion("firm_type between", value1, value2, "firmType");
            return (Criteria) this;
        }

        public Criteria andFirmTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("firm_type not between", value1, value2, "firmType");
            return (Criteria) this;
        }

        public Criteria andLicenceIsNull() {
            addCriterion("licence is null");
            return (Criteria) this;
        }

        public Criteria andLicenceIsNotNull() {
            addCriterion("licence is not null");
            return (Criteria) this;
        }

        public Criteria andLicenceEqualTo(String value) {
            addCriterion("licence =", value, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceNotEqualTo(String value) {
            addCriterion("licence <>", value, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceGreaterThan(String value) {
            addCriterion("licence >", value, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceGreaterThanOrEqualTo(String value) {
            addCriterion("licence >=", value, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceLessThan(String value) {
            addCriterion("licence <", value, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceLessThanOrEqualTo(String value) {
            addCriterion("licence <=", value, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceLike(String value) {
            addCriterion("licence like", value, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceNotLike(String value) {
            addCriterion("licence not like", value, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceIn(List<String> values) {
            addCriterion("licence in", values, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceNotIn(List<String> values) {
            addCriterion("licence not in", values, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceBetween(String value1, String value2) {
            addCriterion("licence between", value1, value2, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceNotBetween(String value1, String value2) {
            addCriterion("licence not between", value1, value2, "licence");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityIsNull() {
            addCriterion("licence_validity is null");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityIsNotNull() {
            addCriterion("licence_validity is not null");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityEqualTo(Date value) {
            addCriterionForJDBCDate("licence_validity =", value, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityNotEqualTo(Date value) {
            addCriterionForJDBCDate("licence_validity <>", value, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityGreaterThan(Date value) {
            addCriterionForJDBCDate("licence_validity >", value, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("licence_validity >=", value, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityLessThan(Date value) {
            addCriterionForJDBCDate("licence_validity <", value, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("licence_validity <=", value, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityIn(List<Date> values) {
            addCriterionForJDBCDate("licence_validity in", values, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityNotIn(List<Date> values) {
            addCriterionForJDBCDate("licence_validity not in", values, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("licence_validity between", value1, value2, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLicenceValidityNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("licence_validity not between", value1, value2, "licenceValidity");
            return (Criteria) this;
        }

        public Criteria andLinkmanIsNull() {
            addCriterion("linkman is null");
            return (Criteria) this;
        }

        public Criteria andLinkmanIsNotNull() {
            addCriterion("linkman is not null");
            return (Criteria) this;
        }

        public Criteria andLinkmanEqualTo(String value) {
            addCriterion("linkman =", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanNotEqualTo(String value) {
            addCriterion("linkman <>", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanGreaterThan(String value) {
            addCriterion("linkman >", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanGreaterThanOrEqualTo(String value) {
            addCriterion("linkman >=", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanLessThan(String value) {
            addCriterion("linkman <", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanLessThanOrEqualTo(String value) {
            addCriterion("linkman <=", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanLike(String value) {
            addCriterion("linkman like", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanNotLike(String value) {
            addCriterion("linkman not like", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanIn(List<String> values) {
            addCriterion("linkman in", values, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanNotIn(List<String> values) {
            addCriterion("linkman not in", values, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanBetween(String value1, String value2) {
            addCriterion("linkman between", value1, value2, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanNotBetween(String value1, String value2) {
            addCriterion("linkman not between", value1, value2, "linkman");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeIsNull() {
            addCriterion("business_scope is null");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeIsNotNull() {
            addCriterion("business_scope is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeEqualTo(String value) {
            addCriterion("business_scope =", value, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeNotEqualTo(String value) {
            addCriterion("business_scope <>", value, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeGreaterThan(String value) {
            addCriterion("business_scope >", value, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeGreaterThanOrEqualTo(String value) {
            addCriterion("business_scope >=", value, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeLessThan(String value) {
            addCriterion("business_scope <", value, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeLessThanOrEqualTo(String value) {
            addCriterion("business_scope <=", value, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeLike(String value) {
            addCriterion("business_scope like", value, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeNotLike(String value) {
            addCriterion("business_scope not like", value, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeIn(List<String> values) {
            addCriterion("business_scope in", values, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeNotIn(List<String> values) {
            addCriterion("business_scope not in", values, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeBetween(String value1, String value2) {
            addCriterion("business_scope between", value1, value2, "businessScope");
            return (Criteria) this;
        }

        public Criteria andBusinessScopeNotBetween(String value1, String value2) {
            addCriterion("business_scope not between", value1, value2, "businessScope");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressIsNull() {
            addCriterion("register_address is null");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressIsNotNull() {
            addCriterion("register_address is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressEqualTo(String value) {
            addCriterion("register_address =", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotEqualTo(String value) {
            addCriterion("register_address <>", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressGreaterThan(String value) {
            addCriterion("register_address >", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressGreaterThanOrEqualTo(String value) {
            addCriterion("register_address >=", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressLessThan(String value) {
            addCriterion("register_address <", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressLessThanOrEqualTo(String value) {
            addCriterion("register_address <=", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressLike(String value) {
            addCriterion("register_address like", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotLike(String value) {
            addCriterion("register_address not like", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressIn(List<String> values) {
            addCriterion("register_address in", values, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotIn(List<String> values) {
            addCriterion("register_address not in", values, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressBetween(String value1, String value2) {
            addCriterion("register_address between", value1, value2, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotBetween(String value1, String value2) {
            addCriterion("register_address not between", value1, value2, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressIsNull() {
            addCriterion("contact_address is null");
            return (Criteria) this;
        }

        public Criteria andContactAddressIsNotNull() {
            addCriterion("contact_address is not null");
            return (Criteria) this;
        }

        public Criteria andContactAddressEqualTo(String value) {
            addCriterion("contact_address =", value, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressNotEqualTo(String value) {
            addCriterion("contact_address <>", value, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressGreaterThan(String value) {
            addCriterion("contact_address >", value, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressGreaterThanOrEqualTo(String value) {
            addCriterion("contact_address >=", value, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressLessThan(String value) {
            addCriterion("contact_address <", value, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressLessThanOrEqualTo(String value) {
            addCriterion("contact_address <=", value, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressLike(String value) {
            addCriterion("contact_address like", value, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressNotLike(String value) {
            addCriterion("contact_address not like", value, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressIn(List<String> values) {
            addCriterion("contact_address in", values, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressNotIn(List<String> values) {
            addCriterion("contact_address not in", values, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressBetween(String value1, String value2) {
            addCriterion("contact_address between", value1, value2, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andContactAddressNotBetween(String value1, String value2) {
            addCriterion("contact_address not between", value1, value2, "contactAddress");
            return (Criteria) this;
        }

        public Criteria andZipCodeIsNull() {
            addCriterion("zip_code is null");
            return (Criteria) this;
        }

        public Criteria andZipCodeIsNotNull() {
            addCriterion("zip_code is not null");
            return (Criteria) this;
        }

        public Criteria andZipCodeEqualTo(String value) {
            addCriterion("zip_code =", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeNotEqualTo(String value) {
            addCriterion("zip_code <>", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeGreaterThan(String value) {
            addCriterion("zip_code >", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeGreaterThanOrEqualTo(String value) {
            addCriterion("zip_code >=", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeLessThan(String value) {
            addCriterion("zip_code <", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeLessThanOrEqualTo(String value) {
            addCriterion("zip_code <=", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeLike(String value) {
            addCriterion("zip_code like", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeNotLike(String value) {
            addCriterion("zip_code not like", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeIn(List<String> values) {
            addCriterion("zip_code in", values, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeNotIn(List<String> values) {
            addCriterion("zip_code not in", values, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeBetween(String value1, String value2) {
            addCriterion("zip_code between", value1, value2, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeNotBetween(String value1, String value2) {
            addCriterion("zip_code not between", value1, value2, "zipCode");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNull() {
            addCriterion("total_assets is null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNotNull() {
            addCriterion("total_assets is not null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsEqualTo(Float value) {
            addCriterion("total_assets =", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotEqualTo(Float value) {
            addCriterion("total_assets <>", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThan(Float value) {
            addCriterion("total_assets >", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThanOrEqualTo(Float value) {
            addCriterion("total_assets >=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThan(Float value) {
            addCriterion("total_assets <", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThanOrEqualTo(Float value) {
            addCriterion("total_assets <=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIn(List<Float> values) {
            addCriterion("total_assets in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotIn(List<Float> values) {
            addCriterion("total_assets not in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsBetween(Float value1, Float value2) {
            addCriterion("total_assets between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotBetween(Float value1, Float value2) {
            addCriterion("total_assets not between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andFaxIsNull() {
            addCriterion("fax is null");
            return (Criteria) this;
        }

        public Criteria andFaxIsNotNull() {
            addCriterion("fax is not null");
            return (Criteria) this;
        }

        public Criteria andFaxEqualTo(String value) {
            addCriterion("fax =", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotEqualTo(String value) {
            addCriterion("fax <>", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxGreaterThan(String value) {
            addCriterion("fax >", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxGreaterThanOrEqualTo(String value) {
            addCriterion("fax >=", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxLessThan(String value) {
            addCriterion("fax <", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxLessThanOrEqualTo(String value) {
            addCriterion("fax <=", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxLike(String value) {
            addCriterion("fax like", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotLike(String value) {
            addCriterion("fax not like", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxIn(List<String> values) {
            addCriterion("fax in", values, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotIn(List<String> values) {
            addCriterion("fax not in", values, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxBetween(String value1, String value2) {
            addCriterion("fax between", value1, value2, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotBetween(String value1, String value2) {
            addCriterion("fax not between", value1, value2, "fax");
            return (Criteria) this;
        }

        public Criteria andLegalNameIsNull() {
            addCriterion("legal_name is null");
            return (Criteria) this;
        }

        public Criteria andLegalNameIsNotNull() {
            addCriterion("legal_name is not null");
            return (Criteria) this;
        }

        public Criteria andLegalNameEqualTo(String value) {
            addCriterion("legal_name =", value, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameNotEqualTo(String value) {
            addCriterion("legal_name <>", value, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameGreaterThan(String value) {
            addCriterion("legal_name >", value, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameGreaterThanOrEqualTo(String value) {
            addCriterion("legal_name >=", value, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameLessThan(String value) {
            addCriterion("legal_name <", value, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameLessThanOrEqualTo(String value) {
            addCriterion("legal_name <=", value, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameLike(String value) {
            addCriterion("legal_name like", value, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameNotLike(String value) {
            addCriterion("legal_name not like", value, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameIn(List<String> values) {
            addCriterion("legal_name in", values, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameNotIn(List<String> values) {
            addCriterion("legal_name not in", values, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameBetween(String value1, String value2) {
            addCriterion("legal_name between", value1, value2, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalNameNotBetween(String value1, String value2) {
            addCriterion("legal_name not between", value1, value2, "legalName");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardIsNull() {
            addCriterion("legal_idcard is null");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardIsNotNull() {
            addCriterion("legal_idcard is not null");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardEqualTo(String value) {
            addCriterion("legal_idcard =", value, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardNotEqualTo(String value) {
            addCriterion("legal_idcard <>", value, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardGreaterThan(String value) {
            addCriterion("legal_idcard >", value, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardGreaterThanOrEqualTo(String value) {
            addCriterion("legal_idcard >=", value, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardLessThan(String value) {
            addCriterion("legal_idcard <", value, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardLessThanOrEqualTo(String value) {
            addCriterion("legal_idcard <=", value, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardLike(String value) {
            addCriterion("legal_idcard like", value, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardNotLike(String value) {
            addCriterion("legal_idcard not like", value, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardIn(List<String> values) {
            addCriterion("legal_idcard in", values, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardNotIn(List<String> values) {
            addCriterion("legal_idcard not in", values, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardBetween(String value1, String value2) {
            addCriterion("legal_idcard between", value1, value2, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andLegalIdcardNotBetween(String value1, String value2) {
            addCriterion("legal_idcard not between", value1, value2, "legalIdcard");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsIsNull() {
            addCriterion("register_assets is null");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsIsNotNull() {
            addCriterion("register_assets is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsEqualTo(Float value) {
            addCriterion("register_assets =", value, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsNotEqualTo(Float value) {
            addCriterion("register_assets <>", value, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsGreaterThan(Float value) {
            addCriterion("register_assets >", value, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsGreaterThanOrEqualTo(Float value) {
            addCriterion("register_assets >=", value, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsLessThan(Float value) {
            addCriterion("register_assets <", value, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsLessThanOrEqualTo(Float value) {
            addCriterion("register_assets <=", value, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsIn(List<Float> values) {
            addCriterion("register_assets in", values, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsNotIn(List<Float> values) {
            addCriterion("register_assets not in", values, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsBetween(Float value1, Float value2) {
            addCriterion("register_assets between", value1, value2, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andRegisterAssetsNotBetween(Float value1, Float value2) {
            addCriterion("register_assets not between", value1, value2, "registerAssets");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyIsNull() {
            addCriterion("sales_amount_ly is null");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyIsNotNull() {
            addCriterion("sales_amount_ly is not null");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyEqualTo(Float value) {
            addCriterion("sales_amount_ly =", value, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyNotEqualTo(Float value) {
            addCriterion("sales_amount_ly <>", value, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyGreaterThan(Float value) {
            addCriterion("sales_amount_ly >", value, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyGreaterThanOrEqualTo(Float value) {
            addCriterion("sales_amount_ly >=", value, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyLessThan(Float value) {
            addCriterion("sales_amount_ly <", value, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyLessThanOrEqualTo(Float value) {
            addCriterion("sales_amount_ly <=", value, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyIn(List<Float> values) {
            addCriterion("sales_amount_ly in", values, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyNotIn(List<Float> values) {
            addCriterion("sales_amount_ly not in", values, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyBetween(Float value1, Float value2) {
            addCriterion("sales_amount_ly between", value1, value2, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andSalesAmountLyNotBetween(Float value1, Float value2) {
            addCriterion("sales_amount_ly not between", value1, value2, "salesAmountLy");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsNull() {
            addCriterion("website is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsNotNull() {
            addCriterion("website is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteEqualTo(String value) {
            addCriterion("website =", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotEqualTo(String value) {
            addCriterion("website <>", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteGreaterThan(String value) {
            addCriterion("website >", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteGreaterThanOrEqualTo(String value) {
            addCriterion("website >=", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLessThan(String value) {
            addCriterion("website <", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLessThanOrEqualTo(String value) {
            addCriterion("website <=", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLike(String value) {
            addCriterion("website like", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotLike(String value) {
            addCriterion("website not like", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteIn(List<String> values) {
            addCriterion("website in", values, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotIn(List<String> values) {
            addCriterion("website not in", values, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteBetween(String value1, String value2) {
            addCriterion("website between", value1, value2, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotBetween(String value1, String value2) {
            addCriterion("website not between", value1, value2, "website");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeIsNull() {
            addCriterion("enterprise_code is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeIsNotNull() {
            addCriterion("enterprise_code is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeEqualTo(String value) {
            addCriterion("enterprise_code =", value, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeNotEqualTo(String value) {
            addCriterion("enterprise_code <>", value, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeGreaterThan(String value) {
            addCriterion("enterprise_code >", value, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeGreaterThanOrEqualTo(String value) {
            addCriterion("enterprise_code >=", value, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeLessThan(String value) {
            addCriterion("enterprise_code <", value, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeLessThanOrEqualTo(String value) {
            addCriterion("enterprise_code <=", value, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeLike(String value) {
            addCriterion("enterprise_code like", value, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeNotLike(String value) {
            addCriterion("enterprise_code not like", value, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeIn(List<String> values) {
            addCriterion("enterprise_code in", values, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeNotIn(List<String> values) {
            addCriterion("enterprise_code not in", values, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeBetween(String value1, String value2) {
            addCriterion("enterprise_code between", value1, value2, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCodeNotBetween(String value1, String value2) {
            addCriterion("enterprise_code not between", value1, value2, "enterpriseCode");
            return (Criteria) this;
        }

        public Criteria andEcValidityIsNull() {
            addCriterion("ec_validity is null");
            return (Criteria) this;
        }

        public Criteria andEcValidityIsNotNull() {
            addCriterion("ec_validity is not null");
            return (Criteria) this;
        }

        public Criteria andEcValidityEqualTo(Date value) {
            addCriterionForJDBCDate("ec_validity =", value, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andEcValidityNotEqualTo(Date value) {
            addCriterionForJDBCDate("ec_validity <>", value, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andEcValidityGreaterThan(Date value) {
            addCriterionForJDBCDate("ec_validity >", value, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andEcValidityGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ec_validity >=", value, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andEcValidityLessThan(Date value) {
            addCriterionForJDBCDate("ec_validity <", value, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andEcValidityLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ec_validity <=", value, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andEcValidityIn(List<Date> values) {
            addCriterionForJDBCDate("ec_validity in", values, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andEcValidityNotIn(List<Date> values) {
            addCriterionForJDBCDate("ec_validity not in", values, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andEcValidityBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ec_validity between", value1, value2, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andEcValidityNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ec_validity not between", value1, value2, "ecValidity");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseIsNull() {
            addCriterion("business_license is null");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseIsNotNull() {
            addCriterion("business_license is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseEqualTo(String value) {
            addCriterion("business_license =", value, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseNotEqualTo(String value) {
            addCriterion("business_license <>", value, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseGreaterThan(String value) {
            addCriterion("business_license >", value, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseGreaterThanOrEqualTo(String value) {
            addCriterion("business_license >=", value, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseLessThan(String value) {
            addCriterion("business_license <", value, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseLessThanOrEqualTo(String value) {
            addCriterion("business_license <=", value, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseLike(String value) {
            addCriterion("business_license like", value, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseNotLike(String value) {
            addCriterion("business_license not like", value, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseIn(List<String> values) {
            addCriterion("business_license in", values, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseNotIn(List<String> values) {
            addCriterion("business_license not in", values, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseBetween(String value1, String value2) {
            addCriterion("business_license between", value1, value2, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBusinessLicenseNotBetween(String value1, String value2) {
            addCriterion("business_license not between", value1, value2, "businessLicense");
            return (Criteria) this;
        }

        public Criteria andBlValidityIsNull() {
            addCriterion("bl_validity is null");
            return (Criteria) this;
        }

        public Criteria andBlValidityIsNotNull() {
            addCriterion("bl_validity is not null");
            return (Criteria) this;
        }

        public Criteria andBlValidityEqualTo(Date value) {
            addCriterionForJDBCDate("bl_validity =", value, "blValidity");
            return (Criteria) this;
        }

        public Criteria andBlValidityNotEqualTo(Date value) {
            addCriterionForJDBCDate("bl_validity <>", value, "blValidity");
            return (Criteria) this;
        }

        public Criteria andBlValidityGreaterThan(Date value) {
            addCriterionForJDBCDate("bl_validity >", value, "blValidity");
            return (Criteria) this;
        }

        public Criteria andBlValidityGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("bl_validity >=", value, "blValidity");
            return (Criteria) this;
        }

        public Criteria andBlValidityLessThan(Date value) {
            addCriterionForJDBCDate("bl_validity <", value, "blValidity");
            return (Criteria) this;
        }

        public Criteria andBlValidityLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("bl_validity <=", value, "blValidity");
            return (Criteria) this;
        }

        public Criteria andBlValidityIn(List<Date> values) {
            addCriterionForJDBCDate("bl_validity in", values, "blValidity");
            return (Criteria) this;
        }

        public Criteria andBlValidityNotIn(List<Date> values) {
            addCriterionForJDBCDate("bl_validity not in", values, "blValidity");
            return (Criteria) this;
        }

        public Criteria andBlValidityBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("bl_validity between", value1, value2, "blValidity");
            return (Criteria) this;
        }

        public Criteria andBlValidityNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("bl_validity not between", value1, value2, "blValidity");
            return (Criteria) this;
        }

        public Criteria andIslcIsNull() {
            addCriterion("islc is null");
            return (Criteria) this;
        }

        public Criteria andIslcIsNotNull() {
            addCriterion("islc is not null");
            return (Criteria) this;
        }

        public Criteria andIslcEqualTo(Integer value) {
            addCriterion("islc =", value, "islc");
            return (Criteria) this;
        }

        public Criteria andIslcNotEqualTo(Integer value) {
            addCriterion("islc <>", value, "islc");
            return (Criteria) this;
        }

        public Criteria andIslcGreaterThan(Integer value) {
            addCriterion("islc >", value, "islc");
            return (Criteria) this;
        }

        public Criteria andIslcGreaterThanOrEqualTo(Integer value) {
            addCriterion("islc >=", value, "islc");
            return (Criteria) this;
        }

        public Criteria andIslcLessThan(Integer value) {
            addCriterion("islc <", value, "islc");
            return (Criteria) this;
        }

        public Criteria andIslcLessThanOrEqualTo(Integer value) {
            addCriterion("islc <=", value, "islc");
            return (Criteria) this;
        }

        public Criteria andIslcIn(List<Integer> values) {
            addCriterion("islc in", values, "islc");
            return (Criteria) this;
        }

        public Criteria andIslcNotIn(List<Integer> values) {
            addCriterion("islc not in", values, "islc");
            return (Criteria) this;
        }

        public Criteria andIslcBetween(Integer value1, Integer value2) {
            addCriterion("islc between", value1, value2, "islc");
            return (Criteria) this;
        }

        public Criteria andIslcNotBetween(Integer value1, Integer value2) {
            addCriterion("islc not between", value1, value2, "islc");
            return (Criteria) this;
        }

        public Criteria andLcNumIsNull() {
            addCriterion("lc_num is null");
            return (Criteria) this;
        }

        public Criteria andLcNumIsNotNull() {
            addCriterion("lc_num is not null");
            return (Criteria) this;
        }

        public Criteria andLcNumEqualTo(String value) {
            addCriterion("lc_num =", value, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumNotEqualTo(String value) {
            addCriterion("lc_num <>", value, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumGreaterThan(String value) {
            addCriterion("lc_num >", value, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumGreaterThanOrEqualTo(String value) {
            addCriterion("lc_num >=", value, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumLessThan(String value) {
            addCriterion("lc_num <", value, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumLessThanOrEqualTo(String value) {
            addCriterion("lc_num <=", value, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumLike(String value) {
            addCriterion("lc_num like", value, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumNotLike(String value) {
            addCriterion("lc_num not like", value, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumIn(List<String> values) {
            addCriterion("lc_num in", values, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumNotIn(List<String> values) {
            addCriterion("lc_num not in", values, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumBetween(String value1, String value2) {
            addCriterion("lc_num between", value1, value2, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcNumNotBetween(String value1, String value2) {
            addCriterion("lc_num not between", value1, value2, "lcNum");
            return (Criteria) this;
        }

        public Criteria andLcValidityIsNull() {
            addCriterion("lc_validity is null");
            return (Criteria) this;
        }

        public Criteria andLcValidityIsNotNull() {
            addCriterion("lc_validity is not null");
            return (Criteria) this;
        }

        public Criteria andLcValidityEqualTo(String value) {
            addCriterion("lc_validity =", value, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityNotEqualTo(String value) {
            addCriterion("lc_validity <>", value, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityGreaterThan(String value) {
            addCriterion("lc_validity >", value, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityGreaterThanOrEqualTo(String value) {
            addCriterion("lc_validity >=", value, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityLessThan(String value) {
            addCriterion("lc_validity <", value, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityLessThanOrEqualTo(String value) {
            addCriterion("lc_validity <=", value, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityLike(String value) {
            addCriterion("lc_validity like", value, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityNotLike(String value) {
            addCriterion("lc_validity not like", value, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityIn(List<String> values) {
            addCriterion("lc_validity in", values, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityNotIn(List<String> values) {
            addCriterion("lc_validity not in", values, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityBetween(String value1, String value2) {
            addCriterion("lc_validity between", value1, value2, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andLcValidityNotBetween(String value1, String value2) {
            addCriterion("lc_validity not between", value1, value2, "lcValidity");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsIsNull() {
            addCriterion("fixed_assets is null");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsIsNotNull() {
            addCriterion("fixed_assets is not null");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsEqualTo(Float value) {
            addCriterion("fixed_assets =", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsNotEqualTo(Float value) {
            addCriterion("fixed_assets <>", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsGreaterThan(Float value) {
            addCriterion("fixed_assets >", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsGreaterThanOrEqualTo(Float value) {
            addCriterion("fixed_assets >=", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsLessThan(Float value) {
            addCriterion("fixed_assets <", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsLessThanOrEqualTo(Float value) {
            addCriterion("fixed_assets <=", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsIn(List<Float> values) {
            addCriterion("fixed_assets in", values, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsNotIn(List<Float> values) {
            addCriterion("fixed_assets not in", values, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsBetween(Float value1, Float value2) {
            addCriterion("fixed_assets between", value1, value2, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsNotBetween(Float value1, Float value2) {
            addCriterion("fixed_assets not between", value1, value2, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andIntroducedIsNull() {
            addCriterion("introduced is null");
            return (Criteria) this;
        }

        public Criteria andIntroducedIsNotNull() {
            addCriterion("introduced is not null");
            return (Criteria) this;
        }

        public Criteria andIntroducedEqualTo(String value) {
            addCriterion("introduced =", value, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedNotEqualTo(String value) {
            addCriterion("introduced <>", value, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedGreaterThan(String value) {
            addCriterion("introduced >", value, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedGreaterThanOrEqualTo(String value) {
            addCriterion("introduced >=", value, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedLessThan(String value) {
            addCriterion("introduced <", value, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedLessThanOrEqualTo(String value) {
            addCriterion("introduced <=", value, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedLike(String value) {
            addCriterion("introduced like", value, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedNotLike(String value) {
            addCriterion("introduced not like", value, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedIn(List<String> values) {
            addCriterion("introduced in", values, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedNotIn(List<String> values) {
            addCriterion("introduced not in", values, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedBetween(String value1, String value2) {
            addCriterion("introduced between", value1, value2, "introduced");
            return (Criteria) this;
        }

        public Criteria andIntroducedNotBetween(String value1, String value2) {
            addCriterion("introduced not between", value1, value2, "introduced");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}