package com.yaopin.entity;

/**
 * @ClassName combination
 * @Author donghongyu
 * @Date 2019/9/21 11:17
 **/
public class Combination implements Cloneable {
    private CreditOrderDetail creditOrderDetail;
    private PurchaseOrderDrugDetails purchaseOrderDrugDetails;
    private PurchaseOrderWarehousing purchaseOrderWarehousing;
    private StatementsDetail statementsDetail;

    public StatementsDetail getStatementsDetail() {
        return statementsDetail;
    }

    public void setStatementsDetail(StatementsDetail statementsDetail) {
        this.statementsDetail = statementsDetail;
    }

    public CreditOrderDetail getCreditOrderDetail() {
        return creditOrderDetail;
    }

    public void setCreditOrderDetail(CreditOrderDetail creditOrderDetail) {
        this.creditOrderDetail = creditOrderDetail;
    }

    public PurchaseOrderDrugDetails getPurchaseOrderDrugDetails() {
        return purchaseOrderDrugDetails;
    }

    public void setPurchaseOrderDrugDetails(PurchaseOrderDrugDetails purchaseOrderDrugDetails) {
        this.purchaseOrderDrugDetails = purchaseOrderDrugDetails;
    }

    public PurchaseOrderWarehousing getPurchaseOrderWarehousing() {
        return purchaseOrderWarehousing;
    }

    public void setPurchaseOrderWarehousing(PurchaseOrderWarehousing purchaseOrderWarehousing) {
        this.purchaseOrderWarehousing = purchaseOrderWarehousing;
    }

    @Override
    public Object clone(){
        Combination combination = null;
        try {
            combination=(Combination)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        combination.purchaseOrderDrugDetails=(PurchaseOrderDrugDetails)purchaseOrderDrugDetails.clone();
        combination.purchaseOrderWarehousing=(PurchaseOrderWarehousing)purchaseOrderWarehousing.clone();
        combination.creditOrderDetail=(CreditOrderDetail)creditOrderDetail.clone();
        return combination;
    }


}
