package com.yaopin.entity;

public class CreditOrderDetail implements Cloneable {
    private Integer id;

    private Integer purchaseOrderId;
    //采购单
    private PurchaseNote purchaseNote;

    private Integer durgInformationId;
    //药品信息
    private DrugInformation drugInformation;

    private Integer creditOrderId;
    //退货单信息
    private CreditOrder creditOrder;

    private Integer returnNum;

    private Float returnSum;

    private Integer state;

    private String returnCause;

    private Integer changeNum;
    
   
   

    public PurchaseNote getPurchaseNote() {
        return purchaseNote;
    }

    public void setPurchaseNote(PurchaseNote purchaseNote) {
        this.purchaseNote = purchaseNote;
    }

    public DrugInformation getDrugInformation() {
        return drugInformation;
    }

    public void setDrugInformation(DrugInformation drugInformation) {
        this.drugInformation = drugInformation;
    }

    public CreditOrder getCreditOrder() {
        return creditOrder;
    }

    public void setCreditOrder(CreditOrder creditOrder) {
        this.creditOrder = creditOrder;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(Integer purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Integer getDurgInformationId() {
        return durgInformationId;
    }

    public void setDurgInformationId(Integer durgInformationId) {
        this.durgInformationId = durgInformationId;
    }

    public Integer getCreditOrderId() {
        return creditOrderId;
    }

    public void setCreditOrderId(Integer creditOrderId) {
        this.creditOrderId = creditOrderId;
    }

    public Integer getReturnNum() {
        return returnNum;
    }

    public void setReturnNum(Integer returnNum) {
        this.returnNum = returnNum;
    }

    public Float getReturnSum() {
        return returnSum;
    }

    public void setReturnSum(Float returnSum) {
        this.returnSum = returnSum;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getReturnCause() {
        return returnCause;
    }

    public void setReturnCause(String returnCause) {
        this.returnCause = returnCause == null ? null : returnCause.trim();
    }

    public Integer getChangeNum() {
        return changeNum;
    }

    public void setChangeNum(Integer changeNum) {
        this.changeNum = changeNum;
    }
    @Override
    public Object clone(){
        CreditOrderDetail creditOrderDetail = null;
        try {
            creditOrderDetail = (CreditOrderDetail) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        creditOrderDetail.creditOrder = (CreditOrder) creditOrder.clone();

        creditOrderDetail.drugInformation = (DrugInformation) drugInformation.clone();

        creditOrderDetail.purchaseNote = (PurchaseNote) purchaseNote.clone();
        return creditOrderDetail;
    }
}