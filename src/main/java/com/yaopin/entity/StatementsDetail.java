package com.yaopin.entity;

public class StatementsDetail implements Cloneable{
    private Integer id;

    private Integer purchaseOrderId;
    private PurchaseNote purchaseNote;

    private Integer durgInformationId;
    private DrugInformation drugInformation;

    private Integer statementsId;
    private Statements statements;

    private Integer settlementAmount;

    private Float settlementSum;

    private Integer state;

    public PurchaseNote getPurchaseNote() {
        return purchaseNote;
    }

    public void setPurchaseNote(PurchaseNote purchaseNote) {
        this.purchaseNote = purchaseNote;
    }

    public DrugInformation getDrugInformation() {
        return drugInformation;
    }

    public void setDrugInformation(DrugInformation drugInformation) {
        this.drugInformation = drugInformation;
    }

    public Statements getStatements() {
        return statements;
    }

    public void setStatements(Statements statements) {
        this.statements = statements;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(Integer purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Integer getDurgInformationId() {
        return durgInformationId;
    }

    public void setDurgInformationId(Integer durgInformationId) {
        this.durgInformationId = durgInformationId;
    }

    public Integer getStatementsId() {
        return statementsId;
    }

    public void setStatementsId(Integer statementsId) {
        this.statementsId = statementsId;
    }

    public Integer getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(Integer settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public Float getSettlementSum() {
        return settlementSum;
    }

    public void setSettlementSum(Float settlementSum) {
        this.settlementSum = settlementSum;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public Object clone()  {
        StatementsDetail statementsDetail=null;
        try {
            statementsDetail = (StatementsDetail)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        statementsDetail.purchaseNote=(PurchaseNote)purchaseNote.clone();
        statementsDetail.drugInformation=(DrugInformation)drugInformation.clone();
        statementsDetail.statements=(Statements)statements.clone();
        return statementsDetail;
    }
}