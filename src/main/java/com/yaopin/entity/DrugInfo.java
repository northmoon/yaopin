package com.yaopin.entity;

import java.io.Serializable;
import java.util.Date;

public class DrugInfo implements Serializable {
    private DrugInformation drugInformation;
    private DrugCategory drugCategory;

    public String getDrugClassification() {
        return drugClassification;
    }

    public void setDrugClassification(String drugClassification) {
        this.drugClassification = drugClassification;
    }

    public String getQualityLevel() {
        return qualityLevel;
    }

    public void setQualityLevel(String qualityLevel) {
        this.qualityLevel = qualityLevel;
    }

    private String drugClassification;
    private String qualityLevel;
    private Float minPrice;
    private Float maxPrice;

    public DrugInformation getDrugInformation() {
        return drugInformation;
    }

    public void setDrugInformation(DrugInformation drugInformation) {
        this.drugInformation = drugInformation;
    }

    public DrugCategory getDrugCategory() {
        return drugCategory;
    }

    public void setDrugCategory(DrugCategory drugCategory) {
        this.drugCategory = drugCategory;
    }

    public Float getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Float minPrice) {
        this.minPrice = minPrice;
    }

    public Float getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Float maxPrice) {
        this.maxPrice = maxPrice;
    }

    @Override
    public String toString() {
        return "DrugInfo{" +
                "drugInformation=" + drugInformation +
                ", drugCategory=" + drugCategory +
                ", drugClassification='" + drugClassification + '\'' +
                ", qualityLevel='" + qualityLevel + '\'' +
                ", minPrice=" + minPrice +
                ", maxPrice=" + maxPrice +
                '}';
    }
}
