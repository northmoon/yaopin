package com.yaopin.entity;

import java.util.ArrayList;
import java.util.List;

public class HospitalExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HospitalExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHospitalNameIsNull() {
            addCriterion("hospital_name is null");
            return (Criteria) this;
        }

        public Criteria andHospitalNameIsNotNull() {
            addCriterion("hospital_name is not null");
            return (Criteria) this;
        }

        public Criteria andHospitalNameEqualTo(String value) {
            addCriterion("hospital_name =", value, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameNotEqualTo(String value) {
            addCriterion("hospital_name <>", value, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameGreaterThan(String value) {
            addCriterion("hospital_name >", value, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameGreaterThanOrEqualTo(String value) {
            addCriterion("hospital_name >=", value, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameLessThan(String value) {
            addCriterion("hospital_name <", value, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameLessThanOrEqualTo(String value) {
            addCriterion("hospital_name <=", value, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameLike(String value) {
            addCriterion("hospital_name like", value, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameNotLike(String value) {
            addCriterion("hospital_name not like", value, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameIn(List<String> values) {
            addCriterion("hospital_name in", values, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameNotIn(List<String> values) {
            addCriterion("hospital_name not in", values, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameBetween(String value1, String value2) {
            addCriterion("hospital_name between", value1, value2, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andHospitalNameNotBetween(String value1, String value2) {
            addCriterion("hospital_name not between", value1, value2, "hospitalName");
            return (Criteria) this;
        }

        public Criteria andMailingAddressIsNull() {
            addCriterion("mailing_address is null");
            return (Criteria) this;
        }

        public Criteria andMailingAddressIsNotNull() {
            addCriterion("mailing_address is not null");
            return (Criteria) this;
        }

        public Criteria andMailingAddressEqualTo(String value) {
            addCriterion("mailing_address =", value, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressNotEqualTo(String value) {
            addCriterion("mailing_address <>", value, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressGreaterThan(String value) {
            addCriterion("mailing_address >", value, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressGreaterThanOrEqualTo(String value) {
            addCriterion("mailing_address >=", value, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressLessThan(String value) {
            addCriterion("mailing_address <", value, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressLessThanOrEqualTo(String value) {
            addCriterion("mailing_address <=", value, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressLike(String value) {
            addCriterion("mailing_address like", value, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressNotLike(String value) {
            addCriterion("mailing_address not like", value, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressIn(List<String> values) {
            addCriterion("mailing_address in", values, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressNotIn(List<String> values) {
            addCriterion("mailing_address not in", values, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressBetween(String value1, String value2) {
            addCriterion("mailing_address between", value1, value2, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andMailingAddressNotBetween(String value1, String value2) {
            addCriterion("mailing_address not between", value1, value2, "mailingAddress");
            return (Criteria) this;
        }

        public Criteria andZipCodeIsNull() {
            addCriterion("zip_code is null");
            return (Criteria) this;
        }

        public Criteria andZipCodeIsNotNull() {
            addCriterion("zip_code is not null");
            return (Criteria) this;
        }

        public Criteria andZipCodeEqualTo(String value) {
            addCriterion("zip_code =", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeNotEqualTo(String value) {
            addCriterion("zip_code <>", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeGreaterThan(String value) {
            addCriterion("zip_code >", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeGreaterThanOrEqualTo(String value) {
            addCriterion("zip_code >=", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeLessThan(String value) {
            addCriterion("zip_code <", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeLessThanOrEqualTo(String value) {
            addCriterion("zip_code <=", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeLike(String value) {
            addCriterion("zip_code like", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeNotLike(String value) {
            addCriterion("zip_code not like", value, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeIn(List<String> values) {
            addCriterion("zip_code in", values, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeNotIn(List<String> values) {
            addCriterion("zip_code not in", values, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeBetween(String value1, String value2) {
            addCriterion("zip_code between", value1, value2, "zipCode");
            return (Criteria) this;
        }

        public Criteria andZipCodeNotBetween(String value1, String value2) {
            addCriterion("zip_code not between", value1, value2, "zipCode");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNull() {
            addCriterion("area_id is null");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNotNull() {
            addCriterion("area_id is not null");
            return (Criteria) this;
        }

        public Criteria andAreaIdEqualTo(Integer value) {
            addCriterion("area_id =", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotEqualTo(Integer value) {
            addCriterion("area_id <>", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThan(Integer value) {
            addCriterion("area_id >", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("area_id >=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThan(Integer value) {
            addCriterion("area_id <", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThanOrEqualTo(Integer value) {
            addCriterion("area_id <=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdIn(List<Integer> values) {
            addCriterion("area_id in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotIn(List<Integer> values) {
            addCriterion("area_id not in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdBetween(Integer value1, Integer value2) {
            addCriterion("area_id between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotBetween(Integer value1, Integer value2) {
            addCriterion("area_id not between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelIsNull() {
            addCriterion("hospital_level is null");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelIsNotNull() {
            addCriterion("hospital_level is not null");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelEqualTo(String value) {
            addCriterion("hospital_level =", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelNotEqualTo(String value) {
            addCriterion("hospital_level <>", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelGreaterThan(String value) {
            addCriterion("hospital_level >", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelGreaterThanOrEqualTo(String value) {
            addCriterion("hospital_level >=", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelLessThan(String value) {
            addCriterion("hospital_level <", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelLessThanOrEqualTo(String value) {
            addCriterion("hospital_level <=", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelLike(String value) {
            addCriterion("hospital_level like", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelNotLike(String value) {
            addCriterion("hospital_level not like", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelIn(List<String> values) {
            addCriterion("hospital_level in", values, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelNotIn(List<String> values) {
            addCriterion("hospital_level not in", values, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelBetween(String value1, String value2) {
            addCriterion("hospital_level between", value1, value2, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelNotBetween(String value1, String value2) {
            addCriterion("hospital_level not between", value1, value2, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andBedNumIsNull() {
            addCriterion("bed_num is null");
            return (Criteria) this;
        }

        public Criteria andBedNumIsNotNull() {
            addCriterion("bed_num is not null");
            return (Criteria) this;
        }

        public Criteria andBedNumEqualTo(Integer value) {
            addCriterion("bed_num =", value, "bedNum");
            return (Criteria) this;
        }

        public Criteria andBedNumNotEqualTo(Integer value) {
            addCriterion("bed_num <>", value, "bedNum");
            return (Criteria) this;
        }

        public Criteria andBedNumGreaterThan(Integer value) {
            addCriterion("bed_num >", value, "bedNum");
            return (Criteria) this;
        }

        public Criteria andBedNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("bed_num >=", value, "bedNum");
            return (Criteria) this;
        }

        public Criteria andBedNumLessThan(Integer value) {
            addCriterion("bed_num <", value, "bedNum");
            return (Criteria) this;
        }

        public Criteria andBedNumLessThanOrEqualTo(Integer value) {
            addCriterion("bed_num <=", value, "bedNum");
            return (Criteria) this;
        }

        public Criteria andBedNumIn(List<Integer> values) {
            addCriterion("bed_num in", values, "bedNum");
            return (Criteria) this;
        }

        public Criteria andBedNumNotIn(List<Integer> values) {
            addCriterion("bed_num not in", values, "bedNum");
            return (Criteria) this;
        }

        public Criteria andBedNumBetween(Integer value1, Integer value2) {
            addCriterion("bed_num between", value1, value2, "bedNum");
            return (Criteria) this;
        }

        public Criteria andBedNumNotBetween(Integer value1, Integer value2) {
            addCriterion("bed_num not between", value1, value2, "bedNum");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitIsNull() {
            addCriterion("isnonprofit is null");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitIsNotNull() {
            addCriterion("isnonprofit is not null");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitEqualTo(Integer value) {
            addCriterion("isnonprofit =", value, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitNotEqualTo(Integer value) {
            addCriterion("isnonprofit <>", value, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitGreaterThan(Integer value) {
            addCriterion("isnonprofit >", value, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitGreaterThanOrEqualTo(Integer value) {
            addCriterion("isnonprofit >=", value, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitLessThan(Integer value) {
            addCriterion("isnonprofit <", value, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitLessThanOrEqualTo(Integer value) {
            addCriterion("isnonprofit <=", value, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitIn(List<Integer> values) {
            addCriterion("isnonprofit in", values, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitNotIn(List<Integer> values) {
            addCriterion("isnonprofit not in", values, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitBetween(Integer value1, Integer value2) {
            addCriterion("isnonprofit between", value1, value2, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andIsnonprofitNotBetween(Integer value1, Integer value2) {
            addCriterion("isnonprofit not between", value1, value2, "isnonprofit");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andFaxIsNull() {
            addCriterion("fax is null");
            return (Criteria) this;
        }

        public Criteria andFaxIsNotNull() {
            addCriterion("fax is not null");
            return (Criteria) this;
        }

        public Criteria andFaxEqualTo(String value) {
            addCriterion("fax =", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotEqualTo(String value) {
            addCriterion("fax <>", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxGreaterThan(String value) {
            addCriterion("fax >", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxGreaterThanOrEqualTo(String value) {
            addCriterion("fax >=", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxLessThan(String value) {
            addCriterion("fax <", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxLessThanOrEqualTo(String value) {
            addCriterion("fax <=", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxLike(String value) {
            addCriterion("fax like", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotLike(String value) {
            addCriterion("fax not like", value, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxIn(List<String> values) {
            addCriterion("fax in", values, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotIn(List<String> values) {
            addCriterion("fax not in", values, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxBetween(String value1, String value2) {
            addCriterion("fax between", value1, value2, "fax");
            return (Criteria) this;
        }

        public Criteria andFaxNotBetween(String value1, String value2) {
            addCriterion("fax not between", value1, value2, "fax");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneIsNull() {
            addCriterion("pharmacy_phone is null");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneIsNotNull() {
            addCriterion("pharmacy_phone is not null");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneEqualTo(String value) {
            addCriterion("pharmacy_phone =", value, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneNotEqualTo(String value) {
            addCriterion("pharmacy_phone <>", value, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneGreaterThan(String value) {
            addCriterion("pharmacy_phone >", value, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("pharmacy_phone >=", value, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneLessThan(String value) {
            addCriterion("pharmacy_phone <", value, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneLessThanOrEqualTo(String value) {
            addCriterion("pharmacy_phone <=", value, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneLike(String value) {
            addCriterion("pharmacy_phone like", value, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneNotLike(String value) {
            addCriterion("pharmacy_phone not like", value, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneIn(List<String> values) {
            addCriterion("pharmacy_phone in", values, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneNotIn(List<String> values) {
            addCriterion("pharmacy_phone not in", values, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneBetween(String value1, String value2) {
            addCriterion("pharmacy_phone between", value1, value2, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andPharmacyPhoneNotBetween(String value1, String value2) {
            addCriterion("pharmacy_phone not between", value1, value2, "pharmacyPhone");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyIsNull() {
            addCriterion("drug_revenue_ly is null");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyIsNotNull() {
            addCriterion("drug_revenue_ly is not null");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyEqualTo(Float value) {
            addCriterion("drug_revenue_ly =", value, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyNotEqualTo(Float value) {
            addCriterion("drug_revenue_ly <>", value, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyGreaterThan(Float value) {
            addCriterion("drug_revenue_ly >", value, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyGreaterThanOrEqualTo(Float value) {
            addCriterion("drug_revenue_ly >=", value, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyLessThan(Float value) {
            addCriterion("drug_revenue_ly <", value, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyLessThanOrEqualTo(Float value) {
            addCriterion("drug_revenue_ly <=", value, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyIn(List<Float> values) {
            addCriterion("drug_revenue_ly in", values, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyNotIn(List<Float> values) {
            addCriterion("drug_revenue_ly not in", values, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyBetween(Float value1, Float value2) {
            addCriterion("drug_revenue_ly between", value1, value2, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andDrugRevenueLyNotBetween(Float value1, Float value2) {
            addCriterion("drug_revenue_ly not between", value1, value2, "drugRevenueLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyIsNull() {
            addCriterion("business_income_ly is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyIsNotNull() {
            addCriterion("business_income_ly is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyEqualTo(Float value) {
            addCriterion("business_income_ly =", value, "businessIncomeLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyNotEqualTo(Float value) {
            addCriterion("business_income_ly <>", value, "businessIncomeLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyGreaterThan(Float value) {
            addCriterion("business_income_ly >", value, "businessIncomeLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyGreaterThanOrEqualTo(Float value) {
            addCriterion("business_income_ly >=", value, "businessIncomeLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyLessThan(Float value) {
            addCriterion("business_income_ly <", value, "businessIncomeLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyLessThanOrEqualTo(Float value) {
            addCriterion("business_income_ly <=", value, "businessIncomeLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyIn(List<Float> values) {
            addCriterion("business_income_ly in", values, "businessIncomeLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyNotIn(List<Float> values) {
            addCriterion("business_income_ly not in", values, "businessIncomeLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyBetween(Float value1, Float value2) {
            addCriterion("business_income_ly between", value1, value2, "businessIncomeLy");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLyNotBetween(Float value1, Float value2) {
            addCriterion("business_income_ly not between", value1, value2, "businessIncomeLy");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}