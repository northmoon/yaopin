package com.yaopin.entity;

import java.io.Serializable;
import java.util.Date;

public class DrugInformation implements Serializable, Cloneable {
    private Integer id;

    private Integer drugCategoryId;

  

    private String serialNum;

    private String enterpriseName;

    private String drugName;

    private Float biddingPrice;

    private String imgPath;

    private String approvalNum;

    private Date validity;

    private Integer isimport;

    private String packingMaterial;

    private String packingUnit;

    private Float retailPrice;

    private String priceSource;

    private Integer qualityLevelId;

    private Integer issurveyreport;

    private String reportNum;

    private Date reportValidity;

    private String description;

    private Integer state;
    private QualityLevel qualityLevel;

    private DrugCategory drugCategory;

    

    private Float maxPrice;
    
    private Float minPrice;
    

    public Float getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Float maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Float getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Float minPrice) {
        this.minPrice = minPrice;
    }
    public QualityLevel getQualityLevel() {
        return qualityLevel;
    }

    public void setQualityLevel(QualityLevel qualityLevel) {
        this.qualityLevel = qualityLevel;
    }

  
    public DrugCategory getDrugCategory() {
        return drugCategory;
    }

    public void setDrugCategory(DrugCategory drugCategory) {
        this.drugCategory = drugCategory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDrugCategoryId() {
        return drugCategoryId;
    }

    public void setDrugCategoryId(Integer drugCategoryId) {
        this.drugCategoryId = drugCategoryId;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum == null ? null : serialNum.trim();
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName == null ? null : enterpriseName.trim();
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName == null ? null : drugName.trim();
    }

    public Float getBiddingPrice() {
        return biddingPrice;
    }

    public void setBiddingPrice(Float biddingPrice) {
        this.biddingPrice = biddingPrice;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath == null ? null : imgPath.trim();
    }

    public String getApprovalNum() {
        return approvalNum;
    }

    public void setApprovalNum(String approvalNum) {
        this.approvalNum = approvalNum == null ? null : approvalNum.trim();
    }

    public Date getValidity() {
        return validity;
    }

    public void setValidity(Date validity) {
        this.validity = validity;
    }

    public Integer getIsimport() {
        return isimport;
    }

    public void setIsimport(Integer isimport) {
        this.isimport = isimport;
    }

    public String getPackingMaterial() {
        return packingMaterial;
    }

    public void setPackingMaterial(String packingMaterial) {
        this.packingMaterial = packingMaterial == null ? null : packingMaterial.trim();
    }

    public String getPackingUnit() {
        return packingUnit;
    }

    public void setPackingUnit(String packingUnit) {
        this.packingUnit = packingUnit == null ? null : packingUnit.trim();
    }

    public Float getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(Float retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getPriceSource() {
        return priceSource;
    }

    public void setPriceSource(String priceSource) {
        this.priceSource = priceSource == null ? null : priceSource.trim();
    }

    public Integer getQualityLevelId() {
        return qualityLevelId;
    }

    public void setQualityLevelId(Integer qualityLevelId) {
        this.qualityLevelId = qualityLevelId;
    }

    public Integer getIssurveyreport() {
        return issurveyreport;
    }

    public void setIssurveyreport(Integer issurveyreport) {
        this.issurveyreport = issurveyreport;
    }

    public String getReportNum() {
        return reportNum;
    }

    public void setReportNum(String reportNum) {
        this.reportNum = reportNum == null ? null : reportNum.trim();
    }

    public Date getReportValidity() {
        return reportValidity;
    }

    public void setReportValidity(Date reportValidity) {
        this.reportValidity = reportValidity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "DrugInformation{" +
                "id=" + id +
                ", drugCategoryId=" + drugCategoryId +
                ", serialNum='" + serialNum + '\'' +
                ", enterpriseName='" + enterpriseName + '\'' +
                ", drugName='" + drugName + '\'' +
                ", biddingPrice=" + biddingPrice +
                ", imgPath='" + imgPath + '\'' +
                ", approvalNum='" + approvalNum + '\'' +
                ", validity=" + validity +
                ", isimport=" + isimport +
                ", packingMaterial='" + packingMaterial + '\'' +
                ", packingUnit='" + packingUnit + '\'' +
                ", retailPrice=" + retailPrice +
                ", priceSource='" + priceSource + '\'' +
                ", qualityLevelId=" + qualityLevelId +
                ", issurveyreport=" + issurveyreport +
                ", reportNum='" + reportNum + '\'' +
                ", reportValidity=" + reportValidity +
                ", description='" + description + '\'' +
                ", state=" + state +
                '}';
    }

    public Object clone() {
        DrugInformation drugInformation = null;
        try {
            drugInformation = (DrugInformation) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        if(drugCategory !=null){
            drugInformation.drugCategory = (DrugCategory) drugCategory.clone();
        }
       if(qualityLevel != null) {
           drugInformation.qualityLevel = (QualityLevel) qualityLevel.clone();
       }
        return drugInformation;
    }
}