package com.yaopin.entity;

import java.util.Date;

public class SearchSettlement implements  Cloneable{
    private PurchaseNote purchaseNote;

    private DrugCategory drugCategory;

    private DrugInformation drugInformation;

    private Statements statements;

    private StatementsDetail statementsDetail;

    private String hospitalname;

    private Date purchasestartTime;

    private Date purchaseendTime;

    private Date settlementstartTime;

    private Date settlementendTime;

    public Date getPurchasestartTime() {
        return purchasestartTime;
    }

    public void setPurchasestartTime(Date purchasestartTime) {
        this.purchasestartTime = purchasestartTime;
    }

    public Date getPurchaseendTime() {
        return purchaseendTime;
    }

    public void setPurchaseendTime(Date purchaseendTime) {
        this.purchaseendTime = purchaseendTime;
    }

    public PurchaseNote getPurchaseNote() {
        return purchaseNote;
    }

    public void setPurchaseNote(PurchaseNote purchaseNote) {
        this.purchaseNote = purchaseNote;
    }

    public DrugCategory getDrugCategory() {
        return drugCategory;
    }

    public void setDrugCategory(DrugCategory drugCategory) {
        this.drugCategory = drugCategory;
    }

    public DrugInformation getDrugInformation() {
        return drugInformation;
    }

    public void setDrugInformation(DrugInformation drugInformation) {
        this.drugInformation = drugInformation;
    }


    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname;
    }

    public Statements getStatements() {
        return statements;
    }

    public void setStatements(Statements statements) {
        this.statements = statements;
    }

    public StatementsDetail getStatementsDetail() {
        return statementsDetail;
    }

    public void setStatementsDetail(StatementsDetail statementsDetail) {
        this.statementsDetail = statementsDetail;
    }

    public Date getSettlementstartTime() {
        return settlementstartTime;
    }

    public void setSettlementstartTime(Date settlementstartTime) {
        this.settlementstartTime = settlementstartTime;
    }

    public Date getSettlementendTime() {
        return settlementendTime;
    }

    public void setSettlementendTime(Date settlementendTime) {
        this.settlementendTime = settlementendTime;
    }

    @Override
    public Object clone()  {
        SearchSettlement searchSettlement=null;
        try {
            searchSettlement=(SearchSettlement)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        if(drugCategory !=null){
            searchSettlement.drugCategory=(DrugCategory)drugCategory.clone();
        }
        if(drugCategory !=null){
            searchSettlement.drugInformation=(DrugInformation)drugInformation.clone();
        }
        if(drugCategory !=null){
            searchSettlement.purchaseNote=(PurchaseNote)purchaseNote.clone();
        }
        if(drugCategory !=null){
            searchSettlement.statements=(Statements)statements.clone();
        }
        return searchSettlement;
    }
}
