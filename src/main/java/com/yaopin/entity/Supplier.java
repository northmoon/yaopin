package com.yaopin.entity;

import java.util.Date;

public class Supplier implements Cloneable {
    private Integer id;

    private String firmName;

    private Integer firmType;

    private String licence;

    private Date licenceValidity;

    private String linkman;

    private String phone;

    private String businessScope;

    private String registerAddress;

    private String contactAddress;

    private String zipCode;

    private Float totalAssets;

    private String fax;

    private String legalName;

    private String legalIdcard;

    private Float registerAssets;

    private Float salesAmountLy;

    private String email;

    private String website;

    private String enterpriseCode;

    private Date ecValidity;

    private String businessLicense;

    private Date blValidity;

    private Integer islc;

    private String lcNum;

    private String lcValidity;

    private Float fixedAssets;

    private String introduced;

    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName == null ? null : firmName.trim();
    }

    public Integer getFirmType() {
        return firmType;
    }

    public void setFirmType(Integer firmType) {
        this.firmType = firmType;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence == null ? null : licence.trim();
    }

    public Date getLicenceValidity() {
        return licenceValidity;
    }

    public void setLicenceValidity(Date licenceValidity) {
        this.licenceValidity = licenceValidity;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman == null ? null : linkman.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getBusinessScope() {
        return businessScope;
    }

    public void setBusinessScope(String businessScope) {
        this.businessScope = businessScope == null ? null : businessScope.trim();
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress == null ? null : registerAddress.trim();
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress == null ? null : contactAddress.trim();
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode == null ? null : zipCode.trim();
    }

    public Float getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(Float totalAssets) {
        this.totalAssets = totalAssets;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax == null ? null : fax.trim();
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName == null ? null : legalName.trim();
    }

    public String getLegalIdcard() {
        return legalIdcard;
    }

    public void setLegalIdcard(String legalIdcard) {
        this.legalIdcard = legalIdcard == null ? null : legalIdcard.trim();
    }

    public Float getRegisterAssets() {
        return registerAssets;
    }

    public void setRegisterAssets(Float registerAssets) {
        this.registerAssets = registerAssets;
    }

    public Float getSalesAmountLy() {
        return salesAmountLy;
    }

    public void setSalesAmountLy(Float salesAmountLy) {
        this.salesAmountLy = salesAmountLy;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website == null ? null : website.trim();
    }

    public String getEnterpriseCode() {
        return enterpriseCode;
    }

    public void setEnterpriseCode(String enterpriseCode) {
        this.enterpriseCode = enterpriseCode == null ? null : enterpriseCode.trim();
    }

    public Date getEcValidity() {
        return ecValidity;
    }

    public void setEcValidity(Date ecValidity) {
        this.ecValidity = ecValidity;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense == null ? null : businessLicense.trim();
    }

    public Date getBlValidity() {
        return blValidity;
    }

    public void setBlValidity(Date blValidity) {
        this.blValidity = blValidity;
    }

    public Integer getIslc() {
        return islc;
    }

    public void setIslc(Integer islc) {
        this.islc = islc;
    }

    public String getLcNum() {
        return lcNum;
    }

    public void setLcNum(String lcNum) {
        this.lcNum = lcNum == null ? null : lcNum.trim();
    }

    public String getLcValidity() {
        return lcValidity;
    }

    public void setLcValidity(String lcValidity) {
        this.lcValidity = lcValidity == null ? null : lcValidity.trim();
    }

    public Float getFixedAssets() {
        return fixedAssets;
    }

    public void setFixedAssets(Float fixedAssets) {
        this.fixedAssets = fixedAssets;
    }

    public String getIntroduced() {
        return introduced;
    }

    public void setIntroduced(String introduced) {
        this.introduced = introduced == null ? null : introduced.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    @Override
    public Object clone()  {
        Supplier supplier=null;
        try {
            supplier = (Supplier) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return supplier;
    }
}