package com.yaopin.entity;

/**
 * @ClassName 入库退货封装类
 * @Author donghongyu
 * @Date 2019/9/20 15:10
 **/
public class WarehousingReturns {
    private CreditOrderDetail creditOrderDetail;
    private PurchaseOrderWarehousing purchaseOrderWarehousing;

    public CreditOrderDetail getCreditOrderDetail() {
        return creditOrderDetail;
    }

    public void setCreditOrderDetail(CreditOrderDetail creditOrderDetail) {
        this.creditOrderDetail = creditOrderDetail;
    }

    public PurchaseOrderWarehousing getPurchaseOrderWarehousing() {
        return purchaseOrderWarehousing;
    }

    public void setPurchaseOrderWarehousing(PurchaseOrderWarehousing purchaseOrderWarehousing) {
        this.purchaseOrderWarehousing = purchaseOrderWarehousing;
    }
}
