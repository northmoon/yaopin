package com.yaopin.entity;

import java.util.Date;

public class PurchaseOrderWarehousing implements Cloneable {
    private Integer id;

    private Integer purchaseOrderId;
    private PurchaseNote purchaseNote;

    private Integer durgInformationId;
    private DrugInformation drugInformation;

    private Integer receipt;

    private Float inventoryAmount;

    private String invoiceNum;

    private String batchNum;

    private Integer drugValidity;

    private Date entryTime;

    private Integer chanseNum;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(Integer purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Integer getDurgInformationId() {
        return durgInformationId;
    }

    public void setDurgInformationId(Integer durgInformationId) {
        this.durgInformationId = durgInformationId;
    }

    public Integer getReceipt() {
        return receipt;
    }

    public void setReceipt(Integer receipt) {
        this.receipt = receipt;
    }

    public Float getInventoryAmount() {
        return inventoryAmount;
    }

    public void setInventoryAmount(Float inventoryAmount) {
        this.inventoryAmount = inventoryAmount;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum == null ? null : invoiceNum.trim();
    }

    public String getBatchNum() {
        return batchNum;
    }

    public void setBatchNum(String batchNum) {
        this.batchNum = batchNum == null ? null : batchNum.trim();
    }

    public Integer getDrugValidity() {
        return drugValidity;
    }

    public void setDrugValidity(Integer drugValidity) {
        this.drugValidity = drugValidity;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public Integer getChanseNum() {
        return chanseNum;
    }

    public void setChanseNum(Integer chanseNum) {
        this.chanseNum = chanseNum;
    }

    @Override
    public Object clone(){
        PurchaseOrderWarehousing purchaseOrderWarehousing = null;
        try {
            purchaseOrderWarehousing = (PurchaseOrderWarehousing) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        purchaseOrderWarehousing.purchaseNote = (PurchaseNote) purchaseNote.clone();
        purchaseOrderWarehousing.drugInformation = (DrugInformation) drugInformation.clone();
        return purchaseOrderWarehousing;
    }
}