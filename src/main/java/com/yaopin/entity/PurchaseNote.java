package com.yaopin.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class PurchaseNote implements Cloneable {
    private Integer id;

    private String purchaseNumber;

    private String purchaseOrderName;

    private Integer hospitalId;

    private String linkman;

    private String phone;

    private Date buildSingle;
    private String buildSingleStr;

    public String getBuildSingleStr() {
        return DateUtil.formatDateToStr(buildSingle);
    }

    public void setBuildSingleStr(String buildSingleStr) {
        this.buildSingleStr = buildSingleStr;
    }

    private Date recentChanges;

    private Date submit;

    private String remark;

    private String purchaseOrderStatus;

    private String auditor;

    private String auditOpinion;

    private Date auditDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;


    private Hospital hospital;

    private Integer changeNum;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPurchaseNumber() {
        return purchaseNumber;
    }

    public void setPurchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber == null ? null : purchaseNumber.trim();
    }

    public String getPurchaseOrderName() {
        return purchaseOrderName;
    }

    public void setPurchaseOrderName(String purchaseOrderName) {
        this.purchaseOrderName = purchaseOrderName == null ? null : purchaseOrderName.trim();
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman == null ? null : linkman.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBuildSingle() {
        return buildSingle;
    }

    public void setBuildSingle(Date buildSingle) {
        this.buildSingle = buildSingle;
    }

    public Date getRecentChanges() {
        return recentChanges;
    }

    public void setRecentChanges(Date recentChanges) {
        this.recentChanges = recentChanges;
    }

    public Date getSubmit() {
        return submit;
    }

    public void setSubmit(Date submit) {
        this.submit = submit;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getPurchaseOrderStatus() {
        return purchaseOrderStatus;
    }

    public void setPurchaseOrderStatus(String purchaseOrderStatus) {
        this.purchaseOrderStatus = purchaseOrderStatus == null ? null : purchaseOrderStatus.trim();
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor == null ? null : auditor.trim();
    }

    public String getAuditOpinion() {
        return auditOpinion;
    }

    public void setAuditOpinion(String auditOpinion) {
        this.auditOpinion = auditOpinion == null ? null : auditOpinion.trim();
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public Integer getChangeNum() {
        return changeNum;
    }

    public void setChangeNum(Integer changeNum) {
        this.changeNum = changeNum;
    }
    @Override
    public Object clone() {
        PurchaseNote purchaseNote = null;
        try {
            purchaseNote = (PurchaseNote) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        purchaseNote.hospital = (Hospital) hospital.clone();
        return purchaseNote;
    }
}