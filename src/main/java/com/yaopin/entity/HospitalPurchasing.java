package com.yaopin.entity;

public class HospitalPurchasing implements Cloneable{
    private Integer id;

    private Integer hospitalId;
	
	private Integer supplierListId;
		
	
	
	private Integer drugInformationId;

    private Integer suppliersId;
    public SupplierList getSupplierList() {
        return supplierList;
    }

   
	
    public void setSupplierList(SupplierList supplierList) {
        this.supplierList = supplierList;
    }
    private SupplierList supplierList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Integer getSupplierListId() {
        return supplierListId;
    }
    public void setSupplierListId(Integer supplierListId) {
        this.supplierListId = supplierListId;
    
    }

    public Integer getDrugInformationId() {
        return drugInformationId;
    }

    public void setDrugInformationId(Integer drugInformationId) {
        this.drugInformationId = drugInformationId;
    }

    public Integer getSuppliersId() {
        return suppliersId;
    }

    public void setSuppliersId(Integer suppliersId) {
        this.suppliersId = suppliersId;
    }

    

    

    
	@Override
    public Object clone() {
        HospitalPurchasing hospitalPurchasing=null;
        try {
            hospitalPurchasing=(HospitalPurchasing) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        hospitalPurchasing.supplierList=(SupplierList)supplierList.clone();
        return hospitalPurchasing;
    }
}