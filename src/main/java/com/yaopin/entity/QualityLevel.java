package com.yaopin.entity;

public class QualityLevel implements Cloneable {
    private Integer id;

    private String qualityLevel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQualityLevel() {
        return qualityLevel;
    }

    public void setQualityLevel(String qualityLevel) {
        this.qualityLevel = qualityLevel == null ? null : qualityLevel.trim();
    }

    public Object clone(){
        QualityLevel qualityLevel = null;
        try{
            qualityLevel = (QualityLevel)super.clone();
        }catch (CloneNotSupportedException e){
            e.printStackTrace();
        }
        return qualityLevel;
    }
}