package com.yaopin.entity;

public class Hospital implements Cloneable{
    private Integer id;

    private String hospitalName;

    private String mailingAddress;

    private String zipCode;

    private Integer areaId;

    private String hospitalLevel;

    private Integer bedNum;

    private Integer isnonprofit;

    private String phone;

    private String fax;

    private String pharmacyPhone;

    private String type;

    private Float drugRevenueLy;

    private Float businessIncomeLy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName == null ? null : hospitalName.trim();
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress == null ? null : mailingAddress.trim();
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode == null ? null : zipCode.trim();
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getHospitalLevel() {
        return hospitalLevel;
    }

    public void setHospitalLevel(String hospitalLevel) {
        this.hospitalLevel = hospitalLevel == null ? null : hospitalLevel.trim();
    }

    public Integer getBedNum() {
        return bedNum;
    }

    public void setBedNum(Integer bedNum) {
        this.bedNum = bedNum;
    }

    public Integer getIsnonprofit() {
        return isnonprofit;
    }

    public void setIsnonprofit(Integer isnonprofit) {
        this.isnonprofit = isnonprofit;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax == null ? null : fax.trim();
    }

    public String getPharmacyPhone() {
        return pharmacyPhone;
    }

    public void setPharmacyPhone(String pharmacyPhone) {
        this.pharmacyPhone = pharmacyPhone == null ? null : pharmacyPhone.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Float getDrugRevenueLy() {
        return drugRevenueLy;
    }

    public void setDrugRevenueLy(Float drugRevenueLy) {
        this.drugRevenueLy = drugRevenueLy;
    }

    public Float getBusinessIncomeLy() {
        return businessIncomeLy;
    }

    public void setBusinessIncomeLy(Float businessIncomeLy) {
        this.businessIncomeLy = businessIncomeLy;
    }
    @Override
    public Object clone() {
        Hospital hospital = null;
        try {
            hospital=(Hospital)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return hospital;
    }
}