package com.yaopin.entity;

import java.io.Serializable;

public class DrugCategory implements Serializable, Cloneable {
    private Integer id;

    private String categoryNum;

    private String genericName;

    private String dosageForm;

    private String specification;

    private String unit;

    private Integer coefficient;

    private Integer drugClassificationId;

    private DrugClassification drugClassification;

    public DrugClassification getDrugClassification() {
        return drugClassification;
    }

    public void setDrugClassification(DrugClassification drugClassification) {
        this.drugClassification = drugClassification;
    }

    private Integer state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryNum() {
        return categoryNum;
    }

    public void setCategoryNum(String categoryNum) {
        this.categoryNum = categoryNum == null ? null : categoryNum.trim();
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName == null ? null : genericName.trim();
    }

    public String getDosageForm() {
        return dosageForm;
    }

    public void setDosageForm(String dosageForm) {
        this.dosageForm = dosageForm == null ? null : dosageForm.trim();
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification == null ? null : specification.trim();
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }

    public Integer getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Integer coefficient) {
        this.coefficient = coefficient;
    }

    public Integer getDrugClassificationId() {
        return drugClassificationId;
    }

    public void setDrugClassificationId(Integer drugClassificationId) {
        this.drugClassificationId = drugClassificationId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "DrugCategory{" +
                "id=" + id +
                ", categoryNum='" + categoryNum + '\'' +
                ", genericName='" + genericName + '\'' +
                ", dosageForm='" + dosageForm + '\'' +
                ", specification='" + specification + '\'' +
                ", unit='" + unit + '\'' +
                ", coefficient=" + coefficient +
                ", drugClassificationId=" + drugClassificationId +
                ", state=" + state +
                '}';
    }

    public Object clone() {
        DrugCategory drugCategory = null;
        try {
            drugCategory = (DrugCategory) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        if(drugClassification != null){
            drugCategory.drugClassification = (DrugClassification) drugClassification.clone();
        }
        return drugCategory;
    }

}