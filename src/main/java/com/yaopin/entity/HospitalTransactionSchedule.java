package com.yaopin.entity;

public class HospitalTransactionSchedule implements Cloneable {
    private Integer id;
	
	private Integer drugDetailsId;
	private Integer purchaseOrderDetailId;
    private PurchaseOrderDrugDetails purchaseOrderDrugDetails;

    private Integer warehouseId;
    private PurchaseOrderWarehousing purchaseOrderWarehousing;

    private Integer goodsReturnedNoteId;
    private CreditOrderDetail creditOrderDetail;

    private Integer finalStatementId;
    private StatementsDetail statementsDetail;

    public PurchaseOrderDrugDetails getPurchaseOrderDrugDetails() {
        return purchaseOrderDrugDetails;
    }

    public void setPurchaseOrderDrugDetails(PurchaseOrderDrugDetails purchaseOrderDrugDetails) {
        this.purchaseOrderDrugDetails = purchaseOrderDrugDetails;
    }

    public PurchaseOrderWarehousing getPurchaseOrderWarehousing() {
        return purchaseOrderWarehousing;
    }

    public void setPurchaseOrderWarehousing(PurchaseOrderWarehousing purchaseOrderWarehousing) {
        this.purchaseOrderWarehousing = purchaseOrderWarehousing;
    }

    public CreditOrderDetail getCreditOrderDetail() {
        return creditOrderDetail;
    }

    public void setCreditOrderDetail(CreditOrderDetail creditOrderDetail) {
        this.creditOrderDetail = creditOrderDetail;
    }

    public StatementsDetail getStatementsDetail() {
        return statementsDetail;
    }

    public void setStatementsDetail(StatementsDetail statementsDetail) {
        this.statementsDetail = statementsDetail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
   public Integer getDrugDetailsId() {
        return drugDetailsId;
    }

    public void setDrugDetailsId(Integer drugDetailsId) {
        this.drugDetailsId = drugDetailsId;
    }







    public Integer getPurchaseOrderDetailId() {
        return purchaseOrderDetailId;
    }

    public void setPurchaseOrderDetailId(Integer purchaseOrderDetailId) {
        this.purchaseOrderDetailId = purchaseOrderDetailId;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getGoodsReturnedNoteId() {
        return goodsReturnedNoteId;
    }

    public void setGoodsReturnedNoteId(Integer goodsReturnedNoteId) {
        this.goodsReturnedNoteId = goodsReturnedNoteId;
    }

    public Integer getFinalStatementId() {
        return finalStatementId;
    }

    public void setFinalStatementId(Integer finalStatementId) {
        this.finalStatementId = finalStatementId;
    }

    public Object clone() {
        HospitalTransactionSchedule hospitalTransactionSchedule = null;
        try {
            hospitalTransactionSchedule = (HospitalTransactionSchedule) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        if (creditOrderDetail != null) {
            hospitalTransactionSchedule.creditOrderDetail = (CreditOrderDetail) creditOrderDetail.clone();
        }
        if (purchaseOrderDrugDetails != null) {
            hospitalTransactionSchedule.purchaseOrderDrugDetails = (PurchaseOrderDrugDetails) purchaseOrderDrugDetails.clone();
        }
        if (statementsDetail != null) {
            hospitalTransactionSchedule.statementsDetail = (StatementsDetail) statementsDetail.clone();
        }
        if (purchaseOrderWarehousing != null) {
            hospitalTransactionSchedule.purchaseOrderWarehousing = (PurchaseOrderWarehousing) purchaseOrderWarehousing.clone();
        }
        return hospitalTransactionSchedule;
    }
}