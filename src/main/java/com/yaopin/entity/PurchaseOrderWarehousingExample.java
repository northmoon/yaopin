package com.yaopin.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PurchaseOrderWarehousingExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PurchaseOrderWarehousingExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdIsNull() {
            addCriterion("purchase_order_id is null");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdIsNotNull() {
            addCriterion("purchase_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdEqualTo(Integer value) {
            addCriterion("purchase_order_id =", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdNotEqualTo(Integer value) {
            addCriterion("purchase_order_id <>", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdGreaterThan(Integer value) {
            addCriterion("purchase_order_id >", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("purchase_order_id >=", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdLessThan(Integer value) {
            addCriterion("purchase_order_id <", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("purchase_order_id <=", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdIn(List<Integer> values) {
            addCriterion("purchase_order_id in", values, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdNotIn(List<Integer> values) {
            addCriterion("purchase_order_id not in", values, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("purchase_order_id between", value1, value2, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("purchase_order_id not between", value1, value2, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdIsNull() {
            addCriterion("durg_information_id is null");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdIsNotNull() {
            addCriterion("durg_information_id is not null");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdEqualTo(Integer value) {
            addCriterion("durg_information_id =", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdNotEqualTo(Integer value) {
            addCriterion("durg_information_id <>", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdGreaterThan(Integer value) {
            addCriterion("durg_information_id >", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("durg_information_id >=", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdLessThan(Integer value) {
            addCriterion("durg_information_id <", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdLessThanOrEqualTo(Integer value) {
            addCriterion("durg_information_id <=", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdIn(List<Integer> values) {
            addCriterion("durg_information_id in", values, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdNotIn(List<Integer> values) {
            addCriterion("durg_information_id not in", values, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdBetween(Integer value1, Integer value2) {
            addCriterion("durg_information_id between", value1, value2, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("durg_information_id not between", value1, value2, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andReceiptIsNull() {
            addCriterion("receipt is null");
            return (Criteria) this;
        }

        public Criteria andReceiptIsNotNull() {
            addCriterion("receipt is not null");
            return (Criteria) this;
        }

        public Criteria andReceiptEqualTo(Integer value) {
            addCriterion("receipt =", value, "receipt");
            return (Criteria) this;
        }

        public Criteria andReceiptNotEqualTo(Integer value) {
            addCriterion("receipt <>", value, "receipt");
            return (Criteria) this;
        }

        public Criteria andReceiptGreaterThan(Integer value) {
            addCriterion("receipt >", value, "receipt");
            return (Criteria) this;
        }

        public Criteria andReceiptGreaterThanOrEqualTo(Integer value) {
            addCriterion("receipt >=", value, "receipt");
            return (Criteria) this;
        }

        public Criteria andReceiptLessThan(Integer value) {
            addCriterion("receipt <", value, "receipt");
            return (Criteria) this;
        }

        public Criteria andReceiptLessThanOrEqualTo(Integer value) {
            addCriterion("receipt <=", value, "receipt");
            return (Criteria) this;
        }

        public Criteria andReceiptIn(List<Integer> values) {
            addCriterion("receipt in", values, "receipt");
            return (Criteria) this;
        }

        public Criteria andReceiptNotIn(List<Integer> values) {
            addCriterion("receipt not in", values, "receipt");
            return (Criteria) this;
        }

        public Criteria andReceiptBetween(Integer value1, Integer value2) {
            addCriterion("receipt between", value1, value2, "receipt");
            return (Criteria) this;
        }

        public Criteria andReceiptNotBetween(Integer value1, Integer value2) {
            addCriterion("receipt not between", value1, value2, "receipt");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountIsNull() {
            addCriterion("inventory_amount is null");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountIsNotNull() {
            addCriterion("inventory_amount is not null");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountEqualTo(Float value) {
            addCriterion("inventory_amount =", value, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountNotEqualTo(Float value) {
            addCriterion("inventory_amount <>", value, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountGreaterThan(Float value) {
            addCriterion("inventory_amount >", value, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountGreaterThanOrEqualTo(Float value) {
            addCriterion("inventory_amount >=", value, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountLessThan(Float value) {
            addCriterion("inventory_amount <", value, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountLessThanOrEqualTo(Float value) {
            addCriterion("inventory_amount <=", value, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountIn(List<Float> values) {
            addCriterion("inventory_amount in", values, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountNotIn(List<Float> values) {
            addCriterion("inventory_amount not in", values, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountBetween(Float value1, Float value2) {
            addCriterion("inventory_amount between", value1, value2, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInventoryAmountNotBetween(Float value1, Float value2) {
            addCriterion("inventory_amount not between", value1, value2, "inventoryAmount");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumIsNull() {
            addCriterion("invoice_num is null");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumIsNotNull() {
            addCriterion("invoice_num is not null");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumEqualTo(String value) {
            addCriterion("invoice_num =", value, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumNotEqualTo(String value) {
            addCriterion("invoice_num <>", value, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumGreaterThan(String value) {
            addCriterion("invoice_num >", value, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumGreaterThanOrEqualTo(String value) {
            addCriterion("invoice_num >=", value, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumLessThan(String value) {
            addCriterion("invoice_num <", value, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumLessThanOrEqualTo(String value) {
            addCriterion("invoice_num <=", value, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumLike(String value) {
            addCriterion("invoice_num like", value, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumNotLike(String value) {
            addCriterion("invoice_num not like", value, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumIn(List<String> values) {
            addCriterion("invoice_num in", values, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumNotIn(List<String> values) {
            addCriterion("invoice_num not in", values, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumBetween(String value1, String value2) {
            addCriterion("invoice_num between", value1, value2, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andInvoiceNumNotBetween(String value1, String value2) {
            addCriterion("invoice_num not between", value1, value2, "invoiceNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumIsNull() {
            addCriterion("batch_num is null");
            return (Criteria) this;
        }

        public Criteria andBatchNumIsNotNull() {
            addCriterion("batch_num is not null");
            return (Criteria) this;
        }

        public Criteria andBatchNumEqualTo(String value) {
            addCriterion("batch_num =", value, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumNotEqualTo(String value) {
            addCriterion("batch_num <>", value, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumGreaterThan(String value) {
            addCriterion("batch_num >", value, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumGreaterThanOrEqualTo(String value) {
            addCriterion("batch_num >=", value, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumLessThan(String value) {
            addCriterion("batch_num <", value, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumLessThanOrEqualTo(String value) {
            addCriterion("batch_num <=", value, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumLike(String value) {
            addCriterion("batch_num like", value, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumNotLike(String value) {
            addCriterion("batch_num not like", value, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumIn(List<String> values) {
            addCriterion("batch_num in", values, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumNotIn(List<String> values) {
            addCriterion("batch_num not in", values, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumBetween(String value1, String value2) {
            addCriterion("batch_num between", value1, value2, "batchNum");
            return (Criteria) this;
        }

        public Criteria andBatchNumNotBetween(String value1, String value2) {
            addCriterion("batch_num not between", value1, value2, "batchNum");
            return (Criteria) this;
        }

        public Criteria andDrugValidityIsNull() {
            addCriterion("drug_validity is null");
            return (Criteria) this;
        }

        public Criteria andDrugValidityIsNotNull() {
            addCriterion("drug_validity is not null");
            return (Criteria) this;
        }

        public Criteria andDrugValidityEqualTo(Integer value) {
            addCriterion("drug_validity =", value, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andDrugValidityNotEqualTo(Integer value) {
            addCriterion("drug_validity <>", value, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andDrugValidityGreaterThan(Integer value) {
            addCriterion("drug_validity >", value, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andDrugValidityGreaterThanOrEqualTo(Integer value) {
            addCriterion("drug_validity >=", value, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andDrugValidityLessThan(Integer value) {
            addCriterion("drug_validity <", value, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andDrugValidityLessThanOrEqualTo(Integer value) {
            addCriterion("drug_validity <=", value, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andDrugValidityIn(List<Integer> values) {
            addCriterion("drug_validity in", values, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andDrugValidityNotIn(List<Integer> values) {
            addCriterion("drug_validity not in", values, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andDrugValidityBetween(Integer value1, Integer value2) {
            addCriterion("drug_validity between", value1, value2, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andDrugValidityNotBetween(Integer value1, Integer value2) {
            addCriterion("drug_validity not between", value1, value2, "drugValidity");
            return (Criteria) this;
        }

        public Criteria andEntryTimeIsNull() {
            addCriterion("entry_time is null");
            return (Criteria) this;
        }

        public Criteria andEntryTimeIsNotNull() {
            addCriterion("entry_time is not null");
            return (Criteria) this;
        }

        public Criteria andEntryTimeEqualTo(Date value) {
            addCriterionForJDBCDate("entry_time =", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("entry_time <>", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("entry_time >", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("entry_time >=", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeLessThan(Date value) {
            addCriterionForJDBCDate("entry_time <", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("entry_time <=", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeIn(List<Date> values) {
            addCriterionForJDBCDate("entry_time in", values, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("entry_time not in", values, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("entry_time between", value1, value2, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("entry_time not between", value1, value2, "entryTime");
            return (Criteria) this;
        }

        public Criteria andChanseNumIsNull() {
            addCriterion("chanse_num is null");
            return (Criteria) this;
        }

        public Criteria andChanseNumIsNotNull() {
            addCriterion("chanse_num is not null");
            return (Criteria) this;
        }

        public Criteria andChanseNumEqualTo(Integer value) {
            addCriterion("chanse_num =", value, "chanseNum");
            return (Criteria) this;
        }

        public Criteria andChanseNumNotEqualTo(Integer value) {
            addCriterion("chanse_num <>", value, "chanseNum");
            return (Criteria) this;
        }

        public Criteria andChanseNumGreaterThan(Integer value) {
            addCriterion("chanse_num >", value, "chanseNum");
            return (Criteria) this;
        }

        public Criteria andChanseNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("chanse_num >=", value, "chanseNum");
            return (Criteria) this;
        }

        public Criteria andChanseNumLessThan(Integer value) {
            addCriterion("chanse_num <", value, "chanseNum");
            return (Criteria) this;
        }

        public Criteria andChanseNumLessThanOrEqualTo(Integer value) {
            addCriterion("chanse_num <=", value, "chanseNum");
            return (Criteria) this;
        }

        public Criteria andChanseNumIn(List<Integer> values) {
            addCriterion("chanse_num in", values, "chanseNum");
            return (Criteria) this;
        }

        public Criteria andChanseNumNotIn(List<Integer> values) {
            addCriterion("chanse_num not in", values, "chanseNum");
            return (Criteria) this;
        }

        public Criteria andChanseNumBetween(Integer value1, Integer value2) {
            addCriterion("chanse_num between", value1, value2, "chanseNum");
            return (Criteria) this;
        }

        public Criteria andChanseNumNotBetween(Integer value1, Integer value2) {
            addCriterion("chanse_num not between", value1, value2, "chanseNum");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}