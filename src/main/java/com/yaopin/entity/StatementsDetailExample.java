package com.yaopin.entity;

import java.util.ArrayList;
import java.util.List;

public class StatementsDetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public StatementsDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdIsNull() {
            addCriterion("purchase_order_id is null");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdIsNotNull() {
            addCriterion("purchase_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdEqualTo(Integer value) {
            addCriterion("purchase_order_id =", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdNotEqualTo(Integer value) {
            addCriterion("purchase_order_id <>", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdGreaterThan(Integer value) {
            addCriterion("purchase_order_id >", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("purchase_order_id >=", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdLessThan(Integer value) {
            addCriterion("purchase_order_id <", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("purchase_order_id <=", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdIn(List<Integer> values) {
            addCriterion("purchase_order_id in", values, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdNotIn(List<Integer> values) {
            addCriterion("purchase_order_id not in", values, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("purchase_order_id between", value1, value2, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("purchase_order_id not between", value1, value2, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdIsNull() {
            addCriterion("durg_information_id is null");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdIsNotNull() {
            addCriterion("durg_information_id is not null");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdEqualTo(Integer value) {
            addCriterion("durg_information_id =", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdNotEqualTo(Integer value) {
            addCriterion("durg_information_id <>", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdGreaterThan(Integer value) {
            addCriterion("durg_information_id >", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("durg_information_id >=", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdLessThan(Integer value) {
            addCriterion("durg_information_id <", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdLessThanOrEqualTo(Integer value) {
            addCriterion("durg_information_id <=", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdIn(List<Integer> values) {
            addCriterion("durg_information_id in", values, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdNotIn(List<Integer> values) {
            addCriterion("durg_information_id not in", values, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdBetween(Integer value1, Integer value2) {
            addCriterion("durg_information_id between", value1, value2, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("durg_information_id not between", value1, value2, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdIsNull() {
            addCriterion("statements_id is null");
            return (Criteria) this;
        }

        public Criteria andStatementsIdIsNotNull() {
            addCriterion("statements_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatementsIdEqualTo(Integer value) {
            addCriterion("statements_id =", value, "statementsId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdNotEqualTo(Integer value) {
            addCriterion("statements_id <>", value, "statementsId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdGreaterThan(Integer value) {
            addCriterion("statements_id >", value, "statementsId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("statements_id >=", value, "statementsId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdLessThan(Integer value) {
            addCriterion("statements_id <", value, "statementsId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdLessThanOrEqualTo(Integer value) {
            addCriterion("statements_id <=", value, "statementsId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdIn(List<Integer> values) {
            addCriterion("statements_id in", values, "statementsId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdNotIn(List<Integer> values) {
            addCriterion("statements_id not in", values, "statementsId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdBetween(Integer value1, Integer value2) {
            addCriterion("statements_id between", value1, value2, "statementsId");
            return (Criteria) this;
        }

        public Criteria andStatementsIdNotBetween(Integer value1, Integer value2) {
            addCriterion("statements_id not between", value1, value2, "statementsId");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountIsNull() {
            addCriterion("settlement_amount is null");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountIsNotNull() {
            addCriterion("settlement_amount is not null");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountEqualTo(Integer value) {
            addCriterion("settlement_amount =", value, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountNotEqualTo(Integer value) {
            addCriterion("settlement_amount <>", value, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountGreaterThan(Integer value) {
            addCriterion("settlement_amount >", value, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountGreaterThanOrEqualTo(Integer value) {
            addCriterion("settlement_amount >=", value, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountLessThan(Integer value) {
            addCriterion("settlement_amount <", value, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountLessThanOrEqualTo(Integer value) {
            addCriterion("settlement_amount <=", value, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountIn(List<Integer> values) {
            addCriterion("settlement_amount in", values, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountNotIn(List<Integer> values) {
            addCriterion("settlement_amount not in", values, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountBetween(Integer value1, Integer value2) {
            addCriterion("settlement_amount between", value1, value2, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementAmountNotBetween(Integer value1, Integer value2) {
            addCriterion("settlement_amount not between", value1, value2, "settlementAmount");
            return (Criteria) this;
        }

        public Criteria andSettlementSumIsNull() {
            addCriterion("settlement_sum is null");
            return (Criteria) this;
        }

        public Criteria andSettlementSumIsNotNull() {
            addCriterion("settlement_sum is not null");
            return (Criteria) this;
        }

        public Criteria andSettlementSumEqualTo(Float value) {
            addCriterion("settlement_sum =", value, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andSettlementSumNotEqualTo(Float value) {
            addCriterion("settlement_sum <>", value, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andSettlementSumGreaterThan(Float value) {
            addCriterion("settlement_sum >", value, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andSettlementSumGreaterThanOrEqualTo(Float value) {
            addCriterion("settlement_sum >=", value, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andSettlementSumLessThan(Float value) {
            addCriterion("settlement_sum <", value, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andSettlementSumLessThanOrEqualTo(Float value) {
            addCriterion("settlement_sum <=", value, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andSettlementSumIn(List<Float> values) {
            addCriterion("settlement_sum in", values, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andSettlementSumNotIn(List<Float> values) {
            addCriterion("settlement_sum not in", values, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andSettlementSumBetween(Float value1, Float value2) {
            addCriterion("settlement_sum between", value1, value2, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andSettlementSumNotBetween(Float value1, Float value2) {
            addCriterion("settlement_sum not between", value1, value2, "settlementSum");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}