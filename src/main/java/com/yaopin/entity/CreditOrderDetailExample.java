package com.yaopin.entity;

import java.util.ArrayList;
import java.util.List;

public class CreditOrderDetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CreditOrderDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdIsNull() {
            addCriterion("purchase_order_id is null");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdIsNotNull() {
            addCriterion("purchase_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdEqualTo(Integer value) {
            addCriterion("purchase_order_id =", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdNotEqualTo(Integer value) {
            addCriterion("purchase_order_id <>", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdGreaterThan(Integer value) {
            addCriterion("purchase_order_id >", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("purchase_order_id >=", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdLessThan(Integer value) {
            addCriterion("purchase_order_id <", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("purchase_order_id <=", value, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdIn(List<Integer> values) {
            addCriterion("purchase_order_id in", values, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdNotIn(List<Integer> values) {
            addCriterion("purchase_order_id not in", values, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("purchase_order_id between", value1, value2, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andPurchaseOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("purchase_order_id not between", value1, value2, "purchaseOrderId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdIsNull() {
            addCriterion("durg_information_id is null");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdIsNotNull() {
            addCriterion("durg_information_id is not null");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdEqualTo(Integer value) {
            addCriterion("durg_information_id =", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdNotEqualTo(Integer value) {
            addCriterion("durg_information_id <>", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdGreaterThan(Integer value) {
            addCriterion("durg_information_id >", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("durg_information_id >=", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdLessThan(Integer value) {
            addCriterion("durg_information_id <", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdLessThanOrEqualTo(Integer value) {
            addCriterion("durg_information_id <=", value, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdIn(List<Integer> values) {
            addCriterion("durg_information_id in", values, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdNotIn(List<Integer> values) {
            addCriterion("durg_information_id not in", values, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdBetween(Integer value1, Integer value2) {
            addCriterion("durg_information_id between", value1, value2, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andDurgInformationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("durg_information_id not between", value1, value2, "durgInformationId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdIsNull() {
            addCriterion("credit_order_id is null");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdIsNotNull() {
            addCriterion("credit_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdEqualTo(Integer value) {
            addCriterion("credit_order_id =", value, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdNotEqualTo(Integer value) {
            addCriterion("credit_order_id <>", value, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdGreaterThan(Integer value) {
            addCriterion("credit_order_id >", value, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("credit_order_id >=", value, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdLessThan(Integer value) {
            addCriterion("credit_order_id <", value, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("credit_order_id <=", value, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdIn(List<Integer> values) {
            addCriterion("credit_order_id in", values, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdNotIn(List<Integer> values) {
            addCriterion("credit_order_id not in", values, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("credit_order_id between", value1, value2, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andCreditOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("credit_order_id not between", value1, value2, "creditOrderId");
            return (Criteria) this;
        }

        public Criteria andReturnNumIsNull() {
            addCriterion("return_num is null");
            return (Criteria) this;
        }

        public Criteria andReturnNumIsNotNull() {
            addCriterion("return_num is not null");
            return (Criteria) this;
        }

        public Criteria andReturnNumEqualTo(Integer value) {
            addCriterion("return_num =", value, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnNumNotEqualTo(Integer value) {
            addCriterion("return_num <>", value, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnNumGreaterThan(Integer value) {
            addCriterion("return_num >", value, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("return_num >=", value, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnNumLessThan(Integer value) {
            addCriterion("return_num <", value, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnNumLessThanOrEqualTo(Integer value) {
            addCriterion("return_num <=", value, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnNumIn(List<Integer> values) {
            addCriterion("return_num in", values, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnNumNotIn(List<Integer> values) {
            addCriterion("return_num not in", values, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnNumBetween(Integer value1, Integer value2) {
            addCriterion("return_num between", value1, value2, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnNumNotBetween(Integer value1, Integer value2) {
            addCriterion("return_num not between", value1, value2, "returnNum");
            return (Criteria) this;
        }

        public Criteria andReturnSumIsNull() {
            addCriterion("return_sum is null");
            return (Criteria) this;
        }

        public Criteria andReturnSumIsNotNull() {
            addCriterion("return_sum is not null");
            return (Criteria) this;
        }

        public Criteria andReturnSumEqualTo(Float value) {
            addCriterion("return_sum =", value, "returnSum");
            return (Criteria) this;
        }

        public Criteria andReturnSumNotEqualTo(Float value) {
            addCriterion("return_sum <>", value, "returnSum");
            return (Criteria) this;
        }

        public Criteria andReturnSumGreaterThan(Float value) {
            addCriterion("return_sum >", value, "returnSum");
            return (Criteria) this;
        }

        public Criteria andReturnSumGreaterThanOrEqualTo(Float value) {
            addCriterion("return_sum >=", value, "returnSum");
            return (Criteria) this;
        }

        public Criteria andReturnSumLessThan(Float value) {
            addCriterion("return_sum <", value, "returnSum");
            return (Criteria) this;
        }

        public Criteria andReturnSumLessThanOrEqualTo(Float value) {
            addCriterion("return_sum <=", value, "returnSum");
            return (Criteria) this;
        }

        public Criteria andReturnSumIn(List<Float> values) {
            addCriterion("return_sum in", values, "returnSum");
            return (Criteria) this;
        }

        public Criteria andReturnSumNotIn(List<Float> values) {
            addCriterion("return_sum not in", values, "returnSum");
            return (Criteria) this;
        }

        public Criteria andReturnSumBetween(Float value1, Float value2) {
            addCriterion("return_sum between", value1, value2, "returnSum");
            return (Criteria) this;
        }

        public Criteria andReturnSumNotBetween(Float value1, Float value2) {
            addCriterion("return_sum not between", value1, value2, "returnSum");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andReturnCauseIsNull() {
            addCriterion("\"return_ cause\" is null");
            return (Criteria) this;
        }

        public Criteria andReturnCauseIsNotNull() {
            addCriterion("\"return_ cause\" is not null");
            return (Criteria) this;
        }

        public Criteria andReturnCauseEqualTo(String value) {
            addCriterion("\"return_ cause\" =", value, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseNotEqualTo(String value) {
            addCriterion("\"return_ cause\" <>", value, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseGreaterThan(String value) {
            addCriterion("\"return_ cause\" >", value, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseGreaterThanOrEqualTo(String value) {
            addCriterion("\"return_ cause\" >=", value, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseLessThan(String value) {
            addCriterion("\"return_ cause\" <", value, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseLessThanOrEqualTo(String value) {
            addCriterion("\"return_ cause\" <=", value, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseLike(String value) {
            addCriterion("\"return_ cause\" like", value, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseNotLike(String value) {
            addCriterion("\"return_ cause\" not like", value, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseIn(List<String> values) {
            addCriterion("\"return_ cause\" in", values, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseNotIn(List<String> values) {
            addCriterion("\"return_ cause\" not in", values, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseBetween(String value1, String value2) {
            addCriterion("\"return_ cause\" between", value1, value2, "returnCause");
            return (Criteria) this;
        }

        public Criteria andReturnCauseNotBetween(String value1, String value2) {
            addCriterion("\"return_ cause\" not between", value1, value2, "returnCause");
            return (Criteria) this;
        }

        public Criteria andChangeNumIsNull() {
            addCriterion("change_num is null");
            return (Criteria) this;
        }

        public Criteria andChangeNumIsNotNull() {
            addCriterion("change_num is not null");
            return (Criteria) this;
        }

        public Criteria andChangeNumEqualTo(Integer value) {
            addCriterion("change_num =", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumNotEqualTo(Integer value) {
            addCriterion("change_num <>", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumGreaterThan(Integer value) {
            addCriterion("change_num >", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("change_num >=", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumLessThan(Integer value) {
            addCriterion("change_num <", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumLessThanOrEqualTo(Integer value) {
            addCriterion("change_num <=", value, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumIn(List<Integer> values) {
            addCriterion("change_num in", values, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumNotIn(List<Integer> values) {
            addCriterion("change_num not in", values, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumBetween(Integer value1, Integer value2) {
            addCriterion("change_num between", value1, value2, "changeNum");
            return (Criteria) this;
        }

        public Criteria andChangeNumNotBetween(Integer value1, Integer value2) {
            addCriterion("change_num not between", value1, value2, "changeNum");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}