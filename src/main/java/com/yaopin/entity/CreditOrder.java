package com.yaopin.entity;

import java.util.Date;

public class CreditOrder implements Cloneable {
    private Integer id;

    private String creditOrderNum;

    private String creditOrderName;

    private Integer hospitalId;
    private Hospital hospital;


    private String linkman;

    private String phone;

    private String createPeople;

    private Date createTime;

    private Date submissionTime;

    private String remark;

    private Integer state;

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreditOrderNum() {
        return creditOrderNum;
    }

    public void setCreditOrderNum(String creditOrderNum) {
        this.creditOrderNum = creditOrderNum == null ? null : creditOrderNum.trim();
    }

    public String getCreditOrderName() {
        return creditOrderName;
    }

    public void setCreditOrderName(String creditOrderName) {
        this.creditOrderName = creditOrderName == null ? null : creditOrderName.trim();
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman == null ? null : linkman.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(String createPeople) {
        this.createPeople = createPeople == null ? null : createPeople.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Date submissionTime) {
        this.submissionTime = submissionTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public Object clone() {
        CreditOrder creditOrder = null;
        try {
            creditOrder = (CreditOrder) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        creditOrder.hospital = (Hospital) hospital.clone();
        return creditOrder;
    }
}