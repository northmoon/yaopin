package com.yaopin.entity;

import java.util.ArrayList;
import java.util.List;

public class DrugCategoryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DrugCategoryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCategoryNumIsNull() {
            addCriterion("category_num is null");
            return (Criteria) this;
        }

        public Criteria andCategoryNumIsNotNull() {
            addCriterion("category_num is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryNumEqualTo(String value) {
            addCriterion("category_num =", value, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumNotEqualTo(String value) {
            addCriterion("category_num <>", value, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumGreaterThan(String value) {
            addCriterion("category_num >", value, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumGreaterThanOrEqualTo(String value) {
            addCriterion("category_num >=", value, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumLessThan(String value) {
            addCriterion("category_num <", value, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumLessThanOrEqualTo(String value) {
            addCriterion("category_num <=", value, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumLike(String value) {
            addCriterion("category_num like", value, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumNotLike(String value) {
            addCriterion("category_num not like", value, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumIn(List<String> values) {
            addCriterion("category_num in", values, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumNotIn(List<String> values) {
            addCriterion("category_num not in", values, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumBetween(String value1, String value2) {
            addCriterion("category_num between", value1, value2, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andCategoryNumNotBetween(String value1, String value2) {
            addCriterion("category_num not between", value1, value2, "categoryNum");
            return (Criteria) this;
        }

        public Criteria andGenericNameIsNull() {
            addCriterion("generic_name is null");
            return (Criteria) this;
        }

        public Criteria andGenericNameIsNotNull() {
            addCriterion("generic_name is not null");
            return (Criteria) this;
        }

        public Criteria andGenericNameEqualTo(String value) {
            addCriterion("generic_name =", value, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameNotEqualTo(String value) {
            addCriterion("generic_name <>", value, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameGreaterThan(String value) {
            addCriterion("generic_name >", value, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameGreaterThanOrEqualTo(String value) {
            addCriterion("generic_name >=", value, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameLessThan(String value) {
            addCriterion("generic_name <", value, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameLessThanOrEqualTo(String value) {
            addCriterion("generic_name <=", value, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameLike(String value) {
            addCriterion("generic_name like", value, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameNotLike(String value) {
            addCriterion("generic_name not like", value, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameIn(List<String> values) {
            addCriterion("generic_name in", values, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameNotIn(List<String> values) {
            addCriterion("generic_name not in", values, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameBetween(String value1, String value2) {
            addCriterion("generic_name between", value1, value2, "genericName");
            return (Criteria) this;
        }

        public Criteria andGenericNameNotBetween(String value1, String value2) {
            addCriterion("generic_name not between", value1, value2, "genericName");
            return (Criteria) this;
        }

        public Criteria andDosageFormIsNull() {
            addCriterion("dosage_form is null");
            return (Criteria) this;
        }

        public Criteria andDosageFormIsNotNull() {
            addCriterion("dosage_form is not null");
            return (Criteria) this;
        }

        public Criteria andDosageFormEqualTo(String value) {
            addCriterion("dosage_form =", value, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormNotEqualTo(String value) {
            addCriterion("dosage_form <>", value, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormGreaterThan(String value) {
            addCriterion("dosage_form >", value, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormGreaterThanOrEqualTo(String value) {
            addCriterion("dosage_form >=", value, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormLessThan(String value) {
            addCriterion("dosage_form <", value, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormLessThanOrEqualTo(String value) {
            addCriterion("dosage_form <=", value, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormLike(String value) {
            addCriterion("dosage_form like", value, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormNotLike(String value) {
            addCriterion("dosage_form not like", value, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormIn(List<String> values) {
            addCriterion("dosage_form in", values, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormNotIn(List<String> values) {
            addCriterion("dosage_form not in", values, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormBetween(String value1, String value2) {
            addCriterion("dosage_form between", value1, value2, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andDosageFormNotBetween(String value1, String value2) {
            addCriterion("dosage_form not between", value1, value2, "dosageForm");
            return (Criteria) this;
        }

        public Criteria andSpecificationIsNull() {
            addCriterion("specification is null");
            return (Criteria) this;
        }

        public Criteria andSpecificationIsNotNull() {
            addCriterion("specification is not null");
            return (Criteria) this;
        }

        public Criteria andSpecificationEqualTo(String value) {
            addCriterion("specification =", value, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationNotEqualTo(String value) {
            addCriterion("specification <>", value, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationGreaterThan(String value) {
            addCriterion("specification >", value, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationGreaterThanOrEqualTo(String value) {
            addCriterion("specification >=", value, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationLessThan(String value) {
            addCriterion("specification <", value, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationLessThanOrEqualTo(String value) {
            addCriterion("specification <=", value, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationLike(String value) {
            addCriterion("specification like", value, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationNotLike(String value) {
            addCriterion("specification not like", value, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationIn(List<String> values) {
            addCriterion("specification in", values, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationNotIn(List<String> values) {
            addCriterion("specification not in", values, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationBetween(String value1, String value2) {
            addCriterion("specification between", value1, value2, "specification");
            return (Criteria) this;
        }

        public Criteria andSpecificationNotBetween(String value1, String value2) {
            addCriterion("specification not between", value1, value2, "specification");
            return (Criteria) this;
        }

        public Criteria andUnitIsNull() {
            addCriterion("unit is null");
            return (Criteria) this;
        }

        public Criteria andUnitIsNotNull() {
            addCriterion("unit is not null");
            return (Criteria) this;
        }

        public Criteria andUnitEqualTo(String value) {
            addCriterion("unit =", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotEqualTo(String value) {
            addCriterion("unit <>", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThan(String value) {
            addCriterion("unit >", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThanOrEqualTo(String value) {
            addCriterion("unit >=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThan(String value) {
            addCriterion("unit <", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThanOrEqualTo(String value) {
            addCriterion("unit <=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLike(String value) {
            addCriterion("unit like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotLike(String value) {
            addCriterion("unit not like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitIn(List<String> values) {
            addCriterion("unit in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotIn(List<String> values) {
            addCriterion("unit not in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitBetween(String value1, String value2) {
            addCriterion("unit between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotBetween(String value1, String value2) {
            addCriterion("unit not between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNull() {
            addCriterion("coefficient is null");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNotNull() {
            addCriterion("coefficient is not null");
            return (Criteria) this;
        }

        public Criteria andCoefficientEqualTo(Integer value) {
            addCriterion("coefficient =", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotEqualTo(Integer value) {
            addCriterion("coefficient <>", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThan(Integer value) {
            addCriterion("coefficient >", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThanOrEqualTo(Integer value) {
            addCriterion("coefficient >=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThan(Integer value) {
            addCriterion("coefficient <", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThanOrEqualTo(Integer value) {
            addCriterion("coefficient <=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientIn(List<Integer> values) {
            addCriterion("coefficient in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotIn(List<Integer> values) {
            addCriterion("coefficient not in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientBetween(Integer value1, Integer value2) {
            addCriterion("coefficient between", value1, value2, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotBetween(Integer value1, Integer value2) {
            addCriterion("coefficient not between", value1, value2, "coefficient");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdIsNull() {
            addCriterion("drug_classification_id is null");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdIsNotNull() {
            addCriterion("drug_classification_id is not null");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdEqualTo(Integer value) {
            addCriterion("drug_classification_id =", value, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdNotEqualTo(Integer value) {
            addCriterion("drug_classification_id <>", value, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdGreaterThan(Integer value) {
            addCriterion("drug_classification_id >", value, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("drug_classification_id >=", value, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdLessThan(Integer value) {
            addCriterion("drug_classification_id <", value, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdLessThanOrEqualTo(Integer value) {
            addCriterion("drug_classification_id <=", value, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdIn(List<Integer> values) {
            addCriterion("drug_classification_id in", values, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdNotIn(List<Integer> values) {
            addCriterion("drug_classification_id not in", values, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdBetween(Integer value1, Integer value2) {
            addCriterion("drug_classification_id between", value1, value2, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andDrugClassificationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("drug_classification_id not between", value1, value2, "drugClassificationId");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}