package com.yaopin.entity;

public class DrugClassification implements Cloneable {
    private Integer id;

    private String drugClassification;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDrugClassification() {
        return drugClassification;
    }

    public void setDrugClassification(String drugClassification) {
        this.drugClassification = drugClassification == null ? null : drugClassification.trim();
    }

    @Override
    public Object clone(){
        DrugClassification drugClassification = null;
        try {
            drugClassification = (DrugClassification) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return drugClassification;
    }

}