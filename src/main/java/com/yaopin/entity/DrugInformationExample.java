package com.yaopin.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class DrugInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DrugInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdIsNull() {
            addCriterion("drug_category_id is null");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdIsNotNull() {
            addCriterion("drug_category_id is not null");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdEqualTo(Integer value) {
            addCriterion("drug_category_id =", value, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdNotEqualTo(Integer value) {
            addCriterion("drug_category_id <>", value, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdGreaterThan(Integer value) {
            addCriterion("drug_category_id >", value, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("drug_category_id >=", value, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdLessThan(Integer value) {
            addCriterion("drug_category_id <", value, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdLessThanOrEqualTo(Integer value) {
            addCriterion("drug_category_id <=", value, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdIn(List<Integer> values) {
            addCriterion("drug_category_id in", values, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdNotIn(List<Integer> values) {
            addCriterion("drug_category_id not in", values, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdBetween(Integer value1, Integer value2) {
            addCriterion("drug_category_id between", value1, value2, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andDrugCategoryIdNotBetween(Integer value1, Integer value2) {
            addCriterion("drug_category_id not between", value1, value2, "drugCategoryId");
            return (Criteria) this;
        }

        public Criteria andSerialNumIsNull() {
            addCriterion("serial_num is null");
            return (Criteria) this;
        }

        public Criteria andSerialNumIsNotNull() {
            addCriterion("serial_num is not null");
            return (Criteria) this;
        }

        public Criteria andSerialNumEqualTo(String value) {
            addCriterion("serial_num =", value, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumNotEqualTo(String value) {
            addCriterion("serial_num <>", value, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumGreaterThan(String value) {
            addCriterion("serial_num >", value, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumGreaterThanOrEqualTo(String value) {
            addCriterion("serial_num >=", value, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumLessThan(String value) {
            addCriterion("serial_num <", value, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumLessThanOrEqualTo(String value) {
            addCriterion("serial_num <=", value, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumLike(String value) {
            addCriterion("serial_num like", value, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumNotLike(String value) {
            addCriterion("serial_num not like", value, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumIn(List<String> values) {
            addCriterion("serial_num in", values, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumNotIn(List<String> values) {
            addCriterion("serial_num not in", values, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumBetween(String value1, String value2) {
            addCriterion("serial_num between", value1, value2, "serialNum");
            return (Criteria) this;
        }

        public Criteria andSerialNumNotBetween(String value1, String value2) {
            addCriterion("serial_num not between", value1, value2, "serialNum");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameIsNull() {
            addCriterion("enterprise_name is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameIsNotNull() {
            addCriterion("enterprise_name is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameEqualTo(String value) {
            addCriterion("enterprise_name =", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameNotEqualTo(String value) {
            addCriterion("enterprise_name <>", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameGreaterThan(String value) {
            addCriterion("enterprise_name >", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameGreaterThanOrEqualTo(String value) {
            addCriterion("enterprise_name >=", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameLessThan(String value) {
            addCriterion("enterprise_name <", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameLessThanOrEqualTo(String value) {
            addCriterion("enterprise_name <=", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameLike(String value) {
            addCriterion("enterprise_name like", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameNotLike(String value) {
            addCriterion("enterprise_name not like", value, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameIn(List<String> values) {
            addCriterion("enterprise_name in", values, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameNotIn(List<String> values) {
            addCriterion("enterprise_name not in", values, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameBetween(String value1, String value2) {
            addCriterion("enterprise_name between", value1, value2, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseNameNotBetween(String value1, String value2) {
            addCriterion("enterprise_name not between", value1, value2, "enterpriseName");
            return (Criteria) this;
        }

        public Criteria andDrugNameIsNull() {
            addCriterion("drug_name is null");
            return (Criteria) this;
        }

        public Criteria andDrugNameIsNotNull() {
            addCriterion("drug_name is not null");
            return (Criteria) this;
        }

        public Criteria andDrugNameEqualTo(String value) {
            addCriterion("drug_name =", value, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameNotEqualTo(String value) {
            addCriterion("drug_name <>", value, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameGreaterThan(String value) {
            addCriterion("drug_name >", value, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameGreaterThanOrEqualTo(String value) {
            addCriterion("drug_name >=", value, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameLessThan(String value) {
            addCriterion("drug_name <", value, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameLessThanOrEqualTo(String value) {
            addCriterion("drug_name <=", value, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameLike(String value) {
            addCriterion("drug_name like", value, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameNotLike(String value) {
            addCriterion("drug_name not like", value, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameIn(List<String> values) {
            addCriterion("drug_name in", values, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameNotIn(List<String> values) {
            addCriterion("drug_name not in", values, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameBetween(String value1, String value2) {
            addCriterion("drug_name between", value1, value2, "drugName");
            return (Criteria) this;
        }

        public Criteria andDrugNameNotBetween(String value1, String value2) {
            addCriterion("drug_name not between", value1, value2, "drugName");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceIsNull() {
            addCriterion("bidding_price is null");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceIsNotNull() {
            addCriterion("bidding_price is not null");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceEqualTo(Float value) {
            addCriterion("bidding_price =", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceNotEqualTo(Float value) {
            addCriterion("bidding_price <>", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceGreaterThan(Float value) {
            addCriterion("bidding_price >", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceGreaterThanOrEqualTo(Float value) {
            addCriterion("bidding_price >=", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceLessThan(Float value) {
            addCriterion("bidding_price <", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceLessThanOrEqualTo(Float value) {
            addCriterion("bidding_price <=", value, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceIn(List<Float> values) {
            addCriterion("bidding_price in", values, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceNotIn(List<Float> values) {
            addCriterion("bidding_price not in", values, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceBetween(Float value1, Float value2) {
            addCriterion("bidding_price between", value1, value2, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andBiddingPriceNotBetween(Float value1, Float value2) {
            addCriterion("bidding_price not between", value1, value2, "biddingPrice");
            return (Criteria) this;
        }

        public Criteria andImgPathIsNull() {
            addCriterion("img_path is null");
            return (Criteria) this;
        }

        public Criteria andImgPathIsNotNull() {
            addCriterion("img_path is not null");
            return (Criteria) this;
        }

        public Criteria andImgPathEqualTo(String value) {
            addCriterion("img_path =", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathNotEqualTo(String value) {
            addCriterion("img_path <>", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathGreaterThan(String value) {
            addCriterion("img_path >", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathGreaterThanOrEqualTo(String value) {
            addCriterion("img_path >=", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathLessThan(String value) {
            addCriterion("img_path <", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathLessThanOrEqualTo(String value) {
            addCriterion("img_path <=", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathLike(String value) {
            addCriterion("img_path like", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathNotLike(String value) {
            addCriterion("img_path not like", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathIn(List<String> values) {
            addCriterion("img_path in", values, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathNotIn(List<String> values) {
            addCriterion("img_path not in", values, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathBetween(String value1, String value2) {
            addCriterion("img_path between", value1, value2, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathNotBetween(String value1, String value2) {
            addCriterion("img_path not between", value1, value2, "imgPath");
            return (Criteria) this;
        }

        public Criteria andApprovalNumIsNull() {
            addCriterion("approval_num is null");
            return (Criteria) this;
        }

        public Criteria andApprovalNumIsNotNull() {
            addCriterion("approval_num is not null");
            return (Criteria) this;
        }

        public Criteria andApprovalNumEqualTo(String value) {
            addCriterion("approval_num =", value, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumNotEqualTo(String value) {
            addCriterion("approval_num <>", value, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumGreaterThan(String value) {
            addCriterion("approval_num >", value, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumGreaterThanOrEqualTo(String value) {
            addCriterion("approval_num >=", value, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumLessThan(String value) {
            addCriterion("approval_num <", value, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumLessThanOrEqualTo(String value) {
            addCriterion("approval_num <=", value, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumLike(String value) {
            addCriterion("approval_num like", value, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumNotLike(String value) {
            addCriterion("approval_num not like", value, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumIn(List<String> values) {
            addCriterion("approval_num in", values, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumNotIn(List<String> values) {
            addCriterion("approval_num not in", values, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumBetween(String value1, String value2) {
            addCriterion("approval_num between", value1, value2, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andApprovalNumNotBetween(String value1, String value2) {
            addCriterion("approval_num not between", value1, value2, "approvalNum");
            return (Criteria) this;
        }

        public Criteria andValidityIsNull() {
            addCriterion("validity is null");
            return (Criteria) this;
        }

        public Criteria andValidityIsNotNull() {
            addCriterion("validity is not null");
            return (Criteria) this;
        }

        public Criteria andValidityEqualTo(Date value) {
            addCriterionForJDBCDate("validity =", value, "validity");
            return (Criteria) this;
        }

        public Criteria andValidityNotEqualTo(Date value) {
            addCriterionForJDBCDate("validity <>", value, "validity");
            return (Criteria) this;
        }

        public Criteria andValidityGreaterThan(Date value) {
            addCriterionForJDBCDate("validity >", value, "validity");
            return (Criteria) this;
        }

        public Criteria andValidityGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("validity >=", value, "validity");
            return (Criteria) this;
        }

        public Criteria andValidityLessThan(Date value) {
            addCriterionForJDBCDate("validity <", value, "validity");
            return (Criteria) this;
        }

        public Criteria andValidityLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("validity <=", value, "validity");
            return (Criteria) this;
        }

        public Criteria andValidityIn(List<Date> values) {
            addCriterionForJDBCDate("validity in", values, "validity");
            return (Criteria) this;
        }

        public Criteria andValidityNotIn(List<Date> values) {
            addCriterionForJDBCDate("validity not in", values, "validity");
            return (Criteria) this;
        }

        public Criteria andValidityBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("validity between", value1, value2, "validity");
            return (Criteria) this;
        }

        public Criteria andValidityNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("validity not between", value1, value2, "validity");
            return (Criteria) this;
        }

        public Criteria andIsimportIsNull() {
            addCriterion("isImport is null");
            return (Criteria) this;
        }

        public Criteria andIsimportIsNotNull() {
            addCriterion("isImport is not null");
            return (Criteria) this;
        }

        public Criteria andIsimportEqualTo(Integer value) {
            addCriterion("isImport =", value, "isimport");
            return (Criteria) this;
        }

        public Criteria andIsimportNotEqualTo(Integer value) {
            addCriterion("isImport <>", value, "isimport");
            return (Criteria) this;
        }

        public Criteria andIsimportGreaterThan(Integer value) {
            addCriterion("isImport >", value, "isimport");
            return (Criteria) this;
        }

        public Criteria andIsimportGreaterThanOrEqualTo(Integer value) {
            addCriterion("isImport >=", value, "isimport");
            return (Criteria) this;
        }

        public Criteria andIsimportLessThan(Integer value) {
            addCriterion("isImport <", value, "isimport");
            return (Criteria) this;
        }

        public Criteria andIsimportLessThanOrEqualTo(Integer value) {
            addCriterion("isImport <=", value, "isimport");
            return (Criteria) this;
        }

        public Criteria andIsimportIn(List<Integer> values) {
            addCriterion("isImport in", values, "isimport");
            return (Criteria) this;
        }

        public Criteria andIsimportNotIn(List<Integer> values) {
            addCriterion("isImport not in", values, "isimport");
            return (Criteria) this;
        }

        public Criteria andIsimportBetween(Integer value1, Integer value2) {
            addCriterion("isImport between", value1, value2, "isimport");
            return (Criteria) this;
        }

        public Criteria andIsimportNotBetween(Integer value1, Integer value2) {
            addCriterion("isImport not between", value1, value2, "isimport");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialIsNull() {
            addCriterion("packing_material is null");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialIsNotNull() {
            addCriterion("packing_material is not null");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialEqualTo(String value) {
            addCriterion("packing_material =", value, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialNotEqualTo(String value) {
            addCriterion("packing_material <>", value, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialGreaterThan(String value) {
            addCriterion("packing_material >", value, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialGreaterThanOrEqualTo(String value) {
            addCriterion("packing_material >=", value, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialLessThan(String value) {
            addCriterion("packing_material <", value, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialLessThanOrEqualTo(String value) {
            addCriterion("packing_material <=", value, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialLike(String value) {
            addCriterion("packing_material like", value, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialNotLike(String value) {
            addCriterion("packing_material not like", value, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialIn(List<String> values) {
            addCriterion("packing_material in", values, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialNotIn(List<String> values) {
            addCriterion("packing_material not in", values, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialBetween(String value1, String value2) {
            addCriterion("packing_material between", value1, value2, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingMaterialNotBetween(String value1, String value2) {
            addCriterion("packing_material not between", value1, value2, "packingMaterial");
            return (Criteria) this;
        }

        public Criteria andPackingUnitIsNull() {
            addCriterion("packing_unit is null");
            return (Criteria) this;
        }

        public Criteria andPackingUnitIsNotNull() {
            addCriterion("packing_unit is not null");
            return (Criteria) this;
        }

        public Criteria andPackingUnitEqualTo(String value) {
            addCriterion("packing_unit =", value, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitNotEqualTo(String value) {
            addCriterion("packing_unit <>", value, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitGreaterThan(String value) {
            addCriterion("packing_unit >", value, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitGreaterThanOrEqualTo(String value) {
            addCriterion("packing_unit >=", value, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitLessThan(String value) {
            addCriterion("packing_unit <", value, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitLessThanOrEqualTo(String value) {
            addCriterion("packing_unit <=", value, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitLike(String value) {
            addCriterion("packing_unit like", value, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitNotLike(String value) {
            addCriterion("packing_unit not like", value, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitIn(List<String> values) {
            addCriterion("packing_unit in", values, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitNotIn(List<String> values) {
            addCriterion("packing_unit not in", values, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitBetween(String value1, String value2) {
            addCriterion("packing_unit between", value1, value2, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andPackingUnitNotBetween(String value1, String value2) {
            addCriterion("packing_unit not between", value1, value2, "packingUnit");
            return (Criteria) this;
        }

        public Criteria andRetailPriceIsNull() {
            addCriterion("retail_price is null");
            return (Criteria) this;
        }

        public Criteria andRetailPriceIsNotNull() {
            addCriterion("retail_price is not null");
            return (Criteria) this;
        }

        public Criteria andRetailPriceEqualTo(Float value) {
            addCriterion("retail_price =", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotEqualTo(Float value) {
            addCriterion("retail_price <>", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceGreaterThan(Float value) {
            addCriterion("retail_price >", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceGreaterThanOrEqualTo(Float value) {
            addCriterion("retail_price >=", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceLessThan(Float value) {
            addCriterion("retail_price <", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceLessThanOrEqualTo(Float value) {
            addCriterion("retail_price <=", value, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceIn(List<Float> values) {
            addCriterion("retail_price in", values, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotIn(List<Float> values) {
            addCriterion("retail_price not in", values, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceBetween(Float value1, Float value2) {
            addCriterion("retail_price between", value1, value2, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andRetailPriceNotBetween(Float value1, Float value2) {
            addCriterion("retail_price not between", value1, value2, "retailPrice");
            return (Criteria) this;
        }

        public Criteria andPriceSourceIsNull() {
            addCriterion("price_source is null");
            return (Criteria) this;
        }

        public Criteria andPriceSourceIsNotNull() {
            addCriterion("price_source is not null");
            return (Criteria) this;
        }

        public Criteria andPriceSourceEqualTo(String value) {
            addCriterion("price_source =", value, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceNotEqualTo(String value) {
            addCriterion("price_source <>", value, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceGreaterThan(String value) {
            addCriterion("price_source >", value, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceGreaterThanOrEqualTo(String value) {
            addCriterion("price_source >=", value, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceLessThan(String value) {
            addCriterion("price_source <", value, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceLessThanOrEqualTo(String value) {
            addCriterion("price_source <=", value, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceLike(String value) {
            addCriterion("price_source like", value, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceNotLike(String value) {
            addCriterion("price_source not like", value, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceIn(List<String> values) {
            addCriterion("price_source in", values, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceNotIn(List<String> values) {
            addCriterion("price_source not in", values, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceBetween(String value1, String value2) {
            addCriterion("price_source between", value1, value2, "priceSource");
            return (Criteria) this;
        }

        public Criteria andPriceSourceNotBetween(String value1, String value2) {
            addCriterion("price_source not between", value1, value2, "priceSource");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdIsNull() {
            addCriterion("quality_level_id is null");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdIsNotNull() {
            addCriterion("quality_level_id is not null");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdEqualTo(Integer value) {
            addCriterion("quality_level_id =", value, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdNotEqualTo(Integer value) {
            addCriterion("quality_level_id <>", value, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdGreaterThan(Integer value) {
            addCriterion("quality_level_id >", value, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("quality_level_id >=", value, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdLessThan(Integer value) {
            addCriterion("quality_level_id <", value, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdLessThanOrEqualTo(Integer value) {
            addCriterion("quality_level_id <=", value, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdIn(List<Integer> values) {
            addCriterion("quality_level_id in", values, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdNotIn(List<Integer> values) {
            addCriterion("quality_level_id not in", values, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdBetween(Integer value1, Integer value2) {
            addCriterion("quality_level_id between", value1, value2, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andQualityLevelIdNotBetween(Integer value1, Integer value2) {
            addCriterion("quality_level_id not between", value1, value2, "qualityLevelId");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportIsNull() {
            addCriterion("isSurveyReport is null");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportIsNotNull() {
            addCriterion("isSurveyReport is not null");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportEqualTo(Integer value) {
            addCriterion("isSurveyReport =", value, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportNotEqualTo(Integer value) {
            addCriterion("isSurveyReport <>", value, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportGreaterThan(Integer value) {
            addCriterion("isSurveyReport >", value, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportGreaterThanOrEqualTo(Integer value) {
            addCriterion("isSurveyReport >=", value, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportLessThan(Integer value) {
            addCriterion("isSurveyReport <", value, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportLessThanOrEqualTo(Integer value) {
            addCriterion("isSurveyReport <=", value, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportIn(List<Integer> values) {
            addCriterion("isSurveyReport in", values, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportNotIn(List<Integer> values) {
            addCriterion("isSurveyReport not in", values, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportBetween(Integer value1, Integer value2) {
            addCriterion("isSurveyReport between", value1, value2, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andIssurveyreportNotBetween(Integer value1, Integer value2) {
            addCriterion("isSurveyReport not between", value1, value2, "issurveyreport");
            return (Criteria) this;
        }

        public Criteria andReportNumIsNull() {
            addCriterion("report_num is null");
            return (Criteria) this;
        }

        public Criteria andReportNumIsNotNull() {
            addCriterion("report_num is not null");
            return (Criteria) this;
        }

        public Criteria andReportNumEqualTo(String value) {
            addCriterion("report_num =", value, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumNotEqualTo(String value) {
            addCriterion("report_num <>", value, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumGreaterThan(String value) {
            addCriterion("report_num >", value, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumGreaterThanOrEqualTo(String value) {
            addCriterion("report_num >=", value, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumLessThan(String value) {
            addCriterion("report_num <", value, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumLessThanOrEqualTo(String value) {
            addCriterion("report_num <=", value, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumLike(String value) {
            addCriterion("report_num like", value, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumNotLike(String value) {
            addCriterion("report_num not like", value, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumIn(List<String> values) {
            addCriterion("report_num in", values, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumNotIn(List<String> values) {
            addCriterion("report_num not in", values, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumBetween(String value1, String value2) {
            addCriterion("report_num between", value1, value2, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportNumNotBetween(String value1, String value2) {
            addCriterion("report_num not between", value1, value2, "reportNum");
            return (Criteria) this;
        }

        public Criteria andReportValidityIsNull() {
            addCriterion("report_validity is null");
            return (Criteria) this;
        }

        public Criteria andReportValidityIsNotNull() {
            addCriterion("report_validity is not null");
            return (Criteria) this;
        }

        public Criteria andReportValidityEqualTo(Date value) {
            addCriterionForJDBCDate("report_validity =", value, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andReportValidityNotEqualTo(Date value) {
            addCriterionForJDBCDate("report_validity <>", value, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andReportValidityGreaterThan(Date value) {
            addCriterionForJDBCDate("report_validity >", value, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andReportValidityGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("report_validity >=", value, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andReportValidityLessThan(Date value) {
            addCriterionForJDBCDate("report_validity <", value, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andReportValidityLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("report_validity <=", value, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andReportValidityIn(List<Date> values) {
            addCriterionForJDBCDate("report_validity in", values, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andReportValidityNotIn(List<Date> values) {
            addCriterionForJDBCDate("report_validity not in", values, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andReportValidityBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("report_validity between", value1, value2, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andReportValidityNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("report_validity not between", value1, value2, "reportValidity");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}