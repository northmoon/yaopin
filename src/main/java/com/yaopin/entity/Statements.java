package com.yaopin.entity;

import java.util.Date;

public class Statements implements Cloneable {
    private Integer id;

    private String statementsNum;

    private String statementsName;

    private Integer hospitalId;

    private String linkman;

    private String phone;

    private String createPeople;

    private Date createTime;

    private Date submitTime;

    private String remark;

    private Integer state;

    private Hospital hospital;

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatementsNum() {
        return statementsNum;
    }

    public void setStatementsNum(String statementsNum) {
        this.statementsNum = statementsNum == null ? null : statementsNum.trim();
    }

    public String getStatementsName() {
        return statementsName;
    }

    public void setStatementsName(String statementsName) {
        this.statementsName = statementsName == null ? null : statementsName.trim();
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman == null ? null : linkman.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(String createPeople) {
        this.createPeople = createPeople == null ? null : createPeople.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public Object clone()  {
        Statements statements=null;
        try {
            statements = (Statements) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        statements.hospital = (Hospital) hospital.clone();
        return statements;
    }
}