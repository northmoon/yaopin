package com.yaopin.entity;

public class PurchaseOrderDrugDetails implements  Cloneable{
    private Integer id;

    private Integer purchaseNoteId;
    private PurchaseNote purchaseNote;

    private Integer drugInformationId;
    private DrugInformation drugInformation;

    private Integer suppliersId;
    private Supplier supplier;

    private Integer biddingPrice;

    private Integer trading;

    private Integer amountPurchased;

    private Float purchaseAmount;

    private String purchasingStatus;

    private String hospitalname;

    private Integer changeNum;


    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname;
    }

    public Integer getChangeNum() {
        return changeNum;
    }

    public void setChangeNum(Integer changeNum) {
        this.changeNum = changeNum;
    }

    public PurchaseNote getPurchaseNote() {
        return purchaseNote;
    }

    public void setPurchaseNote(PurchaseNote purchaseNote) {
        this.purchaseNote = purchaseNote;
    }



    public DrugInformation getDrugInformation() {
        return drugInformation;
    }

    public void setDrugInformation(DrugInformation drugInformation) {
        this.drugInformation = drugInformation;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPurchaseNoteId() {
        return purchaseNoteId;
    }

    public void setPurchaseNoteId(Integer purchaseNoteId) {
        this.purchaseNoteId = purchaseNoteId;
    }

    public Integer getDrugInformationId() {
        return drugInformationId;
    }

    public void setDrugInformationId(Integer drugInformationId) {
        this.drugInformationId = drugInformationId;
    }

    public Integer getSuppliersId() {
        return suppliersId;
    }

    public void setSuppliersId(Integer suppliersId) {
        this.suppliersId = suppliersId;
    }

    public Integer getBiddingPrice() {
        return biddingPrice;
    }

    public void setBiddingPrice(Integer biddingPrice) {
        this.biddingPrice = biddingPrice;
    }

    public Integer getTrading() {
        return trading;
    }

    public void setTrading(Integer trading) {
        this.trading = trading;
    }

    public Integer getAmountPurchased() {
        return amountPurchased;
    }

    public void setAmountPurchased(Integer amountPurchased) {
        this.amountPurchased = amountPurchased;
    }

    public Float getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(Float purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    public String getPurchasingStatus() {
        return purchasingStatus;
    }

    public void setPurchasingStatus(String purchasingStatus) {
        this.purchasingStatus = purchasingStatus == null ? null : purchasingStatus.trim();
    }

    @Override
    public Object clone()  {
        PurchaseOrderDrugDetails purchaseOrderDrugDetails=null;
        try {
            purchaseOrderDrugDetails=(PurchaseOrderDrugDetails)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        purchaseOrderDrugDetails.purchaseNote = (PurchaseNote) purchaseNote.clone();
        purchaseOrderDrugDetails.drugInformation = (DrugInformation) drugInformation.clone();
        purchaseOrderDrugDetails.supplier = (Supplier) supplier.clone();
        return purchaseOrderDrugDetails;
    }
}