package com.yaopin.service;

import com.yaopin.entity.CreditOrder;
import com.yaopin.entity.PageResult;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName CreditOrderService
 * @Author donghongyu
 * @Date 2019/9/20 9:55
 **/
public interface CreditOrderService {
    void add(CreditOrder creditOrder);

    PageResult findPage(CreditOrder creditOrder, int page, int rows);

    void preservation(HttpServletRequest request);
}
