package com.yaopin.service;

import com.yaopin.entity.HospitalPurchasing;
import com.yaopin.entity.PageResult;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


public interface HospitalPurchasingService {
    /*
     * 查询全部
     **/
    PageResult findPage(HospitalPurchasing hospitalPurchasing, int page, int rows);

/**
** @Description  根据采购单id删除
 * @Param  id
 */
    void removeIdList(int[] idList);
/**
** @Description  导出
 *  @Param
 */
    void exportExcel(ServletOutputStream outputStream);

    void addPn(List<Integer> idList, HttpServletRequest request);
}
