package com.yaopin.service;
import com.yaopin.entity.DetailedWarehousing;
import com.yaopin.entity.PurchaseOrderDrugDetails;

import java.util.List;

public interface PurchaseOrderWarehousingService {
   /**
   ** @Description  查询已发货的药品明细
    * @Param
    */
    List<DetailedWarehousing> findAll(PurchaseOrderDrugDetails purchaseOrderDrugDetails);
    void add( List<String>ids,List<String>receipts,List<String>invoiceNums,List<String>batchNums,List<String>drugValiditys);
}
