package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.PurchaseNote;

/**
 * @ClassName 采购单维护
 * @Author donghongyu
 * @Date 2019/9/9 14:36
 **/


public interface PurchaseNoteService {

    PageResult findPage(PurchaseNote purchaseNote, int page, int rows);
/**
** @Description  添加采购单
 * @Param  purchaseNote
 */
  void add(PurchaseNote purchaseNote);
/**
** @Description 根据id删除
 * @Param  id
 */
    void deleteOne(Integer id);
/**
** @Description 修改采购单
 * @Param
 */
    void update(PurchaseNote purchaseNote);
/**
** @Description  获取实体
 * @Param  id
 */
    PurchaseNote findOne(Integer id);


    PurchaseNote findSession(Integer id);
}
