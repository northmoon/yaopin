package com.yaopin.service;

import com.yaopin.entity.UserInfo;
import com.yaopin.entity.PageResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.util.Map;

public interface IUserService {
    /**
     * 返回全部列表
     *
     * @return
     */
    public Map<String,UserInfo> findAll();


    /**
     * 返回分页列表
     *
     * @return
     */
    public PageResult findPage(int pageNum, int pageSize);


    /**
     * 增加
     */
    public void add(UserInfo UserInfo, int[] ids) throws Exception;


    /**
     * 修改
     */
    public void update(UserInfo userInfo,int[] ids) throws Exception;


    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    public UserInfo findOne(Integer id);


    /**
     * 批量删除
     *
     * @param ids
     */
    public void delete(Integer[] ids);

    /**
     * 分页
     *
     * @param pageNum  当前页 码
     * @param pageSize 每页记录数
     * @return
     */
    public PageResult findPage(UserInfo userInfo, int pageNum, int pageSize);

    /**
     * 删除单个
     *
     * @param id
     */
    public void deleteOne(Integer id);

    Map<String,String> selectUnit(Integer ucategory);
}
