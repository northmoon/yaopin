package com.yaopin.service;

import com.yaopin.entity.AreaInfo;
import com.yaopin.entity.PageResult;

import java.util.List;

public interface IAreaInfoService {
    List<AreaInfo> findAll();

    PageResult findPage(AreaInfo areaInfo, int pageNum, int pageSize);

    void add(AreaInfo areaInfo);

    AreaInfo findone(int id);

    void update(AreaInfo areaInfo);

    void deleteOne(int id);

}
