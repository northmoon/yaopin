package com.yaopin.service;

import com.yaopin.entity.DrugCategory;
import com.yaopin.entity.PageResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.util.List;
import java.util.Map;

public interface IDrugCategoryService {
    /**
     * 返回全部列表
     *
     * @return
     */
    public Map<String,DrugCategory> findAll();


    /**
     * 返回分页列表
     *
     * @return
     */
    public PageResult findPage(int pageNum, int pageSize);


    /**
     * 增加
     */
    public void add(DrugCategory drugCategory) throws Exception;


    /**
     * 修改
     */
    public void update(DrugCategory drugCategory);


    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    public DrugCategory findOne(Integer id);


    /**
     * 批量删除
     *
     * @param ids
     */
    public void delete(Integer[] ids);

    /**
     * 分页
     *
     * @param pageNum  当前页 码
     * @param pageSize 每页记录数
     * @return
     */
    public PageResult findPage(DrugCategory drugCategory, int pageNum, int pageSize);

    /**
     * 删除单个
     *
     * @param id
     */
    public void deleteOne(Integer id);

    Map<String, String> saveMore(MultipartFile file) throws Exception;

    void exportExcel(ServletOutputStream outputStream, DrugCategory drugCategory);

    void exportsearch(ServletOutputStream outputStream, DrugCategory drugCategory);
}
