package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.UserInfo;

public interface UserService {



    /**
     * 分页
     *
     * @param pageNum  当前页 码
     * @param pageSize 每页记录数
     * @return
     */
    public PageResult findPage(UserInfo userInfo, int pageNum , int pageSize);

}
