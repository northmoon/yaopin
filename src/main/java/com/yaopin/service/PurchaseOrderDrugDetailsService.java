package com.yaopin.service;

import com.yaopin.entity.PurchaseOrderDrugDetails;

import java.util.List;

/**
 * @ClassName PurchaseOrderDrugDetailsService
 * @Author donghongyu
 * @Date 2019/9/14 17:24
 **/
public interface PurchaseOrderDrugDetailsService {
/**
** @Description  根据采购单id查询药品明细
 * @Param
 */
List<PurchaseOrderDrugDetails> findList(int id);

/**
** @Description  根据id删除
 * @Param
 */
   void delete(Integer[] ids);

   void baoCun(List<Integer> ids, List<Integer>caigouliang);
}
