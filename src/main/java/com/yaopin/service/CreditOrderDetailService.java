package com.yaopin.service;

import com.yaopin.entity.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface CreditOrderDetailService {
    /**
     * * @Description  展示退货单信息
     *
     * @Param
     */
    CreditOrder findSession(Integer creditOrderId1);
    /**
     * * @Description  把退货药品添加到退货单明细中
     *
     * @Param
     */
    void add(Integer [] ids, HttpServletRequest request);
    /**
    ** @Description  查询退货明细
     * @Param
     */
    PageResult findPage(CreditOrderDetail creditOrderDetail, int page, int rows,HttpServletRequest request);

    void deleteSome(Integer []  ids);

    void update(List<String> ids, List<String> returnNum, List<String> returnSum);
}
