package com.yaopin.service;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.Statements;
import com.yaopin.entity.StatementsDetail;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
public interface StatementsDetailService {
    Statements getSession(Integer statementsId);

    void add(List<Integer> ids, HttpServletRequest request);

    PageResult findPage(StatementsDetail statementsDetail, int page, int rows,HttpServletRequest request);

    void delete(List<Integer> ids);
}
