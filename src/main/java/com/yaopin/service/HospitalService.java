package com.yaopin.service;

import com.yaopin.entity.AreaInfo;
import com.yaopin.entity.Hospital;
import com.yaopin.entity.PageResult;

import java.util.List;

public interface HospitalService {
    /**
     * 查询所有数据 分页查询
     * @param hospital
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageResult findPage(Hospital hospital,int pageNum , int pageSize);

    /**
     * 按照医院表id删除单个信息
     * @param id
     */
    public void deletehospital(Integer id);

    /**
     * 新增
     * @param hospital
     */
    public int addhospital(Hospital hospital) throws Exception;


    /**
     * 根据id修改
     * @param id
     * @return
     */
    public Hospital findOne(Integer id);
    /**
     * 修改
     * @param hospital
     */
    public void update(Hospital hospital);



    /**
     * 获得地区表信息
     * @return
     */
     List<AreaInfo> findAll();




}
