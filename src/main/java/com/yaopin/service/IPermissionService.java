package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.Permission;

import java.util.List;
import java.util.Map;

public interface IPermissionService {
    /**
     * 返回全部列表
     *
     * @return
     */
    public List<Permission> findAll();


    /**
     * 返回分页列表
     *
     * @return
     */
    public PageResult findPage(int pageNum, int pageSize);


    /**
     * 增加
     */
    public void add(Permission Permission) throws Exception;


    /**
     * 修改
     */
    public void update(Permission permission) throws Exception;


    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    public Permission findOne(Integer id);





    /**
     * 删除单个
     *
     * @param id
     */
    public void deleteOne(Integer id);

}
