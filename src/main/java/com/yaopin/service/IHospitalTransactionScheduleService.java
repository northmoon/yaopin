package com.yaopin.service;

import com.yaopin.entity.HospitalTransactionSchedule;
import com.yaopin.entity.PageResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.util.List;
import java.util.Map;

public interface IHospitalTransactionScheduleService {
    /**
     * 返回全部列表
     *
     * @return
     */
    public List<HospitalTransactionSchedule> findAll();


    /**
     * 返回分页列表
     *
     * @return
     */
    public PageResult findPage(int pageNum, int pageSize);


    /**
     * 增加
     */
    public void add(HospitalTransactionSchedule hospitalTransactionSchedule) throws Exception;


    /**
     * 修改
     */
    public void update(HospitalTransactionSchedule hospitalTransactionSchedule) throws Exception;


    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    public HospitalTransactionSchedule findOne(Integer id);


    /**
     * 批量删除
     *
     * @param ids
     */
    public void delete(Integer[] ids);

    /**
     * 分页
     *
     * @param pageNum  当前页 码
     * @param pageSize 每页记录数
     * @return
     */
    public PageResult findPage(HospitalTransactionSchedule hospitalTransactionSchedule, int pageNum, int pageSize);


    void exportExcel(ServletOutputStream outputStream,HospitalTransactionSchedule hospitalTransactionSchedule);
}
