package com.yaopin.service;

import com.yaopin.entity.UserInfo;

public interface ILoginService {
    UserInfo findUid(String uid);
}
