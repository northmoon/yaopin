package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.SupplierList;

import java.util.List;
import java.util.Map;

public interface ISupplierListAuditService {
    /**
     * 返回全部列表
     *
     * @return
     */
    public Map<String,SupplierList> findAll();


    /**
     * 返回分页列表
     *
     * @return
     */
    public PageResult findPage(int pageNum, int pageSize);




    /**
     * 修改
     */
    public String update(Map<String, List<String>> map);


    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    public SupplierList findOne(Integer id);



    /**
     * 分页
     *
     * @param pageNum  当前页 码
     * @param pageSize 每页记录数
     * @return
     */
    public PageResult findPage(SupplierList supplierList, int pageNum, int pageSize);



}
