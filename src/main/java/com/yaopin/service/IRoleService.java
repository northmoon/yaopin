package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.Role;

import java.util.List;
import java.util.Map;

public interface IRoleService {
    /**
     * 返回全部列表
     *
     * @return
     */
    public List<Role> findAll();



    /**
     * 增加
     */
    public void add(Role role,int[] ids) throws Exception;


    /**
     * 修改
     */
    public void update(Role role,int[] ids) throws Exception;


    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    public Role findOne(Integer id);




    /**
     * 删除单个
     *
     * @param id
     */
    public void deleteOne(Integer id);

}
