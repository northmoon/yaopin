package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.Statements;

import javax.servlet.http.HttpServletRequest;

public interface StatementsService {
    /**
    ** @Description  添加结算单
     * @Param  
     */
    void add(Statements statements);
    /**
    ** @Description  查询全部结算单
     * @Param  
     */
    PageResult findPage(Statements statements, int page, int rows);

    void save(HttpServletRequest request);
}
