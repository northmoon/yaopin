package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.QualityLevel;
import com.yaopin.entity.Supplier;

import java.util.List;
import java.util.Map;

public interface ISupplierService {
    void saveWithList(List<Integer> list);

    void removeWithList(List<Integer> list);

    Map<String,Supplier> findAll();

    PageResult findPage(Supplier supplier,int pageNum, int pageSize);

    void add(Supplier supplier);

    Supplier findone(int id);

    void deleteOne(int id);

    void updete(Supplier supplier);
}
