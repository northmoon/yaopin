package com.yaopin.service;

import com.yaopin.entity.OrganizationInfo;
import com.yaopin.entity.PageResult;

public interface OrganizationInfoService  {
   /**
    * 分页查询
    * @param organizationInfo
    * @param pageNum
    * @param pageSize
    * @return
    */
   public PageResult findPage(OrganizationInfo organizationInfo,int pageNum , int pageSize);

   /**
    * 新增
    * @param organizationInfo
    * @return
    * @throws Exception
    */
   public int addhospital(OrganizationInfo organizationInfo) throws Exception;

   /**
    * 删除
    * @param oid
    */
   public void deleteOrInfo(Integer oid);

   /**
    * 根据所选id进行查询并然后修改
    * @param oid
    * @return
    */
   public OrganizationInfo findOne(Integer oid);
   /**
    * 修改
    * @param organizationInfo
    */
   public void update(OrganizationInfo organizationInfo);



}
