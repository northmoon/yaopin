package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.SearchReturnorder;
import com.yaopin.entity.SearchSettlement;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.util.List;
import java.util.Map;

public interface ISettlementService {
    /**
     * 分页
     *
     * @param pageNum  当前页 码
     * @param pageSize 每页记录数
     * @return
     */
    PageResult findReturn(SearchSettlement searchSettlement, int pageNum, int pageSize);

    Integer confirmSupplier(List<Integer> list);

    Integer unableSupplier(List<Integer> list);

    void exportsearch(ServletOutputStream outputStream, SearchSettlement searchSettlement1);

    Map<String, String> saveMore(MultipartFile file) throws Exception;

    void saveWithList(List<Integer> list);

}
