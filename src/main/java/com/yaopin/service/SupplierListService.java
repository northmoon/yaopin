package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.SupplierList;

import java.util.List;

public interface SupplierListService {

/*
* 查询
**/
    PageResult findPage(SupplierList supplierList, int page, int rows);

    void addPn(List<Integer> idList);
}
