package com.yaopin.service.impl;

import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.*;
import com.yaopin.service.StatementsDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName StatementsDetailServiceImpl
 * @Author donghongyu
 * @Date 2019/9/23 9:27
 **/
@Service
public class StatementsDetailServiceImpl  implements StatementsDetailService {
    @Resource
    private StatementsMapper statementsMapper;
    @Resource
    private StatementsDetailMapper statementsDetailMapper;
    @Resource
    private PurchaseOrderWarehousingMapper purchaseOrderWarehousingMapper;
    @Resource
    private PurchaseOrderDrugDetailsMapper purchaseOrderDrugDetailsMapper;
    @Resource
    private CreditOrderDetailMapper creditOrderDetailMapper;
    @Override
    public Statements getSession(Integer statementsId) {
        return statementsMapper.selectByPrimaryKey(statementsId);
    }

    @Override
    public void add(List<Integer> ids, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer statementsId =(Integer) session.getAttribute("statementsId");
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample = new PurchaseOrderDrugDetailsExample();
        PurchaseOrderDrugDetailsExample.Criteria criteria = purchaseOrderDrugDetailsExample.createCriteria();
        for (int i = 0; i < ids.size(); i++) {
            StatementsDetail statementsDetail = new StatementsDetail();
            PurchaseOrderWarehousing purchaseOrderWarehousing = purchaseOrderWarehousingMapper.selectByPrimaryKey(ids.get(i));
           Integer purchaseOrderId = purchaseOrderWarehousing.getPurchaseOrderId();
            Integer durgInformationId = purchaseOrderWarehousing.getDurgInformationId();
            criteria.andDrugInformationIdEqualTo(durgInformationId);
            criteria.andPurchaseNoteIdEqualTo(purchaseOrderId);
            List<PurchaseOrderDrugDetails> list = purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
            for (PurchaseOrderDrugDetails purchaseOrderDrugDetails : list) {
                Float purchaseAmount = purchaseOrderDrugDetails.getPurchaseAmount();
                Integer amountPurchased = purchaseOrderDrugDetails.getAmountPurchased();
                statementsDetail.setPurchaseOrderId(purchaseOrderId);
                statementsDetail.setStatementsId(statementsId);
                statementsDetail.setDurgInformationId(durgInformationId);
                statementsDetail.setSettlementAmount(amountPurchased);
                statementsDetail.setSettlementSum(purchaseAmount);
                statementsDetail.setState(1);
                statementsDetailMapper.insert(statementsDetail);
            }
        }

    }

    @Override
    public void delete(List<Integer> ids) {
        for (int i = 0; i < ids.size(); i++) {
            statementsDetailMapper.deleteByPrimaryKey(ids.get(i));
        }
    }

    @Override
    public PageResult findPage(StatementsDetail statementsDetail, int page, int rows,HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer statementsId = (Integer)session.getAttribute("statementsId");
        StatementsDetailExample statementsExample = new StatementsDetailExample();
        StatementsDetailExample.Criteria criteria3 = statementsExample.createCriteria();
        criteria3.andStatementsIdEqualTo(statementsId);
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample = new PurchaseOrderDrugDetailsExample();
        PurchaseOrderDrugDetailsExample.Criteria criteria = purchaseOrderDrugDetailsExample.createCriteria();
        PurchaseOrderWarehousingExample purchaseOrderWarehousingExample = new PurchaseOrderWarehousingExample();
        PurchaseOrderWarehousingExample.Criteria criteria1 = purchaseOrderWarehousingExample.createCriteria();
        CreditOrderDetailExample creditOrderDetailExample = new CreditOrderDetailExample();
        CreditOrderDetailExample.Criteria criteria2 = creditOrderDetailExample.createCriteria();
        List<StatementsDetail> statementsDetails = statementsDetailMapper.selectByExample(statementsExample);
        List<Combination> objects = new ArrayList<>();
        List<Combination> objects1 = new ArrayList<>();
        for (StatementsDetail detail : statementsDetails) {
            Combination combination = new Combination();
            combination.setStatementsDetail(detail);
            Integer durgInformationId = detail.getDurgInformationId();
            Integer purchaseOrderId = detail.getPurchaseOrderId();
            criteria.andPurchaseNoteIdEqualTo(purchaseOrderId);
            criteria.andDrugInformationIdEqualTo(durgInformationId);
            List<PurchaseOrderDrugDetails> list = purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
            for (int i = 0; i < list.size(); i++) {
                combination.setPurchaseOrderDrugDetails(list.get(0));
            }
            criteria1.andPurchaseOrderIdEqualTo(purchaseOrderId);
            criteria1.andDurgInformationIdEqualTo(durgInformationId);
            List<PurchaseOrderWarehousing> purchaseOrderWarehousings = purchaseOrderWarehousingMapper.selectByExample(purchaseOrderWarehousingExample);
            for (int i = 0; i < purchaseOrderWarehousings.size(); i++) {
                combination.setPurchaseOrderWarehousing(purchaseOrderWarehousings.get(0));
            }
            criteria2.andPurchaseOrderIdEqualTo(purchaseOrderId);
            criteria2.andDurgInformationIdEqualTo(durgInformationId);
            List<CreditOrderDetail> creditOrderDetails = creditOrderDetailMapper.selectByExample(creditOrderDetailExample);
            for (int i = 0; i < creditOrderDetails.size(); i++) {
                combination.setCreditOrderDetail(creditOrderDetails.get(0));
            }
            objects.add(combination);
        }
        for (Combination object : objects) {
            Combination clone = (Combination) object.clone();
            objects1.add(clone);
        }
        PageHelper.startPage(page,rows);
        return new PageResult( Long.valueOf(objects1.size()),objects1);
    }
}
