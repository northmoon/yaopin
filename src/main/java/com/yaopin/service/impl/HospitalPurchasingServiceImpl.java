package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.*;
import com.yaopin.service.HospitalPurchasingService;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * @ClassName HospitalPurchasingServiceImpl
 * @Author donghongyu
 * @Date 2019/9/9 9:23
 **/
@Service
public class HospitalPurchasingServiceImpl implements HospitalPurchasingService {
    @Resource
    private SupplierListMapper supplierListMapper;
    @Resource
    private HospitalPurchasingMapper hospitalPurchasingMapper;
    @Resource
    private DrugCategoryMapper drugCategoryMapper;
    @Resource
    private DrugInformationMapper drugInformationMapper;
    @Resource
    private DrugClassificationMapper drugClassificationMapper;
    @Resource
    private PurchaseOrderDrugDetailsMapper purchaseOrderDrugDetailsMapper;
    /**
     * * @Description  分页+查询
     *
     * @Param
     */
    @Override
    public PageResult findPage(HospitalPurchasing hospitalPurchasing, int page, int rows) {
        HospitalPurchasingExample hospitalPurchasingExample = new HospitalPurchasingExample();
        HospitalPurchasingExample.Criteria criteria1 = hospitalPurchasingExample.createCriteria();
        //拼接采购单维护条件
        SupplierListExample supplierListExample = new SupplierListExample();
        SupplierListExample.Criteria criteria = supplierListExample.createCriteria();
        SupplierList supplierList = hospitalPurchasing.getSupplierList();
        //获取供货单里面的id
        List<Integer> idList = new ArrayList<>();
        List<SupplierList> supplierLists = supplierListMapper.selectByExample(null);
        for (SupplierList list : supplierLists) {
            Integer id = list.getId();
            idList.add(id);
        }
        if (hospitalPurchasing != null) {
            //拼接供货单条件
            if (supplierList != null) {
                DrugInformation drugInformation = supplierList.getDrugInformation();
                DrugInformationExample drugInformationExample = new DrugInformationExample();
                DrugInformationExample.Criteria criteria2 = drugInformationExample.createCriteria();
                //拼接药品信息条件
                if (drugInformation != null) {
                    DrugCategory drugCategory = drugInformation.getDrugCategory();
                    DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
                    DrugCategoryExample.Criteria criteria3 = drugCategoryExample.createCriteria();
                    //拼接药品品目条件
                    if (drugCategory != null) {
                        if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                            criteria3.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                        }
                        if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                            criteria3.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                        }
                        if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                            criteria3.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                        }
                        if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                            criteria3.andUnitLike("%" + drugCategory.getUnit() + "%");
                        }
                        if (drugCategory.getCoefficient() != null) {
                            criteria3.andCoefficientEqualTo(drugCategory.getCoefficient());
                        }
                        if (drugCategory.getDrugClassificationId() != null) {
                            criteria3.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                        }

                        List<Integer> drugCategoryIds = new ArrayList<Integer>();
                        List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                        if (drugCategories.size() != 0) {
                            for (DrugCategory category : drugCategories) {
                                Integer drugCategoryId = category.getId();
                                drugCategoryIds.add(drugCategoryId);
                            }
                        }
                        if (drugCategoryIds.size() != 0) {
                            criteria2.andDrugCategoryIdIn(drugCategoryIds);
                        } else {
                            drugCategoryIds.add(0);
                            criteria2.andDrugCategoryIdIn(drugCategoryIds);
                        }
                    }
                    if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0) {
                        criteria2.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                    }
                    if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0) {
                        criteria2.andEnterpriseNameLike("%" + drugInformation.getEnterpriseName() + "%");
                    }
                    if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0) {
                        criteria2.andDrugNameLike("%" + drugInformation.getDrugName() + "%");
                    }
                    if (drugInformation.getState() != null) {
                        criteria2.andStateEqualTo(drugInformation.getState());
                    }
                    if (drugInformation.getMinPrice() != null && drugInformation.getMinPrice().toString().length() > 0 && drugInformation.getMaxPrice() != null && drugInformation.getMaxPrice().toString().length() > 0) {
                        criteria2.andBiddingPriceBetween(drugInformation.getMinPrice(), drugInformation.getMaxPrice());
                    }
                }
                List<Integer> drugInformationIds = new ArrayList<Integer>();
                List<DrugInformation> drugInformations = drugInformationMapper.selectByExample(drugInformationExample);
                for (DrugInformation information : drugInformations) {
                    Integer informationId = information.getId();
                    drugInformationIds.add(informationId);
                }
                if (drugInformationIds.size() != 0) {
                    criteria.andDrugInformationIdIn(drugInformationIds);
                } else {
                    drugInformationIds.add(0);
                    criteria.andDrugInformationIdIn(drugInformationIds);
                }
                if (supplierList.getSupplyState() != null) {
                    criteria.andSupplyStateEqualTo(supplierList.getSupplyState());
                }
            }
            List<Integer> supplierListIds = new ArrayList<Integer>();
            List<SupplierList> drugInformations = supplierListMapper.selectByExample(supplierListExample);
            for (SupplierList information : drugInformations) {
                Integer informationId = information.getId();
                supplierListIds.add(informationId);
            }
            if (supplierListIds.size() != 0) {
                criteria1.andSupplierListIdIn(supplierListIds);
            } else {
                supplierListIds.add(0);
                criteria1.andSupplierListIdIn(supplierListIds);
            }
           /* if (idList.size()!=0) {
                criteria1.andSupplierListIdNotIn(idList);
            }*/
        }
        PageHelper.startPage(page, rows);
        Page<HospitalPurchasing> hospitalPurchasing1 = (Page<HospitalPurchasing>) hospitalPurchasingMapper.selectByExample(hospitalPurchasingExample);
        List<HospitalPurchasing> result = hospitalPurchasing1.getResult();
        List<HospitalPurchasing> objects = new ArrayList<HospitalPurchasing>();
        for (HospitalPurchasing purchasing : result) {
            HospitalPurchasing clone = (HospitalPurchasing) purchasing.clone();
            objects.add(clone);
        }
        return new PageResult(hospitalPurchasing1.getTotal(), objects);
    }
    /**
     * * @Description  根据id删除
     *
     * @Param
     */
    @Override
    public void removeIdList(int[] idList) {
        for (int i = 0; i < idList.length; i++) {
            hospitalPurchasingMapper.deleteByPrimaryKey(idList[i]);
        }
    }
    /**
     * * @Description  导出
     * @Param
     */
    @Override
    public void exportExcel(ServletOutputStream outputStream) {
        List<HospitalPurchasing> hospitalPurchasings = new ArrayList<>();
        List<HospitalPurchasing> hospitalPurchasing1 = hospitalPurchasingMapper.selectByExample(null);
        String  type[] = {"正常","取消交易"};
        String  type1[] = {"正常","关闭"};

        //遍历采购单信息
        for (HospitalPurchasing drugInformation : hospitalPurchasing1) {
            HospitalPurchasing hospitalPurchasing = new HospitalPurchasing();
            Integer supplierListId = drugInformation.getSupplierListId();
            SupplierList supplierList = supplierListMapper.selectByPrimaryKey(supplierListId);
            hospitalPurchasing.setSupplierList(supplierList);
            //将供货单信息存到采购单里面
            hospitalPurchasings.add(hospitalPurchasing);
        }
        //遍历药品类别
        Map<Integer, String> map = new HashMap<>();
        List<DrugClassification> drugClassifications = drugClassificationMapper.selectByExample(null);
        for (DrugClassification drugClassification : drugClassifications) {
            map.put(drugClassification.getId(),drugClassification.getDrugClassification());
        }
        // 创建一个workbook 对应一个excel应用文件
        // HSSWorkbook 是03版excel 的个格式，XSSWorkbook 是07以上版本，HSS 和 XSS 开头分别代表03,07可以切换
        //HSSFWorkbook workBook = new HSSFWorkbook();
        XSSFWorkbook workbook = new XSSFWorkbook();
        // 在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = workbook.createSheet("采购信息导出");
        XSSFCell cell = null;
        XSSFRow bodyRow = sheet.createRow(0);
        cell = bodyRow.createCell(0);
        cell.setCellValue("药品流水号");

        cell = bodyRow.createCell(1);
        cell.setCellValue("通用名");

        cell = bodyRow.createCell(2);
        cell.setCellValue("剂型");

        cell = bodyRow.createCell(3);
        cell.setCellValue("规格");

        cell = bodyRow.createCell(4);
        cell.setCellValue("单位");

        cell = bodyRow.createCell(5);
        cell.setCellValue("转换系数");

        cell = bodyRow.createCell(6);
        cell.setCellValue("生产企业");

        cell = bodyRow.createCell(7);
        cell.setCellValue("药品名:");

        cell = bodyRow.createCell(8);
        cell.setCellValue("中标价格");

        cell = bodyRow.createCell(9);
        cell.setCellValue("药品类别");

        cell = bodyRow.createCell(10);
        cell.setCellValue("药品交易状态");

        cell = bodyRow.createCell(11);
        cell.setCellValue("供货商 ");

        cell = bodyRow.createCell(12);
        cell.setCellValue("供货状态");
        for (int i = 0; i < hospitalPurchasings.size(); i++) {
            HospitalPurchasing hospitalPurchasing = hospitalPurchasings.get(i);
            XSSFRow dateRow = sheet.createRow(1 + i);
            cell = dateRow.createCell(0);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getDrugInformation().getSerialNum());

            cell = dateRow.createCell(1);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getDrugInformation().getDrugCategory().getGenericName());

            cell = dateRow.createCell(2);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getDrugInformation().getDrugCategory().getDosageForm());

            cell = dateRow.createCell(3);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getDrugInformation().getDrugCategory().getSpecification());

            cell = dateRow.createCell(4);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getDrugInformation().getDrugCategory().getUnit());

            cell = dateRow.createCell(5);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getDrugInformation().getDrugCategory().getCoefficient());

            cell = dateRow.createCell(6);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getDrugInformation().getEnterpriseName());

            cell = dateRow.createCell(7);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getDrugInformation().getDrugName());

            cell = dateRow.createCell(8);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getDrugInformation().getBiddingPrice());

            cell = dateRow.createCell(9);
            cell.setCellValue(map.get(hospitalPurchasing.getSupplierList().getDrugInformation().getDrugCategory().getDrugClassificationId()));

            cell = dateRow.createCell(10);
            cell.setCellValue(type[hospitalPurchasing.getSupplierList().getDrugInformation().getState()-1]);

            cell = dateRow.createCell(11);
            cell.setCellValue(hospitalPurchasing.getSupplierList().getSupplier().getFirmName());

            cell = dateRow.createCell(12);
            cell.setCellValue(type1[hospitalPurchasing.getSupplierList().getSupplyState()-1]);
        }
        try {
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
     /**
      ** @Description  将药品信息添加到药品明细中
       * @Param
     */
    @Override
    public void addPn(List<Integer> idList, HttpServletRequest request) {
        PurchaseOrderDrugDetails purchaseOrderDrugDetails = new PurchaseOrderDrugDetails();
        HttpSession session=request.getSession();
        Integer hospitalId = (Integer)session.getAttribute("hospitalId");
        for (int i = 0; i < idList.size(); i++) {
            Integer integer = idList.get(i);
            HospitalPurchasing hospitalPurchasing = hospitalPurchasingMapper.selectByPrimaryKey(integer);
            Integer supplierListId = hospitalPurchasing.getSupplierListId();
            purchaseOrderDrugDetails.setSuppliersId(supplierListId);
            SupplierList supplierList = supplierListMapper.selectByPrimaryKey(supplierListId);
            Integer drugInformationId = supplierList.getDrugInformationId();
            purchaseOrderDrugDetails.setDrugInformationId(drugInformationId);
            purchaseOrderDrugDetails.setPurchaseNoteId(hospitalId);
            purchaseOrderDrugDetails.setPurchasingStatus("1");
            purchaseOrderDrugDetails.setChangeNum(0);
           purchaseOrderDrugDetailsMapper.insert(purchaseOrderDrugDetails);
        }
    }
}

