package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.*;
import com.yaopin.service.IReturnorderService;
import com.yaopin.service.ISettlementService;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SettlementServiceImpl implements ISettlementService {
    @Autowired
    private PurchaseNoteMapper purchaseNoteMapper;

    @Autowired
    private DrugCategoryMapper drugCategoryMapper;

    @Autowired
    private DrugInformationMapper drugInformationMapper;

    @Autowired
    private HospitalMapper hospitalMapper;

    @Autowired
    private PurchaseOrderDrugDetailsMapper purchaseOrderDrugDetailsMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private CreditOrderMapper creditOrderMapper;

    @Autowired
    private CreditOrderDetailMapper creditOrderDetailMapper;

    @Autowired
    private StatementsMapper statementsMapper;

    @Autowired
    private StatementsDetailMapper statementsDetailMapper;

    @Override
    public PageResult findReturn(SearchSettlement searchSettlement,int pageNum, int pageSize) {
        DrugCategory drugCategory = searchSettlement.getDrugCategory();
        DrugInformation drugInformation = searchSettlement.getDrugInformation();
        PurchaseNote purchaseNote = searchSettlement.getPurchaseNote();
        Statements statements=searchSettlement.getStatements();
        StatementsDetail statementsDetail=searchSettlement.getStatementsDetail();
        String hospitalname=searchSettlement.getHospitalname();

        DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
        DrugInformationExample drugInformationExample =new DrugInformationExample();
        PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
        StatementsExample statementsExample=new StatementsExample();
        StatementsDetailExample statementsDetailExample=new StatementsDetailExample();
        HospitalExample hospitalExample = new HospitalExample();

        DrugCategoryExample.Criteria criteria = drugCategoryExample.createCriteria();
        DrugInformationExample.Criteria criteria2 = drugInformationExample.createCriteria();
        PurchaseNoteExample.Criteria criteria3 = purchaseNoteExample.createCriteria();
        StatementsExample.Criteria criteria4=statementsExample.createCriteria();
        HospitalExample.Criteria criteria5=hospitalExample.createCriteria();
        StatementsDetailExample.Criteria criteria6=statementsDetailExample.createCriteria();

        List<Integer> idList = new ArrayList<Integer>();
        List<Integer> idList2 = new ArrayList<Integer>();
        List<Integer> idList3= new ArrayList<Integer>();
        List<Integer> idList4= new ArrayList<Integer>();
        List<Integer> idList5= new ArrayList<Integer>();


        boolean flag = false;
        if (drugCategory != null) {
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                flag = true;
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                flag = true;
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                flag = true;
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                flag = true;
            }
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }
            if (flag) {
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                if (drugCategories.size() != 0) {
                    for (DrugCategory category : drugCategories) {
                        Integer drugCategoryId = category.getId();
                        idList.add(drugCategoryId);
                    }
                }
            }
        }
            boolean flag2 = false;
            if (idList.size() != 0){
                criteria2.andDrugCategoryIdIn(idList);
                flag2 = true;
            }
            if (drugInformation != null) {
                if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0){
                    criteria2.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                    flag2 = true;
                }
                if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0){
                    criteria2.andSerialNumLike("%" + drugInformation.getEnterpriseName() + "%");
                    flag2 = true;
                }
                if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0){
                    criteria2.andSerialNumLike("%" + drugInformation.getDrugName() + "%");
                    flag2 = true;
                }
                if (drugInformation.getQualityLevelId() != null){
                    criteria2.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
                    flag2 = true;
                }

            }
            if (flag2) {
                List<DrugInformation> drugInformations=drugInformationMapper.selectByExample(drugInformationExample);
                if (drugInformations.size() != 0) {
                    for (DrugInformation information : drugInformations) {
                        Integer drugInformationId = information.getId();
                        idList2.add(drugInformationId);
                    }
                }
            }
        boolean flag3 = false;
        if(hospitalname != null && hospitalname.equals("")){
            criteria5.andHospitalNameLike("%" + hospitalname + "%");
            List<Hospital> hospitals=hospitalMapper.selectByExample(hospitalExample);
            for (Hospital hospital:hospitals) {
                Integer hospitalid=hospital.getId();
                idList3.add(hospitalid);
            }
        }

        if (purchaseNote != null) {
            if (purchaseNote.getPurchaseNumber() != null){
                criteria3.andPurchaseNumberEqualTo(purchaseNote.getPurchaseNumber());
                flag3 = true;
            }
            if (purchaseNote.getPurchaseOrderName() != null){
                criteria3.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
                flag3 = true;
            }
        }

        if (idList3 !=null && idList3.size()>0){
            criteria3.andHospitalIdIn(idList3);
            flag3 = true;
        }
        if (searchSettlement.getPurchasestartTime() != null && searchSettlement.getPurchaseendTime() != null ){
            criteria3.andBuildSingleBetween(searchSettlement.getPurchasestartTime(),searchSettlement.getPurchaseendTime());
            flag3 = true;
        }

        if (flag3) {
            List<PurchaseNote> PurchaseNotes=purchaseNoteMapper.selectByExample(purchaseNoteExample);
            if (PurchaseNotes.size() != 0) {
                for (PurchaseNote purchaseNoteobj : PurchaseNotes) {
                    Integer purchaseNoteId = purchaseNoteobj.getId();
                    idList4.add(purchaseNoteId);
                }
            }
        }

        boolean flag4 = false;
        if (statements != null) {
            if (statements.getStatementsNum() != null && statements.getStatementsNum().length() > 0){
                criteria4.andStatementsNumEqualTo(statements.getStatementsNum());
                flag4 = true;
            }
            if (statements.getStatementsName() != null && statements.getStatementsName().length() > 0){
                criteria4.andStatementsNameLike("%" +statements.getStatementsName() + "%");
                flag4 = true;
            }
        }
        if (searchSettlement.getSettlementstartTime() !=null && searchSettlement.getSettlementendTime() != null){
            criteria4.andCreateTimeBetween(searchSettlement.getSettlementstartTime(),searchSettlement.getSettlementendTime());
            flag4 = true;
        }
        if (flag4) {
           List<Statements> list=statementsMapper.selectByExample(statementsExample);
            for (Statements statements1 : list) {
                idList5.add(statements1.getId());
            }
        }
        if(idList2 !=null && idList2.size()>0){
            criteria6.andDurgInformationIdIn(idList2);
        }

        if(idList4 !=null && idList4.size()>0){
            criteria6.andPurchaseOrderIdIn(idList4);
        }

        if(idList5 !=null && idList5.size()>0){
            criteria6.andStatementsIdIn(idList5);
        }
        if(statementsDetail !=null && statementsDetail.getState() != 0){
            criteria6.andStateEqualTo(statementsDetail.getState());
        }

        ArrayList<StatementsDetail> statementsDetailist=(ArrayList<StatementsDetail>)statementsDetailMapper.selectByExample(statementsDetailExample);
        ArrayList<SearchSettlement> searchSettlement2=new Page<SearchSettlement>();
        for (StatementsDetail orderDetail : statementsDetailist) {
            SearchSettlement searchSettlement1=new SearchSettlement();
            DrugInformation drugInformation1=drugInformationMapper.selectByPrimaryKey(orderDetail.getDurgInformationId());
            DrugCategory drugCategory1=drugCategoryMapper.selectByPrimaryKey(drugInformation1.getDrugCategoryId());
            PurchaseNote purchaseNote1=purchaseNoteMapper.selectByPrimaryKey(orderDetail.getPurchaseOrderId());
            Statements statements2=statementsMapper.selectByPrimaryKey(orderDetail.getStatementsId());
            Hospital hospital=hospitalMapper.selectByPrimaryKey(statements2.getHospitalId());
            searchSettlement1.setDrugInformation(drugInformation1);
            searchSettlement1.setDrugCategory(drugCategory1);
            searchSettlement1.setPurchaseNote(purchaseNote1);
            searchSettlement1.setStatements(statements2);
            searchSettlement1.setStatementsDetail(orderDetail);
            searchSettlement1.setHospitalname(hospital.getHospitalName());
            SearchSettlement SearchSettlement3=(SearchSettlement)searchSettlement1.clone();
            searchSettlement2.add(SearchSettlement3);
        }
        PageHelper.startPage(pageNum, pageSize);
        Page<SearchSettlement> page = (Page<SearchSettlement>) searchSettlement2;
        page.setTotal(page.size());
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public Integer confirmSupplier(List<Integer> list) {
        Integer i=0;
        for (Integer integer : list) {
            PurchaseOrderDrugDetails purchaseOrderDrugDetails=new PurchaseOrderDrugDetails();
            purchaseOrderDrugDetails.setId(integer);
            purchaseOrderDrugDetails.setPurchasingStatus("2");
            purchaseOrderDrugDetailsMapper.updateStateById(purchaseOrderDrugDetails);
            i++;
        }
        return i;
    }

    @Override
    public Integer unableSupplier(List<Integer> list) {
        Integer i=0;
        for (Integer integer : list) {
            PurchaseOrderDrugDetails purchaseOrderDrugDetails=new PurchaseOrderDrugDetails();
            purchaseOrderDrugDetails.setId(integer);
            purchaseOrderDrugDetails.setPurchasingStatus("4");
            purchaseOrderDrugDetailsMapper.updateStateById(purchaseOrderDrugDetails);
            i++;
        }
        return i;
    }

    @Override
    public void exportsearch(ServletOutputStream outputStream, SearchSettlement searchSettlement) {
        DrugCategory drugCategory = searchSettlement.getDrugCategory();
        DrugInformation drugInformation = searchSettlement.getDrugInformation();
        PurchaseNote purchaseNote = searchSettlement.getPurchaseNote();
        Statements statements=searchSettlement.getStatements();
        StatementsDetail statementsDetail=searchSettlement.getStatementsDetail();
        String hospitalname=searchSettlement.getHospitalname();

        DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
        DrugInformationExample drugInformationExample =new DrugInformationExample();
        PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
        StatementsExample statementsExample=new StatementsExample();
        StatementsDetailExample statementsDetailExample=new StatementsDetailExample();
        HospitalExample hospitalExample = new HospitalExample();

        DrugCategoryExample.Criteria criteria = drugCategoryExample.createCriteria();
        DrugInformationExample.Criteria criteria2 = drugInformationExample.createCriteria();
        PurchaseNoteExample.Criteria criteria3 = purchaseNoteExample.createCriteria();
        StatementsExample.Criteria criteria4=statementsExample.createCriteria();
        HospitalExample.Criteria criteria5=hospitalExample.createCriteria();
        StatementsDetailExample.Criteria criteria6=statementsDetailExample.createCriteria();

        List<Integer> idList = new ArrayList<Integer>();
        List<Integer> idList2 = new ArrayList<Integer>();
        List<Integer> idList3= new ArrayList<Integer>();
        List<Integer> idList4= new ArrayList<Integer>();
        List<Integer> idList5= new ArrayList<Integer>();


        boolean flag = false;
        if (drugCategory != null) {
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                flag = true;
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                flag = true;
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                flag = true;
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                flag = true;
            }
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }
            if (flag) {
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                if (drugCategories.size() != 0) {
                    for (DrugCategory category : drugCategories) {
                        Integer drugCategoryId = category.getId();
                        idList.add(drugCategoryId);
                    }
                }
            }
        }
        boolean flag2 = false;
        if (idList.size() != 0){
            criteria2.andDrugCategoryIdIn(idList);
            flag2 = true;
        }
        if (drugInformation != null) {
            if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0){
                criteria2.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                flag2 = true;
            }
            if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0){
                criteria2.andSerialNumLike("%" + drugInformation.getEnterpriseName() + "%");
                flag2 = true;
            }
            if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0){
                criteria2.andSerialNumLike("%" + drugInformation.getDrugName() + "%");
                flag2 = true;
            }
            if (drugInformation.getQualityLevelId() != null){
                criteria2.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
                flag2 = true;
            }

        }
        if (flag2) {
            List<DrugInformation> drugInformations=drugInformationMapper.selectByExample(drugInformationExample);
            if (drugInformations.size() != 0) {
                for (DrugInformation information : drugInformations) {
                    Integer drugInformationId = information.getId();
                    idList2.add(drugInformationId);
                }
            }
        }
        boolean flag3 = false;
        if(hospitalname != null && hospitalname.equals("")){
            criteria5.andHospitalNameLike("%" + hospitalname + "%");
            List<Hospital> hospitals=hospitalMapper.selectByExample(hospitalExample);
            for (Hospital hospital:hospitals) {
                Integer hospitalid=hospital.getId();
                idList3.add(hospitalid);
            }
        }

        if (purchaseNote != null) {
            if (purchaseNote.getPurchaseNumber() != null){
                criteria3.andPurchaseNumberEqualTo(purchaseNote.getPurchaseNumber());
                flag3 = true;
            }
            if (purchaseNote.getPurchaseOrderName() != null){
                criteria3.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
                flag3 = true;
            }
        }

        if (idList3 !=null && idList3.size()>0){
            criteria3.andHospitalIdIn(idList3);
            flag3 = true;
        }
        if (searchSettlement.getPurchasestartTime() != null && searchSettlement.getPurchaseendTime() != null ){
            criteria3.andBuildSingleBetween(searchSettlement.getPurchasestartTime(),searchSettlement.getPurchaseendTime());
            flag3 = true;
        }

        if (flag3) {
            List<PurchaseNote> PurchaseNotes=purchaseNoteMapper.selectByExample(purchaseNoteExample);
            if (PurchaseNotes.size() != 0) {
                for (PurchaseNote purchaseNoteobj : PurchaseNotes) {
                    Integer purchaseNoteId = purchaseNoteobj.getId();
                    idList4.add(purchaseNoteId);
                }
            }
        }

        boolean flag4 = false;
        if (statements != null) {
            if (statements.getStatementsNum() != null && statements.getStatementsNum().length() > 0){
                criteria4.andStatementsNumEqualTo(statements.getStatementsNum());
                flag4 = true;
            }
            if (statements.getStatementsName() != null && statements.getStatementsName().length() > 0){
                criteria4.andStatementsNameLike("%" +statements.getStatementsName() + "%");
                flag4 = true;
            }
        }
        if (searchSettlement.getSettlementstartTime() !=null && searchSettlement.getSettlementendTime() != null){
            criteria4.andCreateTimeBetween(searchSettlement.getSettlementstartTime(),searchSettlement.getSettlementendTime());
            flag4 = true;
        }
        if (flag4) {
            List<Statements> list=statementsMapper.selectByExample(statementsExample);
            for (Statements statements1 : list) {
                idList5.add(statements1.getId());
            }
        }
        if(idList2 !=null && idList2.size()>0){
            criteria6.andDurgInformationIdIn(idList2);
        }

        if(idList4 !=null && idList4.size()>0){
            criteria6.andPurchaseOrderIdIn(idList4);
        }

        if(idList5 !=null && idList5.size()>0){
            criteria6.andStatementsIdIn(idList5);
        }
        if(statementsDetail !=null && statementsDetail.getState() != 0){
            criteria6.andStateEqualTo(statementsDetail.getState());
        }

        ArrayList<StatementsDetail> statementsDetailist=(ArrayList<StatementsDetail>)statementsDetailMapper.selectByExample(statementsDetailExample);
        ArrayList<SearchSettlement> searchSettlement2=new Page<SearchSettlement>();
        for (StatementsDetail orderDetail : statementsDetailist) {
            SearchSettlement searchSettlement1=new SearchSettlement();
            DrugInformation drugInformation1=drugInformationMapper.selectByPrimaryKey(orderDetail.getDurgInformationId());
            DrugCategory drugCategory1=drugCategoryMapper.selectByPrimaryKey(drugInformation1.getDrugCategoryId());
            PurchaseNote purchaseNote1=purchaseNoteMapper.selectByPrimaryKey(orderDetail.getPurchaseOrderId());
            Statements statements2=statementsMapper.selectByPrimaryKey(orderDetail.getStatementsId());
            Hospital hospital=hospitalMapper.selectByPrimaryKey(statements2.getHospitalId());
            searchSettlement1.setDrugInformation(drugInformation1);
            searchSettlement1.setDrugCategory(drugCategory1);
            searchSettlement1.setPurchaseNote(purchaseNote1);
            searchSettlement1.setStatements(statements2);
            searchSettlement1.setStatementsDetail(orderDetail);
            searchSettlement1.setHospitalname(hospital.getHospitalName());
            SearchSettlement SearchSettlement3=(SearchSettlement)searchSettlement1.clone();
            searchSettlement2.add(SearchSettlement3);
        }
        // 创建一个workbook 对应一个excel应用文件
        // HSSWorkbook 是03版excel 的个格式，XSSWorkbook 是07以上版本，HSS 和 XSS 开头分别代表03,07可以切换
        //HSSFWorkbook workBook = new HSSFWorkbook();
        XSSFWorkbook workbook = new XSSFWorkbook();
        // 在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = workbook.createSheet("导出说明");
        XSSFCell cell = null;
        XSSFRow bodyRow = sheet.createRow(0);
        cell = bodyRow.createCell(0);
        cell.setCellValue("结算单编号");

        cell = bodyRow.createCell(1);
        cell.setCellValue("结算单名称");

        cell = bodyRow.createCell(2);
        cell.setCellValue("下单医院");

        cell = bodyRow.createCell(3);
        cell.setCellValue("对应采购单编号");

        cell = bodyRow.createCell(4);
        cell.setCellValue("对应采购单名称");

        cell = bodyRow.createCell(5);
        cell.setCellValue("流水号");

        cell = bodyRow.createCell(6);
        cell.setCellValue("通用名");

        cell = bodyRow.createCell(7);
        cell.setCellValue("商品名称");

        cell = bodyRow.createCell(8);
        cell.setCellValue("剂型");

        cell = bodyRow.createCell(9);
        cell.setCellValue("规格");

        cell = bodyRow.createCell(10);
        cell.setCellValue("单位");

        cell = bodyRow.createCell(11);
        cell.setCellValue("转换系数");

        for (int i = 0; i < searchSettlement2.size(); i++) {
            SearchSettlement searchSettlements = searchSettlement2.get(i);
            XSSFRow dateRow = sheet.createRow(1 + i);
            cell = dateRow.createCell(0);
            cell.setCellValue(searchSettlements.getStatements().getStatementsNum());
            cell = dateRow.createCell(1);
            cell.setCellValue(searchSettlements.getStatements().getStatementsName());
            cell = dateRow.createCell(2);
            cell.setCellValue(searchSettlements.getHospitalname());
            cell = dateRow.createCell(3);
            cell.setCellValue(searchSettlements.getPurchaseNote().getPurchaseNumber());
            cell = dateRow.createCell(4);
            cell.setCellValue(searchSettlements.getPurchaseNote().getPurchaseOrderName());
            cell = dateRow.createCell(5);
            cell.setCellValue(searchSettlements.getDrugInformation().getSerialNum());
            cell = dateRow.createCell(6);
            cell.setCellValue(searchSettlements.getDrugCategory().getGenericName());
            cell = dateRow.createCell(7);
            cell.setCellValue(searchSettlements.getDrugInformation().getDrugName());
            cell = dateRow.createCell(8);
            cell.setCellValue(searchSettlements.getDrugCategory().getDosageForm());
            cell = dateRow.createCell(9);
            cell.setCellValue(searchSettlements.getDrugCategory().getSpecification());
            cell = dateRow.createCell(10);
            cell.setCellValue(searchSettlements.getDrugCategory().getUnit());
            cell = dateRow.createCell(11);
            cell.setCellValue(searchSettlements.getDrugCategory().getCoefficient());
        }
        try {
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }




    }

    @Override
    public Map<String, String> saveMore(MultipartFile file) throws Exception {
        return null;
    }


    @Override
    public void saveWithList(List<Integer> list) {
        for (Integer integer : list) {
            StatementsDetail statementsDetail=new StatementsDetail();
            statementsDetail.setId(integer);
            statementsDetail.setState(2);
            statementsDetailMapper.updateByid(statementsDetail);
        }
    }


}
