package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.entity.UserInfoExample.Criteria;
import com.yaopin.mapper.*;
import com.yaopin.service.IPermissionService;
import com.yaopin.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PermissionService implements IPermissionService {
    @Autowired
    private PermissionMapper permissionMapper;


    public List<Permission> findAll() {
        List<Permission> drugCategories = permissionMapper.selectByExample(null);
        return drugCategories;
    }

    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<Permission> page = (Page<Permission>) permissionMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void add(Permission permission) throws Exception {
        Permission returnPremission = permissionMapper.selectByUrl(permission.getUrl());
        if (returnPremission == null) {
            permissionMapper.insert(permission);
        } else {
            String message = "权限已存在，请勿重复添加！";
            throw new Exception(message);
        }
    }

    @Override
    public void update(Permission permission) throws Exception {
        Permission oldDrugCategory = permissionMapper.selectByPrimaryKey(permission.getId());
        Permission returnDrugCategory = permissionMapper.selectByUrl(permission.getUrl());
        if (returnDrugCategory == null || returnDrugCategory.getUrl().equals(oldDrugCategory.getUrl())) {
            permissionMapper.updateByPrimaryKey(permission);
        } else {
            String message = "账号已存在，请重新输入！";
            throw new Exception(message);
        }
    }

    @Override
    public Permission findOne(Integer id) {
        Permission permission = permissionMapper.selectByPrimaryKey(id);
        return permission;
    }

    @Override
    public void deleteOne(Integer id) {
        permissionMapper.deleteByPrimaryKey(id);
    }

}


