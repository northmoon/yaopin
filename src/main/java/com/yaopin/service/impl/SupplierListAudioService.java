package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.entity.SupplierListExample.Criteria;
import com.yaopin.mapper.*;
import com.yaopin.service.ISupplierListAuditService;
import com.yaopin.service.ISupplierListAuditService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SupplierListAudioService implements ISupplierListAuditService {

    @Autowired
    private SupplierListMapper supplierListMapper;

    @Autowired
    private DrugCategoryMapper drugCategoryMapper;

    @Autowired
    private DrugInformationMapper drugInformationMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Override
    public Map<String, SupplierList> findAll() {
        return null;
    }

    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        return null;
    }


    @Override
    public String update(Map<String, List<String>> map) {
        int passNum = 0;
        int noPassNum = 0;
        int errorNum = 0;
        List<String> ids = map.get("ids");
        List<String> states = map.get("states");
        for (int i = 0; i < ids.size(); i++) {
            SupplierList supplierList = supplierListMapper.selectByPrimaryKey(Integer.parseInt(ids.get(i)));
            supplierList.setState(Integer.parseInt(states.get(i)));
            supplierList.setChangenum(supplierList.getChangenum() + 1);
            try {
                supplierListMapper.updateByPrimaryKey(supplierList);
            } catch (Exception e) {
                e.printStackTrace();
                errorNum++;
            }
            if (supplierList.getState() == 1) {
                passNum++;
            } else {
                noPassNum++;
            }
        }
        return "审核通过" + passNum + "条，不通过" + noPassNum + "条，提交失败" + errorNum + "条";
    }

    @Override
    public SupplierList findOne(Integer id) {
        SupplierList supplierList = supplierListMapper.selectByPrimaryKey(id);
        return supplierList;
    }


    @Override
    public PageResult findPage(SupplierList supplierList, int pageNum, int pageSize) {
        SupplierListExample supplierListExample = new SupplierListExample();
        Criteria supplierListCriteria = supplierListExample.createCriteria();
        if (supplierList != null) {
            DrugInformation drugInformation = supplierList.getDrugInformation();
            DrugInformationExample drugInformationExample = new DrugInformationExample();
            DrugInformationExample.Criteria drugInformationCriteria = drugInformationExample.createCriteria();
            if (drugInformation != null) {
                DrugCategory drugCategory = drugInformation.getDrugCategory();
                DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
                DrugCategoryExample.Criteria drugCategoryCriteria = drugCategoryExample.createCriteria();
                boolean flag = false;
                if (drugCategory != null) {
                    if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                        drugCategoryCriteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                    }
                    if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                        drugCategoryCriteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                    }
                    if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                        drugCategoryCriteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                    }
                    if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                        drugCategoryCriteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                    }
                    if (drugCategory.getCoefficient() != null) {
                        drugCategoryCriteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                    }
                    if (drugCategory.getDrugClassificationId() != null) {
                        drugCategoryCriteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                    }
                }
                List<Integer> drugCategoryIds = new ArrayList<Integer>();
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                for (DrugCategory category : drugCategories) {
                    drugCategoryIds.add(category.getId());
                }
                if (drugCategoryIds.size() != 0) {
                    drugInformationCriteria.andDrugCategoryIdIn(drugCategoryIds);
                } else {
                    drugCategoryIds.add(0);
                    drugInformationCriteria.andDrugCategoryIdIn(drugCategoryIds);
                }
                if (drugInformation.getMinPrice() != null && drugInformation.getMaxPrice() != null) {
                    drugInformationCriteria.andBiddingPriceBetween(drugInformation.getMinPrice(), drugInformation.getMaxPrice());
                }
                if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0) {
                    drugInformationCriteria.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                }
                if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0) {
                    drugInformationCriteria.andEnterpriseNameLike("%" + drugInformation.getEnterpriseName() + "%");

                }
                if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0) {
                    drugInformationCriteria.andDrugNameLike("%" + drugInformation.getDrugName() + "%");
                }
                if (drugInformation.getQualityLevelId() != null) {
                    drugInformationCriteria.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
                }
                if (drugInformation.getState() != null) {
                    drugInformationCriteria.andStateEqualTo(drugInformation.getState());
                }
            }
            List<Integer> drugInformationIds = new ArrayList<Integer>();
            List<DrugInformation> drugInformations = drugInformationMapper.selectByExample(drugInformationExample);
            for (DrugInformation information : drugInformations) {
                drugInformationIds.add(information.getId());
            }
            if (drugInformationIds.size() != 0) {
                supplierListCriteria.andDrugInformationIdIn(drugInformationIds);
            }else {
                drugInformationIds.add(0);
                supplierListCriteria.andDrugInformationIdIn(drugInformationIds);
            }
            if (supplierList.getEnterpriseId() != null) {
                supplierListCriteria.andEnterpriseIdEqualTo(supplierList.getEnterpriseId());
            }
            if (supplierList.getState() != null) {
                supplierListCriteria.andStateEqualTo(supplierList.getState());
            }
        }
        PageHelper.startPage(pageNum, pageSize);
        Page<SupplierList> page = (Page<SupplierList>) supplierListMapper.selectByExample(supplierListExample);
        List<SupplierList> supplierLists = page.getResult();
        List<SupplierList> returnList = new ArrayList<SupplierList>();
        for (SupplierList sl : supplierLists) {
            SupplierList supplierList1 = (SupplierList) sl.clone();
            returnList.add(supplierList1);
        }
        return new PageResult(page.getTotal(), returnList);
    }
}


