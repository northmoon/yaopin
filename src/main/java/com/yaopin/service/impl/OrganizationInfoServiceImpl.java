package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.OrganizationInfo;
import com.yaopin.entity.OrganizationInfoExample.Criteria;
import com.yaopin.entity.OrganizationInfoExample;
import com.yaopin.entity.PageResult;
import com.yaopin.mapper.OrganizationInfoMapper;
import com.yaopin.service.OrganizationInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OrganizationInfoServiceImpl implements OrganizationInfoService {

    @Resource
    private OrganizationInfoMapper organizationInfoDao;

    @Override
    public PageResult findPage(OrganizationInfo organizationInfo, int pageNum, int pageSize) {
        OrganizationInfoExample example = new OrganizationInfoExample();
        Criteria criteria = example.createCriteria();
        if (organizationInfo != null){
            if (organizationInfo.getOname() != null && organizationInfo.getOname().length()>0) {
                criteria.andOnameLike("%" + organizationInfo.getOname() + "%");
            }
            if (organizationInfo.getAddress() != null && organizationInfo.getAddress().length() > 0){
                criteria.andAddressLike("%"+ organizationInfo.getAddress() +"%");
            }
            if (organizationInfo.getContactname() != null && organizationInfo.getContactname().length() > 0){
                criteria.andContactnameLike("%"+ organizationInfo.getContactname() +"%");
            }
            if (organizationInfo.getFax() != null && organizationInfo.getFax().length() > 0){
                criteria.andFaxEqualTo("%"+ organizationInfo.getFax() +"%");
            }
        }
        PageHelper.startPage(pageNum,pageSize);
        Page<OrganizationInfo> page = (Page<OrganizationInfo>) organizationInfoDao.selectByExample(example);
        List<OrganizationInfo> result = page.getResult();
        return new PageResult(page.getTotal(),result);
    }


    @Override
    public int addhospital(OrganizationInfo organizationInfo) throws Exception {
        OrganizationInfo returnOrInfo = organizationInfoDao.selectOrganizationInfo(organizationInfo);
    if (returnOrInfo == null) {
        return organizationInfoDao.insert(organizationInfo);
    }else {
        String message = "监督机构名称: [" + returnOrInfo.getOname() + "],联系地址:[" + returnOrInfo.getAddress() + "], 邮政编码:[" + returnOrInfo.getPostcode() + "], 联系人:[" + returnOrInfo.getContactname() + "], 联系电话:[" + returnOrInfo.getContactphone() + "], 传真:[" + returnOrInfo.getFax() + "], 邮箱email:[" + returnOrInfo.getEmail() + "],网址:[" + returnOrInfo.getWebsite() + "]";
        throw  new Exception(message);
    }
    }

    @Override
    public void deleteOrInfo(Integer oid) {
        organizationInfoDao.deleteByPrimaryKey(oid);
    }


    @Override
    public OrganizationInfo findOne(Integer oid) {
        OrganizationInfo organizationInfo = organizationInfoDao.selectByPrimaryKey(oid);
        return organizationInfo;
    }
    @Override
    public void update(OrganizationInfo organizationInfo) {
        organizationInfoDao.updateByPrimaryKey(organizationInfo);
    }



}
