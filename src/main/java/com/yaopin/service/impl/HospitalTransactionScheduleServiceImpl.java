package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.entity.DrugCategoryExample.Criteria;
import com.yaopin.mapper.*;
import com.yaopin.service.IDrugInformationService;
import com.yaopin.service.IHospitalTransactionScheduleService;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HospitalTransactionScheduleServiceImpl implements IHospitalTransactionScheduleService {

    @Autowired
    private HospitalTransactionScheduleMapper hospitalTransactionScheduleMapper;

    @Autowired
    private PurchaseOrderDrugDetailsMapper purchaseOrderDrugDetailsMapper;
    @Autowired
    private DrugCategoryMapper drugCategoryMapper;

    @Autowired
    private DrugInformationMapper drugInformationMapper;

    @Autowired
    private HospitalMapper hospitalMapper;

    @Autowired
    private PurchaseNoteMapper purchaseNoteMapper;

    @Override
    public List<HospitalTransactionSchedule> findAll() {
        return null;
    }

    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        return null;
    }

    @Override
    public void add(HospitalTransactionSchedule hospitalTransactionSchedule) throws Exception {

    }

    @Override
    public void update(HospitalTransactionSchedule hospitalTransactionSchedule) throws Exception {

    }

    @Override
    public HospitalTransactionSchedule findOne(Integer id) {
        HospitalTransactionSchedule hospitalTransactionSchedule = hospitalTransactionScheduleMapper.selectByPrimaryKey(id);
        HospitalTransactionSchedule clone = (HospitalTransactionSchedule) hospitalTransactionSchedule.clone();
        return clone;
    }

    @Override
    public void delete(Integer[] ids) {

    }

    @Override
    public PageResult findPage(HospitalTransactionSchedule hospitalTransactionSchedule, int pageNum, int pageSize) {
        HospitalTransactionScheduleExample hospitalTransactionScheduleExample = new HospitalTransactionScheduleExample();
        HospitalTransactionScheduleExample.Criteria transactionScheduleCriteria = hospitalTransactionScheduleExample.createCriteria();

        PurchaseOrderDrugDetails purchaseOrderDrugDetails = hospitalTransactionSchedule.getPurchaseOrderDrugDetails();
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample = new PurchaseOrderDrugDetailsExample();
        PurchaseOrderDrugDetailsExample.Criteria purchaseOrderDrugDetailsCriteria = purchaseOrderDrugDetailsExample.createCriteria();

        //拼接采购单药品信息详情条件
        if (purchaseOrderDrugDetails != null) {
            DrugInformation drugInformation = purchaseOrderDrugDetails.getDrugInformation();
            DrugInformationExample drugInformationExample = new DrugInformationExample();
            DrugInformationExample.Criteria drugInformationCriteria = drugInformationExample.createCriteria();
            PurchaseNote purchaseNote = purchaseOrderDrugDetails.getPurchaseNote();
            PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
            PurchaseNoteExample.Criteria purchaseNoteCriteria = purchaseNoteExample.createCriteria();

            //药品信息条件拼接
            if (drugInformation != null) {
                DrugCategory drugCategory = drugInformation.getDrugCategory();
                DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
                Criteria drugCategoryCriteria = drugCategoryExample.createCriteria();
                if (drugCategory != null) {
                    if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                        drugCategoryCriteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                    }
                    if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                        drugCategoryCriteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                    }
                    if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                        drugCategoryCriteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                    }
                    if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                        drugCategoryCriteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                    }
                    if (drugCategory.getCoefficient() != null) {
                        drugCategoryCriteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                    }
                    if (drugCategory.getDrugClassificationId() != null) {
                        drugCategoryCriteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                    }
                }
                List<Integer> drugCategoryIds = new ArrayList<Integer>();
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                if (drugCategories.size() != 0) {
                    for (DrugCategory category : drugCategories) {
                        Integer drugCategoryId = category.getId();
                        drugCategoryIds.add(drugCategoryId);
                    }
                }
                if (drugCategoryIds.size() != 0) {
                    drugInformationCriteria.andDrugCategoryIdIn(drugCategoryIds);
                } else {
                    drugCategoryIds.add(0);
                    drugInformationCriteria.andDrugCategoryIdIn(drugCategoryIds);
                }

                if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0) {
                    drugInformationCriteria.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                }
                if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0) {
                    drugInformationCriteria.andEnterpriseNameLike("%" + drugInformation.getEnterpriseName() + "%");
                }
                if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0) {
                    drugInformationCriteria.andDrugNameLike("%" + drugInformation.getDrugName() + "%");
                }
                if (drugInformation.getQualityLevelId() != null) {
                    drugInformationCriteria.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
                }
            }
            List<Integer> drugInformationIds = new ArrayList<Integer>();
            List<DrugInformation> drugInformations = drugInformationMapper.selectByExample(drugInformationExample);
            for (DrugInformation information : drugInformations) {
                Integer informationId = information.getId();
                drugInformationIds.add(informationId);
            }
            if (drugInformationIds.size() != 0) {
                purchaseOrderDrugDetailsCriteria.andDrugInformationIdIn(drugInformationIds);
            } else {
                drugInformationIds.add(0);
                purchaseOrderDrugDetailsCriteria.andDrugInformationIdIn(drugInformationIds);
            }

            //采购单条件拼接
            if (purchaseNote != null) {
                Hospital hospital = purchaseNote.getHospital();
                HospitalExample hospitalExample = new HospitalExample();
                HospitalExample.Criteria hospitalCriteria = hospitalExample.createCriteria();
                if (hospital != null) {
                    if (hospital.getHospitalName() != null && hospital.getHospitalName().length() > 0) {
                        hospitalCriteria.andHospitalNameLike("%" + hospital.getHospitalName() + "%");
                    }
                }
                List<Hospital> hospitals = hospitalMapper.selectByExample(hospitalExample);
                List<Integer> hospitalIds = new ArrayList<Integer>();
                for (Hospital hospital1 : hospitals) {
                    Integer hospital1Id = hospital1.getId();
                    hospitalIds.add(hospital1Id);
                }
                if (hospitalIds.size() != 0) {
                    purchaseNoteCriteria.andHospitalIdIn(hospitalIds);
                } else {
                    hospitalIds.add(0);
                    purchaseNoteCriteria.andHospitalIdIn(hospitalIds);
                }
                if (purchaseNote.getPurchaseNumber() != null && purchaseNote.getPurchaseNumber().length() > 0) {
                    purchaseNoteCriteria.andPurchaseNumberLike("%" + purchaseNote.getPurchaseNumber() + "%");
                }
                if (purchaseNote.getPurchaseOrderName() != null && purchaseNote.getPurchaseOrderName().length() > 0) {
                    purchaseNoteCriteria.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
                }
                if (purchaseNote.getPurchaseOrderStatus() != null) {
                    purchaseNoteCriteria.andPurchaseOrderStatusEqualTo(purchaseNote.getPurchaseOrderStatus());
                }
                if (purchaseNote.getStartTime() != null && purchaseNote.getEndTime() != null) {
                    purchaseNoteCriteria.andBuildSingleBetween(purchaseNote.getStartTime(), purchaseNote.getEndTime());
                }
            }
            List<PurchaseNote> purchaseNotes = purchaseNoteMapper.selectByExample(purchaseNoteExample);
            List<Integer> purchaseNoteIds = new ArrayList<Integer>();
            for (PurchaseNote note : purchaseNotes) {
                Integer noteId = note.getId();
                purchaseNoteIds.add(noteId);
            }
            if (purchaseNoteIds.size() != 0) {
                purchaseOrderDrugDetailsCriteria.andPurchaseNoteIdIn(purchaseNoteIds);
            } else {
                purchaseNoteIds.add(0);
                purchaseOrderDrugDetailsCriteria.andPurchaseNoteIdIn(purchaseNoteIds);
            }
            if (purchaseOrderDrugDetails.getSuppliersId() != null) {
                purchaseOrderDrugDetailsCriteria.andSuppliersIdEqualTo(purchaseOrderDrugDetails.getSuppliersId());
            }

            if (purchaseOrderDrugDetails.getPurchasingStatus() != null && purchaseOrderDrugDetails.getPurchasingStatus().length() != 0) {
                purchaseOrderDrugDetailsCriteria.andPurchasingStatusEqualTo(purchaseOrderDrugDetails.getPurchasingStatus());
            }
        }
        List<PurchaseOrderDrugDetails> purchaseOrderDrugDetailsList = purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
        List<Integer> purchaseOrderDrugDetailsIds = new ArrayList<Integer>();
        for (PurchaseOrderDrugDetails orderDrugDetails : purchaseOrderDrugDetailsList) {
            Integer id = orderDrugDetails.getId();
            purchaseOrderDrugDetailsIds.add(id);
        }
        if (purchaseOrderDrugDetailsIds.size() != 0) {
            transactionScheduleCriteria.andPurchaseOrderDetailIdIn(purchaseOrderDrugDetailsIds);
        } else {
            purchaseOrderDrugDetailsIds.add(0);
            transactionScheduleCriteria.andPurchaseOrderDetailIdIn(purchaseOrderDrugDetailsIds);
        }

        PageHelper.startPage(pageNum, pageSize);
        Page<HospitalTransactionSchedule> page = (Page<HospitalTransactionSchedule>) hospitalTransactionScheduleMapper.selectByExample(hospitalTransactionScheduleExample);
        List<HospitalTransactionSchedule> result = page.getResult();
        List<HospitalTransactionSchedule> returnList = new ArrayList<HospitalTransactionSchedule>();
        for (HospitalTransactionSchedule transactionSchedule : result) {
            HospitalTransactionSchedule hospitalTransactionSchedule1 = (HospitalTransactionSchedule) transactionSchedule.clone();
            returnList.add(hospitalTransactionSchedule1);
        }
        return new PageResult(page.getTotal(), returnList);
    }

    @Override
    public void exportExcel(ServletOutputStream outputStream, HospitalTransactionSchedule hospitalTransactionSchedule) {
        HospitalTransactionScheduleExample hospitalTransactionScheduleExample = new HospitalTransactionScheduleExample();
        HospitalTransactionScheduleExample.Criteria transactionScheduleCriteria = hospitalTransactionScheduleExample.createCriteria();

        PurchaseOrderDrugDetails purchaseOrderDrugDetails = hospitalTransactionSchedule.getPurchaseOrderDrugDetails();
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample = new PurchaseOrderDrugDetailsExample();
        PurchaseOrderDrugDetailsExample.Criteria purchaseOrderDrugDetailsCriteria = purchaseOrderDrugDetailsExample.createCriteria();

        //拼接采购单药品信息详情条件
        if (purchaseOrderDrugDetails != null) {
            DrugInformation drugInformation = purchaseOrderDrugDetails.getDrugInformation();
            DrugInformationExample drugInformationExample = new DrugInformationExample();
            DrugInformationExample.Criteria drugInformationCriteria = drugInformationExample.createCriteria();
            PurchaseNote purchaseNote = purchaseOrderDrugDetails.getPurchaseNote();
            PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
            PurchaseNoteExample.Criteria purchaseNoteCriteria = purchaseNoteExample.createCriteria();

            //药品信息条件拼接
            if (drugInformation != null) {
                DrugCategory drugCategory = drugInformation.getDrugCategory();
                DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
                Criteria drugCategoryCriteria = drugCategoryExample.createCriteria();
                if (drugCategory != null) {
                    if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                        drugCategoryCriteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                    }
                    if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                        drugCategoryCriteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                    }
                    if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                        drugCategoryCriteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                    }
                    if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                        drugCategoryCriteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                    }
                    if (drugCategory.getCoefficient() != null) {
                        drugCategoryCriteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                    }
                    if (drugCategory.getDrugClassificationId() != null) {
                        drugCategoryCriteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                    }
                }
                List<Integer> drugCategoryIds = new ArrayList<Integer>();
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                if (drugCategories.size() != 0) {
                    for (DrugCategory category : drugCategories) {
                        Integer drugCategoryId = category.getId();
                        drugCategoryIds.add(drugCategoryId);
                    }
                }
                if (drugCategoryIds.size() != 0) {
                    drugInformationCriteria.andDrugCategoryIdIn(drugCategoryIds);
                } else {
                    drugCategoryIds.add(0);
                    drugInformationCriteria.andDrugCategoryIdIn(drugCategoryIds);
                }

                if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0) {
                    drugInformationCriteria.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                }
                if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0) {
                    drugInformationCriteria.andEnterpriseNameLike("%" + drugInformation.getEnterpriseName() + "%");
                }
                if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0) {
                    drugInformationCriteria.andDrugNameLike("%" + drugInformation.getDrugName() + "%");
                }
                if (drugInformation.getQualityLevelId() != null) {
                    drugInformationCriteria.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
                }
            }
            List<Integer> drugInformationIds = new ArrayList<Integer>();
            List<DrugInformation> drugInformations = drugInformationMapper.selectByExample(drugInformationExample);
            for (DrugInformation information : drugInformations) {
                Integer informationId = information.getId();
                drugInformationIds.add(informationId);
            }
            if (drugInformationIds.size() != 0) {
                purchaseOrderDrugDetailsCriteria.andDrugInformationIdIn(drugInformationIds);
            } else {
                drugInformationIds.add(0);
                purchaseOrderDrugDetailsCriteria.andDrugInformationIdIn(drugInformationIds);
            }

            //采购单条件拼接
            if (purchaseNote != null) {
                Hospital hospital = purchaseNote.getHospital();
                HospitalExample hospitalExample = new HospitalExample();
                HospitalExample.Criteria hospitalCriteria = hospitalExample.createCriteria();
                if (hospital != null) {
                    if (hospital.getHospitalName() != null && hospital.getHospitalName().length() > 0) {
                        hospitalCriteria.andHospitalNameLike("%" + hospital.getHospitalName() + "%");
                    }
                }
                List<Hospital> hospitals = hospitalMapper.selectByExample(hospitalExample);
                List<Integer> hospitalIds = new ArrayList<Integer>();
                for (Hospital hospital1 : hospitals) {
                    Integer hospital1Id = hospital1.getId();
                    hospitalIds.add(hospital1Id);
                }
                if (hospitalIds.size() != 0) {
                    purchaseNoteCriteria.andHospitalIdIn(hospitalIds);
                } else {
                    hospitalIds.add(0);
                    purchaseNoteCriteria.andHospitalIdIn(hospitalIds);
                }
                if (purchaseNote.getPurchaseNumber() != null && purchaseNote.getPurchaseNumber().length() > 0) {
                    purchaseNoteCriteria.andPurchaseNumberLike("%" + purchaseNote.getPurchaseNumber() + "%");
                }
                if (purchaseNote.getPurchaseOrderName() != null && purchaseNote.getPurchaseOrderName().length() > 0) {
                    purchaseNoteCriteria.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
                }
                if (purchaseNote.getPurchaseOrderStatus() != null) {
                    purchaseNoteCriteria.andPurchaseOrderStatusEqualTo(purchaseNote.getPurchaseOrderStatus());
                }
                if (purchaseNote.getStartTime() != null && purchaseNote.getEndTime() != null) {
                    purchaseNoteCriteria.andBuildSingleBetween(purchaseNote.getStartTime(), purchaseNote.getEndTime());
                }
            }
            List<PurchaseNote> purchaseNotes = purchaseNoteMapper.selectByExample(purchaseNoteExample);
            List<Integer> purchaseNoteIds = new ArrayList<Integer>();
            for (PurchaseNote note : purchaseNotes) {
                Integer noteId = note.getId();
                purchaseNoteIds.add(noteId);
            }
            if (purchaseNoteIds.size() != 0) {
                purchaseOrderDrugDetailsCriteria.andPurchaseNoteIdIn(purchaseNoteIds);
            } else {
                purchaseNoteIds.add(0);
                purchaseOrderDrugDetailsCriteria.andPurchaseNoteIdIn(purchaseNoteIds);
            }
            if (purchaseOrderDrugDetails.getSuppliersId() != null) {
                purchaseOrderDrugDetailsCriteria.andSuppliersIdEqualTo(purchaseOrderDrugDetails.getSuppliersId());
            }

            if (purchaseOrderDrugDetails.getPurchasingStatus() != null && purchaseOrderDrugDetails.getPurchasingStatus().length() != 0) {
                purchaseOrderDrugDetailsCriteria.andPurchasingStatusEqualTo(purchaseOrderDrugDetails.getPurchasingStatus());
            }
        }
        List<PurchaseOrderDrugDetails> purchaseOrderDrugDetailsList = purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
        List<Integer> purchaseOrderDrugDetailsIds = new ArrayList<Integer>();
        for (PurchaseOrderDrugDetails orderDrugDetails : purchaseOrderDrugDetailsList) {
            Integer id = orderDrugDetails.getId();
            purchaseOrderDrugDetailsIds.add(id);
        }
        if (purchaseOrderDrugDetailsIds.size() != 0) {
            transactionScheduleCriteria.andPurchaseOrderDetailIdIn(purchaseOrderDrugDetailsIds);
        } else {
            purchaseOrderDrugDetailsIds.add(0);
            transactionScheduleCriteria.andPurchaseOrderDetailIdIn(purchaseOrderDrugDetailsIds);
        }

        List<HospitalTransactionSchedule> hospitalTransactionSchedules = hospitalTransactionScheduleMapper.selectByExample(hospitalTransactionScheduleExample);
        String[] states = {"未确认发货", "已发货", "已入库", "无法供货", "到期未供货"};
        // 创建一个workbook 对应一个excel应用文件
        // HSSWorkbook 是03版excel 的个格式，XSSWorkbook 是07以上版本，HSS 和 XSS 开头分别代表03,07可以切换
        //HSSFWorkbook workBook = new HSSFWorkbook();
        XSSFWorkbook workbook = new XSSFWorkbook();
        // 在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = workbook.createSheet("药品交易信息导出");
        XSSFCell cell = null;
        XSSFRow bodyRow = sheet.createRow(0);
        cell = bodyRow.createCell(0);
        cell.setCellValue("采购医院");

        cell = bodyRow.createCell(1);
        cell.setCellValue("采购单编号");

        cell = bodyRow.createCell(2);
        cell.setCellValue("采购单名称");

        cell = bodyRow.createCell(3);
        cell.setCellValue("建单时间");

        cell = bodyRow.createCell(4);
        cell.setCellValue("流水号");

        cell = bodyRow.createCell(5);
        cell.setCellValue("通用名");

        cell = bodyRow.createCell(6);
        cell.setCellValue("药品名");

        cell = bodyRow.createCell(7);
        cell.setCellValue("剂型");

        cell = bodyRow.createCell(8);
        cell.setCellValue("规格");

        cell = bodyRow.createCell(9);
        cell.setCellValue("单位");

        cell = bodyRow.createCell(10);
        cell.setCellValue("转换系数");

        cell = bodyRow.createCell(11);
        cell.setCellValue("质量层次");

        cell = bodyRow.createCell(12);
        cell.setCellValue("生产企业");

        cell = bodyRow.createCell(13);
        cell.setCellValue("中标价");

        cell = bodyRow.createCell(14);
        cell.setCellValue("交易价");

        cell = bodyRow.createCell(15);
        cell.setCellValue("采购量");

        cell = bodyRow.createCell(16);
        cell.setCellValue("采购金额");

        cell = bodyRow.createCell(17);
        cell.setCellValue("采购状态");

        cell = bodyRow.createCell(18);
        cell.setCellValue("入库量");

        cell = bodyRow.createCell(19);
        cell.setCellValue("入库金额");

        cell = bodyRow.createCell(20);
        cell.setCellValue("退货量");

        cell = bodyRow.createCell(21);
        cell.setCellValue("退货金额");

        cell = bodyRow.createCell(22);
        cell.setCellValue("结算量");

        cell = bodyRow.createCell(23);
        cell.setCellValue("结算金额");

        for (int i = 0; i < hospitalTransactionSchedules.size(); i++) {
            HospitalTransactionSchedule hospitalTransactionSchedule1 = hospitalTransactionSchedules.get(i);
            XSSFRow dateRow = sheet.createRow(1 + i);
            cell = dateRow.createCell(0);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getPurchaseNote().getHospital().getHospitalName());
            cell = dateRow.createCell(1);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getPurchaseNote().getPurchaseNumber());
            cell = dateRow.createCell(2);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getPurchaseNote().getPurchaseOrderName());
            cell = dateRow.createCell(3);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getPurchaseNote().getBuildSingleStr());
            cell = dateRow.createCell(4);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getSerialNum());
            cell = dateRow.createCell(5);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getDrugCategory().getGenericName());
            cell = dateRow.createCell(6);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getDrugName());
            cell = dateRow.createCell(7);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getDrugCategory().getDosageForm());
            cell = dateRow.createCell(8);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getDrugCategory().getSpecification());
            cell = dateRow.createCell(9);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getDrugCategory().getUnit());
            cell = dateRow.createCell(10);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getDrugCategory().getCoefficient());
            cell = dateRow.createCell(11);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getQualityLevel().getQualityLevel());
            cell = dateRow.createCell(12);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getEnterpriseName());
            cell = dateRow.createCell(13);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getDrugInformation().getBiddingPrice());
            cell = dateRow.createCell(14);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getTrading());
            cell = dateRow.createCell(15);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getAmountPurchased());
            cell = dateRow.createCell(16);
            cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getPurchaseAmount());
            cell = dateRow.createCell(17);
            cell.setCellValue(states[Integer.valueOf(hospitalTransactionSchedule1.getPurchaseOrderDrugDetails().getPurchasingStatus()) - 1]);
            cell = dateRow.createCell(18);
            if (hospitalTransactionSchedule1.getPurchaseOrderWarehousing() != null) {
                cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderWarehousing().getReceipt());
            } else {
                cell.setCellValue("");
            }
            cell = dateRow.createCell(19);
            if (hospitalTransactionSchedule1.getPurchaseOrderWarehousing() != null) {
                cell.setCellValue(hospitalTransactionSchedule1.getPurchaseOrderWarehousing().getInventoryAmount());
            } else {
                cell.setCellValue("");
            }
            cell = dateRow.createCell(20);
            if (hospitalTransactionSchedule1.getCreditOrderDetail() != null) {
                cell.setCellValue(hospitalTransactionSchedule1.getCreditOrderDetail().getReturnNum());
            } else {
                cell.setCellValue("");
            }
            cell = dateRow.createCell(21);
            if (hospitalTransactionSchedule1.getCreditOrderDetail() != null) {
                cell.setCellValue(hospitalTransactionSchedule1.getCreditOrderDetail().getReturnSum());
            } else {
                cell.setCellValue("");
            }
            cell = dateRow.createCell(22);
            if (hospitalTransactionSchedule1.getStatementsDetail() != null) {
                cell.setCellValue(hospitalTransactionSchedule1.getStatementsDetail().getSettlementAmount());
            } else {
                cell.setCellValue("");
            }
            cell = dateRow.createCell(23);
            if (hospitalTransactionSchedule1.getStatementsDetail() != null) {
                cell.setCellValue(hospitalTransactionSchedule1.getStatementsDetail().getSettlementSum());
            } else {
                cell.setCellValue("");
            }
        }
        try {
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


