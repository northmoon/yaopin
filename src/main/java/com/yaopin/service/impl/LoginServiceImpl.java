package com.yaopin.service.impl;

import com.yaopin.entity.UserInfo;
import com.yaopin.mapper.UserInfoMapper;
import com.yaopin.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements ILoginService {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo findUid(String uid) {
        return userInfoMapper.selectByUid(uid);
    }

}
