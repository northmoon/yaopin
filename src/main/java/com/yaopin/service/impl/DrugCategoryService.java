package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.entity.DrugCategoryExample.Criteria;
import com.yaopin.mapper.DrugCategoryMapper;
import com.yaopin.mapper.DrugClassificationMapper;
import com.yaopin.mapper.DrugInformationMapper;
import com.yaopin.service.IDrugCategoryService;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DrugCategoryService implements IDrugCategoryService {
    @Autowired
    private DrugCategoryMapper drugCategoryMapper;

    @Autowired
    private DrugClassificationMapper drugClassificationMapper;


    @Override
    public Map<String, DrugCategory> findAll() {
        List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(null);
        Map<String, DrugCategory> map = new HashMap<String, DrugCategory>();
        for (DrugCategory drugCategory : drugCategories) {
            map.put(drugCategory.getId().toString(), drugCategory);
        }
        return map;
    }

    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<DrugCategory> page = (Page<DrugCategory>) drugCategoryMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void add(DrugCategory drugCategory) throws Exception {
        DrugCategory returnDrugCategory = drugCategoryMapper.selectByDC(drugCategory);
        if (returnDrugCategory == null) {
            String s = drugCategoryMapper.slectMaxNum();
            Integer i = Integer.parseInt(s);
            i = ++i;
            String s1 = i.toString();
            while (s1.length() < 5) {
                s1 = "0" + s1;
            }
            drugCategory.setCategoryNum(s1);
            drugCategoryMapper.insert(drugCategory);
        } else {
            String message = "药品品目已存在，通用名：【" + returnDrugCategory.getGenericName() + "】,剂型：【" + returnDrugCategory.getDosageForm() + "】，规格：【" + returnDrugCategory.getSpecification() + "】，单位：【" + returnDrugCategory.getUnit() + "】，转换系数：【" + returnDrugCategory.getCoefficient() + "】！";
            throw new Exception(message);
        }
    }

    @Override
    public void update(DrugCategory drugCategory) {
        drugCategoryMapper.updateByPrimaryKey(drugCategory);
    }

    @Override
    public DrugCategory findOne(Integer id) {
        return drugCategoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public void delete(Integer[] ids) {
        for (Integer id : ids) {
            drugCategoryMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public PageResult findPage(DrugCategory drugCategory, int pageNum, int pageSize) {

        DrugCategoryExample example = new DrugCategoryExample();
        Criteria criteria = example.createCriteria();
        if (drugCategory != null) {
            if (drugCategory.getId() != null) {
                criteria.andIdEqualTo(drugCategory.getId());
            }
            if (drugCategory.getCategoryNum() != null && drugCategory.getCategoryNum().length() > 0) {
                criteria.andCategoryNumLike("%" + drugCategory.getCategoryNum() + "%");
            }
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
            }
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
            }
            if (drugCategory.getState() != null) {
                criteria.andStateEqualTo(drugCategory.getState());
            }
        }
        PageHelper.startPage(pageNum, pageSize);
        Page<DrugCategory> page = (Page<DrugCategory>) drugCategoryMapper.selectByExample(example);
        List<DrugCategory> result = page.getResult();
        List<DrugCategory> cloneresult = new ArrayList<>();
        for (DrugCategory category : result) {
            DrugCategory drugCategory1 = (DrugCategory) category.clone();
            cloneresult.add(drugCategory1);
        }
        return new PageResult(page.getTotal(), cloneresult);
    }

    @Override
    public void deleteOne(Integer id) {
        drugCategoryMapper.deleteByPrimaryKey(id);
    }

    /*
     * 导入
     * */
    @Override
    public Map<String, String> saveMore(MultipartFile file) throws Exception {
        List<DrugClassification> drugClassifications = drugClassificationMapper.selectByExample(null);
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (DrugClassification drugClassification : drugClassifications) {
            map.put(drugClassification.getDrugClassification(), drugClassification.getId());
        }
        if (file.isEmpty()) {
            throw new Exception("文件不存在！");
        }
        InputStream in = null;
        try {
            in = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("输入流异常");
        }
        List<List<Object>> listob = null;
        try {
            listob = new ExcelUtils().getBankListByExcel(in, file.getOriginalFilename());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("工具类打开异常");
        }
        Map<String, String> map1 = new HashMap<String, String>();
        Integer successSum = 0;
        List<Integer> errorList = new ArrayList<Integer>();
        for (int i = 0; i < listob.size(); i++) {
            List<Object> lo = listob.get(i);
            DrugCategory vo = new DrugCategory();
            if (String.valueOf(lo.get(0)) == null || String.valueOf(lo.get(0)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(1)) == null || String.valueOf(lo.get(1)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(2)) == null || String.valueOf(lo.get(2)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(3)) == null || String.valueOf(lo.get(3)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(4)) == null || String.valueOf(lo.get(4)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            vo.setGenericName(String.valueOf(lo.get(0)));
            vo.setDosageForm(String.valueOf(lo.get(1)));
            vo.setSpecification(String.valueOf(lo.get(2)));
            vo.setUnit(String.valueOf(lo.get(3)));
            vo.setCoefficient(Integer.parseInt(String.valueOf(lo.get(4))));
            Integer drugClId = map.get(String.valueOf(lo.get(5)));
            if (drugClId != null) {
                vo.setDrugClassificationId(drugClId);
            }
            try {
                add(vo);
            } catch (Exception e) {
                errorList.add(i + 2);
                continue;
            }
            ++successSum;
        }
        map1.put("successSum", String.valueOf(successSum));
        map1.put("errorList", errorList.toString());
        return map1;
    }

    @Override
    public void exportExcel(ServletOutputStream outputStream, DrugCategory drugCategory) {
        DrugCategoryExample example = new DrugCategoryExample();
        Criteria criteria = example.createCriteria();
        if (drugCategory != null) {
            if (drugCategory.getId() != null) {
                criteria.andIdEqualTo(drugCategory.getId());
            }
            if (drugCategory.getCategoryNum() != null && drugCategory.getCategoryNum().length() > 0) {
                criteria.andCategoryNumLike("%" + drugCategory.getCategoryNum() + "%");
            }
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
            }
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
            }
            if (drugCategory.getState() != null) {
                criteria.andStateEqualTo(drugCategory.getState());
            }
        }
        List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(example);
        // 创建一个workbook 对应一个excel应用文件
        // HSSWorkbook 是03版excel 的个格式，XSSWorkbook 是07以上版本，HSS 和 XSS 开头分别代表03,07可以切换
        //HSSFWorkbook workBook = new HSSFWorkbook();
        XSSFWorkbook workbook = new XSSFWorkbook();
        // 在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = workbook.createSheet("商品导出");
        XSSFCell cell = null;
        XSSFRow bodyRow = sheet.createRow(0);
        cell = bodyRow.createCell(0);
        cell.setCellValue("通用名");

        cell = bodyRow.createCell(1);
        cell.setCellValue("剂型");

        cell = bodyRow.createCell(2);
        cell.setCellValue("规格");

        cell = bodyRow.createCell(3);
        cell.setCellValue("单位");

        cell = bodyRow.createCell(4);
        cell.setCellValue("转换系数");

        cell = bodyRow.createCell(5);
        cell.setCellValue("药品类别");

        for (int i = 0; i < drugCategories.size(); i++) {
            DrugCategory drugCategory1 = drugCategories.get(i);
            XSSFRow dateRow = sheet.createRow(1 + i);
            cell = dateRow.createCell(0);
            cell.setCellValue(drugCategory1.getGenericName());
            cell = dateRow.createCell(1);
            cell.setCellValue(drugCategory1.getDosageForm());
            cell = dateRow.createCell(2);
            cell.setCellValue(drugCategory1.getSpecification());
            cell = dateRow.createCell(3);
            cell.setCellValue(drugCategory1.getUnit());
            cell = dateRow.createCell(4);
            cell.setCellValue(drugCategory1.getCoefficient());
            cell = dateRow.createCell(5);
            cell.setCellValue(drugCategory1.getDrugClassification().getDrugClassification());
        }
        try {
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void exportsearch(ServletOutputStream outputStream, DrugCategory drugCategory) {
        DrugCategoryExample example = new DrugCategoryExample();
        Criteria criteria = example.createCriteria();
        if (drugCategory != null) {
            if (drugCategory.getId() != null) {
                criteria.andIdEqualTo(drugCategory.getId());
            }
            if (drugCategory.getCategoryNum() != null && drugCategory.getCategoryNum().length() > 0) {
                criteria.andCategoryNumLike("%" + drugCategory.getCategoryNum() + "%");
            }
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
            }
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
            }
            if (drugCategory.getState() != null) {
                criteria.andStateEqualTo(drugCategory.getState());
            }
        }
        List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(example);
        List<DrugClassification> drugClassifications = drugClassificationMapper.selectByExample(null);
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (DrugClassification drugClassification : drugClassifications) {
            map.put(drugClassification.getId(), drugClassification.getDrugClassification());
        }
        // 创建一个workbook 对应一个excel应用文件
        // HSSWorkbook 是03版excel 的个格式，XSSWorkbook 是07以上版本，HSS 和 XSS 开头分别代表03,07可以切换
        //HSSFWorkbook workBook = new HSSFWorkbook();
        XSSFWorkbook workbook = new XSSFWorkbook();
        // 在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = workbook.createSheet("商品导出");
        XSSFCell cell = null;
        XSSFRow bodyRow = sheet.createRow(0);
        cell = bodyRow.createCell(0);
        cell.setCellValue("通用名");

        cell = bodyRow.createCell(1);
        cell.setCellValue("剂型");

        cell = bodyRow.createCell(2);
        cell.setCellValue("规格");

        cell = bodyRow.createCell(3);
        cell.setCellValue("单位");

        cell = bodyRow.createCell(4);
        cell.setCellValue("转换系数");

        cell = bodyRow.createCell(5);
        cell.setCellValue("药品类别");

        for (int i = 0; i < drugCategories.size(); i++) {
            DrugCategory drugCategorye = drugCategories.get(i);
            XSSFRow dateRow = sheet.createRow(1 + i);
            cell = dateRow.createCell(0);
            cell.setCellValue(drugCategorye.getGenericName());
            cell = dateRow.createCell(1);
            cell.setCellValue(drugCategorye.getDosageForm());
            cell = dateRow.createCell(2);
            cell.setCellValue(drugCategorye.getSpecification());
            cell = dateRow.createCell(3);
            cell.setCellValue(drugCategorye.getUnit());
            cell = dateRow.createCell(4);
            cell.setCellValue(drugCategorye.getCoefficient());
            cell = dateRow.createCell(5);
            cell.setCellValue(map.get(drugCategorye.getDrugClassificationId()));
        }
        try {
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}


