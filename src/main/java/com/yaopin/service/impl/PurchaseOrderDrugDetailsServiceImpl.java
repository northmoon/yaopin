package com.yaopin.service.impl;
import com.yaopin.entity.*;
import com.yaopin.mapper.*;
import com.yaopin.service.PurchaseOrderDrugDetailsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName PurchaseOrderDrugDetailsServiceImpl
 * @Author donghongyu
 * @Date 2019/9/14 17:26
 **/
@Service
public class PurchaseOrderDrugDetailsServiceImpl implements PurchaseOrderDrugDetailsService {
    @Resource
    private PurchaseOrderDrugDetailsMapper purchaseOrderDrugDetailsMapper;
    /**
     ** @Description  根据采购单id查询药品明细
     * @Param
     */
    @Override
    public List<PurchaseOrderDrugDetails> findList(int id) {
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample = new PurchaseOrderDrugDetailsExample();
        PurchaseOrderDrugDetailsExample.Criteria criteria = purchaseOrderDrugDetailsExample.createCriteria();
        criteria.andPurchaseNoteIdEqualTo(id);
        List<PurchaseOrderDrugDetails> list = purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
        return list;
    }
    @Override
    public void delete(Integer[] ids) {
        for (int i = 0; i < ids.length; i++) {
            purchaseOrderDrugDetailsMapper.deleteByPrimaryKey(ids[i]);
        }
    }
    @Override
    public void baoCun(List<Integer> ids, List<Integer>caigouliang) {
        for (int i = 0; i < ids.size(); i++) {
            PurchaseOrderDrugDetails purchaseOrderDrugDetails = purchaseOrderDrugDetailsMapper.selectByPrimaryKey(ids.get(i));
            Float biddingPrice = purchaseOrderDrugDetails.getDrugInformation().getBiddingPrice();
            purchaseOrderDrugDetails.setAmountPurchased(caigouliang.get(i));
            purchaseOrderDrugDetails.setPurchaseAmount(biddingPrice*caigouliang.get(i));
            purchaseOrderDrugDetails.setChangeNum(1);
            purchaseOrderDrugDetailsMapper.updateByPrimaryKey(purchaseOrderDrugDetails);
        }
    }
    }


