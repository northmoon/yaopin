package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.*;
import com.yaopin.service.IPurchasingorderService;
import com.yaopin.service.ISupplierService;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PurchasingorderImpl implements IPurchasingorderService {
    @Autowired
    private PurchaseNoteMapper purchaseNoteMapper;

    @Autowired
    private DrugCategoryMapper drugCategoryMapper;

    @Autowired
    private DrugInformationMapper drugInformationMapper;

    @Autowired
    private HospitalMapper hospitalMapper;

    @Autowired
    private PurchaseOrderDrugDetailsMapper purchaseOrderDrugDetailsMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Override
    public PageResult findPurchasing(SearchPurchasing searchPurchasing,int pageNum, int pageSize) {
        DrugCategory drugCategory = searchPurchasing.getDrugCategory();
        DrugInformation drugInformation = searchPurchasing.getDrugInformation();
        PurchaseNote purchaseNote = searchPurchasing.getPurchaseNote();
        PurchaseOrderDrugDetails purchaseOrderDrugDetails = searchPurchasing.getPurchaseOrderDrugDetails();
        String hospitalname=searchPurchasing.getHospitalname();

        DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
        DrugInformationExample drugInformationExample =new DrugInformationExample();
        PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample =new PurchaseOrderDrugDetailsExample();
        HospitalExample hospitalExample = new HospitalExample();

        DrugCategoryExample.Criteria criteria = drugCategoryExample.createCriteria();
        DrugInformationExample.Criteria criteria2 = drugInformationExample.createCriteria();
        PurchaseNoteExample.Criteria criteria3 = purchaseNoteExample.createCriteria();
        PurchaseOrderDrugDetailsExample.Criteria criteria4 = purchaseOrderDrugDetailsExample.createCriteria();
        HospitalExample.Criteria criteria5=hospitalExample.createCriteria();

        List<Integer> idList = new ArrayList<Integer>();
        List<Integer> idList2 = new ArrayList<Integer>();
        List<Integer> idList3= new ArrayList<Integer>();
        List<Integer> idList4= new ArrayList<Integer>();

        boolean flag = false;
        if (drugCategory != null) {
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                flag = true;
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                flag = true;
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                flag = true;
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                flag = true;
            }
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }

            if (flag) {
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                if (drugCategories.size() != 0) {
                    for (DrugCategory category : drugCategories) {
                        Integer drugCategoryId = category.getId();
                        idList.add(drugCategoryId);
                    }
                }
            }
        }
            boolean flag2 = false;
            if (idList != null && idList.size() != 0){
                criteria2.andDrugCategoryIdIn(idList);
                flag2 = true;
            }
            if (drugInformation != null) {
                if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0){
                    criteria2.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                    flag2 = true;
                }
                if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0){
                    criteria2.andSerialNumLike("%" + drugInformation.getEnterpriseName() + "%");
                    flag2 = true;
                }
                if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0){
                    criteria2.andSerialNumLike("%" + drugInformation.getDrugName() + "%");
                    flag2 = true;
                }
            }

            if (flag2) {
                List<DrugInformation> drugInformations=drugInformationMapper.selectByExample(drugInformationExample);
                if (drugInformations.size() != 0) {
                    for (DrugInformation information : drugInformations) {
                        Integer drugInformationId = information.getId();
                        idList2.add(drugInformationId);
                    }
                }
            }
        boolean flag3 = false;
        if(hospitalname != null && hospitalname.equals("")){
            criteria5.andHospitalNameLike("%" + hospitalname + "%");
            List<Hospital> hospitals=hospitalMapper.selectByExample(hospitalExample);
            for (Hospital hospital:hospitals) {
                Integer hospitalid=hospital.getId();
                idList3.add(hospitalid);
            }
            flag3 = true;
        }

        if (purchaseNote != null) {
            if (purchaseNote.getPurchaseNumber() != null){
                criteria3.andPurchaseNumberEqualTo(purchaseNote.getPurchaseNumber());
                flag3 = true;
            }
            if (purchaseNote.getPurchaseOrderName() != null){
                criteria3.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
                flag3 = true;
            }
        }

        if(searchPurchasing.getStartTime() != null && searchPurchasing.getStartTime().toString().length()>0 && searchPurchasing.getEndTime() != null && searchPurchasing.getEndTime().toString().length()>0){
            criteria3.andBuildSingleBetween(searchPurchasing.getStartTime(),searchPurchasing.getEndTime());
            flag3 = true;
        }

        if (flag3) {
            List<PurchaseNote> PurchaseNotes=purchaseNoteMapper.selectByExample(purchaseNoteExample);
            if (PurchaseNotes.size() != 0) {
                for (PurchaseNote purchaseNoteobj : PurchaseNotes) {
                    Integer purchaseNoteId = purchaseNoteobj.getId();
                    idList4.add(purchaseNoteId);
                }
            }
        }
        boolean flag4 = false;
        if (purchaseOrderDrugDetails != null) {
            if (purchaseOrderDrugDetails.getPurchasingStatus() != null && purchaseOrderDrugDetails.getPurchasingStatus().length() > 0){
                criteria4.andPurchasingStatusEqualTo(purchaseOrderDrugDetails.getPurchasingStatus());
                flag4 = true;
            }
        }
        if (idList2 != null && idList2.size() != 0){
            criteria4.andPurchaseNoteIdIn(idList2);
            flag4 = true;
        }
        if (idList4 != null && idList4.size() != 0){
            criteria4.andDrugInformationIdIn(idList4);
            flag4 = true;
        }
        ArrayList<PurchaseOrderDrugDetails> purchaseOrderDrugDetails1ist=(ArrayList<PurchaseOrderDrugDetails>)purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
        ArrayList<PurchaseOrderDrugDetails> pagelist=new Page<PurchaseOrderDrugDetails>();
        for (PurchaseOrderDrugDetails purchaseOrderDrugDetail:purchaseOrderDrugDetails1ist) {
            purchaseOrderDrugDetail.setDrugInformation(drugInformationMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getDrugInformationId()));
            purchaseOrderDrugDetail.getDrugInformation().setDrugCategory(drugCategoryMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getDrugInformation().getDrugCategoryId()));
            purchaseOrderDrugDetail.setPurchaseNote(purchaseNoteMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getPurchaseNoteId()));
            Hospital hospital=hospitalMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getPurchaseNote().getHospitalId());
            purchaseOrderDrugDetail.setHospitalname(hospital.getHospitalName());
            Supplier supplier=supplierMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getSuppliersId());
            purchaseOrderDrugDetail.setSupplier(supplier);
            pagelist.add(purchaseOrderDrugDetail);
        }
        ArrayList<PurchaseOrderDrugDetails> clonepagelist=new Page<PurchaseOrderDrugDetails>();
        for (PurchaseOrderDrugDetails orderDrugDetails : pagelist) {
            PurchaseOrderDrugDetails clone =(PurchaseOrderDrugDetails)orderDrugDetails.clone();
            clonepagelist.add(clone);
        }
        PageHelper.startPage(pageNum, pageSize);
        Page<PurchaseOrderDrugDetails> page = (Page<PurchaseOrderDrugDetails>) clonepagelist;
        page.setTotal(pagelist.size());
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public Integer confirmSupplier(List<Integer> list) {
        Integer i=0;
        for (Integer integer : list) {
            PurchaseOrderDrugDetails purchaseOrderDrugDetails=new PurchaseOrderDrugDetails();
            purchaseOrderDrugDetails.setId(integer);
            purchaseOrderDrugDetails.setPurchasingStatus("2");
            purchaseOrderDrugDetailsMapper.updateStateById(purchaseOrderDrugDetails);
            i++;
        }
        return i;
    }

    @Override
    public Integer unableSupplier(List<Integer> list) {
        Integer i=0;
        for (Integer integer : list) {
            PurchaseOrderDrugDetails purchaseOrderDrugDetails=new PurchaseOrderDrugDetails();
            purchaseOrderDrugDetails.setId(integer);
            purchaseOrderDrugDetails.setPurchasingStatus("4");
            purchaseOrderDrugDetailsMapper.updateStateById(purchaseOrderDrugDetails);
            i++;
        }
        return i;
    }

    @Override
    public void exportsearch(ServletOutputStream outputStream, SearchPurchasing searchPurchasing) {
        DrugCategory drugCategory = searchPurchasing.getDrugCategory();
        DrugInformation drugInformation = searchPurchasing.getDrugInformation();
        PurchaseNote purchaseNote = searchPurchasing.getPurchaseNote();
        PurchaseOrderDrugDetails purchaseOrderDrugDetails = searchPurchasing.getPurchaseOrderDrugDetails();
        String hospitalname=searchPurchasing.getHospitalname();

        DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
        DrugInformationExample drugInformationExample =new DrugInformationExample();
        PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample =new PurchaseOrderDrugDetailsExample();
        HospitalExample hospitalExample = new HospitalExample();

        DrugCategoryExample.Criteria criteria = drugCategoryExample.createCriteria();
        DrugInformationExample.Criteria criteria2 = drugInformationExample.createCriteria();
        PurchaseNoteExample.Criteria criteria3 = purchaseNoteExample.createCriteria();
        PurchaseOrderDrugDetailsExample.Criteria criteria4 = purchaseOrderDrugDetailsExample.createCriteria();
        HospitalExample.Criteria criteria5=hospitalExample.createCriteria();

        List<Integer> idList = new ArrayList<Integer>();
        List<Integer> idList2 = new ArrayList<Integer>();
        List<Integer> idList3= new ArrayList<Integer>();
        List<Integer> idList4= new ArrayList<Integer>();

        boolean flag = false;
        if (drugCategory != null) {
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                flag = true;
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                flag = true;
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                flag = true;
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                flag = true;
            }
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }

            if (flag) {
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                if (drugCategories.size() != 0) {
                    for (DrugCategory category : drugCategories) {
                        Integer drugCategoryId = category.getId();
                        idList.add(drugCategoryId);
                    }
                }
            }
        }
        boolean flag2 = false;
        if (idList != null && idList.size() != 0){
            criteria2.andDrugCategoryIdIn(idList);
            flag2 = true;
        }
        if (drugInformation != null) {
            if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0){
                criteria2.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                flag2 = true;
            }
            if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0){
                criteria2.andSerialNumLike("%" + drugInformation.getEnterpriseName() + "%");
                flag2 = true;
            }
            if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0){
                criteria2.andSerialNumLike("%" + drugInformation.getDrugName() + "%");
                flag2 = true;
            }
        }

        if (flag2) {
            List<DrugInformation> drugInformations=drugInformationMapper.selectByExample(drugInformationExample);
            if (drugInformations.size() != 0) {
                for (DrugInformation information : drugInformations) {
                    Integer drugInformationId = information.getId();
                    idList2.add(drugInformationId);
                }
            }
        }
        boolean flag3 = false;
        if(hospitalname != null && hospitalname.equals("")){
            criteria5.andHospitalNameLike("%" + hospitalname + "%");
            List<Hospital> hospitals=hospitalMapper.selectByExample(hospitalExample);
            for (Hospital hospital:hospitals) {
                Integer hospitalid=hospital.getId();
                idList3.add(hospitalid);
            }
            flag3 = true;
        }

        if (purchaseNote != null) {
            if (purchaseNote.getPurchaseNumber() != null){
                criteria3.andPurchaseNumberEqualTo(purchaseNote.getPurchaseNumber());
                flag3 = true;
            }
            if (purchaseNote.getPurchaseOrderName() != null){
                criteria3.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
                flag3 = true;
            }
        }

        if(searchPurchasing.getStartTime() != null && searchPurchasing.getStartTime().toString().length()>0 && searchPurchasing.getEndTime() != null && searchPurchasing.getEndTime().toString().length()>0){
            criteria3.andBuildSingleBetween(searchPurchasing.getStartTime(),searchPurchasing.getEndTime());
            flag3 = true;
        }

        if (flag3) {
            List<PurchaseNote> PurchaseNotes=purchaseNoteMapper.selectByExample(purchaseNoteExample);
            if (PurchaseNotes.size() != 0) {
                for (PurchaseNote purchaseNoteobj : PurchaseNotes) {
                    Integer purchaseNoteId = purchaseNoteobj.getId();
                    idList4.add(purchaseNoteId);
                }
            }
        }
        boolean flag4 = false;
        if (purchaseOrderDrugDetails != null) {
            if (purchaseOrderDrugDetails.getPurchasingStatus() != null && purchaseOrderDrugDetails.getPurchasingStatus().length() > 0){
                criteria4.andPurchasingStatusEqualTo(purchaseOrderDrugDetails.getPurchasingStatus());
                flag4 = true;
            }
        }
        if (idList2 != null && idList2.size() != 0){
            criteria4.andPurchaseNoteIdIn(idList2);
            flag4 = true;
        }
        if (idList4 != null && idList4.size() != 0){
            criteria4.andDrugInformationIdIn(idList4);
            flag4 = true;
        }
        ArrayList<PurchaseOrderDrugDetails> purchaseOrderDrugDetails1ist=(ArrayList<PurchaseOrderDrugDetails>)purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
        ArrayList<PurchaseOrderDrugDetails> pagelist=new Page<PurchaseOrderDrugDetails>();
        for (PurchaseOrderDrugDetails purchaseOrderDrugDetail:purchaseOrderDrugDetails1ist) {
            purchaseOrderDrugDetail.setDrugInformation(drugInformationMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getDrugInformationId()));
            purchaseOrderDrugDetail.getDrugInformation().setDrugCategory(drugCategoryMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getDrugInformation().getDrugCategoryId()));
            purchaseOrderDrugDetail.setPurchaseNote(purchaseNoteMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getPurchaseNoteId()));
            Hospital hospital=hospitalMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getPurchaseNote().getHospitalId());
            purchaseOrderDrugDetail.setHospitalname(hospital.getHospitalName());
            Supplier supplier=supplierMapper.selectByPrimaryKey(purchaseOrderDrugDetail.getSuppliersId());
            purchaseOrderDrugDetail.setSupplier(supplier);
            pagelist.add(purchaseOrderDrugDetail);
        }
        ArrayList<PurchaseOrderDrugDetails> clonepagelist=new Page<PurchaseOrderDrugDetails>();
        for (PurchaseOrderDrugDetails orderDrugDetails : pagelist) {
            PurchaseOrderDrugDetails clone =(PurchaseOrderDrugDetails)orderDrugDetails.clone();
            clonepagelist.add(clone);
        }

        // 创建一个workbook 对应一个excel应用文件
        // HSSWorkbook 是03版excel 的个格式，XSSWorkbook 是07以上版本，HSS 和 XSS 开头分别代表03,07可以切换
        //HSSFWorkbook workBook = new HSSFWorkbook();
        XSSFWorkbook workbook = new XSSFWorkbook();
        // 在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = workbook.createSheet("导出说明");
        XSSFCell cell = null;
        XSSFRow bodyRow = sheet.createRow(0);
        cell = bodyRow.createCell(0);
        cell.setCellValue("采购单号");

        cell = bodyRow.createCell(1);
        cell.setCellValue("采购医院名称");

        cell = bodyRow.createCell(2);
        cell.setCellValue("药品流水号");

        cell = bodyRow.createCell(3);
        cell.setCellValue("通用名");

        cell = bodyRow.createCell(4);
        cell.setCellValue("商品名称");

        cell = bodyRow.createCell(5);
        cell.setCellValue("剂型");

        cell = bodyRow.createCell(6);
        cell.setCellValue("规格");

        cell = bodyRow.createCell(7);
        cell.setCellValue("单位");

        cell = bodyRow.createCell(8);
        cell.setCellValue("转换系数");

        cell = bodyRow.createCell(9);
        cell.setCellValue("生产企业名称");

        for (int i = 0; i < clonepagelist.size(); i++) {
            PurchaseOrderDrugDetails purchase = clonepagelist.get(i);
            XSSFRow dateRow = sheet.createRow(1 + i);
            cell = dateRow.createCell(0);
            cell.setCellValue(purchase.getPurchaseNote().getPurchaseNumber());
            cell = dateRow.createCell(1);
            cell.setCellValue(purchase.getHospitalname());
            cell = dateRow.createCell(2);
            cell.setCellValue(purchase.getDrugInformation().getSerialNum());
            cell = dateRow.createCell(3);
            cell.setCellValue(purchase.getDrugInformation().getDrugCategory().getGenericName());
            cell = dateRow.createCell(4);
            cell.setCellValue(purchase.getDrugInformation().getDrugName());
            cell = dateRow.createCell(5);
            cell.setCellValue(purchase.getDrugInformation().getDrugCategory().getDosageForm());
            cell = dateRow.createCell(6);
            cell.setCellValue(purchase.getDrugInformation().getDrugCategory().getSpecification());
            cell = dateRow.createCell(7);
            cell.setCellValue(purchase.getDrugInformation().getDrugCategory().getUnit());
            cell = dateRow.createCell(8);
            cell.setCellValue(purchase.getDrugInformation().getDrugCategory().getCoefficient());
            cell = dateRow.createCell(9);
            cell.setCellValue(purchase.getDrugInformation().getEnterpriseName());
        }
        try {
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    /*
     * 导入
     * */
    @Override
    public Map<String, String> saveMore(MultipartFile file) throws Exception {
        Map<String, Integer> map = new HashMap<String, Integer>();
        if (file.isEmpty()) {
            throw new Exception("文件不存在！");
        }
        InputStream in = null;
        try {
            in = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("输入流异常");
        }
        List<List<Object>> listob = null;
        try {
            listob = new ExcelUtils().getBankListByExcel(in, file.getOriginalFilename());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("工具类打开异常");
        }
        Map<String, String> map1 = new HashMap<String, String>();
        Integer successSum = 0;
        List<Integer> errorList = new ArrayList<Integer>();
        for (int i = 0; i < listob.size(); i++) {
            List<Object> lo = listob.get(i);
            if (String.valueOf(lo.get(0)) == null || String.valueOf(lo.get(0)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(1)) == null || String.valueOf(lo.get(1)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(2)) == null || String.valueOf(lo.get(2)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            String purchasenumber=String.valueOf(lo.get(0));
            String hospitalname=String.valueOf(lo.get(1));
            String serialnum=String.valueOf(lo.get(2));
            try {
                add(purchasenumber,hospitalname,serialnum);
            } catch (Exception e) {
                errorList.add(i + 2);
                continue;
            }
            ++successSum;
        }
        map1.put("successSum", String.valueOf(successSum));
        map1.put("errorList", errorList.toString());
        return map1;
    }

    @Override
    public void add(String purchasenumber,String hospitalname,String serialnum) throws Exception {
        DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
        DrugInformationExample drugInformationExample =new DrugInformationExample();
        PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample =new PurchaseOrderDrugDetailsExample();
        HospitalExample hospitalExample = new HospitalExample();

        DrugCategoryExample.Criteria criteria = drugCategoryExample.createCriteria();
        DrugInformationExample.Criteria criteria2 = drugInformationExample.createCriteria();
        PurchaseNoteExample.Criteria criteria3 = purchaseNoteExample.createCriteria();
        PurchaseOrderDrugDetailsExample.Criteria criteria4 = purchaseOrderDrugDetailsExample.createCriteria();
        HospitalExample.Criteria criteria5=hospitalExample.createCriteria();

        List<Integer> list=new ArrayList<Integer>();
        List<Integer> list2=new ArrayList<Integer>();
        List<Integer> list3=new ArrayList<Integer>();



        criteria5.andHospitalNameLike("%" + hospitalname + "%");
        List<Hospital> hospitals=hospitalMapper.selectByExample(hospitalExample);
        for (Hospital hospital : hospitals) {
            list.add(hospital.getId());
        }

        criteria3.andPurchaseNumberEqualTo(purchasenumber);
        criteria3.andHospitalIdIn(list);
        List<PurchaseNote> PurchaseNotes=purchaseNoteMapper.selectByExample(purchaseNoteExample);
        for (PurchaseNote purchaseNote : PurchaseNotes) {
            list2.add(purchaseNote.getId());
        }

        criteria2.andSerialNumEqualTo(serialnum);
        List<DrugInformation> drugInformations=drugInformationMapper.selectByExample(drugInformationExample);
        for (DrugInformation drugInformation : drugInformations) {
            list3.add(drugInformation.getId());
        }
        criteria4.andDrugInformationIdIn(list3);
        criteria4.andPurchaseNoteIdIn(list2);
        criteria4.andPurchasingStatusEqualTo("1");
        List<PurchaseOrderDrugDetails> purchaseOrderDrugDetails1ist=purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
        if (purchaseOrderDrugDetails1ist.size() == 1) {
            purchaseOrderDrugDetails1ist.get(0).setPurchasingStatus("2");
            purchaseOrderDrugDetailsMapper.updateStateById(purchaseOrderDrugDetails1ist.get(0));
        }else {
            String message = "信息输入不正确，采购单编号：【" + purchasenumber + "】,采购医院名称：【" + hospitalname + "】，药品流水号：【" + serialnum + "】";
            throw new Exception(message);
        }

    }


}
