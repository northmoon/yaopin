package com.yaopin.service.impl;

import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.CreditOrderDetailMapper;
import com.yaopin.mapper.CreditOrderMapper;
import com.yaopin.mapper.PurchaseOrderDrugDetailsMapper;
import com.yaopin.mapper.PurchaseOrderWarehousingMapper;
import com.yaopin.service.CreditOrderDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName CreditOrderDetailServiceImpl
 * @Author donghongyu
 * @Date 2019/9/20 14:25
 **/
@Service
public class CreditOrderDetailServiceImpl implements CreditOrderDetailService {
    @Resource
    private CreditOrderMapper creditOrderMapper;
    @Resource
    private PurchaseOrderWarehousingMapper purchaseOrderWarehousingMapper;
    @Resource
    private CreditOrderDetailMapper creditOrderDetailMapper;
    @Resource
    private PurchaseOrderDrugDetailsMapper purchaseOrderDrugDetailsMapper;
    /**
     * * @Description  退货维护初始化退货单信息
     *
     * @Param
     */
    @Override
    public CreditOrder findSession(Integer creditOrderId1) {
        CreditOrder creditOrder=creditOrderMapper.selectByPrimaryKey(creditOrderId1);
        return creditOrder;
    }
    /**
     * * @Description  把退货药品添加到退货单明细中
     *
     * @Param
     */
    @Override
    public void add(Integer[] ids, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer creditOrderId = (Integer) session.getAttribute("creditOrderId");
        for (int i = 0; i < ids.length; i++) {
            PurchaseOrderWarehousing purchaseOrderWarehousing = purchaseOrderWarehousingMapper.selectByPrimaryKey(ids[i]);
            Integer durgInformationId = purchaseOrderWarehousing.getDurgInformationId();
            Integer purchaseOrderId = purchaseOrderWarehousing.getPurchaseOrderId();
            CreditOrderDetail creditOrderDetail = new CreditOrderDetail();
            creditOrderDetail.setDurgInformationId(durgInformationId);
            creditOrderDetail.setPurchaseOrderId(purchaseOrderId);
            creditOrderDetail.setCreditOrderId(creditOrderId);
            creditOrderDetail.setChangeNum(0);
            creditOrderDetailMapper.insert1(creditOrderDetail);
        }
    }

    /**
     * * @Description  查询退货单明细
     *
     * @Param
     */
    @Override
    public PageResult findPage(CreditOrderDetail creditOrderDetail, int page, int rows,HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer creditOrderId = (Integer)session.getAttribute("creditOrderId");
        CreditOrderDetailExample creditOrderDetailExample = new CreditOrderDetailExample();
        CreditOrderDetailExample.Criteria criteria2 = creditOrderDetailExample.createCriteria();
        criteria2.andCreditOrderIdEqualTo(creditOrderId);
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample = new PurchaseOrderDrugDetailsExample();
        PurchaseOrderDrugDetailsExample.Criteria criteria = purchaseOrderDrugDetailsExample.createCriteria();
        PurchaseOrderWarehousingExample purchaseOrderWarehousingExample = new PurchaseOrderWarehousingExample();
        PurchaseOrderWarehousingExample.Criteria criteria1 = purchaseOrderWarehousingExample.createCriteria();
        List<CreditOrderDetail> creditOrderDetails = creditOrderDetailMapper.selectByExample(creditOrderDetailExample);
        List<Combination> objects = new ArrayList<>();
        List<Combination> objects1 = new ArrayList<>();
        for (CreditOrderDetail orderDetail : creditOrderDetails) {
            Combination combination = new Combination();
            combination.setCreditOrderDetail(orderDetail);
            Integer durgInformationId = orderDetail.getDurgInformationId();
            Integer purchaseOrderId = orderDetail.getPurchaseOrderId();
            criteria.andPurchaseNoteIdEqualTo(purchaseOrderId);
            criteria.andDrugInformationIdEqualTo(durgInformationId);
            List<PurchaseOrderDrugDetails> list = purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
            for (int i = 0; i < list.size(); i++) {
                combination.setPurchaseOrderDrugDetails(list.get(0));
            }
            criteria1.andDurgInformationIdEqualTo(durgInformationId);
            criteria1.andPurchaseOrderIdEqualTo(purchaseOrderId);
            List<PurchaseOrderWarehousing> list1 = purchaseOrderWarehousingMapper.selectByExample(purchaseOrderWarehousingExample);
            for (int i = 0; i < list1.size(); i++) {
                combination.setPurchaseOrderWarehousing(list1.get(0));
            }
            objects.add(combination);
        }
        for (Combination object : objects) {
            Combination clone = (Combination) object.clone();
            objects1.add(clone);
        }
        PageHelper.startPage(page, rows);
        return new PageResult(Long.valueOf(objects1.size()), objects1);
    }

    @Override
    public void deleteSome(Integer[] ids) {
        for (int i = 0; i < ids.length; i++) {
            creditOrderDetailMapper.deleteByPrimaryKey(ids[i]);
        }
    }
    /**
     * * @Description  保存退货两
     *
     * @Param
     */
    @Override
    public void update(List<String> ids, List<String> returnNum, List<String> returnSum) {
        for (int i = 0; i < ids.size(); i++) {
            CreditOrderDetail creditOrderDetail = creditOrderDetailMapper.selectByPrimaryKey(Integer.valueOf(ids.get(i)));
            creditOrderDetail.setReturnNum(Integer.valueOf(returnNum.get(i)));
            creditOrderDetail.setChangeNum(1);
            creditOrderDetail.setReturnSum(Float.valueOf(returnSum.get(i)));
            creditOrderDetail.setState(1);
            creditOrderDetailMapper.updateByPrimaryKey1(creditOrderDetail);
        }
    }
}



