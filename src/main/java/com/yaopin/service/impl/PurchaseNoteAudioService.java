package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.entity.PurchaseNoteExample.Criteria;
import com.yaopin.mapper.HospitalMapper;
import com.yaopin.mapper.PurchaseNoteMapper;
import com.yaopin.mapper.DrugClassificationMapper;
import com.yaopin.service.IPurchaseNoteAuditService;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Service
public class PurchaseNoteAudioService implements IPurchaseNoteAuditService {

    @Autowired
    private PurchaseNoteMapper purchaseNoteMapper;

    @Autowired
    private HospitalMapper hospitalMapper;

    @Override
    public Map<String, PurchaseNote> findAll() {
        return null;
    }

    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        return null;
    }


    @Override
    public String update(Map<String, List<String>> map) {
        int successNum = 0;
        int errorNum = 0;
        List<String> ids = map.get("ids");
        List<String> purchaseOrderStatu = map.get("purchaseOrderStatu");
        List<String> auditOpinions = map.get("auditOpinions");
        for (int i = 0; i < ids.size(); i++) {
            PurchaseNote purchaseNote = purchaseNoteMapper.selectByPrimaryKey(Integer.parseInt(ids.get(i)));
            purchaseNote.setPurchaseOrderStatus(purchaseOrderStatu.get(i));
            if (auditOpinions.get(i) != null && auditOpinions.get(i).length() > 0) {
                purchaseNote.setAuditOpinion(auditOpinions.get(i));
            } else {
                purchaseNote.setAuditOpinion("");
            }
            purchaseNote.setAuditDate(new Date());
            purchaseNote.setChangeNum(purchaseNote.getChangeNum() + 1);
            purchaseNoteMapper.updateByPrimaryKey(purchaseNote);
            successNum++;
        }
        return "提交成功" + successNum + "条";
    }

    @Override
    public PurchaseNote findOne(Integer id) {
        PurchaseNote purchaseNote = purchaseNoteMapper.selectByPrimaryKey(id);
        Integer hospitalId = purchaseNote.getHospitalId();
        Hospital hospital = hospitalMapper.selectByPrimaryKey(hospitalId);
        purchaseNote.setHospital(hospital);
        return purchaseNote;
    }


    @Override
    public PageResult findPage(PurchaseNote purchaseNote, int pageNum, int pageSize) {
        Hospital hospital = purchaseNote.getHospital();
        PurchaseNoteExample example = new PurchaseNoteExample();
        Criteria criteria = example.createCriteria();
        if (hospital != null) {
            List<Integer> idList = hospitalMapper.selectLikeName("%" + hospital.getHospitalName() + "%");
            if (idList.size() != 0) {
                criteria.andHospitalIdIn(idList);
            } else {
                idList.add(0);
                criteria.andHospitalIdIn(idList);
            }
        }
        if (purchaseNote != null) {
            if (purchaseNote.getPurchaseNumber() != null && purchaseNote.getPurchaseNumber().length() > 0) {
                criteria.andPurchaseNumberLike("%" + purchaseNote.getPurchaseNumber() + "%");
            }
            if (purchaseNote.getPurchaseOrderName() != null && purchaseNote.getPurchaseOrderName().length() > 0) {
                criteria.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
            }
            if (purchaseNote.getPurchaseOrderStatus() != null && purchaseNote.getPurchaseOrderStatus().length() > 0) {
                criteria.andPurchaseOrderStatusEqualTo(purchaseNote.getPurchaseOrderStatus());
            }
            if (purchaseNote.getStartTime() != null && purchaseNote.getEndTime() != null) {
                criteria.andBuildSingleBetween(purchaseNote.getStartTime(), purchaseNote.getEndTime());
            }
        }
        PageHelper.startPage(pageNum, pageSize);
        Page<PurchaseNote> page = (Page<PurchaseNote>) purchaseNoteMapper.selectByExample(example);
        List<PurchaseNote> result = page.getResult();
        List<PurchaseNote> returnList = new ArrayList<>();
        for (PurchaseNote note : result) {
            Integer hospitalId = note.getHospitalId();
            Hospital hospital2 = new Hospital();
            Hospital hospital1 = hospitalMapper.selectByPrimaryKey(hospitalId);
            BeanUtils.copyProperties(hospital1, hospital2);
            note.setHospital(hospital2);
            returnList.add(note);
        }
        return new PageResult(page.getTotal(), returnList);
    }

    @Override
    public void deleteOne(Integer id) {

    }


}


