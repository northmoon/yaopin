package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.AreaInfo;
import com.yaopin.entity.AreaInfoExample;
import com.yaopin.entity.DrugCategory;
import com.yaopin.entity.PageResult;
import com.yaopin.mapper.AreaInfoMapper;
import com.yaopin.service.IAreaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AreaInfoServiceImpl implements IAreaInfoService {
    @Autowired
    private AreaInfoMapper areaInfoMapper;

    @Override
    public List<AreaInfo> findAll() {
        return areaInfoMapper.selectByExample(null);
    }

    @Override
    public PageResult findPage(AreaInfo areaInfo, int pageNum, int pageSize) {
        AreaInfoExample areaInfoExample=new AreaInfoExample();
        AreaInfoExample.Criteria areaInfoCriteria=areaInfoExample.createCriteria();
        if(areaInfo != null){
            if(areaInfo.getAname() !=null && !areaInfo.getAname().equals("")){
                areaInfoCriteria.andAnameLike("%"+areaInfo.getAname()+"%");
            }
            if(areaInfo.getGrade() !=null){
                areaInfoCriteria.andGradeEqualTo(areaInfo.getGrade());
            }
            if(areaInfo.getFid() != null){
                areaInfoCriteria.andFidEqualTo(areaInfo.getFid());
            }
        }
        PageHelper.startPage(pageNum, pageSize);
        Page<AreaInfo> page = (Page<AreaInfo>) areaInfoMapper.selectByExample(areaInfoExample);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void add(AreaInfo areaInfo) {
        areaInfoMapper.insert(areaInfo);
    }

    @Override
    public AreaInfo findone(int id) {
        return  areaInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public void update(AreaInfo areaInfo) {
        areaInfoMapper.updateByPrimaryKey(areaInfo);
    }

    @Override
    public void deleteOne(int id) {
        areaInfoMapper.deleteByPrimaryKey(id);
    }
}
