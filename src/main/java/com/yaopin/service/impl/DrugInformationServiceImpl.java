package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yaopin.entity.*;
import com.yaopin.entity.DrugCategoryExample.Criteria;
import com.yaopin.mapper.*;
import com.yaopin.service.IDrugInformationService;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Service
public class DrugInformationServiceImpl implements IDrugInformationService {
    @Autowired
    private DrugInformationMapper drugInformationMapper;

    @Autowired
    private DrugClassificationMapper drugClassificationMapper;

    @Autowired
    private DrugCategoryMapper drugCategoryMapper;

    @Autowired
    private QualityLevelMapper qualityLevelMapper;

    @Autowired
    private SupplierListMapper supplierListMapper;

    @Override
    public List<DrugInfo> findAll() {
        DrugInfo drugInfo = new DrugInfo();
        List<DrugInfo> drugInfos = new ArrayList<DrugInfo>();
        List<DrugInformation> drugInformations = drugInformationMapper.selectByExample(null);
        for (DrugInformation drugInformation : drugInformations) {
            Integer drugCategoryId = drugInformation.getDrugCategoryId();
            Integer qualityLevelId = drugInformation.getQualityLevelId();
            DrugCategory drugCategory = drugCategoryMapper.selectByPrimaryKey(drugCategoryId);
            QualityLevel qualityLevel = qualityLevelMapper.selectByPrimaryKey(qualityLevelId);
            drugInfo.setDrugInformation(drugInformation);
            drugInfo.setDrugCategory(drugCategory);
            drugInfos.add(drugInfo);
        }
        return drugInfos;
    }

    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<DrugInformation> page = (Page<DrugInformation>) drugInformationMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void add(DrugInfo drugInfo) throws Exception {
        DrugCategory drugCategory = drugInfo.getDrugCategory();
        DrugInformation drugInformation = drugInfo.getDrugInformation();
        DrugCategory returnDrugCategory = drugCategoryMapper.selectByDC(drugCategory);
        if (returnDrugCategory == null) {
            throw new Exception("未找到响应药品品目，请检查");
        } else {
            drugInformation.setDrugCategoryId(returnDrugCategory.getId());
        }
        DrugInformation returnDrugInformation = drugInformationMapper.selectByDI(drugInformation);
        if (returnDrugInformation == null) {
            if (drugInformation.getSerialNum() == null) {
                String s = drugInformationMapper.slectMaxNum();
                Integer i = Integer.parseInt(s);
                i = ++i;
                String s1 = i.toString();
                while (s1.length() < 8) {
                    s1 = "0" + s1;
                }
                drugInformation.setSerialNum(s1);
            }
            drugInformationMapper.insert(drugInformation);
        } else {
            String message = "药品信息已存在，通用名：【" + returnDrugCategory.getGenericName() + "】,剂型：【" + returnDrugCategory.getDosageForm() + "】，规格：【" + returnDrugCategory.getSpecification() + "】，单位：【" + returnDrugCategory.getUnit() + "】，转换系数：【" + returnDrugCategory.getCoefficient() + "】，生产企业名称：【" + returnDrugInformation.getEnterpriseName() + "】，药品名称：【" + returnDrugInformation.getDrugName() + "】!";
            throw new Exception(message);
        }
    }

    @Override
    public void update(DrugInfo drugInfo) throws Exception {
        DrugCategory drugCategory = drugInfo.getDrugCategory();
        DrugInformation drugInformation = drugInfo.getDrugInformation();
        DrugCategory returnDrugCategory = drugCategoryMapper.selectByDC(drugCategory);
        if (returnDrugCategory == null) {
            throw new Exception("未找到相应药品品目，请检查");
        } else {
            drugInformation.setDrugCategoryId(returnDrugCategory.getId());
        }
        DrugInformation returnDrugInformation = drugInformationMapper.selectByDI(drugInformation);
        if (returnDrugInformation == null) {
            String s = drugInformationMapper.slectMaxNum();
            Integer i = Integer.parseInt(s);
            i = ++i;
            String s1 = i.toString();
            while (s1.length() < 8) {
                s1 = "0" + s1;
            }
            drugInformation.setSerialNum(s1);
            drugInformationMapper.insert(drugInformation);
        } else {
            String message = "药品信息已存在，通用名：【" + returnDrugCategory.getGenericName() + "】,剂型：【" + returnDrugCategory.getDosageForm() + "】，规格：【" + returnDrugCategory.getSpecification() + "】，单位：【" + returnDrugCategory.getUnit() + "】，转换系数：【" + returnDrugCategory.getCoefficient() + "】，生产企业名称：【" + returnDrugInformation.getEnterpriseName() + "】，药品名称：【" + returnDrugInformation.getDrugName() + "】!";
            throw new Exception(message);
        }
        drugInformationMapper.updateByPrimaryKey(drugInformation);
    }

    @Override
    public DrugInfo findOne(Integer id) {
        DrugInfo drugInfo = new DrugInfo();
        DrugInformation drugInformation = drugInformationMapper.selectByPrimaryKey(id);
        Integer drugCategoryId = drugInformation.getDrugCategoryId();
        DrugCategory drugCategory = drugCategoryMapper.selectByPrimaryKey(drugCategoryId);
        drugInfo.setDrugInformation(drugInformation);
        drugInfo.setDrugCategory(drugCategory);
        return drugInfo;
    }

    @Override
    public void delete(Integer[] ids) {
        for (Integer id : ids) {
            drugInformationMapper.deleteByPrimaryKey(id);
        }
    }

    /*
     * 条件分页查询
     *
     * */
    @Override
    public PageResult findPage(DrugInfo drugInfo, int pageNum, int pageSize) {
        DrugCategory drugCategory = drugInfo.getDrugCategory();
        DrugInformation drugInformation = drugInfo.getDrugInformation();
        DrugCategoryExample example = new DrugCategoryExample();
        DrugInformationExample drugInformationExample = new DrugInformationExample();
        DrugInformationExample.Criteria criteria1 = drugInformationExample.createCriteria();
        Criteria criteria = example.createCriteria();
        if (drugCategory != null) {
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
            }
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");

            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");

            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");

            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());

            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
            }
        }
        List<Integer> idList = new ArrayList<Integer>();
        List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(example);
        if (drugCategories.size() != 0) {
            for (DrugCategory category : drugCategories) {
                Integer drugCategoryId = category.getId();
                idList.add(drugCategoryId);
            }
        }
        if (drugInfo.getMinPrice() != null && drugInfo.getMinPrice().toString().length() > 0 && drugInfo.getMaxPrice() != null && drugInfo.getMaxPrice().toString().length() > 0) {
            criteria1.andBiddingPriceBetween(drugInfo.getMinPrice(), drugInfo.getMaxPrice());
        }
        if (idList.size() != 0) {
            criteria1.andDrugCategoryIdIn(idList);
        } else {
            idList.add(0);
            criteria1.andDrugCategoryIdIn(idList);
        }
        if (drugInformation != null) {
            if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0) {
                criteria1.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
            }
            if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0) {
                criteria1.andEnterpriseNameLike("%" + drugInformation.getEnterpriseName() + "%");
            }
            if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0) {
                criteria1.andDrugNameLike("%" + drugInformation.getDrugName() + "%");
            }
            if (drugInformation.getQualityLevelId() != null) {
                criteria1.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
            }
            if (drugInformation.getState() != null) {
                criteria1.andStateEqualTo(drugInformation.getState());
            }
        }
        PageHelper.startPage(pageNum, pageSize);
        Page<DrugInformation> page = (Page<DrugInformation>) drugInformationMapper.selectByExample(drugInformationExample);
        List<DrugInformation> result = page.getResult();
        List<DrugInformation> returnList = new ArrayList<DrugInformation>();
        for (DrugInformation information : result) {
            DrugInformation cloneInformation = (DrugInformation) information.clone();
            returnList.add(cloneInformation);
        }
        return new PageResult(page.getTotal(), returnList);
    }

    @Override
    public PageResult findPageWithSupply(DrugInfo drugInfo, int pageNum, int pageSize) {
        DrugCategory drugCategory = drugInfo.getDrugCategory();
        DrugInformation drugInformation = drugInfo.getDrugInformation();
        DrugCategoryExample example = new DrugCategoryExample();
        DrugInformationExample drugInformationExample = new DrugInformationExample();
        DrugInformationExample.Criteria criteria1 = drugInformationExample.createCriteria();
        Criteria criteria = example.createCriteria();
        boolean flag = false;
        if (drugCategory != null) {
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                flag = true;
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                flag = true;
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                flag = true;
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                flag = true;
            }
        }
        List<Integer> idList = new ArrayList<Integer>();
        if (flag) {
            List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(example);
            if (drugCategories.size() != 0) {
                for (DrugCategory category : drugCategories) {
                    Integer drugCategoryId = category.getId();
                    idList.add(drugCategoryId);
                }
            }
        }
        if (drugInfo.getMinPrice() != null && drugInfo.getMinPrice().toString().length() > 0 && drugInfo.getMaxPrice() != null && drugInfo.getMaxPrice().toString().length() > 0) {
            criteria1.andBiddingPriceBetween(drugInfo.getMinPrice(), drugInfo.getMaxPrice());
        }
        if (idList != null && idList.size() != 0){
            criteria1.andDrugCategoryIdIn(idList);
        }
        if (drugInformation != null) {
            if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0) {
                criteria1.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
            }
            if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0) {
                criteria1.andEnterpriseNameLike("%" + drugInformation.getEnterpriseName() + "%");
            }
            if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0) {
                criteria1.andDrugNameLike("%" + drugInformation.getDrugName() + "%");
            }
            if (drugInformation.getQualityLevelId() != null) {
                criteria1.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
            }
            if (drugInformation.getState() != null) {
                criteria1.andStateEqualTo(drugInformation.getState());
            }
        }

        PageHelper.startPage(pageNum, pageSize);
        List<DrugInformation> list=drugInformationMapper.selectByExample(drugInformationExample);
        Set<Integer> set=new HashSet<Integer>();
        for (SupplierList supplierList:supplierListMapper.selectByExample(null)) {
            set.add(supplierList.getDrugInformationId());
        }
        ArrayList<DrugInformation> list2=new Page<DrugInformation>();
        for (int i=0;i<list.size();i++){
            if (set.add(list.get(i).getId())) {
                list2.add(list.get(i));
            }
        }
        ArrayList<DrugInformation> result=new ArrayList<DrugInformation>();
        Page<DrugInformation> page = (Page<DrugInformation>)list2;
        for (DrugInformation information : list2) {
            DrugInformation drugInformationclone=(DrugInformation)information.clone();
            result.add(drugInformationclone);
        }
        page.setTotal(list2.size());
        return new PageResult(page.getTotal(), result);
    }

    @Override
    public PageResult findSupply(DrugInfo drugInfo, int pageNum, int pageSize) {
        DrugCategory drugCategory = drugInfo.getDrugCategory();
        DrugInformation drugInformation = drugInfo.getDrugInformation();
        DrugCategoryExample example = new DrugCategoryExample();
        DrugInformationExample drugInformationExample = new DrugInformationExample();
        DrugInformationExample.Criteria criteria1 = drugInformationExample.createCriteria();
        Criteria criteria = example.createCriteria();
        boolean flag = false;
        if (drugCategory != null) {
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                flag = true;
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                flag = true;
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                flag = true;
            }

            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                flag = true;
            }
        }
        List<Integer> idList = new ArrayList<Integer>();
        if (flag) {
            List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(example);
            if (drugCategories.size() != 0) {
                for (DrugCategory category : drugCategories) {
                    Integer drugCategoryId = category.getId();
                    idList.add(drugCategoryId);
                }
            }
        }
        if (drugInfo.getMinPrice() != null && drugInfo.getMinPrice().toString().length() > 0 && drugInfo.getMaxPrice() != null && drugInfo.getMaxPrice().toString().length() > 0) {
            criteria1.andBiddingPriceBetween(drugInfo.getMinPrice(), drugInfo.getMaxPrice());
        }
        if (idList != null && idList.size() != 0){
            criteria1.andDrugCategoryIdIn(idList);
        }
        if (drugInformation != null) {
            if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0) {
                criteria1.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
            }
            if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0) {
                criteria1.andEnterpriseNameLike("%" + drugInformation.getEnterpriseName() + "%");
            }
            if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0) {
                criteria1.andDrugNameLike("%" + drugInformation.getDrugName() + "%");
            }
            if (drugInformation.getQualityLevelId() != null) {
                criteria1.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
            }
            if (drugInformation.getState() != null) {
                criteria1.andStateEqualTo(drugInformation.getState());
            }
        }

        PageHelper.startPage(pageNum, pageSize);
        List<DrugInformation> list=drugInformationMapper.selectByExample(drugInformationExample);
        Set<Integer> set=new HashSet<Integer>();
        for (SupplierList supplierList:supplierListMapper.selectByExample(null)) {
            set.add(supplierList.getDrugInformationId());
        }
        ArrayList<DrugInformation> list2=new Page<DrugInformation>();
        for (int i=0;i<list.size();i++){
            if (!set.add(list.get(i).getId())) {
                list2.add(list.get(i));
            }
        }
        Page<DrugInformation> page = (Page<DrugInformation>)list2;
        ArrayList<DrugInformation> result=new ArrayList<DrugInformation>();
        for (DrugInformation information : list2) {
            DrugInformation drugInformationclone=(DrugInformation)information.clone();
            result.add(drugInformationclone);
        }
        page.setTotal(list2.size());
        return new PageResult(page.getTotal(), result);
    }

    @Override
    public void deleteOne(Integer id) {
        drugInformationMapper.deleteByPrimaryKey(id);
    }

    /*
     * 导入
     * */
    @Override
    public Map<String, String> saveMore(MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new Exception("文件不存在！");
        }
        InputStream in = null;
        try {
            in = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("输入流异常");
        }
        List<List<Object>> listob = null;
        try {
            listob = new ExcelUtils().getBankListByExcel(in, file.getOriginalFilename());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("工具类打开异常");
        }

        Map<String, String> map1 = new HashMap<String, String>();
        Integer successSum = 0;
        List<Integer> errorList = new ArrayList<Integer>();
        DrugCategory drugCategory = new DrugCategory();
        DrugInfo drugInfo = new DrugInfo();
        for (int i = 0; i < listob.size(); i++) {
            List<Object> lo = listob.get(i);
            DrugInformation vo = new DrugInformation();
            if (String.valueOf(lo.get(0)) == null || String.valueOf(lo.get(0)).equals("")) {
                errorList.add(i + 2);
                continue;
            } else {
                DrugInformation drugInformation = drugInformationMapper.selectBySerialNum(String.valueOf(lo.get(0)));
                if (drugInformation != null) {
                    errorList.add(i + 2);
                    continue;
                }
            }
            if (String.valueOf(lo.get(1)) == null || String.valueOf(lo.get(1)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(2)) == null || String.valueOf(lo.get(2)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(3)) == null || String.valueOf(lo.get(3)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(4)) == null || String.valueOf(lo.get(4)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(5)) == null || String.valueOf(lo.get(5)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(6)) == null || String.valueOf(lo.get(6)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(7)) == null || String.valueOf(lo.get(7)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            drugCategory.setGenericName(String.valueOf(lo.get(1)));
            drugCategory.setDosageForm(String.valueOf(lo.get(2)));
            drugCategory.setSpecification(String.valueOf(lo.get(3)));
            drugCategory.setUnit(String.valueOf(lo.get(4)));
            drugCategory.setCoefficient(Integer.parseInt(String.valueOf(lo.get(5))));
            //封装药品品目信息
            drugInfo.setDrugCategory(drugCategory);

            vo.setSerialNum(String.valueOf(lo.get(0)));
            vo.setEnterpriseName(String.valueOf(lo.get(6)));
            vo.setDrugName(String.valueOf(lo.get(7)));
            if (String.valueOf(lo.get(8)) != null && String.valueOf(lo.get(8)).length() > 0) {
                vo.setBiddingPrice(Float.valueOf(String.valueOf(lo.get(8))));
            }
            QualityLevel qualityLevel = qualityLevelMapper.selectByQuality(String.valueOf(lo.get(9)));
            if (qualityLevel != null) {
                vo.setQualityLevelId(qualityLevel.getId());
            }
            //封装药品信息
            drugInfo.setDrugInformation(vo);
            try {
                add(drugInfo);
            } catch (Exception e) {
                errorList.add(i + 2);
                continue;
            }
            ++successSum;
        }
        map1.put("successSum", String.valueOf(successSum));
        map1.put("errorList", errorList.toString());
        return map1;
    }

    @Override
    public void exportExcel(ServletOutputStream outputStream) {
        List<DrugInfo> drugInfos = new ArrayList<DrugInfo>();
        List<DrugInformation> drugInformations = drugInformationMapper.selectByExample(null);
        Map<Integer,String> map=new HashMap<Integer,String>();
        List<QualityLevel> list=qualityLevelMapper.selectByExample(null);
        for (QualityLevel qualityLevel:list) {
            map.put(qualityLevel.getId(),qualityLevel.getQualityLevel());
        }
        for (DrugInformation drugInformation : drugInformations) {
            DrugInfo drugInfo = new DrugInfo();
            System.out.println(drugInformation.getDrugCategoryId());
            Integer drugCategoryId = drugInformation.getDrugCategoryId();
            DrugCategory drugCategory = drugCategoryMapper.selectByPrimaryKey(drugCategoryId);
            drugInfo.setDrugInformation(drugInformation);
            System.out.println(drugInformation);
            drugInfo.setDrugCategory(drugCategory);
            System.out.println(drugCategory);
            drugInfos.add(drugInfo);
        }
        // 创建一个workbook 对应一个excel应用文件
        // HSSWorkbook 是03版excel 的个格式，XSSWorkbook 是07以上版本，HSS 和 XSS 开头分别代表03,07可以切换
        //HSSFWorkbook workBook = new HSSFWorkbook();
        XSSFWorkbook workbook = new XSSFWorkbook();
        // 在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = workbook.createSheet("商品导出");
        XSSFCell cell = null;
        XSSFRow bodyRow = sheet.createRow(0);
        cell = bodyRow.createCell(0);
        cell.setCellValue("药品流水号");

        cell = bodyRow.createCell(1);
        cell.setCellValue("通用名");

        cell = bodyRow.createCell(2);
        cell.setCellValue("剂型");

        cell = bodyRow.createCell(3);
        cell.setCellValue("规格");

        cell = bodyRow.createCell(4);
        cell.setCellValue("单位");

        cell = bodyRow.createCell(5);
        cell.setCellValue("转换系数");

        cell = bodyRow.createCell(6);
        cell.setCellValue("生产企业名称");

        cell = bodyRow.createCell(7);
        cell.setCellValue("药品名:");

        cell = bodyRow.createCell(8);
        cell.setCellValue("中标价格");

        cell = bodyRow.createCell(9);
        cell.setCellValue("质量层次");

        cell = bodyRow.createCell(10);
        cell.setCellValue("状态(1:正常,2:取消交易)");

        for (int i = 0; i < drugInfos.size(); i++) {
            DrugInfo drugInfo2 = drugInfos.get(i);
            XSSFRow dateRow = sheet.createRow(1 + i);
            cell = dateRow.createCell(0);
            cell.setCellValue(drugInfo2.getDrugInformation().getSerialNum());
            cell = dateRow.createCell(1);
            cell.setCellValue(drugInfo2.getDrugCategory().getGenericName());
            cell = dateRow.createCell(2);
            cell.setCellValue(drugInfo2.getDrugCategory().getDosageForm());
            cell = dateRow.createCell(3);
            cell.setCellValue(drugInfo2.getDrugCategory().getSpecification());
            cell = dateRow.createCell(4);
            cell.setCellValue(drugInfo2.getDrugCategory().getUnit());
            cell = dateRow.createCell(5);
            cell.setCellValue(drugInfo2.getDrugCategory().getCoefficient());
            cell = dateRow.createCell(6);
            cell.setCellValue(drugInfo2.getDrugInformation().getEnterpriseName());
            cell = dateRow.createCell(7);
            cell.setCellValue(drugInfo2.getDrugInformation().getDrugName());
            cell = dateRow.createCell(8);
            cell.setCellValue(drugInfo2.getDrugInformation().getBiddingPrice());
            cell = dateRow.createCell(9);
            cell.setCellValue(map.get(drugInfo2.getDrugInformation().getQualityLevelId()));
            cell = dateRow.createCell(10);
            cell.setCellValue(drugInfo2.getDrugInformation().getState());
        }
        try {
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void exportExcelWithSearch(ServletOutputStream outputStream, DrugInfo drugInfo1) {
            List<DrugInfo> drugInfos = new ArrayList<DrugInfo>();
            List<DrugInformation> drugInformations = findDrugInfoWithSearch(drugInfo1);
            Map<Integer,String> map=new HashMap<Integer,String>();
            List<QualityLevel> list=qualityLevelMapper.selectByExample(null);
            for (QualityLevel qualityLevel:list) {
                map.put(qualityLevel.getId(),qualityLevel.getQualityLevel());
            }
            for (DrugInformation drugInformation : drugInformations) {
                DrugInfo drugInfo = new DrugInfo();
                System.out.println(drugInformation.getDrugCategoryId());
                Integer drugCategoryId = drugInformation.getDrugCategoryId();
                DrugCategory drugCategory = drugCategoryMapper.selectByPrimaryKey(drugCategoryId);
                drugInfo.setDrugInformation(drugInformation);
                System.out.println(drugInformation);
                drugInfo.setDrugCategory(drugCategory);
                System.out.println(drugCategory);
                drugInfos.add(drugInfo);
            }
            // 创建一个workbook 对应一个excel应用文件
            // HSSWorkbook 是03版excel 的个格式，XSSWorkbook 是07以上版本，HSS 和 XSS 开头分别代表03,07可以切换
            //HSSFWorkbook workBook = new HSSFWorkbook();
            XSSFWorkbook workbook = new XSSFWorkbook();
            // 在workbook中添加一个sheet,对应Excel文件中的sheet
            XSSFSheet sheet = workbook.createSheet("商品导出");
            XSSFCell cell = null;
            XSSFRow bodyRow = sheet.createRow(0);

            cell = bodyRow.createCell(0);
            cell.setCellValue("药品流水号");

            cell = bodyRow.createCell(1);
            cell.setCellValue("通用名");

            cell = bodyRow.createCell(2);
            cell.setCellValue("剂型");

            cell = bodyRow.createCell(3);
            cell.setCellValue("规格");

            cell = bodyRow.createCell(4);
            cell.setCellValue("单位");

            cell = bodyRow.createCell(5);
            cell.setCellValue("转换系数");

            cell = bodyRow.createCell(6);
            cell.setCellValue("生产企业名称");

            cell = bodyRow.createCell(7);
            cell.setCellValue("药品名:");

            cell = bodyRow.createCell(8);
            cell.setCellValue("中标价格");

            cell = bodyRow.createCell(9);
            cell.setCellValue("质量层次");

            cell = bodyRow.createCell(10);
            cell.setCellValue("状态(1:正常,2:取消交易)");

            for (int i = 0; i < drugInfos.size(); i++) {
                DrugInfo drugInfo2 = drugInfos.get(i);
                XSSFRow dateRow = sheet.createRow(1 + i);
                cell = dateRow.createCell(0);
                cell.setCellValue(drugInfo2.getDrugInformation().getSerialNum());
                cell = dateRow.createCell(1);
                cell.setCellValue(drugInfo2.getDrugCategory().getGenericName());
                cell = dateRow.createCell(2);
                cell.setCellValue(drugInfo2.getDrugCategory().getDosageForm());
                cell = dateRow.createCell(3);
                cell.setCellValue(drugInfo2.getDrugCategory().getSpecification());
                cell = dateRow.createCell(4);
                cell.setCellValue(drugInfo2.getDrugCategory().getUnit());
                cell = dateRow.createCell(5);
                cell.setCellValue(drugInfo2.getDrugCategory().getCoefficient());
                cell = dateRow.createCell(6);
                cell.setCellValue(drugInfo2.getDrugInformation().getEnterpriseName());
                cell = dateRow.createCell(7);
                cell.setCellValue(drugInfo2.getDrugInformation().getDrugName());
                cell = dateRow.createCell(8);
                cell.setCellValue(drugInfo2.getDrugInformation().getBiddingPrice());
                cell = dateRow.createCell(9);
                cell.setCellValue(map.get(drugInfo2.getDrugInformation().getQualityLevelId()));
                cell = dateRow.createCell(10);
                cell.setCellValue(drugInfo2.getDrugInformation().getState());
            }
            try {
                workbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    public List<DrugInformation> findDrugInfoWithSearch(DrugInfo drugInfo) {
        DrugCategory drugCategory = drugInfo.getDrugCategory();
        DrugInformation drugInformation = drugInfo.getDrugInformation();
        DrugCategoryExample example = new DrugCategoryExample();
        DrugInformationExample drugInformationExample = new DrugInformationExample();
        DrugInformationExample.Criteria criteria1 = drugInformationExample.createCriteria();
        Criteria criteria = example.createCriteria();
        boolean flag = false;
        if (drugCategory != null) {
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                flag = true;
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                flag = true;
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                flag = true;
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                flag = true;
            }
        }
        List<Integer> idList = new ArrayList<Integer>();
        if (flag) {
            List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(example);
            if (drugCategories.size() != 0) {
                for (DrugCategory category : drugCategories) {
                    Integer drugCategoryId = category.getId();
                    idList.add(drugCategoryId);
                }
            }
        }
        if (drugInfo.getMinPrice() != null && drugInfo.getMinPrice().toString().length() > 0 && drugInfo.getMaxPrice() != null && drugInfo.getMaxPrice().toString().length() > 0) {
            criteria1.andBiddingPriceBetween(drugInfo.getMinPrice(), drugInfo.getMaxPrice());
        }
        if (idList != null && idList.size() != 0) {
            criteria1.andDrugCategoryIdIn(idList);
        }
        if (drugInformation != null) {
            if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0) {
                criteria1.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
            }
            if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0) {
                criteria1.andEnterpriseNameLike("%" + drugInformation.getEnterpriseName() + "%");
            }
            if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0) {
                criteria1.andDrugNameLike("%" + drugInformation.getDrugName() + "%");
            }
            if (drugInformation.getQualityLevelId() != null) {
                criteria1.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
            }
            if (drugInformation.getState() != null) {
                criteria1.andStateEqualTo(drugInformation.getState());
            }
        }
        List<DrugInformation> list= drugInformationMapper.selectByExample(drugInformationExample);
        return list;
    }
}


