package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.DrugCategoryMapper;
import com.yaopin.mapper.DrugInformationMapper;
import com.yaopin.mapper.HospitalPurchasingMapper;
import com.yaopin.mapper.SupplierListMapper;
import com.yaopin.service.SupplierListService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName SupplierListServiceImpl
 * @Author donghongyu
 * @Date 2019/9/6 14:47
 * 采购供货实现
 **/
@Service
public class SupplierListServiceImpl implements SupplierListService {
    @Resource
    private SupplierListMapper supplierListMapper;
    @Resource
    private HospitalPurchasingMapper hospitalPurchasingMapper;
    @Resource
    private DrugCategoryMapper drugCategoryMapper;
    @Resource
    private DrugInformationMapper drugInformationMapper;
    @Override
    public PageResult findPage(SupplierList supplierList, int page, int rows) {
        DrugInformation drugInformation = supplierList.getDrugInformation();
        DrugInformationExample drugInformationExample = new DrugInformationExample();
        DrugInformationExample.Criteria criteria = drugInformationExample.createCriteria();
        SupplierListExample supplierListExample = new SupplierListExample();
        SupplierListExample.Criteria criteria2 = supplierListExample.createCriteria();
        //获得采购单里面的供货单id
        List<Integer> supplierListIds = new ArrayList<Integer>();
        List<HospitalPurchasing> hospitalPurchasings = hospitalPurchasingMapper.selectByExample(null);
        for (HospitalPurchasing hospitalPurchasing : hospitalPurchasings) {
            Integer supplierListId = hospitalPurchasing.getSupplierListId();
            supplierListIds.add(supplierListId);
        }
        //拼接药品信息条件
        if (drugInformation != null) {
            DrugCategory drugCategory = drugInformation.getDrugCategory();
            //获取药品品目条件
            DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
            DrugCategoryExample.Criteria criteria1 = drugCategoryExample.createCriteria();
            //拼接药品品目条件
            if (drugCategory != null) {
                if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                    criteria1.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                }
                if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                    criteria1.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                }
                if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                    criteria1.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                }
                if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                    criteria1.andUnitLike("%" + drugCategory.getUnit() + "%");
                }
                if (drugCategory.getCoefficient() != null) {
                    criteria1.andCoefficientEqualTo(drugCategory.getCoefficient());
                }
                if (drugCategory.getDrugClassificationId() != null) {
                    criteria1.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                }
                List<Integer> drugCategoryIds = new ArrayList<Integer>();
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                if (drugCategories.size() != 0) {
                    for (DrugCategory category : drugCategories) {
                        Integer drugCategoryId = category.getId();
                        drugCategoryIds.add(drugCategoryId);
                    }
                }

                if (drugCategoryIds.size() != 0) {
                    criteria.andDrugCategoryIdIn(drugCategoryIds);
                } else {
                    drugCategoryIds.add(0);
                    criteria.andDrugCategoryIdIn(drugCategoryIds);
                }
            }
            if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0) {
                criteria.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
            }
            if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0) {
                criteria.andEnterpriseNameLike("%" + drugInformation.getEnterpriseName() + "%");
            }
            if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0) {
                criteria.andDrugNameLike("%" + drugInformation.getDrugName() + "%");
            }
            if (drugInformation.getState() != null) {
                criteria.andStateEqualTo(drugInformation.getState());
            }
            if (drugInformation.getMinPrice() != null && drugInformation.getMinPrice().toString().length() > 0 && drugInformation.getMaxPrice() != null && drugInformation.getMaxPrice().toString().length() > 0) {
                criteria.andBiddingPriceBetween(drugInformation.getMinPrice(), drugInformation.getMaxPrice());
            }
        }
        List<Integer> drugInformationIds = new ArrayList<Integer>();
        List<DrugInformation> drugInformations = drugInformationMapper.selectByExample(drugInformationExample);
        for (DrugInformation information : drugInformations) {
            Integer informationId = information.getId();
            drugInformationIds.add(informationId);
        }
        if (drugInformationIds.size() != 0) {
            criteria2.andDrugInformationIdIn(drugInformationIds);
        } else {
            drugInformationIds.add(0);
            criteria2.andDrugInformationIdIn(drugInformationIds);
        }
        if (supplierList.getSupplyState() != null) {
            criteria2.andSupplyStateEqualTo(supplierList.getSupplyState());
        }
        if (supplierListIds.size() != 0) {
            criteria2.andIdNotIn(supplierListIds);
        }
        PageHelper.startPage(page, rows);
        Page<SupplierList> supplierLists = (Page<SupplierList>) supplierListMapper.selectByExample(supplierListExample);
        List<SupplierList> purchaseOrderDrugDetails1 = new ArrayList<>();
        for (SupplierList supplierList1 : supplierLists) {
            SupplierList clone = (SupplierList) supplierList1.clone();
            purchaseOrderDrugDetails1.add(clone);
        }
        return new PageResult(supplierLists.getTotal(), purchaseOrderDrugDetails1);
    }
    @Override
    public void addPn(List<Integer> idList) {
        HospitalPurchasing hospitalPurchasing = new HospitalPurchasing();
        for (int i = 0; i < idList.size(); i++) {
            hospitalPurchasing.setHospitalId(1);
            hospitalPurchasing.setSupplierListId(idList.get(i));
            hospitalPurchasingMapper.insert(hospitalPurchasing);
        }
    }
}


