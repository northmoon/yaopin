package com.yaopin.service.impl;

import com.yaopin.entity.DrugClassification;
import com.yaopin.entity.QualityLevel;
import com.yaopin.mapper.DrugClassificationMapper;
import com.yaopin.mapper.QualityLevelMapper;
import com.yaopin.service.IDrugClassificationService;
import com.yaopin.service.IQualityLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class QualityLevelServiceImpl implements IQualityLevelService {
    @Autowired
    private QualityLevelMapper qualityLevelMapper;

    @Override
    public Map<String, QualityLevel> findAll() {
        List<QualityLevel> qualityLevels = qualityLevelMapper.selectByExample(null);
        Map<String, QualityLevel> map = new HashMap<>();
        for (QualityLevel qualityLevel : qualityLevels) {
            map.put(qualityLevel.getId().toString(), qualityLevel);
        }
        return map;
    }
}
