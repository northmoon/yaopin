package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.entity.UserInfoExample.Criteria;
import com.yaopin.mapper.*;
import com.yaopin.service.IRoleService;
import com.yaopin.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RoleService implements IRoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public List<Role> findAll() {
        List<Role> roles = roleMapper.selectByExample(null);
        return roles;
    }


    @Override
    public void add(Role role, int[] ids) throws Exception {
        Role returnRole = roleMapper.selectByRoleName(role.getRoleName());
        if (returnRole == null) {
            roleMapper.insert(role);
            Role role1 = roleMapper.selectByRoleName(role.getRoleName());
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(role1.getId());
            for (int id : ids) {
                rolePermission.setPermissionId(id);
                rolePermissionMapper.insert(rolePermission);
            }
        } else {
            String message = "角色已存在，请勿重复添加！";
            throw new Exception(message);
        }
    }

    @Override
    public void update(Role role, int[] ids) throws Exception {
        Role oldRole = roleMapper.selectByPrimaryKey(role.getId());
        Role returnRole = roleMapper.selectByRoleName(role.getRoleName());
        if (returnRole == null || oldRole.getRoleName().equals(returnRole.getRoleName())) {
            roleMapper.updateByPrimaryKey(role);
            rolePermissionMapper.deleteByRoleId(role.getId());
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(role.getId());
            for (int id : ids) {
                rolePermission.setPermissionId(id);
                rolePermissionMapper.insert(rolePermission);
            }
        } else {
            String message = "角色已存在，请勿重复添加！";
            throw new Exception(message);
        }
    }

    @Override
    public Role findOne(Integer id) {
        Role role = roleMapper.selectByPrimaryKey(id);
        return role;
    }


    @Override
    public void deleteOne(Integer id) {
        roleMapper.deleteByPrimaryKey(id);
        rolePermissionMapper.deleteByRoleId(id);
        userRoleMapper.deleteByRoleId(id);
    }


}


