package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.PageResult;
import com.yaopin.entity.UserInfo;
import com.yaopin.entity.UserInfoExample;
import com.yaopin.mapper.UserInfoMapper;
import com.yaopin.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserInfoMapper UserDao;

    /**
     * 分页
     *
     * @param userInfo
     * @param pageNum  当前页 码
     * @param pageSize 每页记录数
     * @return
     */
    @Override
    public PageResult findPage(UserInfo userInfo, int pageNum, int pageSize) {
        UserInfoExample userInfoExample = new UserInfoExample();
        UserInfoExample.Criteria criteria = userInfoExample.createCriteria();
        if (userInfo != null) {
            if (userInfo.getUid() != null) {
                criteria.andUidEqualTo(userInfo.getUid());
            }
            if (userInfo.getUname() != null && userInfo.getUname().length() > 0) {
                criteria.andUnameLike("%" +userInfo.getUname() + "%");
            }
            if (userInfo.getContactsphone() != null && userInfo.getContactsphone().length() > 0) {
                    criteria.andContactsphoneLike("%" +userInfo.getContactsphone() + "%");
                }
            }
            PageHelper.startPage(pageNum, pageSize);
            Page<UserInfo> page = (Page<UserInfo>) UserDao.selectByExample(userInfoExample);
            PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
            return pageResult;
           // return new PageResult(page.getTotal(), page.getResult());
        }

    }

