package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.*;
import com.yaopin.service.IPurchasingorderService;
import com.yaopin.service.IReturnorderService;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReturnorderServiceImpl implements IReturnorderService {
    @Autowired
    private PurchaseNoteMapper purchaseNoteMapper;

    @Autowired
    private DrugCategoryMapper drugCategoryMapper;

    @Autowired
    private DrugInformationMapper drugInformationMapper;

    @Autowired
    private HospitalMapper hospitalMapper;

    @Autowired
    private PurchaseOrderDrugDetailsMapper purchaseOrderDrugDetailsMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private CreditOrderMapper creditOrderMapper;

    @Autowired
    private CreditOrderDetailMapper creditOrderDetailMapper;

    @Override
    public PageResult findReturn(SearchReturnorder SearchReturnorder,int pageNum, int pageSize) {
        DrugCategory drugCategory = SearchReturnorder.getDrugCategory();
        DrugInformation drugInformation = SearchReturnorder.getDrugInformation();
        PurchaseNote purchaseNote = SearchReturnorder.getPurchaseNote();
        CreditOrder creditOrder=SearchReturnorder.getCreditOrder();
        CreditOrderDetail creditOrderDetail=SearchReturnorder.getCreditOrderDetail();
        String hospitalname=SearchReturnorder.getHospitalname();

        DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
        DrugInformationExample drugInformationExample =new DrugInformationExample();
        PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
        CreditOrderExample creditOrderExample=new CreditOrderExample();
        CreditOrderDetailExample creditOrderDetailExample=new CreditOrderDetailExample();
        HospitalExample hospitalExample = new HospitalExample();

        DrugCategoryExample.Criteria criteria = drugCategoryExample.createCriteria();
        DrugInformationExample.Criteria criteria2 = drugInformationExample.createCriteria();
        PurchaseNoteExample.Criteria criteria3 = purchaseNoteExample.createCriteria();
        CreditOrderExample.Criteria criteria4=creditOrderExample.createCriteria();
        HospitalExample.Criteria criteria5=hospitalExample.createCriteria();
        CreditOrderDetailExample.Criteria criteria6=creditOrderDetailExample.createCriteria();

        List<Integer> idList = new ArrayList<Integer>();
        List<Integer> idList2 = new ArrayList<Integer>();
        List<Integer> idList3= new ArrayList<Integer>();
        List<Integer> idList4= new ArrayList<Integer>();
        List<Integer> idList5= new ArrayList<Integer>();


        boolean flag = false;
        if (drugCategory != null) {
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                flag = true;
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                flag = true;
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                flag = true;
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                flag = true;
            }
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }
            if (flag) {
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                if (drugCategories.size() != 0) {
                    for (DrugCategory category : drugCategories) {
                        Integer drugCategoryId = category.getId();
                        idList.add(drugCategoryId);
                    }
                }
            }
        }
            boolean flag2 = false;
            if (idList != null && idList.size() != 0){
                criteria2.andDrugCategoryIdIn(idList);
                flag2 = true;
            }
            if (drugInformation != null) {
                if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0){
                    criteria2.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                    flag2 = true;
                }
                if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0){
                    criteria2.andSerialNumLike("%" + drugInformation.getEnterpriseName() + "%");
                    flag2 = true;
                }
                if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0){
                    criteria2.andSerialNumLike("%" + drugInformation.getDrugName() + "%");
                    flag2 = true;
                }
                if (drugInformation.getQualityLevelId() != null){
                    criteria2.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
                    flag2 = true;
                }

            }
            if (flag2) {
                List<DrugInformation> drugInformations=drugInformationMapper.selectByExample(drugInformationExample);
                if (drugInformations.size() != 0) {
                    for (DrugInformation information : drugInformations) {
                        Integer drugInformationId = information.getId();
                        idList2.add(drugInformationId);
                    }
                }
            }
        boolean flag3 = false;
        if(hospitalname != null && hospitalname.equals("")){
            criteria5.andHospitalNameLike("%" + hospitalname + "%");
            List<Hospital> hospitals=hospitalMapper.selectByExample(hospitalExample);
            for (Hospital hospital:hospitals) {
                Integer hospitalid=hospital.getId();
                idList3.add(hospitalid);
            }
        }

        if (purchaseNote != null) {
            if (purchaseNote.getPurchaseNumber() != null){
                criteria3.andPurchaseNumberEqualTo(purchaseNote.getPurchaseNumber());
                flag3 = true;
            }
            if (purchaseNote.getPurchaseOrderName() != null){
                criteria3.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
                flag3 = true;
            }
        }

        if (idList3 !=null && idList3.size()>0){
            criteria3.andHospitalIdIn(idList3);
            flag3 = true;
        }
        if (SearchReturnorder.getPurchasestartTime() != null && SearchReturnorder.getPurchaseendTime() != null ){
            criteria3.andBuildSingleBetween(SearchReturnorder.getPurchasestartTime(),SearchReturnorder.getPurchaseendTime());
            flag3 = true;
        }

        if (flag3) {
            List<PurchaseNote> PurchaseNotes=purchaseNoteMapper.selectByExample(purchaseNoteExample);
            if (PurchaseNotes.size() != 0) {
                for (PurchaseNote purchaseNoteobj : PurchaseNotes) {
                    Integer purchaseNoteId = purchaseNoteobj.getId();
                    idList4.add(purchaseNoteId);
                }
            }
        }

        boolean flag4 = false;
        if (creditOrder != null) {
            if (creditOrder.getCreditOrderNum() != null && creditOrder.getCreditOrderNum().length() > 0){
                criteria4.andCreditOrderNumEqualTo(creditOrder.getCreditOrderNum());
                flag4 = true;
            }
            if (creditOrder.getCreditOrderName() != null && creditOrder.getCreditOrderName().length() > 0){
                criteria4.andCreditOrderNameLike("%" + creditOrder.getCreditOrderName() + "%");
                flag4 = true;
            }
        }
        if (SearchReturnorder.getReturnstartTime() !=null && SearchReturnorder.getReturnendTime() != null){
            criteria4.andCreateTimeBetween(SearchReturnorder.getReturnstartTime(),SearchReturnorder.getReturnendTime());
            flag4 = true;
        }
        if (flag4) {
           List<CreditOrder> list=creditOrderMapper.selectByExample(creditOrderExample);
            for (CreditOrder order : list) {
                idList5.add(order.getId());
            }
        }
        if(idList2 !=null && idList2.size()>0){
            criteria6.andDurgInformationIdIn(idList2);
        }

        if(idList4 !=null && idList4.size()>0){
            criteria6.andPurchaseOrderIdIn(idList4);
        }

        if(idList5 !=null && idList5.size()>0){
            criteria6.andCreditOrderIdIn(idList5);
        }
        if(creditOrderDetail !=null && creditOrderDetail.getState() != 0){
            criteria6.andStateEqualTo(creditOrderDetail.getState());
        }

        ArrayList<CreditOrderDetail> creditOrderDetailist=(ArrayList<CreditOrderDetail>)creditOrderDetailMapper.selectByExample(creditOrderDetailExample);
        ArrayList<SearchReturnorder> searchReturnorder2=new Page<SearchReturnorder>();
        for (CreditOrderDetail orderDetail : creditOrderDetailist) {
            SearchReturnorder searchReturnorder=new SearchReturnorder();
            DrugInformation drugInformation1=drugInformationMapper.selectByPrimaryKey(orderDetail.getDurgInformationId());
            DrugCategory drugCategory1=drugCategoryMapper.selectByPrimaryKey(drugInformation1.getDrugCategoryId());
            PurchaseNote purchaseNote1=purchaseNoteMapper.selectByPrimaryKey(orderDetail.getPurchaseOrderId());
            CreditOrder creditOrder1=creditOrderMapper.selectByPrimaryKey(orderDetail.getCreditOrderId());
            Hospital hospital=hospitalMapper.selectByPrimaryKey(creditOrder1.getHospitalId());
            searchReturnorder.setDrugInformation(drugInformation1);
            searchReturnorder.setDrugCategory(drugCategory1);
            searchReturnorder.setPurchaseNote(purchaseNote1);
            searchReturnorder.setCreditOrder(creditOrder1);
            searchReturnorder.setCreditOrderDetail(orderDetail);
            searchReturnorder.setHospitalname(hospital.getHospitalName());
            SearchReturnorder searchReturnorder3=(SearchReturnorder)searchReturnorder.clone();
            searchReturnorder2.add(searchReturnorder3);
        }


//        ArrayList<PurchaseOrderDrugDetails> pagelist=new Page<PurchaseOrderDrugDetails>();
//        ArrayList<PurchaseOrderDrugDetails> clonepagelist=new Page<PurchaseOrderDrugDetails>();
//        for (PurchaseOrderDrugDetails orderDrugDetails : pagelist) {
//            PurchaseOrderDrugDetails clone =(PurchaseOrderDrugDetails)orderDrugDetails.clone();
//            clonepagelist.add(clone);
//        }
        PageHelper.startPage(pageNum, pageSize);
        Page<SearchReturnorder> page = (Page<SearchReturnorder>) searchReturnorder2;
        page.setTotal(page.size());
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public Integer confirmSupplier(List<Integer> list) {
        Integer i=0;
        for (Integer integer : list) {
            PurchaseOrderDrugDetails purchaseOrderDrugDetails=new PurchaseOrderDrugDetails();
            purchaseOrderDrugDetails.setId(integer);
            purchaseOrderDrugDetails.setPurchasingStatus("2");
            purchaseOrderDrugDetailsMapper.updateStateById(purchaseOrderDrugDetails);
            i++;
        }
        return i;
    }

    @Override
    public Integer unableSupplier(List<Integer> list) {
        Integer i=0;
        for (Integer integer : list) {
            PurchaseOrderDrugDetails purchaseOrderDrugDetails=new PurchaseOrderDrugDetails();
            purchaseOrderDrugDetails.setId(integer);
            purchaseOrderDrugDetails.setPurchasingStatus("4");
            purchaseOrderDrugDetailsMapper.updateStateById(purchaseOrderDrugDetails);
            i++;
        }
        return i;
    }

    @Override
    public void exportsearch(ServletOutputStream outputStream, SearchReturnorder SearchReturnorder) {
        DrugCategory drugCategory = SearchReturnorder.getDrugCategory();
        DrugInformation drugInformation = SearchReturnorder.getDrugInformation();
        PurchaseNote purchaseNote = SearchReturnorder.getPurchaseNote();
        CreditOrder creditOrder=SearchReturnorder.getCreditOrder();
        CreditOrderDetail creditOrderDetail=SearchReturnorder.getCreditOrderDetail();
        String hospitalname=SearchReturnorder.getHospitalname();

        DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
        DrugInformationExample drugInformationExample =new DrugInformationExample();
        PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
        CreditOrderExample creditOrderExample=new CreditOrderExample();
        CreditOrderDetailExample creditOrderDetailExample=new CreditOrderDetailExample();
        HospitalExample hospitalExample = new HospitalExample();

        DrugCategoryExample.Criteria criteria = drugCategoryExample.createCriteria();
        DrugInformationExample.Criteria criteria2 = drugInformationExample.createCriteria();
        PurchaseNoteExample.Criteria criteria3 = purchaseNoteExample.createCriteria();
        CreditOrderExample.Criteria criteria4=creditOrderExample.createCriteria();
        HospitalExample.Criteria criteria5=hospitalExample.createCriteria();
        CreditOrderDetailExample.Criteria criteria6=creditOrderDetailExample.createCriteria();

        List<Integer> idList = new ArrayList<Integer>();
        List<Integer> idList2 = new ArrayList<Integer>();
        List<Integer> idList3= new ArrayList<Integer>();
        List<Integer> idList4= new ArrayList<Integer>();
        List<Integer> idList5= new ArrayList<Integer>();


        boolean flag = false;
        if (drugCategory != null) {
            if (drugCategory.getDosageForm() != null && drugCategory.getDosageForm().length() > 0) {
                criteria.andDosageFormLike("%" + drugCategory.getDosageForm() + "%");
                flag = true;
            }
            if (drugCategory.getSpecification() != null && drugCategory.getSpecification().length() > 0) {
                criteria.andSpecificationLike("%" + drugCategory.getSpecification() + "%");
                flag = true;
            }
            if (drugCategory.getUnit() != null && drugCategory.getUnit().length() > 0) {
                criteria.andUnitLike("%" + drugCategory.getUnit() + "%");
                flag = true;
            }
            if (drugCategory.getCoefficient() != null) {
                criteria.andCoefficientEqualTo(drugCategory.getCoefficient());
                flag = true;
            }
            if (drugCategory.getDrugClassificationId() != null) {
                criteria.andDrugClassificationIdEqualTo(drugCategory.getDrugClassificationId());
                flag = true;
            }
            if (drugCategory.getGenericName() != null && drugCategory.getGenericName().length() > 0) {
                criteria.andGenericNameLike("%" + drugCategory.getGenericName() + "%");
                flag = true;
            }
            if (flag) {
                List<DrugCategory> drugCategories = drugCategoryMapper.selectByExample(drugCategoryExample);
                if (drugCategories.size() != 0) {
                    for (DrugCategory category : drugCategories) {
                        Integer drugCategoryId = category.getId();
                        idList.add(drugCategoryId);
                    }
                }
            }
        }
        boolean flag2 = false;
        if (idList != null && idList.size() != 0){
            criteria2.andDrugCategoryIdIn(idList);
            flag2 = true;
        }
        if (drugInformation != null) {
            if (drugInformation.getSerialNum() != null && drugInformation.getSerialNum().length() > 0){
                criteria2.andSerialNumLike("%" + drugInformation.getSerialNum() + "%");
                flag2 = true;
            }
            if (drugInformation.getEnterpriseName() != null && drugInformation.getEnterpriseName().length() > 0){
                criteria2.andSerialNumLike("%" + drugInformation.getEnterpriseName() + "%");
                flag2 = true;
            }
            if (drugInformation.getDrugName() != null && drugInformation.getDrugName().length() > 0){
                criteria2.andSerialNumLike("%" + drugInformation.getDrugName() + "%");
                flag2 = true;
            }
            if (drugInformation.getQualityLevelId() != null){
                criteria2.andQualityLevelIdEqualTo(drugInformation.getQualityLevelId());
                flag2 = true;
            }

        }
        if (flag2) {
            List<DrugInformation> drugInformations=drugInformationMapper.selectByExample(drugInformationExample);
            if (drugInformations.size() != 0) {
                for (DrugInformation information : drugInformations) {
                    Integer drugInformationId = information.getId();
                    idList2.add(drugInformationId);
                }
            }
        }
        boolean flag3 = false;
        if(hospitalname != null && hospitalname.equals("")){
            criteria5.andHospitalNameLike("%" + hospitalname + "%");
            List<Hospital> hospitals=hospitalMapper.selectByExample(hospitalExample);
            for (Hospital hospital:hospitals) {
                Integer hospitalid=hospital.getId();
                idList3.add(hospitalid);
            }
        }

        if (purchaseNote != null) {
            if (purchaseNote.getPurchaseNumber() != null){
                criteria3.andPurchaseNumberEqualTo(purchaseNote.getPurchaseNumber());
                flag3 = true;
            }
            if (purchaseNote.getPurchaseOrderName() != null){
                criteria3.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
                flag3 = true;
            }
        }

        if (idList3 !=null && idList3.size()>0){
            criteria3.andHospitalIdIn(idList3);
            flag3 = true;
        }
        if (SearchReturnorder.getPurchasestartTime() != null && SearchReturnorder.getPurchaseendTime() != null ){
            criteria3.andBuildSingleBetween(SearchReturnorder.getPurchasestartTime(),SearchReturnorder.getPurchaseendTime());
            flag3 = true;
        }

        if (flag3) {
            List<PurchaseNote> PurchaseNotes=purchaseNoteMapper.selectByExample(purchaseNoteExample);
            if (PurchaseNotes.size() != 0) {
                for (PurchaseNote purchaseNoteobj : PurchaseNotes) {
                    Integer purchaseNoteId = purchaseNoteobj.getId();
                    idList4.add(purchaseNoteId);
                }
            }
        }

        boolean flag4 = false;
        if (creditOrder != null) {
            if (creditOrder.getCreditOrderNum() != null && creditOrder.getCreditOrderNum().length() > 0){
                criteria4.andCreditOrderNumEqualTo(creditOrder.getCreditOrderNum());
                flag4 = true;
            }
            if (creditOrder.getCreditOrderName() != null && creditOrder.getCreditOrderName().length() > 0){
                criteria4.andCreditOrderNameLike("%" + creditOrder.getCreditOrderName() + "%");
                flag4 = true;
            }
        }
        if (SearchReturnorder.getReturnstartTime() !=null && SearchReturnorder.getReturnendTime() != null){
            criteria4.andCreateTimeBetween(SearchReturnorder.getReturnstartTime(),SearchReturnorder.getReturnendTime());
            flag4 = true;
        }
        if (flag4) {
            List<CreditOrder> list=creditOrderMapper.selectByExample(creditOrderExample);
            for (CreditOrder order : list) {
                idList5.add(order.getId());
            }
        }
        if(idList2 !=null && idList2.size()>0){
            criteria6.andDurgInformationIdIn(idList2);
        }

        if(idList4 !=null && idList4.size()>0){
            criteria6.andPurchaseOrderIdIn(idList4);
        }

        if(idList5 !=null && idList5.size()>0){
            criteria6.andCreditOrderIdIn(idList5);
        }
        if(creditOrderDetail !=null && creditOrderDetail.getState() != 0){
            criteria6.andStateEqualTo(creditOrderDetail.getState());
        }

        ArrayList<CreditOrderDetail> creditOrderDetailist=(ArrayList<CreditOrderDetail>)creditOrderDetailMapper.selectByExample(creditOrderDetailExample);
        ArrayList<SearchReturnorder> searchReturnorder2=new Page<SearchReturnorder>();
        for (CreditOrderDetail orderDetail : creditOrderDetailist) {
            SearchReturnorder searchReturnorder=new SearchReturnorder();
            DrugInformation drugInformation1=drugInformationMapper.selectByPrimaryKey(orderDetail.getDurgInformationId());
            DrugCategory drugCategory1=drugCategoryMapper.selectByPrimaryKey(drugInformation1.getDrugCategoryId());
            PurchaseNote purchaseNote1=purchaseNoteMapper.selectByPrimaryKey(orderDetail.getPurchaseOrderId());
            CreditOrder creditOrder1=creditOrderMapper.selectByPrimaryKey(orderDetail.getCreditOrderId());
            Hospital hospital=hospitalMapper.selectByPrimaryKey(creditOrder1.getHospitalId());
            searchReturnorder.setDrugInformation(drugInformation1);
            searchReturnorder.setDrugCategory(drugCategory1);
            searchReturnorder.setPurchaseNote(purchaseNote1);
            searchReturnorder.setCreditOrder(creditOrder1);
            searchReturnorder.setCreditOrderDetail(orderDetail);
            searchReturnorder.setHospitalname(hospital.getHospitalName());
            SearchReturnorder searchReturnorder3=(SearchReturnorder)searchReturnorder.clone();
            searchReturnorder2.add(searchReturnorder3);
        }
        // 创建一个workbook 对应一个excel应用文件
        // HSSWorkbook 是03版excel 的个格式，XSSWorkbook 是07以上版本，HSS 和 XSS 开头分别代表03,07可以切换
        //HSSFWorkbook workBook = new HSSFWorkbook();
        XSSFWorkbook workbook = new XSSFWorkbook();
        // 在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = workbook.createSheet("导出说明");
        XSSFCell cell = null;
        XSSFRow bodyRow = sheet.createRow(0);
        cell = bodyRow.createCell(0);
        cell.setCellValue("退货单编号");

        cell = bodyRow.createCell(1);
        cell.setCellValue("退货单名称");

        cell = bodyRow.createCell(2);
        cell.setCellValue("下单医院");

        cell = bodyRow.createCell(3);
        cell.setCellValue("对应采购单编号");

        cell = bodyRow.createCell(4);
        cell.setCellValue("对应采购单名称");

        cell = bodyRow.createCell(5);
        cell.setCellValue("流水号");

        cell = bodyRow.createCell(6);
        cell.setCellValue("通用名");

        cell = bodyRow.createCell(7);
        cell.setCellValue("商品名称");

        cell = bodyRow.createCell(8);
        cell.setCellValue("剂型");

        cell = bodyRow.createCell(9);
        cell.setCellValue("规格");

        cell = bodyRow.createCell(10);
        cell.setCellValue("单位");

        cell = bodyRow.createCell(11);
        cell.setCellValue("转换系数");

        for (int i = 0; i < searchReturnorder2.size(); i++) {
            SearchReturnorder searchReturnorder = searchReturnorder2.get(i);
            XSSFRow dateRow = sheet.createRow(1 + i);
            cell = dateRow.createCell(0);
            cell.setCellValue(searchReturnorder.getCreditOrder().getCreditOrderNum());
            cell = dateRow.createCell(1);
            cell.setCellValue(searchReturnorder.getCreditOrder().getCreditOrderName());
            cell = dateRow.createCell(2);
            cell.setCellValue(searchReturnorder.getHospitalname());
            cell = dateRow.createCell(3);
            cell.setCellValue(searchReturnorder.getPurchaseNote().getPurchaseNumber());
            cell = dateRow.createCell(4);
            cell.setCellValue(searchReturnorder.getPurchaseNote().getPurchaseOrderName());
            cell = dateRow.createCell(5);
            cell.setCellValue(searchReturnorder.getDrugInformation().getSerialNum());
            cell = dateRow.createCell(6);
            cell.setCellValue(searchReturnorder.getDrugCategory().getGenericName());
            cell = dateRow.createCell(7);
            cell.setCellValue(searchReturnorder.getDrugInformation().getDrugName());
            cell = dateRow.createCell(8);
            cell.setCellValue(searchReturnorder.getDrugCategory().getDosageForm());
            cell = dateRow.createCell(9);
            cell.setCellValue(searchReturnorder.getDrugCategory().getSpecification());
            cell = dateRow.createCell(10);
            cell.setCellValue(searchReturnorder.getDrugCategory().getUnit());
            cell = dateRow.createCell(11);
            cell.setCellValue(searchReturnorder.getDrugCategory().getCoefficient());
        }
        try {
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }


    /*
     * 导入
     * */
    @Override
    public Map<String, String> saveMore(MultipartFile file) throws Exception {
        Map<String, Integer> map = new HashMap<String, Integer>();
        if (file.isEmpty()) {
            throw new Exception("文件不存在！");
        }
        InputStream in = null;
        try {
            in = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("输入流异常");
        }
        List<List<Object>> listob = null;
        try {
            listob = new ExcelUtils().getBankListByExcel(in, file.getOriginalFilename());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("工具类打开异常");
        }
        Map<String, String> map1 = new HashMap<String, String>();
        Integer successSum = 0;
        List<Integer> errorList = new ArrayList<Integer>();
        for (int i = 0; i < listob.size(); i++) {
            List<Object> lo = listob.get(i);
            if (String.valueOf(lo.get(0)) == null || String.valueOf(lo.get(0)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(1)) == null || String.valueOf(lo.get(1)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            if (String.valueOf(lo.get(2)) == null || String.valueOf(lo.get(2)).equals("")) {
                errorList.add(i + 2);
                continue;
            }
            String purchasenumber=String.valueOf(lo.get(0));
            String hospitalname=String.valueOf(lo.get(1));
            String serialnum=String.valueOf(lo.get(2));
            try {
                add(purchasenumber,hospitalname,serialnum);
            } catch (Exception e) {
                errorList.add(i + 2);
                continue;
            }
            ++successSum;
        }
        map1.put("successSum", String.valueOf(successSum));
        map1.put("errorList", errorList.toString());
        return map1;
    }

    @Override
    public void add(String purchasenumber,String hospitalname,String serialnum) throws Exception {
        DrugCategoryExample drugCategoryExample = new DrugCategoryExample();
        DrugInformationExample drugInformationExample =new DrugInformationExample();
        PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample =new PurchaseOrderDrugDetailsExample();
        HospitalExample hospitalExample = new HospitalExample();

        DrugCategoryExample.Criteria criteria = drugCategoryExample.createCriteria();
        DrugInformationExample.Criteria criteria2 = drugInformationExample.createCriteria();
        PurchaseNoteExample.Criteria criteria3 = purchaseNoteExample.createCriteria();
        PurchaseOrderDrugDetailsExample.Criteria criteria4 = purchaseOrderDrugDetailsExample.createCriteria();
        HospitalExample.Criteria criteria5=hospitalExample.createCriteria();

        List<Integer> list=new ArrayList<Integer>();
        List<Integer> list2=new ArrayList<Integer>();
        List<Integer> list3=new ArrayList<Integer>();



        criteria5.andHospitalNameLike("%" + hospitalname + "%");
        List<Hospital> hospitals=hospitalMapper.selectByExample(hospitalExample);
        for (Hospital hospital : hospitals) {
            list.add(hospital.getId());
        }

        criteria3.andPurchaseNumberEqualTo(purchasenumber);
        criteria3.andHospitalIdIn(list);
        List<PurchaseNote> PurchaseNotes=purchaseNoteMapper.selectByExample(purchaseNoteExample);
        for (PurchaseNote purchaseNote : PurchaseNotes) {
            list2.add(purchaseNote.getId());
        }

        criteria2.andSerialNumEqualTo(serialnum);
        List<DrugInformation> drugInformations=drugInformationMapper.selectByExample(drugInformationExample);
        for (DrugInformation drugInformation : drugInformations) {
            list3.add(drugInformation.getId());
        }
        criteria4.andDrugInformationIdIn(list3);
        criteria4.andPurchaseNoteIdIn(list2);
        criteria4.andPurchasingStatusEqualTo("1");
        List<PurchaseOrderDrugDetails> purchaseOrderDrugDetails1ist=purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
        if (purchaseOrderDrugDetails1ist.size() == 1) {
            purchaseOrderDrugDetails1ist.get(0).setPurchasingStatus("2");
            purchaseOrderDrugDetailsMapper.updateStateById(purchaseOrderDrugDetails1ist.get(0));
        }else {
            String message = "信息输入不正确，采购单编号：【" + purchasenumber + "】,采购医院名称：【" + hospitalname + "】，药品流水号：【" + serialnum + "】";
            throw new Exception(message);
        }

    }

    @Override
    public void saveWithList(List<Integer> list) {
        for (Integer integer : list) {
            CreditOrderDetail creditOrderDetail=new CreditOrderDetail();
            creditOrderDetail.setId(integer);
            creditOrderDetail.setState(2);
            creditOrderDetailMapper.updateByid(creditOrderDetail);
        }
    }


}
