package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.HospitalMapper;
import com.yaopin.mapper.PurchaseNoteMapper;
import com.yaopin.service.PurchaseNoteService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName 采购单维护实现类
 * @Author donghongyu
 * @Date 2019/9/9 14:38
 **/
@Service
public class PurchaseNoteServiceImpl implements PurchaseNoteService {

    @Resource
    private PurchaseNoteMapper purchaseNoteMapper;
    @Resource
    private HospitalMapper hospitalMapper;
/**
** @Description 查询采购单加分页
 * @Param
 */
    @Override
    public PageResult findPage(PurchaseNote purchaseNote, int page, int rows) {
        PurchaseNoteExample purchaseNoteExample = new PurchaseNoteExample();
        PurchaseNoteExample.Criteria criteria = purchaseNoteExample.createCriteria();
        //拼接采购单条件
        if (purchaseNote != null) {
            Hospital hospital = purchaseNote.getHospital();
            //判断医院是否为空
            if (hospital != null) {
                HospitalExample hospitalExample = new HospitalExample();
                HospitalExample.Criteria criteria1 = hospitalExample.createCriteria();
                if (hospital.getHospitalName() != null && hospital.getHospitalName().length() > 0) {
                    criteria1.andHospitalNameEqualTo(hospital.getHospitalName());
                }
                List<Integer> hospitalsIds = new ArrayList<Integer>();
                List<Hospital> hospital1 = hospitalMapper.selectByExample(hospitalExample);
                if (hospital1.size() != 0) {
                    for (Hospital category : hospital1) {
                        Integer hospitalsId = category.getId();
                        hospitalsIds.add(hospitalsId);
                    }
                }

                if (hospitalsIds.size() != 0) {
                    criteria.andHospitalIdIn(hospitalsIds);
                } else {
                    hospitalsIds.add(0);
                    criteria.andHospitalIdIn(hospitalsIds);
                }
            }
            if (purchaseNote.getPurchaseNumber() != null) {
                criteria.andPurchaseNumberEqualTo(purchaseNote.getPurchaseNumber());
            }
            if (purchaseNote.getPurchaseOrderName() != null && purchaseNote.getPurchaseOrderName().length() > 0) {
                criteria.andPurchaseOrderNameLike("%" + purchaseNote.getPurchaseOrderName() + "%");
            }
            if (purchaseNote.getPurchaseOrderStatus() != null) {
                criteria.andPurchaseOrderStatusEqualTo(purchaseNote.getPurchaseOrderStatus());
            }
            if (purchaseNote.getStartTime() != null && purchaseNote.getStartTime().toString().length() > 0 && purchaseNote.getEndTime() != null) {
                criteria.andBuildSingleBetween(purchaseNote.getStartTime(), purchaseNote.getEndTime());
            }
        }
        PageHelper.startPage(page, rows);
        Page<PurchaseNote> purchaseNotes = (Page<PurchaseNote>) purchaseNoteMapper.selectByExample(purchaseNoteExample);
        List<PurchaseNote> result = purchaseNotes.getResult();
        List<PurchaseNote> purchaseNote1 = new ArrayList<>();
        for (PurchaseNote note : purchaseNotes) {
            PurchaseNote clone = (PurchaseNote) note.clone();
            purchaseNote1.add(clone);
        }
        return new PageResult(purchaseNotes.getTotal(), purchaseNote1);

    }
  /**
   ** @Description  添加采购单
    * @Param  purchaseNote
  */
    @Override
    public void add(PurchaseNote purchaseNote) {
        purchaseNote.setChangeNum(0);
        purchaseNoteMapper.insert(purchaseNote);
    }


    /**
     * * @Description 采购单条件查询加分页
     *
     * @Param
     */
    @Override
    public void deleteOne(Integer id) {
        PurchaseNote purchaseNote1 = purchaseNoteMapper.selectByPrimaryKey(id);
        String purchaseOrderStatus = purchaseNote1.getPurchaseOrderStatus();
        if (purchaseOrderStatus.equals("1")) {
            purchaseNoteMapper.deleteByPrimaryKey(id);
        }
    }

    /**
     * * @Description 修改采购单
     *
     * @Param
     */
    @Override
    public void update(PurchaseNote purchaseNote) {
        purchaseNoteMapper.updateByPrimaryKey(purchaseNote);
    }

    /**
     * * @Description 查询实体
     *
     * @Param
     */
    @Override
    public PurchaseNote findOne(Integer id) {
        PurchaseNote purchaseNote = purchaseNoteMapper.selectByPrimaryKey(id);
        return purchaseNote;
    }
/**
** @Description  根据采购单id查询采购单
 * @Param
 */
    @Override
    public PurchaseNote findSession(Integer id) {
        PurchaseNote purchaseNote = purchaseNoteMapper.selectByPrimaryKey(id);
        return purchaseNote;
    }
}






