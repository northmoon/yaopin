package com.yaopin.service.impl;

import com.yaopin.entity.*;
import com.yaopin.mapper.PurchaseOrderDrugDetailsMapper;
import com.yaopin.mapper.PurchaseOrderWarehousingMapper;
import com.yaopin.service.PurchaseOrderWarehousingService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName PurchaseOrderWarehousingServiceImpl
 * @Author donghongyu
 * @Date 2019/9/18 17:03
 **/
@Service
public class PurchaseOrderWarehousingServiceImpl implements PurchaseOrderWarehousingService {
    @Resource
    private PurchaseOrderDrugDetailsMapper purchaseOrderDrugDetailsMapper;
    @Resource
    private PurchaseOrderWarehousingMapper purchaseOrderWarehousingMapper;
    /**
     * * @Description  查询药品明细和入库
     *
     * @Param
     */
    @Override
    public List<DetailedWarehousing> findAll(PurchaseOrderDrugDetails purchaseOrderDrugDetails) {
        PurchaseOrderDrugDetailsExample purchaseOrderDrugDetailsExample = new PurchaseOrderDrugDetailsExample();
        PurchaseOrderDrugDetailsExample.Criteria criteria = purchaseOrderDrugDetailsExample.createCriteria();
        List<DetailedWarehousing> objects = new ArrayList<>();
        List<DetailedWarehousing> objects1 = new ArrayList<>();
        if (purchaseOrderDrugDetails != null) {
            if (purchaseOrderDrugDetails.getPurchasingStatus() !=null) {
                criteria.andPurchasingStatusEqualTo(purchaseOrderDrugDetails.getPurchasingStatus());
            }
        }
        List<PurchaseOrderDrugDetails> list = purchaseOrderDrugDetailsMapper.selectByExample(purchaseOrderDrugDetailsExample);
        for (PurchaseOrderDrugDetails orderDrugDetails : list) {
            DetailedWarehousing detailedWarehousing1 = new DetailedWarehousing();
            Integer drugInformationId = orderDrugDetails.getDrugInformationId();
            Integer purchaseNoteId = orderDrugDetails.getPurchaseNoteId();
            PurchaseOrderWarehousingExample purchaseOrderWarehousingExample = new PurchaseOrderWarehousingExample();
            PurchaseOrderWarehousingExample.Criteria criteria1 = purchaseOrderWarehousingExample.createCriteria();
            criteria1.andDurgInformationIdEqualTo(drugInformationId);
            criteria1.andPurchaseOrderIdEqualTo(purchaseNoteId);
            List<PurchaseOrderWarehousing> purchaseOrderWarehousings = purchaseOrderWarehousingMapper.selectByExample(purchaseOrderWarehousingExample);
            detailedWarehousing1.setPurchaseOrderDrugDetails(orderDrugDetails);
            if (purchaseOrderWarehousings.size()!=0) {
                detailedWarehousing1.setPurchaseOrderWarehousing(purchaseOrderWarehousings.get(0));
            }else{
                PurchaseOrderWarehousing purchaseOrderWarehousing = new PurchaseOrderWarehousing();
                purchaseOrderWarehousing.setChanseNum(0);
                detailedWarehousing1.setPurchaseOrderWarehousing(purchaseOrderWarehousing);
            }
            objects.add(detailedWarehousing1);
        }
        for (DetailedWarehousing object : objects) {
            DetailedWarehousing clone = (DetailedWarehousing) object.clone();
            objects1.add(clone);
        }
        return objects1;
    }
    /**
     * * @Description  添加入库
     *
     * @Param
     */
    @Override
    public void add(List<String> ids, List<String> receipts, List<String> invoiceNums, List<String> batchNums, List<String> drugValiditys) {
        for (int i = 0; i < ids.size(); i++) {
            PurchaseOrderWarehousing purchaseOrderWarehousing = new PurchaseOrderWarehousing();
            PurchaseOrderDrugDetails purchaseOrderDrugDetails = purchaseOrderDrugDetailsMapper.selectByPrimaryKey(Integer.valueOf(ids.get(i)));
            purchaseOrderDrugDetails.setPurchasingStatus("3");
            purchaseOrderDrugDetailsMapper.updateByPrimaryKey(purchaseOrderDrugDetails);
            Integer purchaseNoteId = purchaseOrderDrugDetails.getPurchaseNoteId();
            purchaseOrderWarehousing.setDurgInformationId(purchaseOrderDrugDetails.getDrugInformationId());
            purchaseOrderWarehousing.setPurchaseOrderId(purchaseNoteId);
            purchaseOrderWarehousing.setReceipt(Integer.valueOf(receipts.get(i)));
            purchaseOrderWarehousing.setInvoiceNum(invoiceNums.get(i));
            purchaseOrderWarehousing.setBatchNum(batchNums.get(i));
            purchaseOrderWarehousing.setDrugValidity(Integer.valueOf(drugValiditys.get(i)));
            purchaseOrderWarehousing.setInventoryAmount(purchaseOrderDrugDetails.getPurchaseAmount());
            purchaseOrderWarehousing.setEntryTime(new Date());
            purchaseOrderWarehousing.setChanseNum(1);
            purchaseOrderWarehousingMapper.insert(purchaseOrderWarehousing);

        }
    }
}
