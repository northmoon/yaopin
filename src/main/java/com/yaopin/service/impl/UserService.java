package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.entity.UserInfoExample.Criteria;
import com.yaopin.mapper.*;
import com.yaopin.service.IDrugCategoryService;
import com.yaopin.service.IUserService;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserService implements IUserService {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private HospitalMapper hospitalMapper;

    @Autowired
    private OrganizationInfoMapper organizationInfoMapper;

    /*
     * 没用的方法
     * */
    @Override
    public Map<String, UserInfo> findAll() {
        List<UserInfo> drugCategories = userInfoMapper.selectByExample(null);
        Map<String, UserInfo> map = new HashMap<String, UserInfo>();
        for (UserInfo userInfo : drugCategories) {
            map.put(userInfo.getId().toString(), userInfo);
        }
        return map;
    }

    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<UserInfo> page = (Page<UserInfo>) userInfoMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void add(UserInfo userInfo, int[] ids) throws Exception {
        UserInfo returnUserInfo = userInfoMapper.selectByUid(userInfo.getUid());
        if (returnUserInfo == null) {
            userInfoMapper.insert(userInfo);
            UserInfo user = userInfoMapper.selectByUid(userInfo.getUid());
            UserRole userRole = new UserRole();
            userRole.setUserId(user.getId());
            for (int id : ids) {
                userRole.setRoleId(id);
                userRoleMapper.insert(userRole);
            }
        } else {
            String message = "账号已存在，请重新输入！";
            throw new Exception(message);
        }
    }

    @Override
    public void update(UserInfo userInfo, int[] ids) throws Exception {
        UserInfo oldDrugCategory = userInfoMapper.selectByPrimaryKey(userInfo.getId());
        UserInfo returnDrugCategory = userInfoMapper.selectByUid(userInfo.getUid());
        if (returnDrugCategory == null || returnDrugCategory.getUid().equals(oldDrugCategory.getUid())) {
            userInfoMapper.updateByPrimaryKey(userInfo);
            userRoleMapper.deleteByUserId(userInfo.getId());
            UserRole userRole = new UserRole();
            userRole.setUserId(userInfo.getId());
            for (int id : ids) {
                userRole.setRoleId(id);
                userRoleMapper.insert(userRole);
            }
        } else {
            String message = "账号已存在，请重新输入！";
            throw new Exception(message);
        }
    }

    @Override
    public UserInfo findOne(Integer id) {
        UserInfo user = userInfoMapper.selectByPrimaryKey(id);
        Integer ucategory = user.getUcategory();
        if (ucategory == 0 || ucategory == 1 || ucategory == 2) {
            OrganizationInfo organizationInfo = organizationInfoMapper.selectByPrimaryKey(user.getUnitid());
            user.setUnitName(organizationInfo.getOname());
        } else if (ucategory == 3) {
            Hospital hospital = hospitalMapper.selectByPrimaryKey(user.getUnitid());
            user.setUnitName(hospital.getHospitalName());
        } else {
            Supplier supplier = supplierMapper.selectByPrimaryKey(user.getUnitid());
            user.setUnitName(supplier.getFirmName());
        }
        return user;
    }

    @Override
    public void delete(Integer[] ids) {
        for (Integer id : ids) {
            userInfoMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public PageResult findPage(UserInfo userInfo, int pageNum, int pageSize) {
        UserInfoExample example = new UserInfoExample();
        Criteria criteria = example.createCriteria();
        if (userInfo != null) {
            if (userInfo.getUid() != null && userInfo.getUid().length() > 0) {
                criteria.andUidEqualTo(userInfo.getUid());
            }
            if (userInfo.getUname() != null && userInfo.getUname().length() > 0) {
                criteria.andUnameLike("%" + userInfo.getUname() + "%");
            }
            if (userInfo.getUcategory() != null) {
                criteria.andUcategoryEqualTo(userInfo.getUcategory());
            }
            if (userInfo.getUstatus() != null) {
                criteria.andUstatusEqualTo(userInfo.getUstatus());
            }
            if (userInfo.getUnitid() != null) {
                criteria.andUnitidEqualTo(userInfo.getUnitid());
            }
        }
        PageHelper.startPage(pageNum, pageSize);
        Page<UserInfo> page = (Page<UserInfo>) userInfoMapper.selectByExample(example);
        List<UserInfo> result = page.getResult();
        for (UserInfo user : result) {
            Integer ucategory = user.getUcategory();
            if (ucategory != null) {
                if (ucategory == 0 || ucategory == 1 || ucategory == 2) {
                    OrganizationInfo organizationInfo = organizationInfoMapper.selectByPrimaryKey(user.getUnitid());
                    user.setUnitName(organizationInfo.getOname());
                } else if (ucategory == 3) {
                    Hospital hospital = hospitalMapper.selectByPrimaryKey(user.getUnitid());
                    user.setUnitName(hospital.getHospitalName());
                } else {
                    Supplier supplier = supplierMapper.selectByPrimaryKey(user.getUnitid());
                    user.setUnitName(supplier.getFirmName());
                }
            }
        }
        return new PageResult(page.getTotal(), result);
    }

    @Override
    public void deleteOne(Integer id) {
        userRoleMapper.deleteByUserId(id);
        userInfoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Map<String, String> selectUnit(Integer ucategory) {
        Map<String, String> unitMap = new HashMap<String, String>();
        if (ucategory == 0 || ucategory == 1 || ucategory == 2) {
            List<OrganizationInfo> organizationInfos = organizationInfoMapper.selectByExample(null);
            for (OrganizationInfo organizationInfo : organizationInfos) {
                unitMap.put(organizationInfo.getOid().toString(), organizationInfo.getOname());
            }
        } else if (ucategory == 3) {
            List<Hospital> hospitals = hospitalMapper.selectByExample(null);
            for (Hospital hospital : hospitals) {
                unitMap.put(hospital.getId().toString(), hospital.getHospitalName());
            }
        } else {
            List<Supplier> suppliers = supplierMapper.selectByExample(null);
            for (Supplier supplier : suppliers) {
                unitMap.put(supplier.getId().toString(), supplier.getFirmName());
            }
        }
        return unitMap;
    }


}


