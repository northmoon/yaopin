package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.CreditOrderDetailMapper;
import com.yaopin.mapper.CreditOrderMapper;
import com.yaopin.service.CreditOrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @ClassName CreditOrderServiceImpl
 * @Author donghongyu
 * @Date 2019/9/20 9:56
 **/
@Service
public class CreditOrderServiceImpl implements CreditOrderService {
    @Resource
    private CreditOrderMapper creditOrderMapper;
    @Resource
    private CreditOrderDetailMapper creditOrderDetailMapper;

    /**
     * * @Description  添加退货单
     *
     * @Param
     */
    @Override
    public void add(CreditOrder creditOrder) {
        creditOrderMapper.insert(creditOrder);
    }

    /**
     * * @Description  查询退货单
     *
     * @Param
     */
    @Override
    public PageResult findPage(CreditOrder creditOrder, int page, int rows) {
        CreditOrderExample creditOrderExample = new CreditOrderExample();
        CreditOrderExample.Criteria criteria = creditOrderExample.createCriteria();
        if (creditOrder != null) {
            if (creditOrder.getCreditOrderNum() != null) {
                criteria.andCreditOrderNumEqualTo(creditOrder.getCreditOrderNum());
            }
            if (creditOrder.getCreditOrderName() != null) {
                criteria.andCreditOrderNameLike("%" + creditOrder.getCreditOrderName() + "%");
            }
        }
        PageHelper.startPage(page, rows);
        Page<CreditOrder> creditOrders = (Page<CreditOrder>) creditOrderMapper.selectByExample(creditOrderExample);
        return new PageResult(creditOrders.getTotal(), creditOrders);
    }

    /**
     * * @Description  保存退货单后修改退货单状态
     *
     * @Param
     */
    @Override
    public void preservation(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer creditOrderId = (Integer) session.getAttribute("creditOrderId");
        CreditOrder creditOrder = creditOrderMapper.selectByPrimaryKey(creditOrderId);
        creditOrder.setState(2);
        creditOrderMapper.updateByPrimaryKey(creditOrder);
        }
    }

