package com.yaopin.service.impl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.StatementsDetailMapper;
import com.yaopin.mapper.StatementsMapper;
import com.yaopin.service.StatementsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @ClassName 结算单
 * @Author donghongyu
 * @Date 2019/9/22 20:38
 **/
@Service
public class StatementsServiceImpl implements StatementsService {
    @Resource
    private StatementsMapper statementsMapper;
    @Resource
    private StatementsDetailMapper statementsDetailMapper;

    /**
     * * @Description  添加结算单
     *
     * @Param
     */
    @Override
    public void add(Statements statements) {
        statementsMapper.insert(statements);
    }

    @Override
    public PageResult findPage(Statements statements, int page, int rows) {
        StatementsExample statementsExample = new StatementsExample();
        StatementsExample.Criteria criteria = statementsExample.createCriteria();
        if (statements != null) {
            if (statements.getStatementsNum() != null && statements.getStatementsNum().length() > 0) {
                criteria.andStatementsNumLike("%" + statements.getStatementsNum() + "%");
            }
            if (statements.getStatementsName() != null && statements.getStatementsName().length() > 0) {
                criteria.andStatementsNameLike("%" + statements.getStatementsName() + "%");
            }
        }
        PageHelper.startPage(page, rows);
        Page<Statements> statements1 = (Page<Statements>) statementsMapper.selectByExample(statementsExample);
        return new PageResult(statements1.getTotal(), statements1);
    }

    @Override
    public void save(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer statementsId = (Integer) session.getAttribute("statementsId");
        Statements statements = statementsMapper.selectByPrimaryKey(statementsId);
        statements.setState(2);
        statementsMapper.updateByPrimaryKey(statements);
        StatementsDetailExample statementsDetailExample = new StatementsDetailExample();
        StatementsDetailExample.Criteria criteria = statementsDetailExample.createCriteria();
        criteria.andStatementsIdEqualTo(statementsId);
        List<StatementsDetail> statementsDetails = statementsDetailMapper.selectByExample(statementsDetailExample);
        for (int i = 0; i < statementsDetails.size(); i++) {
            StatementsDetail statementsDetail = statementsDetails.get(0);
            statementsDetail.setState(2);
            statementsDetailMapper.updateByPrimaryKey(statementsDetail);
        }
    }
    }
