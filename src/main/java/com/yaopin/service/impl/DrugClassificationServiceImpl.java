package com.yaopin.service.impl;

import com.yaopin.entity.AreaInfo;
import com.yaopin.entity.DrugClassification;
import com.yaopin.mapper.AreaInfoMapper;
import com.yaopin.mapper.DrugClassificationMapper;
import com.yaopin.service.IAreaInfoService;
import com.yaopin.service.IDrugClassificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrugClassificationServiceImpl implements IDrugClassificationService {
    @Autowired
    private DrugClassificationMapper drugClassificationMapper;

    @Override
    public List<DrugClassification> findAll() {
        return drugClassificationMapper.selectByExample(null);
    }
}
