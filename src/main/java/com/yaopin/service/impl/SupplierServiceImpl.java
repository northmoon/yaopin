package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yaopin.entity.*;
import com.yaopin.mapper.QualityLevelMapper;
import com.yaopin.mapper.SupplierListMapper;
import com.yaopin.mapper.SupplierMapper;
import com.yaopin.mapper.UserInfoMapper;
import com.yaopin.service.IQualityLevelService;
import com.yaopin.service.ISupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements ISupplierService {
    @Autowired
    private SupplierListMapper supplierListMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private UserInfoMapper userInfoMapper;


    @Override
    public void saveWithList(List<Integer> list) {
        for (Integer i:list) {
            SupplierList supplierlist=new SupplierList();
            supplierlist.setDrugInformationId(i);
            supplierlist.setEnterpriseId(1);
            supplierlist.setState(1);
            supplierListMapper.insert(supplierlist);
        }


    }

    @Override
    public void removeWithList(List<Integer> list) {
        for (Integer i:list) {
            supplierListMapper.deleteByPrimaryKey(i);
        }
    }

    @Override
    public Map<String, Supplier> findAll() {
        List<Supplier> suppliers = supplierMapper.selectByExample(null);
        Map<String, Supplier> map = new HashMap<>();
        String uid = SecurityContextHolder.getContext().getAuthentication().getName();
        UserInfo user = userInfoMapper.selectByUid(uid);
        if(user != null && user.getUcategory()==4){
            Supplier supplierfire=supplierMapper.selectByName(user.getUname());
            map.put(supplierfire.getId().toString(), supplierfire);
        }else{
            for (Supplier supplier : suppliers) {
                map.put(supplier.getId().toString(), supplier);
            }
        }
        return map;
    }

    @Override
    public PageResult findPage(Supplier supplier, int pageNum, int pageSize) {
        SupplierExample supplierExample=new SupplierExample();
        SupplierExample.Criteria suppliercriteria = supplierExample.createCriteria();
        if(supplier != null){
            if(supplier.getFirmName()!=null && !supplier.getFirmName().equals("") ){
                suppliercriteria.andFirmNameLike("%" + supplier.getFirmName() + "%");
            }
            if(supplier.getFirmType()!=null && supplier.getFirmType()!=0){
                suppliercriteria.andFirmTypeEqualTo(supplier.getFirmType());
            }
            if(supplier.getLinkman()!=null && !supplier.getLinkman().equals("")){
                suppliercriteria.andLinkmanLike("%" + supplier.getLinkman() + "%");
            }
            if(supplier.getBusinessScope()!=null && !supplier.getBusinessScope().equals("")){
                suppliercriteria.andBusinessScopeLike("%" + supplier.getBusinessScope() + "%");
            }
            if(supplier.getRegisterAddress()!=null && !supplier.getRegisterAddress().equals("")){
                suppliercriteria.andRegisterAddressLike("%" + supplier.getRegisterAddress() + "%");
            }
            if(supplier.getContactAddress()!=null && !supplier.getContactAddress().equals("")){
                suppliercriteria.andContactAddressLike("%" + supplier.getContactAddress() + "%");
            }
            if(supplier.getZipCode()!=null && !supplier.getZipCode().equals("")){
                suppliercriteria.andZipCodeLike("%" + supplier.getZipCode() + "%");
            }
            if(supplier.getTotalAssets()!=null ){
                suppliercriteria.andTotalAssetsEqualTo(supplier.getTotalAssets());
            }
            if(supplier.getFax()!=null && !supplier.getFax().equals("")){
                suppliercriteria.andFaxLike(supplier.getFax());
            }
            if(supplier.getLegalName()!=null && !supplier.getLegalName().equals("")){
                suppliercriteria.andLegalNameLike(supplier.getLegalName());
            }
            if(supplier.getRegisterAssets()!=null && !supplier.getRegisterAssets().equals("")){
                suppliercriteria.andRegisterAssetsEqualTo(supplier.getRegisterAssets());
            }
            if(supplier.getEmail()!=null && !supplier.getEmail().equals("")){
                suppliercriteria.andEmailLike(supplier.getEmail());
            }
            if(supplier.getWebsite()!=null && !supplier.getWebsite().equals("")){
                suppliercriteria.andWebsiteLike("%" + supplier.getWebsite() + "%");
            }
        }
        supplierMapper.selectByExample(supplierExample);
        PageHelper.startPage(pageNum, pageSize);
        Page<Supplier> page = (Page<Supplier>) supplierMapper.selectByExample(supplierExample);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void add(Supplier supplier) {
        supplierMapper.insert(supplier);
    }

    @Override
    public void deleteOne(int id) {
        supplierMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updete(Supplier supplier) {
        supplierMapper.updateByPrimaryKey(supplier);
    }

    @Override
    public Supplier findone(int id) {
        return  supplierMapper.selectByPrimaryKey(id);
    }
}
