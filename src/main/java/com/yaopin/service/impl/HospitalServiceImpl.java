package com.yaopin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
//import com.yaopin.entity.*;
import com.yaopin.entity.HospitalExample.Criteria;
import com.yaopin.entity.AreaInfo;
import com.yaopin.entity.Hospital;
import com.yaopin.entity.HospitalExample;
import com.yaopin.entity.PageResult;
import com.yaopin.mapper.AreaInfoMapper;
import com.yaopin.mapper.HospitalMapper;
import com.yaopin.service.HospitalService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HospitalServiceImpl implements HospitalService {

    @Resource
    private HospitalMapper hospitalDao;
    @Resource
    private AreaInfoMapper areaInfoDao;

    /**
     * 查询
     * @param hospital
     * @param pageNum
     * @param pageSize
     * @return
     */

    @Override
    public PageResult findPage(Hospital hospital, int pageNum, int pageSize) {
        HospitalExample example = new HospitalExample();
        Criteria criteria = example.createCriteria();
        if (hospital != null) {
            if (hospital.getHospitalName() != null && hospital.getHospitalName().length() > 0) {
                criteria.andHospitalNameLike("%" + hospital.getHospitalName() + "%");
            }
            if (hospital.getAreaId() != null) {
                criteria.andAreaIdEqualTo(hospital.getAreaId());
            }
            if (hospital.getHospitalLevel() != null && hospital.getHospitalLevel().length() > 0) {
                criteria.andHospitalLevelLike("%" + hospital.getHospitalLevel() + "%");
            }
            if (hospital.getPhone() != null && hospital.getPhone().length() > 0) {
                criteria.andPhoneLike("%" + hospital.getPhone() + "%");
            }
        }
        PageHelper.startPage(pageNum, pageSize);
        Page<Hospital> page = (Page<Hospital>) hospitalDao.selectByExample(example);
        List<Hospital> result = page.getResult();
        List<Hospital> newResult = new ArrayList<Hospital>();
        for (Hospital spitall : result) {
            Hospital hospital1 = (Hospital) spitall.clone();
            newResult.add(hospital1);
        }
        return new PageResult(page.getTotal(), newResult);
    }

    @Override
    public void deletehospital(Integer id) {
        hospitalDao.deleteByPrimaryKey(id);
    }

    /**
     * 新增
     * @param hospital
     */
    @Override
    public int addhospital(Hospital hospital) throws Exception {
        Hospital returnHospital = hospitalDao.selectHospital(hospital);
        if (returnHospital == null) {
            //    selectMaxHospital
            return hospitalDao.insert(hospital);
        } else {
            String message = "医院名称: [ " + returnHospital.getHospitalName() + " ], 通讯地址: [" + returnHospital.getMailingAddress() + " ], 邮政编码: [" + returnHospital.getZipCode() + " ],所属地区: [" + returnHospital.getAreaId() + " ], 医院级别: [" + returnHospital.getHospitalLevel() + " ], 床位数: [" + returnHospital.getBedNum() + " ], 院办电话: [" + returnHospital.getPhone() + " ], 医院类型: [" + returnHospital.getType() + " ]";
            throw new Exception(message);
        }
    }

    @Override
    public void update(Hospital hospital) {
        hospitalDao.updateByPrimaryKey(hospital);
    }
    @Override
    public Hospital findOne(Integer id) {
        Hospital hospital = hospitalDao.selectByPrimaryKey(id);
        return hospital;
    }

    @Override
    public List<AreaInfo> findAll() {
        return areaInfoDao.selectByExample(null);
    }


}


