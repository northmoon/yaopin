package com.yaopin.service;

import com.yaopin.entity.*;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.util.List;
import java.util.Map;

public interface IPurchasingorderService {
    /**
     * 分页
     *
     * @param pageNum  当前页 码
     * @param pageSize 每页记录数
     * @return
     */
    PageResult findPurchasing(SearchPurchasing searchPurchasing,int pageNum, int pageSize);

    Integer confirmSupplier(List<Integer> list);

    Integer unableSupplier(List<Integer> list);

    void exportsearch(ServletOutputStream outputStream, SearchPurchasing searchPurchasing);

    Map<String, String> saveMore(MultipartFile file) throws Exception;

    void add(String purchasenumber, String hospitalname, String serialnum) throws Exception;

}
