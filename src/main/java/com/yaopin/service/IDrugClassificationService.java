package com.yaopin.service;

import com.yaopin.entity.DrugClassification;

import java.util.List;

public interface IDrugClassificationService {
    List<DrugClassification> findAll();
}
