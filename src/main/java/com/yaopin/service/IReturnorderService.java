package com.yaopin.service;

import com.yaopin.entity.PageResult;
import com.yaopin.entity.SearchPurchasing;
import com.yaopin.entity.SearchReturnorder;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.util.List;
import java.util.Map;

public interface IReturnorderService {
    /**
     * 分页
     *
     * @param pageNum  当前页 码
     * @param pageSize 每页记录数
     * @return
     */
    PageResult findReturn(SearchReturnorder searchReturnorder, int pageNum, int pageSize);

    Integer confirmSupplier(List<Integer> list);

    Integer unableSupplier(List<Integer> list);

    void exportsearch(ServletOutputStream outputStream, SearchReturnorder searchReturnorder);

    Map<String, String> saveMore(MultipartFile file) throws Exception;

    void add(String purchasenumber, String hospitalname, String serialnum) throws Exception;

    void saveWithList(List<Integer> list);

}
