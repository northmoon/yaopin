package com.yaopin.service;

import com.yaopin.entity.DrugClassification;
import com.yaopin.entity.QualityLevel;

import java.util.List;
import java.util.Map;

public interface IQualityLevelService {
    Map<String,QualityLevel> findAll();
}
